package com.rasz.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.controller.MainController;
import com.rasz.domain.BankHoliday;
import com.rasz.domain.Contract;
import com.rasz.domain.Department;
import com.rasz.domain.Employee;
import com.rasz.domain.EmployeeAllowance;
import com.rasz.domain.EmployeeHoliday;
import com.rasz.domain.HolidayCategory;
import com.rasz.domain.HolidayDay;
import com.rasz.domain.Usuario;
import com.rasz.repository.BankHolidayRepository;
import com.rasz.repository.ContractRepository;
import com.rasz.repository.DepartmentRepository;
import com.rasz.repository.EmployeeAllowanceRepository;
import com.rasz.repository.EmployeeHolidayRepository;
import com.rasz.repository.EmployeeRepository;
import com.rasz.repository.HolidayCategoryRepository;
import com.rasz.repository.HolidayDayRepository;
import com.rasz.utils.Utils;

	
@Service
public class BookingHolidayService {

	@Autowired private MainController mainController;
	@Autowired private EmployeeRepository employeeRepository;
	@Autowired private ContractRepository contractRepository; 
	@Autowired private HolidayDayRepository holidayDayRepository;
	@Autowired private HolidayCategoryRepository holidayCategoryRepository;
	@Autowired private DepartmentRepository departmentRepository;
	@Autowired private BankHolidayRepository bankHolidayRepository;
	@Autowired private EmployeeAllowanceRepository employeeAllowanceRepository;
	@Autowired private EmployeeHolidayRepository employeeHolidayRepository;
	@PersistenceContext EntityManager em;
	
	boolean permissaoSalvar;
	private Employee employee;
	private EmployeeHoliday employeeHoliday;
	List<HolidayCategory> holidayCategoryList = new ArrayList<>();
	List<HolidayDay> listHolidayDay = new ArrayList<>();
	
	private String text = "";
	boolean init;
	boolean disabledEndDate;
	boolean disabledStartDate;
	boolean disabledHC;
	boolean disabledSetDayOff;
	boolean disabledDaysHours;
	boolean maximumDaysDepartment = true;
	boolean maximumDays = true;
	boolean maximumEmployees = true;
	
	public void loadEmployee(){
		Usuario u = mainController.getUsuarioLogado();
		employee = employeeRepository.findByUsuario(u.getId());
//		employee = employeeRepository.findByUsuario(2L);
		employeeHoliday = new EmployeeHoliday();
		listHolidayDay = new ArrayList<>();
		holidayCategoryList = holidayCategoryRepository.listarTodos("", 0);
		disabledControl(false);
		setPermissaoSalvar(true);
		
		if(employee != null){
			Contract c = contractRepository.findByEmployee(employee.getIdEmployee());
			if(c == null){
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Warn!", "This Employee has no contract.");
				FacesContext.getCurrentInstance().addMessage(null, message);
				setEmployee(null);
			}else{
				List<HolidayDay> hd = holidayDayRepository.countHoursDaysByEmployeeYear(employee.getIdEmployee().toString(), (long)Utils.getDateYear(new Date()));
				for (HolidayDay holidayDay : hd) {
					employeeHoliday.setBalance(holidayDay.getEmployeeHoliday().getBalance());
					employeeHoliday.setWorkingHours(holidayDay.getEmployeeHoliday().getWorkingHours());
					break;
				}
			}
			
			text = "Hello "+employee.getName()+", your current balance is "+employeeHoliday.getBalanceString();
			setInit(true); 
		}else{
			text = "Please link User to an Employee!";
			setInit(true); 
		}
	}
	
	@Transactional
	public boolean save(EmployeeHoliday employeeHoliday) {
		List<EmployeeAllowance> listEmployeeAllowance = new ArrayList<>();
		
		employeeHoliday.setRequisitionDate(new Date());
		
		if(employee != null && employee.getIdEmployee() > 0){
				employeeHoliday.setEmployee(employee);
				listEmployeeAllowance = employeeAllowanceRepository.findByEmployeeYear(employee.getIdEmployee(), Utils.getDateYear(employeeHoliday.getStartDate()), Utils.getDateYear(employeeHoliday.getEndDate()));
		}
		
		boolean save = true;
		
		if(!maximumDays){
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Warn!", "Some day requested is not available - Check the chosen period again.");
			FacesContext.getCurrentInstance().addMessage(null, message);
			save = false;
		}
		
		if(!maximumDaysDepartment){
			List<Department> deparments = departmentRepository.listaByEmployee(employee.getIdEmployee());
			if(deparments.size() == 1){
				for (Department d : deparments) {
					setMaximumDaysDepartment(false);
					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Warn!", "The maximum required holidays in the "+d.getDescription().toUpperCase()+" department is only "+d.getMaximumDays().toString());
					FacesContext.getCurrentInstance().addMessage(null, message);
				}
			}
		}
		
		if(!maximumEmployees){
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Warn!", "Some day requested is fully booked - Check the chosen period again.");
			FacesContext.getCurrentInstance().addMessage(null, message);
			save = false;
		}
		
		if(save){
			try {
				employeeHoliday = employeeHolidayRepository.salvar(employeeHoliday);
				
				for (HolidayDay hd : listHolidayDay) {
					holidayDayRepository.salvar(hd);
				}
				
				//CRIAR OU ATUALIZAR employee_allowance
				if(listEmployeeAllowance.size() > 0){
//					//UPDATE
//					
				}else{
					//CREATE
					saveEmployeeAllowance(employeeHoliday.getEmployee().getIdEmployee(), employeeHoliday);
				}
				
				RequestContext.getCurrentInstance().execute("PF('btnPrint').show();");
				//sendEmail();
				save = true;
			} catch (Exception e) {
				// TODO: handle exception
				Utils.getStackTrace(e);
				save = false;
			}
		}
		
		return save;
	}
	
	
	
	public void saveEmployeeAllowance(Long idEmployee, EmployeeHoliday eHoliday){
		List<Long> years = new ArrayList<>();
		//VERIFICANDO SE O PERIDO DE FERIAS COMECA E TERMINA E ANOS DIFERENTES
		years.add(Long.valueOf(Utils.getDateYear(eHoliday.getStartDate())));
		if(Utils.getDateYear(eHoliday.getStartDate()) != Utils.getDateYear(eHoliday.getEndDate())){
			years.add(Long.valueOf(Utils.getDateYear(eHoliday.getEndDate())));
		}
		
		//PARA CADA ANO CRIAR EMPLOYEE_ALLOWANCE
		for (Long y : years) {
			EmployeeAllowance eAllowance = new EmployeeAllowance();
			Contract contract = contractRepository.findByEmployee(idEmployee);
			eAllowance.setContract(contract);
			eAllowance.setCurrentYear(y);
			
			//SE ANO ANO DO PERIODO DE FERIAS > QUE ANO DO CONTRATO, USAR TOTAL DE DIAS DISPONIVEIS DO CONTRATO
			if(y > Long.valueOf(Utils.getDateYear(contract.getStartDate()))){
				eAllowance.setAvailableDays(Double.valueOf(contract.getAllowance().getAllowance()));
			}else{//SE NAO, CALCULAR DIAS DISPONIVEIS USANDO DATA INICIAL DO CONTRATO E ULTIMO DIA DO ANO
				Calendar last = Calendar.getInstance();
				last.set(Utils.getDateYear(contract.getStartDate()), 12, 0);
				
				long dif = Utils.retornarDiferencaEmDiasEntreDatas(contract.getStartDate(), last.getTime());
				long v1 = dif * contract.getAllowance().getAllowance();
				eAllowance.setAvailableDays((double)Math.round((double)v1 / 365));
			}
			
			eAllowance.setTotalDaysCounted(holidayDayRepository.countDaysByEmployee(idEmployee, y));
			eAllowance.setTotalHoursCounted(holidayDayRepository.countHoursByEmployee(idEmployee, y));
			employeeAllowanceRepository.salvar(eAllowance);
			
		}
	}
	
	public void sendEmail(){
		String subject = "HG WALTER - Holiday Request";
		String text = "Your Holiday Request has been made!\n\n"+
					  
					  "Requested days:\n";
		
		for (HolidayDay holidayDay : listHolidayDay) {
			text += Utils.formataDataStringSemHora(holidayDay.getDay())+"\n";
		}
		
	    text += "\nAwait for your manager's response.\n\n"+
				
			    "Kind Regards \n"+
			    "HG WALTER";
		
		Utils.sendEmail("gustavombraz2@gmail.com", "!Jesuschrist1992@", "gustavo.mafrabraz@hotmail.com", subject, text);
	}
	
	public void validatePeriod(){
		if(employeeHoliday.getHolidayCategory() != null && employeeHoliday.getHolidayCategory().getIdHolidayCategory() > 0){
				if(employeeHoliday.getHolidayCategory().getReckon().equals("D") || employeeHoliday.getHolidayCategory().getReckon().equals("N")){
					if(employeeHoliday.getStartDate() != null && employeeHoliday.getEndDate() != null){
						if(employeeHoliday.getStartDate().getTime() > employeeHoliday.getEndDate().getTime()){
							FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Warn!", "End Date must be equal to or later than Start Date.");
							FacesContext.getCurrentInstance().addMessage(null, message);
							employeeHoliday.setEndDate(null);
						}else{
							if(employee != null){
								createHolidayDay();
							}
							
							int x = 0;
							for (HolidayDay hd : listHolidayDay) {
								if(hd.isCount()){
									x += 1;
								}
							}
							employeeHoliday.setDayCounted(x);
							
							if(employeeHoliday.getBalance() < x){
								setMaximumDays(false);
								FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Warn!", "The requested time has been exceeded your available days.");
								FacesContext.getCurrentInstance().addMessage(null, message);
							}
							
							List<Department> deparments = departmentRepository.listaByEmployee(employee.getIdEmployee());
							if(deparments.size() == 1){
								for (Department d : deparments) {
									if(d.getMaximumDays() < x){
										setMaximumDaysDepartment(false);
										FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Warn!", "The maximum required holidays in the "+d.getDescription().toUpperCase()+" department is only "+d.getMaximumDays().toString());
										FacesContext.getCurrentInstance().addMessage(null, message);
									}
								}
							}
						}
					}
				}
				
				if(employeeHoliday.getHolidayCategory().getReckon().equals("H")){
					employeeHoliday.setEndDate(employeeHoliday.getStartDate());
					
					if(employee != null){
						createHolidayDay();
					}
					
					Contract c = contractRepository.findByEmployee(employee.getIdEmployee());
					long x = c.getAllowance().getWorkingHours() / 2; 
					
					if(employeeHoliday.getStartDate() != null){
						employeeHoliday.setHourCounted((int)x);
					}else{
						employeeHoliday.setHourCounted(null);
					}
				}
			}
	}
	
	public void createHolidayDay(){
		//CREATE holiday_day
		listHolidayDay = new ArrayList<>();
		long stDt = employeeHoliday.getStartDate().getTime();
		setMaximumEmployees(true);
		
		if(employeeHoliday.getHolidayCategory() != null){
			List<Department> departments = new ArrayList<>();
			long saveCode = Utils.getTimeInMillis(new Date());
			long hashCode = saveCode;
			
			if(employeeHoliday.getHolidayCategory().getReckon() != null && employeeHoliday.getHolidayCategory().getReckon().equals("H")){
				HolidayDay day = new HolidayDay();
				Calendar c = Calendar.getInstance();
				c.setTimeInMillis(stDt);
				
				day.setHolidayPerDay(null);
				if(employee != null){
					departments = departmentRepository.listaByEmployee(employee.getIdEmployee());
					if(departments.size() > 1){
						//5-MANY DEPARTMENTS (SEE DETAILS)
						day.setStatus(5L);
						day.setStatusLog(5L);
					}else{
						for (Department d : departments) {
							List<Department> holidayByDayDepartments = departmentRepository.listHolidayByDayDepartment(c.getTime(), d.getIdDepartment().toString());
							if(holidayByDayDepartments.size() == 0){
								//0-NO ONE SLOT BOOKED
								day.setStatus(0L);
								day.setStatusLog(0L);
							}else{
								for (Department dd : holidayByDayDepartments) {
									//2-SLOT FULLY BOOKED
									if(dd.getMaximumEmployees() <= dd.getHolidayPerDay()){
										day.setStatus(2L);
										day.setStatusLog(2L);
										setMaximumEmployees(false);
										
										FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Warn!", Utils.formataDataStringSemHora(c.getTime())+" is fully booked.");
										FacesContext.getCurrentInstance().addMessage(null, message);
									}
									
									//1-SOME SLOTS AVAILABLE
									if(dd.getMaximumEmployees() > dd.getHolidayPerDay() && dd.getHolidayPerDay() > 0){
										day.setStatus(1L);
										day.setStatusLog(1L);
									}
								}
							}
						}
					}
				}
				
				day.setDay(c.getTime());
				day.setYear((long) Utils.getDateYear(c.getTime()));
				day.setEmployeeHoliday(employeeHoliday);
				day.setSaveCode(saveCode);
				day.setHashCode(hashCode+1);
				hashCode++;
				
				//SE DIA DA SEMANA NAO ESTIVER NO ALLOWANCE SETAR FALSE E TBM SE FOR BANK HOLIDAY
				BankHoliday bh = bankHolidayRepository.findByDate(Utils.formataDataStringSemHora(c.getTime()));
				if(bh != null){
					day.setCount(false);
					day.setStatus(6L);
					day.setStatusLog(6L);
				}else{
					if(employee != null){
						Contract contract = contractRepository.findByEmployee(employee.getIdEmployee());
						int init = Utils.WeekDayToInt(contract.getAllowance().getStartDayWorking());
						int end = Utils.WeekDayToInt(contract.getAllowance().getEndDayWorking());
						if(c.get(Calendar.DAY_OF_WEEK) >= init && c.get(Calendar.DAY_OF_WEEK) <= end){
							day.setCount(true);
						}else{
							day.setCount(false);
							day.setStatus(3L);
						}
					}
				}
				
				if(day.getIdHolidayDay() == null || day.getIdHolidayDay() > 0){
					listHolidayDay.add(day);
				}
			}else{
				while(stDt <= employeeHoliday.getEndDate().getTime()){
					HolidayDay day = new HolidayDay();
					Calendar c = Calendar.getInstance();
					c.setTimeInMillis(stDt);
					
					day.setHolidayPerDay(null);
					if(employee != null){
						departments = departmentRepository.listaByEmployee(employee.getIdEmployee());
						if(departments.size() > 1){
							//5-MANY DEPARTMENTS (SEE DETAILS)
							day.setStatus(5L);
							day.setStatusLog(5L);
						}else{
							for (Department d : departments) {
								List<Department> holidayByDayDepartments = departmentRepository.listHolidayByDayDepartment(c.getTime(), d.getIdDepartment().toString());
								if(holidayByDayDepartments.size() == 0){
									//0-NO ONE SLOT BOOKED
									day.setStatus(0L);
									day.setStatusLog(0L);
								}else{
									for (Department dd : holidayByDayDepartments) {
										//2-SLOT FULLY BOOKED
										if(dd.getMaximumEmployees() <= dd.getHolidayPerDay()){
											day.setStatus(2L);
											day.setStatusLog(2L);
											setMaximumEmployees(false);
											
											FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Warn!", Utils.formataDataStringSemHora(c.getTime())+" is fully booked.");
											FacesContext.getCurrentInstance().addMessage(null, message);
										}
										
										//1-SOME SLOTS AVAILABLE
										if(dd.getMaximumEmployees() > dd.getHolidayPerDay() && dd.getHolidayPerDay() > 0){
											day.setStatus(1L);
											day.setStatusLog(1L);
										}
									}
								}
							}
						}
					}
					
					day.setDay(c.getTime());
					day.setYear((long) Utils.getDateYear(c.getTime()));
					day.setEmployeeHoliday(employeeHoliday);
					day.setSaveCode(saveCode);
					day.setHashCode(hashCode+1);
					hashCode++;
					
					//BANK HOLIDAY
					//	WEEKEND
					//		COUNTED HOLIDAY
					
					//SE DIA DA SEMANA NAO ESTIVER NO ALLOWANCE SETAR FALSE E TBM SE FOR BANK HOLIDAY
					BankHoliday bh = bankHolidayRepository.findByDate(Utils.formataDataStringSemHora(c.getTime()));
					if(bh != null){
						day.setCount(false);
						day.setStatus(6L);
						day.setStatusLog(6L);
					}else{
						if(employee != null){
							Contract contract = contractRepository.findByEmployee(employee.getIdEmployee());
							int init = Utils.WeekDayToInt(contract.getAllowance().getStartDayWorking());
							int end = Utils.WeekDayToInt(contract.getAllowance().getEndDayWorking());
							if(c.get(Calendar.DAY_OF_WEEK) >= init && c.get(Calendar.DAY_OF_WEEK) <= end){
								if(employeeHoliday.getHolidayCategory().getReckon().equals("N")){
									day.setCount(false);
									day.setStatus(3L);
								}else{
									day.setCount(true);
								}
							}else{
								day.setCount(false);
								day.setStatus(3L);
								day.setStatusLog(3L);
							}
						}
					}
					
					if(day.getIdHolidayDay() == null || day.getIdHolidayDay() > 0){
						listHolidayDay.add(day);
					}
					
					stDt = Utils.addDias(c.getTime(), 1).getTime();
				}
			}
		}
	}
	
	public void setTrue(){
		setDisabledStartDate(true);
		setDisabledEndDate(true);
		setDisabledHC(true);
		setDisabledSetDayOff(true);
	}
	
	public void setFalse(){
		setDisabledStartDate(false);
		setDisabledEndDate(false);
		setDisabledHC(false);
		setDisabledSetDayOff(false);
	}
	
	public void partTime(boolean validatePeriod){
		if(employeeHoliday.getHolidayCategory() != null){
			if(employeeHoliday.getHolidayCategory().getReckon() != null && !employeeHoliday.getHolidayCategory().getReckon().equals("")){
				if(employeeHoliday.getHolidayCategory().getReckon().equals("H")){
					setDisabledEndDate(true);
					employeeHoliday.setEndDate(employeeHoliday.getStartDate());
				}else{
					setDisabledEndDate(false);
					listHolidayDay = new ArrayList<>();
				}
				
				if(validatePeriod)
					validatePeriod();
			}
		}
		
		setDisabledDaysHours();
	}
	
	public void setDayOff(HolidayDay hd){
		long saveCode = Utils.getTimeInMillis(new Date());
		for (HolidayDay hDay : listHolidayDay) {
			hDay.setSaveCode(saveCode);
			if(hd != null && hDay != null){
				if(hd.getHashCode() == hDay.getHashCode()){
					if(hd.isCount()){
						hDay.setStatus(hDay.getStatusLog());
					}else{
						hDay.setStatus(4L);
					}
					
					hDay.setCount(hd.isCount());
				}
			}
		}
		
		if(employeeHoliday.getHolidayCategory().getReckon().equals("D") || employeeHoliday.getHolidayCategory().getReckon().equals("N")){
			int x = 0;
			for (HolidayDay hoDay : listHolidayDay) {
				if(hoDay.isCount()){
					x += 1;
				}
			}
			employeeHoliday.setDayCounted(x);
		}
		
		if(employeeHoliday.getHolidayCategory().getReckon().equals("H")){
			employeeHoliday.setEndDate(employeeHoliday.getStartDate());
			
			Contract c = contractRepository.findByEmployee(employee.getIdEmployee());
			long x = c.getAllowance().getWorkingHours() / 2; 
			
			employeeHoliday.setHourCounted((int)x);
		}
	}
	
	public boolean renderedSetDayOff(Long status){
		if(status != null){
			if(status == 3 || status == 6){
				return false;
			}else{
				return true;
			}
		}else{
			return false;
		}
	}
	
	public String StringDescription(HolidayDay hd){
		if(hd != null){
			if(hd.isCount()){
				if(employeeHoliday.getHolidayCategory() != null){
					return employeeHoliday.getHolidayCategory().getDescription();
				}else{
					return "";
				}
			}else{
				if(hd.getStatus() == 3){
					return "NOT COUNTED";
				}else if(hd.getStatus() == 6){
					return "BANK HOLIDAY";
				}else{
					return "DAY OFF";
				}
			}
		}else{
			return "";
		}
	}
	
	public String StringDayHour(HolidayDay hd){
		String s = "";
		
		if(employeeHoliday.getHolidayCategory() != null){
			if(employeeHoliday.getHolidayCategory().getReckon().equals("H")){
				if(hd != null){
					if(hd.isCount()){
						if(employeeHoliday.getHourCounted() != null){
							if(employeeHoliday.getHourCounted() == 1){
								s = "1 hour";
							}else{
								s = employeeHoliday.getHourCounted().toString()+" hours";
							}
						}
					}else{
						s = "0 hour";
					}
				}
			}else{
				if(hd != null){
					if(hd.isCount()){
						s = "1 day";
					}else{
						s = "0 day";
					}
				}
			}
		}
		
		return s;
	}
	
	public boolean getImageStatus(long status, long fixo){
		if(status == fixo){
			return true;
		}else{
			return false;
		}
	}
	
	public void setDisabledDaysHours(){
		setDisabledDaysHours(true);
		if(employeeHoliday.getHolidayCategory() != null){
			if(employeeHoliday.getHolidayCategory().getIdHolidayCategory() != null && employeeHoliday.getHolidayCategory().getIdHolidayCategory() > 0){
				if(employeeHoliday.getHolidayCategory().getReckon().equals("H")){
					setDisabledDaysHours(false);
				}
			}
		}
	}
	
	public void disabledControl(boolean validatePeriod){
		if(permissaoSalvar){
			setFalse();
			partTime(validatePeriod);
			
			if(employeeHoliday.getHolidayCategory() == null){
				setTrue();
				setDisabledHC(false);
			}else{
				if(employeeHoliday.getHolidayCategory().getIdHolidayCategory() == null){
					setTrue();
					setDisabledHC(false);
				}
			}
		}else{
			setTrue();
		}
	}
	
	public void setHCDisabledControl() {
		employeeHoliday.setHolidayCategory(null);
		disabledControl(true);
	}
	
	public List<String> completeHolidayCategory(String busca) {
		List<String> resultados = new ArrayList<String>();
		
		for (HolidayCategory holidayCategory : holidayCategoryList) {
			if (holidayCategory.getDescription().toLowerCase().contains(busca.toLowerCase())) {
    			resultados.add(holidayCategory.toString());
			}
		} 
		return resultados;
	}
	
	public Date getNewDate() {
		return new Date();
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isPermissaoSalvar() {
		return permissaoSalvar;
	}

	public void setPermissaoSalvar(boolean permissaoSalvar) {
		this.permissaoSalvar = permissaoSalvar;
	}

	public EmployeeHoliday getEmployeeHoliday() {
		return employeeHoliday;
	}

	public void setEmployeeHoliday(EmployeeHoliday employeeHoliday) {
		this.employeeHoliday = employeeHoliday;
	}

	public List<HolidayCategory> getHolidayCategoryList() {
		return holidayCategoryList;
	}

	public void setHolidayCategoryList(List<HolidayCategory> holidayCategoryList) {
		this.holidayCategoryList = holidayCategoryList;
	}

	public List<HolidayDay> getListHolidayDay() {
		return listHolidayDay;
	}

	public void setListHolidayDay(List<HolidayDay> listHolidayDay) {
		this.listHolidayDay = listHolidayDay;
	}

	public boolean isDisabledEndDate() {
		return disabledEndDate;
	}

	public void setDisabledEndDate(boolean disabledEndDate) {
		this.disabledEndDate = disabledEndDate;
	}

	public boolean isDisabledStartDate() {
		return disabledStartDate;
	}

	public void setDisabledStartDate(boolean disabledStartDate) {
		this.disabledStartDate = disabledStartDate;
	}

	public boolean isDisabledHC() {
		return disabledHC;
	}

	public void setDisabledHC(boolean disabledHC) {
		this.disabledHC = disabledHC;
	}

	public boolean isDisabledSetDayOff() {
		return disabledSetDayOff;
	}

	public void setDisabledSetDayOff(boolean disabledSetDayOff) {
		this.disabledSetDayOff = disabledSetDayOff;
	}

	public boolean isDisabledDaysHours() {
		return disabledDaysHours;
	}

	public void setDisabledDaysHours(boolean disabledDaysHours) {
		this.disabledDaysHours = disabledDaysHours;
	}

	public boolean isInit() {
		return init;
	}

	public void setInit(boolean init) {
		this.init = init;
	}

	public boolean isMaximumDays() {
		return maximumDays;
	}

	public void setMaximumDays(boolean maximumDays) {
		this.maximumDays = maximumDays;
	}

	public boolean isMaximumEmployees() {
		return maximumEmployees;
	}

	public void setMaximumEmployees(boolean maximumEmployees) {
		this.maximumEmployees = maximumEmployees;
	}

	public boolean isMaximumDaysDepartment() {
		return maximumDaysDepartment;
	}

	public void setMaximumDaysDepartment(boolean maximumDaysDepartment) {
		this.maximumDaysDepartment = maximumDaysDepartment;
	}

}
