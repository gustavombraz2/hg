package com.rasz.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.controller.MainController;
import com.rasz.domain.Employee;
import com.rasz.domain.EmployeeAllowance;
import com.rasz.domain.EmployeeHoliday;
import com.rasz.domain.FlowRuleEnum;
import com.rasz.domain.HolidayCategory;
import com.rasz.domain.HolidayDay;
import com.rasz.domain.Usuario;
import com.rasz.repository.EmployeeAllowanceRepository;
import com.rasz.repository.EmployeeHolidayRepository;
import com.rasz.repository.EmployeeRepository;
import com.rasz.repository.HolidayCategoryRepository;
import com.rasz.repository.HolidayDayRepository;
import com.rasz.utils.FlowUtilsFactory;
import com.rasz.utils.Utils;

public class MyHolidayCriteria extends AbstractCriteria<EmployeeHoliday> {

	private static final long serialVersionUID = 1L;
	
	@Autowired private MainController mainController;
	@Autowired private transient EmployeeHolidayRepository employeeHolidayRepository;
	@Autowired private transient EmployeeRepository employeeRepository;
	@Autowired private transient HolidayCategoryRepository holidayCategoryRepository;
	@Autowired private transient HolidayDayRepository holidayDayRepository;
	@Autowired private transient EmployeeAllowanceRepository employeeAllowanceRepository;
	
	private EmployeeHoliday employeeHoliday;
	private Employee employee = new Employee();
	private HolidayCategory holidayCategory = new HolidayCategory();
	private Long selectedYear = (long)Utils.getDateYear(new Date());
	List<Employee> employeeList = new ArrayList<>();
	List<HolidayCategory> holidayCategoryList = new ArrayList<>();
	
	@Override
	public Page<EmployeeHoliday> load(Pageable pageable) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		Usuario u = mainController.getUsuarioLogado();
		employee = employeeRepository.findByUsuario(u.getId());
		
		updateEmployeeAllowance();
		
		Long idHolidayCategory = 0L;
		if(holidayCategory != null){
			if(holidayCategory.getIdHolidayCategory() != null && holidayCategory.getIdHolidayCategory() > 0){
				idHolidayCategory = holidayCategory.getIdHolidayCategory();
			}
		}
		
		if (auth != null) {	
			PageImpl<EmployeeHoliday> page = (PageImpl<EmployeeHoliday>) employeeHolidayRepository.listarTodos(employee.getIdEmployee(), idHolidayCategory, pageable);
			
			List<HolidayDay> listDaysByEmployeeYear = holidayDayRepository.countHoursDaysByEmployeeYear(employee.getIdEmployee().toString(), selectedYear);
			List<EmployeeHoliday> listPage = (List<EmployeeHoliday>)page.getContent();
			
			for (EmployeeHoliday eh : listPage) {
				if(eh.getEmployee() != null){
					if(eh.getEmployee().getIdEmployee() != null){
						for (HolidayDay hd : listDaysByEmployeeYear) {
							if(hd.getEmployeeHoliday() != null){
								if(hd.getEmployeeHoliday().getEmployee() != null){
									if(eh.getEmployee().getIdEmployee() == hd.getEmployeeHoliday().getEmployee().getIdEmployee()){
										eh.setCountedDays(hd.getEmployeeHoliday().getCountedDays());
										eh.setAvailableDays(hd.getEmployeeHoliday().getAvailableDays());
										eh.setBalance(hd.getEmployeeHoliday().getBalance());
										eh.setWorkingHours(hd.getEmployeeHoliday().getWorkingHours());
										eh.setCountedHours(hd.getEmployeeHoliday().getCountedHours());
									}
								}
							}
						}
					}
				}
			}
			
			return new PageImpl<EmployeeHoliday>(listPage, pageable, page.getTotalElements());
		}else{
			return null;
		}
	}
	
	@Transactional
	public void updateEmployeeAllowance() {
		List<Employee> listEmployee =  employeeRepository.listEmployeeContainsEH();
		
		List<Long> years = new ArrayList<>();
		years.add((long)Utils.getDateYear(new Date()) - 1);
		years.add((long)Utils.getDateYear(new Date()));
		years.add((long)Utils.getDateYear(new Date()) + 1);
		for (Long y : years) {
			List<HolidayDay> listDaysByEmployeeYear = holidayDayRepository.countHoursDaysByEmployeeYear(employee.getIdEmployee().toString(), y);
			for (HolidayDay hd : listDaysByEmployeeYear) {
				long d = 0;
				long h = 0;
				if(hd.getEmployeeHoliday().getCountedDays() != null){
					d = hd.getEmployeeHoliday().getCountedDays();
				}
				
				if(hd.getEmployeeHoliday().getCountedHours() != null){
					h = hd.getEmployeeHoliday().getCountedHours()/2;
				}
				employeeAllowanceRepository.updateDays(employee.getIdEmployee(), y, d, h);
			}
		}
	}
	
	public void loading(){
		employeeList = employeeRepository.getAll();
		holidayCategoryList = holidayCategoryRepository.listarTodos("", 0);
	}
	
	public void someListenerSelecionarEmployeeHoliday(SelectEvent event){
		if(!Utils.isMobileDevice())
			return;
		
		if(event != null){
			employeeHoliday = (EmployeeHoliday) event.getObject();
			FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).put("employeeHoliday", employeeHoliday);
		}
	}
	
	public List<String> completeEmployee(String busca) {
		List<String> resultados = new ArrayList<String>();
		
		for (Employee employee : employeeList) {
			if (employee.getFullName().toLowerCase().contains(busca.toLowerCase())) {
    			resultados.add(employee.toString());
			}
		}
		return resultados;
	}
	
	public List<String> completeHolidayCategory(String busca) {
		List<String> resultados = new ArrayList<String>();
		
		for (HolidayCategory holidayCategory : holidayCategoryList) {
			if (holidayCategory.getDescription().toLowerCase().contains(busca.toLowerCase())){
    			resultados.add(holidayCategory.toString());
			}
		}
		
		return resultados;
	}
	
	public EmployeeHoliday retornaEmployeeHoliday(){
		return employeeHolidayRepository.findOne(((EmployeeHoliday) this.employeeHoliday).getIdEmployeeHoliday());
	}
	
	@Override
    public Object getRowKey(EmployeeHoliday employeeHoliday) {
        return employeeHoliday != null ? employeeHoliday.getIdEmployeeHoliday() : null;
    }

    @Override
    public EmployeeHoliday getRowData(String rowKey) {
		List<EmployeeHoliday> list = (List<EmployeeHoliday>) getWrappedData();

        for (EmployeeHoliday p : list) {
            if (p.getIdEmployeeHoliday().toString().equals(rowKey)) {
                return p;
            }
        }

        return null;
    }
    
    
    public void limparCampos() {
    	employee = new Employee();
    	holidayCategory = new HolidayCategory();
    	employeeHoliday = null;
    }
    
    public EmployeeHoliday getEmployeeHoliday() {
		return employeeHoliday;
	}

	public void setEmployeeHoliday(EmployeeHoliday employeeHoliday) {
		this.employeeHoliday = employeeHoliday;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public HolidayCategory getHolidayCategory() {
		return holidayCategory;
	}

	public void setHolidayCategory(HolidayCategory holidayCategory) {
		this.holidayCategory = holidayCategory;
	}

	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}

	public List<HolidayCategory> getHolidayCategoryList() {
		return holidayCategoryList;
	}

	public void setHolidayCategoryList(List<HolidayCategory> holidayCategoryList) {
		this.holidayCategoryList = holidayCategoryList;
	}
	
	public String getDate(Date date) {
		return Utils.formataDataStringSemHora(date);
	}
	
	public String toStringHeaderBalance(){
		if(selectedYear != null){
			return "Balance " + selectedYear.toString();
		}else{
			return "";
		}
	}

	public Long getSelectedYear() {
		return selectedYear;
	}

	public void setSelectedYear(Long selectedYear) {
		this.selectedYear = selectedYear;
	}
	
}
