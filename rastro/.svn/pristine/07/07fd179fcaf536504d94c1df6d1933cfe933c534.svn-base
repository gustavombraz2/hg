package com.rasz.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;

import com.rasz.domain.Fornecedor;
import com.rasz.domain.OrdemRecebimento;
import com.rasz.domain.StatusOrdemRecebimentoEnum;
import com.rasz.domain.UnidadeNegocio;
import com.rasz.utils.Utils;

public class OrdemRecebimentoRepositoryImpl implements OrdemRecebimentoRepositoryCustom{

	private String nf = "%%";
	private String serie = "%%";
	private String gt = "%%";
	private String cnpjComprador = "%%";
	private String cnpjVendedor = "%%";
	private String status = "%%";
	private Long codigo = 0L;
	private UnidadeNegocio un = null;
	private Long fornecedor = null;
	
	@PersistenceContext private EntityManager em;
	
	@Override
	public Page<OrdemRecebimento> listarTodos(String descricao, Pageable pageable) {
		
		String filtro = "";
		
		String select = "SELECT o ";
		String selectCount = "SELECT count(o) ";
		
		String sql      = " FROM " + OrdemRecebimento.class.getName() + " o "
				+ " where 1=1 "+ filtro;
		
		Query query = em.createQuery(select + sql);
		Query queryCount = em.createQuery(selectCount + sql);
		
		@SuppressWarnings("unchecked")
		List<OrdemRecebimento> listaOrdemRecebimento = query.getResultList();
		
		int count = ((Number) queryCount.getSingleResult()).intValue();
		
		return new PageImpl<OrdemRecebimento>(listaOrdemRecebimento, pageable, count);
	}

	
	private void setValues(OrdemRecebimento or) {
		try {
			if (or.getNotaFiscal() != null && !or.getNotaFiscal().trim().isEmpty()) {
				nf = or.getNotaFiscal().replaceFirst("^0+(?!$)", "");
			}else{
				nf = "%%";
			}
			if (or.getId() != null) {
				codigo = or.getId();
			}else{
				codigo = 0L;
			}
			
			if (or.getSerieNotaFiscal() != null && !or.getSerieNotaFiscal().trim().isEmpty()) {
				serie = or.getSerieNotaFiscal().trim().replaceFirst("^0+(?!$)", "");
			}else {
				serie = "%%";
			}
			
			if (or.getGuiaTrafego() != null  && !or.getGuiaTrafego().trim().isEmpty()){
				gt = or.getGuiaTrafego().trim().replaceFirst("^0+(?!$)", "");
			}else{
				gt = "%%";
			}
			
			if (or.getCnpjComprador() != null  && !Utils.retirarMascaras(or.getCnpjComprador().trim()).isEmpty() && 
					Utils.retirarMascaras(or.getCnpjComprador().trim()).length() == 14) {
				cnpjComprador = Utils.retirarMascaras(or.getCnpjComprador().trim());
			}else{
				cnpjComprador = "%%";
			}
			
			if (or.getCnpjVendedor() != null  && !Utils.retirarMascaras(or.getCnpjVendedor().trim()).isEmpty() && 
					Utils.retirarMascaras(or.getCnpjVendedor().trim()).length() == 14) {
				cnpjVendedor = Utils.retirarMascaras(or.getCnpjVendedor().trim());
			}else{
				cnpjVendedor = "%%";
			}
			
			if (or.getStatus() != null  && !or.getStatus().getCanonicalValue().trim().isEmpty()) {
				status = or.getStatus().getCanonicalValue();
			}else{
				status = "%%";
			}
			
			if (or.getUnidadeNegocio() != null  && !or.getUnidadeNegocio().getNome().trim().isEmpty()) {
				un = or.getUnidadeNegocio();
			}else{
				un = null;
			}
			
			if (or.getFornecedor() != null  && or.getFornecedor().getId().longValue() > 0) {
				fornecedor = or.getFornecedor().getId();
			}else{
				fornecedor = null;
			}
		} catch (NullPointerException e) {
			return;
		}
	}

	@Override
	public Page<OrdemRecebimento> filter(OrdemRecebimento or, Pageable pageable) {
		String where = "";

		setValues(or);

		if (nf != null && !nf.equals("%%")) {
			where += " and TRIM(LEADING '0' FROM a.notaFiscal) = :nf ";
		}
		if (codigo != null && codigo.longValue() > 0) {
			where += " and a.id = :codigo ";
		}
		if (serie != null && !serie.equals("%%")) {
			where += " and UPPER(TRIM(a.serieNotaFiscal)) like upper(:serie) ";
		}
		if (gt != null && !gt.equals("%%")) {
			where += " and TRIM(TRIM(LEADING '0' FROM a.guiaTrafego)) like :gt ";
		}
		if (cnpjComprador != null && !cnpjComprador.equals("%%")) {
			where += " and UPPER(a.cnpjComprador) like :cnpjComprador ";
		}
		if (cnpjVendedor != null && !cnpjVendedor.equals("%%")) {
			where += " and UPPER(a.cnpjVendedor) like :cnpjVendedor ";
		}
		if (status != null && !status.equals("%%")) {
			where += " and UPPER(a.status) like :status ";
		}
		
		if (un != null) {
			where += " and a.unidadeNegocio = :unidadeNegocio ";
		}
		
		if (fornecedor != null) {
			where += " and a.fornecedor.id = :fornecedor ";
		}

		String select = " select a ";
		String selectCount = " select count(a) ";

		Order order = pageable.getSort().iterator().next();
		String orderby = " order by a." + order.getProperty() + " " + order.getDirection();

		String sql = " from OrdemRecebimento a where 1=1" + where;

		Query query = em.createQuery(select + sql + orderby);
		Query queryCount = em.createQuery(selectCount + sql);

		if (nf != null && !nf.equals("%%")) {
			query.setParameter("nf", nf.toUpperCase());
			queryCount.setParameter("nf", nf.toUpperCase());
		}
		if (codigo != null && codigo.longValue() > 0) {
			query.setParameter("codigo", codigo);
			queryCount.setParameter("codigo", codigo);
		}
		if (serie != null && !serie.equals("%%")) {
			query.setParameter("serie", serie.toUpperCase());
			queryCount.setParameter("serie", serie.toUpperCase());
		}
		if (gt != null && !gt.equals("%%")) {
			query.setParameter("gt", gt.toUpperCase());
			queryCount.setParameter("gt", gt.toUpperCase());
		}
		if (cnpjComprador != null && !cnpjComprador.equals("%%")) {
			query.setParameter("cnpjComprador", cnpjComprador.toUpperCase());
			queryCount.setParameter("cnpjComprador", cnpjComprador.toUpperCase());
		}
		if (cnpjVendedor != null && !cnpjVendedor.equals("%%")) {
			where += " and UPPER(a.cnpjVendedor) like :cnpjVendedor ";
			query.setParameter("cnpjVendedor", cnpjVendedor.toUpperCase());
			queryCount.setParameter("cnpjVendedor", cnpjVendedor.toUpperCase());
		}
		if (status != null && !status.equals("%%")) {
			query.setParameter("status", StatusOrdemRecebimentoEnum.valueOf(status));
			queryCount.setParameter("status", StatusOrdemRecebimentoEnum.valueOf(status));
		}
		if (un != null) {
			query.setParameter("unidadeNegocio", un);
			queryCount.setParameter("unidadeNegocio", un);
		}
		if (fornecedor != null) {
			query.setParameter("fornecedor", fornecedor);
			queryCount.setParameter("fornecedor", fornecedor);
		}

		query.setMaxResults(pageable.getPageSize());
		query.setFirstResult(pageable.getOffset());

		List<OrdemRecebimento> lista = query.getResultList();

		int count = 0;

		count = ((Number) queryCount.getSingleResult()).intValue();

		return new PageImpl<OrdemRecebimento>(lista, pageable, count);
	}


	@Override
	public void updateStatus(StatusOrdemRecebimentoEnum status, Long idOrdem) {
		String sql = ""
				+ "UPDATE ordem_recebimento "
				+ "SET    status = ? "
				+ "WHERE  ordem_recebimento.id_ordem_recebimento = ?";

		Query query = em.createNativeQuery(sql);
		query.setParameter(1, status.getCanonicalValue());
		query.setParameter(2, idOrdem);
		query.executeUpdate();
	}
	
	
	public List<String> listarIisDaOrdem(Long idOrdem){
		String sql = "SELECT iis_sub_item.iis "+
					 "FROM iis_sub_item "+
					 "INNER JOIN sub_item_ordem_recebimento sior ON sior.id_iis_sub_item = iis_sub_item.id_iis_sub_item "+
					 "INNER JOIN item_ordem_recebimento ior ON ior.id_item_ordem_recebimento = sior.id_item_ordem_recebimento "+
					 "INNER JOIN lote_item_ordem_recebimento lior ON lior.id_lote_item_ordem_recebimento = ior.id_lote_ordem_recebimento "+
					 "INNER JOIN iis_item ON iis_item.id_iis_item = ior.id_iis_item "+
					 "INNER JOIN paiol_ordem_recebimento por ON por.id_paiol_ordem_recebimento = lior.id_paiol_item_ordem_recebimento "+
					 "INNER JOIN header_item_ordem_recebimento hior ON hior.id_header_item_ordem_recebimento = por.id_header_item_ordem_recebimento "+
					 "INNER JOIN ordem_recebimento recebimento ON recebimento.id_ordem_recebimento = hior.id_ordem_recebimento "+
					 "WHERE recebimento.id_ordem_recebimento = " + idOrdem.toString()+
					 "AND iis_sub_item.id_iis_item = iis_item.id_iis_item "+
				 	 "GROUP BY iis_sub_item.iis, iis_item.iis "+
				 	 "UNION ALL "+
					 "SELECT iis_item.iis "+
					 "FROM iis_item "+
					 "INNER JOIN item_ordem_recebimento ior ON ior.id_iis_item = iis_item.id_iis_item "+
					 "INNER JOIN lote_item_ordem_recebimento lior ON lior.id_lote_item_ordem_recebimento = ior.id_lote_ordem_recebimento "+
					 "INNER JOIN paiol_ordem_recebimento por ON por.id_paiol_ordem_recebimento = lior.id_paiol_item_ordem_recebimento "+
					 "INNER JOIN header_item_ordem_recebimento hior ON hior.id_header_item_ordem_recebimento = por.id_header_item_ordem_recebimento "+
					 "INNER JOIN ordem_recebimento recebimento ON recebimento.id_ordem_recebimento = hior.id_ordem_recebimento "+
					 "WHERE recebimento.id_ordem_recebimento = " + idOrdem.toString()+
					 "GROUP BY iis_item.iis ";
		
		Query query = em.createNativeQuery(sql);
		return query.getResultList();
	}

	@Override
	public OrdemRecebimento listarPorOteEPlanta(OrdemRecebimento or) {
		
		String select = "SELECT o ";
		
		String sql      = " FROM OrdemRecebimento o "
				+ " where idTransferenciaImportado = "+ or.getIdTransferenciaImportado()
				+ " and plantaOrigem = "+or.getPlantaOrigem();
		
		Query query = em.createQuery(select + sql);
		try{
			return (OrdemRecebimento) query.getSingleResult();
		}catch (NoResultException nre){
			return null;
		}
	}

	@Override
	public List<OrdemRecebimento> listarOrdensPorFornecedor(Fornecedor fornecedor) {
		String sql = "SELECT r FROM OrdemRecebimento r WHERE r.fornecedor.id = :idFornecedor";
		
		Query query = em.createQuery(sql);
		query.setParameter("idFornecedor", fornecedor.getId());
		
		try {
			return query.getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
}
