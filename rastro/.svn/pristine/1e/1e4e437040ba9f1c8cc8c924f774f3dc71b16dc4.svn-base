package com.rasz.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "blaster")
public class Blaster implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "pk_blaster")
//	@SequenceGenerator(name = "pk_blaster", sequenceName = "id_blaster_seq")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_blaster")
	private Long id;

	@Column(name = "nome")
	private String nome;

	@OneToOne
	@JoinColumn(name = "id_unidade_negocio", referencedColumnName = "id_unidade_negocio")
	private UnidadeNegocio un;

	@Column(name = "cr")
	private String cr;

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_blaster")
	private Set<EmailBlaster> listEmailBlaster;

	@Transient
	private EmailBlaster emailBlasterTemp;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_pessoa")
	private Pessoa pessoa;

	// Cidade Temporaria que ser� utilizado para busca
	@Transient
	private Cidade cidade;
	
	@OneToOne
	@JoinColumn(name = "id_planta", referencedColumnName = "id_planta")
	private Planta planta;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCr() {
		return cr;
	}

	public void setCr(String cr) {
		this.cr = cr;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public UnidadeNegocio getUn() {
		return un;
	}

	public void setUn(UnidadeNegocio un) {
		this.un = un;
	}

	public Set<EmailBlaster> getListEmailBlaster() {
		return listEmailBlaster;
	}

	public void setListEmailBlaster(Set<EmailBlaster> listEmailBlaster) {
		this.listEmailBlaster = listEmailBlaster;
	}

	public EmailBlaster getEmailBlasterTemp() {
		return emailBlasterTemp;
	}

	public void setEmailBlasterTemp(EmailBlaster emailBlasterTemp) {
		this.emailBlasterTemp = emailBlasterTemp;
	}

	public Planta getPlanta() {
		return planta;
	}

	public void setPlanta(Planta planta) {
		this.planta = planta;
	}

	@Override
	public String toString() {
		return nome + " - CPF: " + pessoa.getDocumento();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Blaster other = (Blaster) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
