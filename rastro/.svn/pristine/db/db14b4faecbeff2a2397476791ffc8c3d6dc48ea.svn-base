package com.rasz.service;

import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.CodigoPaisEnum;
import com.rasz.domain.Estado;
import com.rasz.domain.Pais;
import com.rasz.repository.EstadoRepository;
import com.rasz.repository.PaisRepository;
import com.rasz.utils.ShowFacesMessage;

@Service
public class PaisService {
	
	@Autowired PaisRepository paisRepository;
	@Autowired EstadoRepository estadoRepository;
	private boolean isConsultar;

	public Pais createPais(){
		return new Pais();
	}
	
	public Pais findByNome(String nome) {
		return paisRepository.findByNome(nome);
	}
	
	public Pais findByCodigo(String codigo) {
		return paisRepository.findByCodigo(codigo);
	}
	
	public List<Pais> findAll() {
		return (List<Pais>) paisRepository.findAll();
	}
	
	@Transactional
	public void save(Pais pais) throws DataAccessException{
		pais.setNome(pais.getNome().toUpperCase());
		pais.setCodigo(pais.getCodigo().toUpperCase());
		
		if(pais.getId() == null && paisRepository.findByCodigo(pais.getCodigo()) != null){
			ShowFacesMessage.error("N�o foi poss�vel realizar a grava��o.", 
					"O Pa�s com o c�digo " + pais.getCodigo() + " j� existe.");
			return;
		}
		
		paisRepository.save(pais);
		ShowFacesMessage.info("Sucesso!", "Todas as informa��es foram gravadas.");
	}
	
	public void salvar(Pais pais){
		try{
			if(pais != null){
				save(pais);
			}
		}catch(DataAccessException e){
			ShowFacesMessage.warn("N�o foi poss�vel realizar o cadastro.", 
					"Verifique se o C�digo e/ou Pa�s j� existem!");
			return;
		}
	}

	public Pais selectById(Long id){
		this.isConsultar = false;
		return paisRepository.selectById(id);
	}
	
	public List<Pais> listarTodos(){
		return (List<Pais>)paisRepository.findAll();
	}
	
	public Pais selectById(Long id, boolean isConsultar){
		this.isConsultar = isConsultar;
		return paisRepository.selectById(id);
	}
	
	@Transactional
	public void remove(Pais pais){
		if(selectEstados(pais).isEmpty()){
			paisRepository.delete(pais);
		}else{
			ShowFacesMessage.info("N�o foi poss�vel realizar a exclus�o.", 
					"O pa�s selecionado (" + pais.getNome()+ ") j� possui estados cadastrados.");	
			return;
		}
	} 
	
	public List<SelectItem> completeCodigoPais(){
		
		List<SelectItem> lista = new ArrayList<SelectItem>();
		
		for (CodigoPaisEnum tipo : CodigoPaisEnum.values()) {
			SelectItem item = new SelectItem(tipo.name(), tipo.getDescricao());
			lista.add(item);
		}
		return lista;
		
	}
	
	public String selecionarPaisEDescricaoPorCodigo(String codigo){
		Pais p = paisRepository.findByCodigo(codigo);
		if(p != null) {
			return p.getCodigo()+" - "+p.getNome();
		}
		return "";
	}
	
	private List<Estado> selectEstados(Pais pais){
		return estadoRepository.selectEstadoByPais(pais.getId());
	}

	public boolean isConsultar() {
		return isConsultar;
	}

	public void setConsultar(boolean isConsultar) {
		this.isConsultar = isConsultar;
	}
}
