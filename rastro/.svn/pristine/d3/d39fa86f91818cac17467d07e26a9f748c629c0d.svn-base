package com.rasz.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;

import com.rasz.domain.TipoEmbalagem;

public class TipoEmbalagemRepositoryImpl implements TipoEmbalagemRepositoryCustom{

	@PersistenceContext
	private EntityManager em;

	@Override
	public TipoEmbalagem findByTipoId(int tipo_id) {
		String sql = "SELECT t FROM TipoEmbalagem t where t.tipoId = :tipoId and t.ativo = TRUE";

		Query query = em.createQuery(sql);
		query.setParameter("tipoId", tipo_id);
		
		try{
			return (TipoEmbalagem) query.getSingleResult();
		}catch(EmptyResultDataAccessException e){
			return null;
		}
	}

	@Override
	public TipoEmbalagem localizarPorNome(String embalagem) {
		String where = "";
		
		if(embalagem != null && !embalagem.equals("")) {
			where += " and UPPER(a.descricao) like :embalagem and a.ativo = TRUE";
		}
		
		String select = " select a ";

		String sql = " from TipoEmbalagem a where 1=1" + where;

		Query query = em.createQuery(select + sql);

		if (embalagem != null && !embalagem.equals("")) {
			query.setParameter("embalagem", embalagem.toUpperCase());
		}
		return (TipoEmbalagem)query.getSingleResult();
	}
	
	@Override
	public List<TipoEmbalagem> listarTodos(){
		String where = "";
		
		where += " and a.ativo= TRUE";
		
		String select = " select a ";

		String sql = " from TipoEmbalagem a where 1=1" + where;

		Query query = em.createQuery(select + sql);

		return (List<TipoEmbalagem>) query.getResultList();
	}
	
	@Override
	public Page<TipoEmbalagem> listarTodos(Pageable pageable, String descricao, Boolean ativo){
		
		String where = "";
		
		//descricao = "%"+descricao+"%";
		
		if (descricao != null && !descricao.equals("%%") && descricao != null && !descricao.equals("")) {
			where += " and UPPER(a.descricao) like :descricao";
		}

		if (ativo) {
			where += " and (a.ativo = :ativo) ";
		}
		
		String select = " select a ";
		String selectCount = " select count(a) ";

		Order order = pageable.getSort().iterator().next();
		String orderby = " order by a." + order.getProperty() + " " + order.getDirection();

		String sql = " from TipoEmbalagem a where 1=1 " + where;

		Query query = em.createQuery(select + sql + orderby);
		Query queryCount = em.createQuery(selectCount + sql);

		if (descricao != null && !descricao.equals("%%") && descricao != null && !descricao.equals("")) {
			query.setParameter("descricao", descricao.toUpperCase());
			queryCount.setParameter("descricao", descricao.toUpperCase());
		}

		if (ativo) {
			query.setParameter("ativo", ativo);
			queryCount.setParameter("ativo", ativo);
		}

		query.setMaxResults(pageable.getPageSize());
		query.setFirstResult(pageable.getOffset());

		List<TipoEmbalagem> lista = query.getResultList();
		
		int count = 0;

		count = ((Number) queryCount.getSingleResult()).intValue();

		return new PageImpl<TipoEmbalagem>(lista, pageable, count);	
	}
}
