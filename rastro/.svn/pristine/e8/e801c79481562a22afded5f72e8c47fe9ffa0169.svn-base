package com.rasz.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.Cidade;
import com.rasz.domain.Distrito;
import com.rasz.domain.Estado;
import com.rasz.domain.FlowRuleEnum;
import com.rasz.repository.CidadeRepository;
import com.rasz.repository.DistritoRepository;
import com.rasz.repository.EnderecoRepository;
import com.rasz.repository.EstadoRepository;
import com.rasz.utils.FlowUtilsFactory;
import com.rasz.utils.ShowFacesMessage;

@Service
public class CidadeService  { 
	@Autowired EstadoRepository 	estadoRepository;
	@Autowired CidadeRepository 	cidadeRepository;
	@Autowired EnderecoRepository 	enderecoRepository;
	@Autowired DistritoRepository  	distritoRepository;
	
	private boolean isConsultar;
	
	public Cidade createCidade(){
		Cidade cidade = new Cidade();
		isConsultar = false;
		return cidade;
	}
	
	public List<Estado> selectAllEstados(){
		return estadoRepository.selectAll();
	}
	
	@Transactional
	private void commitSave(Cidade cidade) throws DataAccessException{
		if(cidade != null){
			cidade.setNome(cidade.getNome().trim().toUpperCase());
			
			if(cidade.getId() == null && cidadeRepository.selectByCodigo(cidade.getCodigo()) != null){
				ShowFacesMessage.error("N�o foi poss�vel realizar a grava��o.", 
						"A Cidade com o c�digo " + cidade.getCodigo() + " j� existe.");
				return;
			}
			
			cidadeRepository.save(cidade);
			ShowFacesMessage.info("Sucesso!", "Todas as informa��es foram gravadas.");
		}
	}
	
	public void save(Cidade cidade){
		try{
			commitSave(cidade);
		}catch(DataAccessException dae){
			ShowFacesMessage.warn("N�o foi poss�vel realizar o cadastro.", 
					"Verifique se o C�digo informado j� existe.");
			return;
		}
	}
	
	@Transactional
	public void remove(Cidade cidade){
		
		if(cidade != null){
			Long countCidades     = enderecoRepository.countByCidadeId(cidade.getId());
			List<Distrito> distritos = distritoRepository.findByIdCidade(cidade.getId());
			
			if(countCidades <= 0 && distritos.isEmpty())
				 cidadeRepository.delete(cidade);
			else{
				ShowFacesMessage.error("N�o foi poss�vel realizar a exclus�o.", 
						"A Cidade selecionada (" + cidade.getNome()+ 
						") j� possui pessoas cadastradas e/ou distritos cadastrados.");
				return;
			} 
		}
		
	}
	
	public List<Cidade> selectAll(){
		return cidadeRepository.selectAll();
	}
	
	public String retornarTituloPagina() {
		if ((Boolean) FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).get("isNovoRegistro"))
			return "CADASTRO DE";
		else if ((Boolean) FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).get("isConsultar"))
			return "CONSULTA DE";
		else if ((Boolean) FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).get("isEditar"))
			return "ALTERA��O DE";
		else
			return "CONSULTA DE";
	}
	
	public Cidade findById(Long id){
		isConsultar = false;
		return cidadeRepository.findById(id);
	}
	
	public Cidade findById(Long id, boolean isConsultar){
		this.isConsultar = isConsultar;
		return cidadeRepository.findById(id);
	}
	
	public Cidade findByNome(String nome){
		return cidadeRepository.findByNome(nome);
	}
	
	public List<Cidade> selectAllEstadosById(Long id){
		return cidadeRepository.selectAllEstadosById(id);
	}
	
	public List<Cidade> listarFiltro(Estado estado){
		if(estado != null){
			return cidadeRepository.selectAllEstadosById(estado.getId());
		}else{
			return cidadeRepository.selectAll();
		}
	}

	public boolean isConsultar() {
		return isConsultar;
	}

	public void setConsultar(boolean isConsultar) {
		this.isConsultar = isConsultar;
	}
} 
