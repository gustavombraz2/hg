package com.rasz.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.RequestContextHolder;

import com.rasz.domain.ArgOrdemProducao;
import com.rasz.domain.Cidade;
import com.rasz.domain.EmailFornecedor;
import com.rasz.domain.Endereco;
import com.rasz.domain.Estado;
import com.rasz.domain.FlowRuleEnum;
import com.rasz.domain.Fornecedor;
import com.rasz.domain.NextPageFactory;
import com.rasz.domain.OrdemRecebimento;
import com.rasz.domain.Pessoa;
import com.rasz.domain.Telefones;
import com.rasz.domain.TipoTelefoneEnum;
import com.rasz.repository.ArgOrdemProducaoRepository;
import com.rasz.repository.CidadeRepository;
import com.rasz.repository.FornecedorRepository;
import com.rasz.repository.OrdemRecebimentoRepository;
import com.rasz.repository.PessoaRepository;
import com.rasz.repository.TelefoneRepository;
import com.rasz.utils.FlowUtilsFactory;
import com.rasz.utils.ShowFacesMessage;
import com.rasz.utils.Utils;
import com.rasz.utils.cep.correios.ClienteWsCorreios;
import com.rasz.utils.cep.correios.EnderecoWSCorreios;
import com.rasz.utils.cep.postmon.ClienteWsPostmon;
import com.rasz.utils.cep.postmon.EnderecoWSPostmon;

@Service
public class FornecedorService {

	private Fornecedor fornecedorFilter;
	private boolean fornecedorReadOnly;
	private boolean isCEPFound;
	
	@Autowired
	CidadeRepository cidadeRepository;
	@Autowired
	PessoaRepository pessoaRepository;
	@Autowired
	TelefoneRepository telefoneRepository;
	@Autowired
	FornecedorRepository fornecedorRepository;
	@Autowired
	ArgOrdemProducaoRepository argOrdemProducaoRepository;
	@Autowired
	OrdemRecebimentoRepository ordemRecebimentoRepository;
	
	public Fornecedor createFornecedor() {
		Fornecedor fornecedor = new Fornecedor();
		Pessoa pessoa = new Pessoa();
		Endereco endereco = new Endereco();
		Cidade cidade = new Cidade();
		Estado estado = new Estado();
		Set<EmailFornecedor> emails = new HashSet<>();
		cidade.setEstado(estado);
		endereco.setCidade(cidade);

		pessoa.setListaTelefones(new ArrayList<Telefones>());
		pessoa.setEndereco(endereco);
		pessoa.setFisica(false);
		pessoa.setJuridica(true);
		fornecedor.setPessoa(pessoa);
		fornecedor.setDescricao("");
		fornecedor.setEmails(emails);

		this.fornecedorReadOnly = false;
		return fornecedor;
	}

	public void validateCnpj(Fornecedor fornecedor) {
		try {
			if (!fornecedor.getPessoa().getDocumento().trim().isEmpty())
				if (!Utils.validaCNPJ(fornecedor.getPessoa().getDocumento())) {
					ShowFacesMessage.warn("C.N.P.J inv�lido",
							"Verifique se o C.N.P.J digitado para a Unidade de Neg�cio � valido.");
					limparCampos(fornecedor);
				}
		} catch (NullPointerException npe) {
			ShowFacesMessage.warn("C.N.P.J inv�lido",
					"Verifique se o C.N.P.J digitado para a Unidade de Neg�cio � valido.");
			limparCampos(fornecedor);
		}
	}
	
	public boolean isDocumentoEmpty(Fornecedor fornecedor){
		try{
			String documento = fornecedor.getPessoa().getDocumento();
			return 
				Utils.retirarMascaras(documento).isEmpty() || !Utils.validaCNPJ(documento) ;
		}catch(NullPointerException npe){
			return false;
		}
	}

	private void limparCampos(Fornecedor fornecedor) {

		try{

			fornecedor.setNome(new String());
			fornecedor.setDescricao(new String());
			fornecedor.setCr(new String());
			fornecedor.setCodigoErp(new String());
			fornecedor.getPessoa().getEndereco().setLogradouro(new String());
			fornecedor.getPessoa().getEndereco().setCep(new String());
			fornecedor.getPessoa().getEndereco().setNomeBairro(new String());
			fornecedor.getPessoa().getEndereco().setCidade(new Cidade());
			fornecedor.setNomeResponsavel(new String());
			fornecedor.getPessoa().setListaTelefones(new ArrayList<Telefones>());
			fornecedor.setFornecedor(false);
		}catch(NullPointerException npe){
			fornecedor = createFornecedor();
		}
	}

	public void validateCnpjExists(Fornecedor fornecedor) {
		String cnpj = fornecedor.getPessoa().getDocumento();
		if (Utils.validaCNPJ(fornecedor.getPessoa().getDocumento())) {
			cnpj = Utils.retirarMascaras(cnpj);
			/*
			 * Se o CPF digitado j� existir para uma Pessoa, ser� buscada o
			 * Blaster dessa pessoa e alterada a refer�ncia do Objeto Blaster.
			 * Com isso, ao inv�s de salvar um novo Blaster e Pessoa, ser� feito
			 * o UPDATE do Blaster:
			 */
			Pessoa pessoa = pessoaRepository.findByDocumento(cnpj.trim());
			if (pessoa != null) {
				Fornecedor fornecedorTemp = fornecedorRepository.findByPessoa(pessoa.getId());

				if (fornecedorTemp == null) {
					pessoa.setCodigo(null);
					fornecedor.setPessoa(pessoa);
					fornecedorTemp = fornecedor;
				}

				RequestContext rc = RequestContextHolder.getRequestContext();
				rc.getConversationScope().put("fornecedor", fornecedorTemp);
				rc.getFlowScope().put("fornecedor", fornecedorTemp);
				RequestContextHolder.setRequestContext(rc);
			} else {
				/*
				 * Se a pessoa for nula, quer dizer que � um novo cadastro. Para
				 * precau��o, reseta os campos evitando informa��es duplicadas:
				 */
				limparCampos(fornecedor);

			}
		} else {
			ShowFacesMessage.warn("C.N.P.J inv�lido",
					"Verifique se o C.N.P.J digitado para a Unidade de Neg�cio � valido.");
			 limparCampos(fornecedor);
		}
	}

	public void searchCEP(Fornecedor fornecedor) {
		String cep = Utils.removeCEPMask(fornecedor.getPessoa().getEndereco().getCep());
		EnderecoWSPostmon endereco = null;
		EnderecoWSCorreios enderecoCorreios = null;
		// FAZ A CONSULTA NO WEB-SERVICE
		try {
			endereco = ClienteWsPostmon.getEnderecoPorCep(cep);
			isCEPFound = true;
		} catch (Exception ex) {
			try{
				if(endereco == null)
					enderecoCorreios = ClienteWsCorreios.getEnderecoPorCep(cep);
			}catch(Exception e){
				sendWarningMessageValidation(fornecedor, "", "N�o foi poss�vel consultar o CEP '" + cep
						+ "'. Verifique se foi digitado corretamente ou se h� conex�o com a Internet", false);
				return;
			}
		}

		// POPULA OS DADOS RELACIONADOS AO ENDERECO:
		try {
			if(endereco != null){
				fornecedor.getPessoa().getEndereco().setLogradouro(endereco.getLogradouro());
				fornecedor.getPessoa().getEndereco().setNomeBairro(endereco.getBairro());
			}else if(enderecoCorreios != null){
				fornecedor.getPessoa().getEndereco().setLogradouro(enderecoCorreios.getLogradouro());
				fornecedor.getPessoa().getEndereco().setNomeBairro(enderecoCorreios.getComplemento());
			}
			isCEPFound = true;
		} catch (Exception ex) {
			sendWarningMessageValidation(fornecedor, "", "", false);
			return;
		}

		// VERIFICA SE A CIDADE EXISTE:
		try {
			if(endereco != null){
				Cidade cidade = cidadeRepository.findByNome(endereco.getCidade().toUpperCase(),
						Utils.removerAcentuacao(endereco.getCidade().toUpperCase()));
				if (cidade != null) {
					fornecedor.getPessoa().getEndereco().setCidade(cidade);
					isCEPFound = true;
				} else
					throw new NullPointerException();
			}else if(enderecoCorreios != null){
				Cidade cidade = cidadeRepository.findByNome(enderecoCorreios.getCidade().toUpperCase(),
						Utils.removerAcentuacao(enderecoCorreios.getCidade().toUpperCase()));
				if (cidade != null) {
					fornecedor.getPessoa().getEndereco().setCidade(cidade);
					isCEPFound = true;
				} else
					throw new NullPointerException();
			}
		} catch (NullPointerException npe) {
			sendWarningMessageValidation(fornecedor, "Cidade n�o cadastrada", "Cadastre a cidade de '"
					+ endereco.getCidade().toUpperCase() + "' antes de cadastrar um Fornecedor.", false);
			return;
		}
	}

	private void sendWarningMessageValidation(Fornecedor fornecedor, String title, String mensagemErro, boolean cepFound) {
		limparValoresCidadeEndereco(fornecedor);
		if (title.isEmpty())
			title = "Erro ao consultar o CEP";

		ShowFacesMessage.warn(title, mensagemErro);
		isCEPFound = cepFound;
	}

	/*
	 * Limpa os valores do objeto de Cidade/Bairro para evitar que se cadastre
	 * informa��es incorretas:
	 */
	private void limparValoresCidadeEndereco(Fornecedor fornecedor) {
		try {
			fornecedor.getPessoa().getEndereco().setLogradouro("");
			fornecedor.getPessoa().getEndereco().setNomeBairro("");
			fornecedor.getPessoa().getEndereco().getCidade().setNome("");
			fornecedor.getPessoa().getEndereco().setCidade(new Cidade());
		} catch (NullPointerException e) {
			return;
		}
	}

	@Transactional
	private boolean saveCommit(Fornecedor fornecedor) throws DataAccessException {

		if (fornecedor != null) {
			boolean numNegativo = verificarNumeroNegativo(fornecedor.getCodigo());
			if(numNegativo) {
				String cnpj = Utils.retirarMascaras(fornecedor.getPessoa().getDocumento());
				//			if (!Utils.validaCNPJ(cnpj)) {
				//				ShowFacesMessage.info("N�o foi poss�vel realizar a opera��o", "O C.N.P.J digitado est� incorreto.");
				//				return;
				//			}


				fornecedor.getPessoa().setDocumento(cnpj);

//				formatTelefoneToPersist(fornecedor);
//				telefoneRepository.save(fornecedor.getPessoa().getListaTelefones());
				
				pessoaRepository.save(fornecedor.getPessoa());
				fornecedorRepository.save(fornecedor);
				ShowFacesMessage.info("Sucesso!", "Todas as informa��es do " + fornecedor.getNome() + " foram gravadas com sucesso.");
				return true;
			}else{
				return false;
			}
		}else{
			ShowFacesMessage.error("Erro!", "Ocorreu um erro inesperado ao tentar realizar o cadastro. Entre em contato com o Administrador.");
			return false;
		}

	}

	public void formatTelefoneToPersist(Fornecedor fornecedor) {

		CopyOnWriteArrayList<Telefones> iter = new CopyOnWriteArrayList<Telefones>(fornecedor.getPessoa().getListaTelefones());

		for (Telefones telefone : iter) {

			String numero = telefone.getNumero();
			numero = numero.trim();
			if (numero.isEmpty())
				continue;

			try {
				telefone.setDdd(Integer.parseInt(numero.substring(0, 3)));
			} catch (NumberFormatException nfe) {
				telefone.setDdd(Integer.parseInt(numero.substring(numero.indexOf('(') + 1, numero.indexOf(')'))));
			}

			telefone.setNumero(numero.substring(numero.indexOf(')') + 1, numero.length()).trim());

			try {
				pessoaRepository.save(fornecedor.getPessoa());
				telefoneRepository.save(fornecedor.getPessoa().getListaTelefones());
			} catch (Exception ex) {
				telefoneRepository.save(fornecedor.getPessoa().getListaTelefones());
				pessoaRepository.save(fornecedor.getPessoa());
				ex.printStackTrace();
			}
		}
	}

	public boolean save(Fornecedor fornecedor) {
		try {
			boolean save = saveCommit(fornecedor);
			if(save) {
				return true;
			}else{
				return false;
			}
		} catch (DataAccessException dae) {
			dae.printStackTrace();
			ShowFacesMessage.warn("N�o foi poss�vel realizar o cadastro.", "Ocorreu um erro inesperado ao realizar o cadastro. Entre em contato com o Administrador.");
			return false;
		}
	}

	@Transactional
	public boolean remove(Fornecedor fornecedor) {
		if (fornecedor != null) {
			List<ArgOrdemProducao> ordensProducao = argOrdemProducaoRepository.listarOrdemPorFornecedor(fornecedor);
			List<OrdemRecebimento> ordensRecebimento = ordemRecebimentoRepository.listarOrdensPorFornecedor(fornecedor);
			if(ordensProducao.isEmpty() || ordensProducao == null
				&& ordensRecebimento.isEmpty() || ordensRecebimento == null) {
				
				fornecedorRepository.delete(fornecedor);
				ShowFacesMessage.info("Sucesso", "Registro exclu�do com sucesso");
				return true;
			}else{
				ShowFacesMessage.error("Erro", "O fornecedor " + fornecedor.getNome() + " possui uma ou mais Ordens vinculadas a ele. "
				+ "E n�o � poss�vel excluir o registro.");
				return false;
			}
		}else{
			ShowFacesMessage.error("Erro ao excluir", "N�o foi poss�vel realizar a exclus�o do registro selecionado.");
			return false;
		}
		
	}

	public Telefones telefoneByEnum(String telefoneEnum, Fornecedor fornecedor) {

		// Recupera o enum passado por String:
		TipoTelefoneEnum telEnum = TipoTelefoneEnum.valueOf(telefoneEnum);

		/*
		 * Verifica se nenhuma lista de telefones existe para esta pessoa, caso
		 * nenhuma lista for existente, � instanciado uma nova lista para esta
		 * pessoa:
		 */
		List<Telefones> listaTelefonesDaPessoa = fornecedor.getPessoa().getListaTelefones();
		if (listaTelefonesDaPessoa == null) {
			listaTelefonesDaPessoa = new ArrayList<Telefones>();
			fornecedor.getPessoa().setListaTelefones(listaTelefonesDaPessoa);
		}

		/*
		 * Percorre cada telefone para formatar com o zero:
		 */
		for (Telefones tel : listaTelefonesDaPessoa) {
			if (tel.getTipoTelefoneEnum() == telEnum) {
				if (tel.getDdd() != null && tel.getNumero() != null)
					tel.setNumero(String.format("%03d", tel.getDdd()) + " " + tel.getNumero().trim());

				return tel;
			}
		}

		Telefones tel = new Telefones();
		tel.setTipoTelefoneEnum(telEnum);
		tel.setPessoa(fornecedor.getPessoa());
		fornecedor.getPessoa().getListaTelefones().add(tel);

		return tel;
	}
	
	//**Trocar t�tulo de pagina para cada a��o**
		public String retornarTituloPagina() {
			if (NextPageFactory.isNextPageExists("isTransitionPage")) {
				return "CADASTRO DE";
			} else {

				if ((Boolean) FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).get("isNovoRegistro"))
					return "CADASTRO DE";
				else if ((Boolean) FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).get("isConsultar"))
					return "CONSULTA DE";
				else if ((Boolean) FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).get("isEditar"))
					return "ALTERA��O DE";
				else
					return "CONSULTA DE";
			}
		}

	public Fornecedor findById(Long id) {
		return fornecedorRepository.findOne(id);
	}

	public Fornecedor findById(Long id, boolean isReadOnly) {
		this.fornecedorReadOnly = isReadOnly;
		return findById(id);
	}
	
	public Iterable<Fornecedor> findAll() {
		return fornecedorRepository.findAll();
	}

	public List<Fornecedor> listarTodos() {
		return (List<Fornecedor>) fornecedorRepository.findAll();
	}
	
	private boolean verificarNumeroNegativo(String numero) {
		if(!numero.equals("") && numero.trim().startsWith("-")) {
			ShowFacesMessage.warn("Aviso", "N�o � permitido cadastar n�mero negativo.");
			return false;
		}
		
		if(!numero.equals("") && numero.trim().startsWith("0")) {
			ShowFacesMessage.warn("Aviso", "N�o � permitido c�digo igual a zero.");
			return false;
		}
		
		if(!numero.equals("") && !numero.trim().startsWith("-")) {
			return true;
		}
		
		return false;
	}
	
	public List<Fornecedor> listarFornecedoresAtivos(boolean ativo) {
		return fornecedorRepository.listarFornecedoresAtivos(ativo);
	}
	
	public Fornecedor getFornecedorFilter() {
		return fornecedorFilter;
	}

	public void setFornecedorFilter(Fornecedor fornecedorFilter) {
		this.fornecedorFilter = fornecedorFilter;
	}

	public void setFornecedorFilterCEP(Fornecedor fornecedorFilter) {
		this.fornecedorFilter = fornecedorFilter;
	}

	public boolean isFornecedorReadOnly() {
		return fornecedorReadOnly;
	}

	public void setFornecedorReadOnly(boolean isFornecedorReadOnly) {
		this.fornecedorReadOnly = isFornecedorReadOnly;
	}

	public boolean isCEPFound() {
		return isCEPFound;
	}

	public void setCEPFound(boolean isCEPFound) {
		this.isCEPFound = isCEPFound;
	}
	
	public String getNumeroEmergencia(long id_fornecedor) {
		return fornecedorRepository.findPhoneEmergencyById(id_fornecedor);
	}

}
