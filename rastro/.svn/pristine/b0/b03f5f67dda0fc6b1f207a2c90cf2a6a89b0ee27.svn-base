package com.rasz.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.rasz.domain.Pais;

public interface PaisRepository extends CrudRepository<Pais, Long> { 
	
	@Query("select x From Pais x where upper(x.nome) LIKE upper(?1)")
	Pais findByNome(String nome);
	
	@Query("select x from Pais x")
	Page<Pais> selectAll(Pageable pageable);	
	
	@Query("select x from Pais x")
	List<Pais> selectAll();
	
	@Query("select x from Pais x where id_pais = ?1")
	Page<Pais> selectAllById(Long id, Pageable pageable);
	
	@Query("select x from Pais x where (lower(x.nome)) LIKE lower(?1)")
	Page<Pais> selectAllByNome(String nome, Pageable pageable);
	
	@Query("select p from Pais p where id_pais = ?1")
	Pais selectById(Long id);
	
	@Query("select p from Pais p where codigo = ?1")
	Page<Pais> findByCodigo(String codigo, Pageable pageable);
	
	@Query("select p from Pais p where upper(p.codigo) = upper(?1)")
	Pais 	   findByCodigo(String codigo);
}
