package com.rasz.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="saldo_itens")
public class SaldoItens implements Serializable{

	private static final long serialVersionUID = 5049179872977830302L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_saldo_produto")
	private Long id;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_paiol", referencedColumnName = "id_paiol")
	private Paiol paiol;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_produto", referencedColumnName = "id_produto")
	private Produto produto;
	
	@Column(name="saldo_anterior")	
	private Long saldoAnterior;
	
	@Column(name="quantidade_entrada")
	private Long quantidadeEntrada;
	
	@Column(name="quantidade_saida")
	private Long quantidadeSaida;
	
	@Column(name="saldo_atual")
	private Long saldoAtual;
	
	@Column(name="mes") //Guia de Trafego
	private Integer mes;

	@Column(name="ano")
	private Integer ano;

	@Column(name="saldo_mes")
	private Long saldoMes;
	
	@Column(name="ultima_modificacao")
	private Date ultimaModificacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Paiol getPaiol() {
		return paiol;
	}

	public void setPaiol(Paiol paiol) {
		this.paiol = paiol;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Long getSaldoAnterior() {
		return saldoAnterior;
	}

	public void setSaldoAnterior(Long saldoAnterior) {
		this.saldoAnterior = saldoAnterior;
	}

	public Long getQuantidadeEntrada() {
		return quantidadeEntrada;
	}

	public void setQuantidadeEntrada(Long quantidadeEntrada) {
		this.quantidadeEntrada = quantidadeEntrada;
	}

	public Long getQuantidadeSaida() {
		return quantidadeSaida;
	}

	public void setQuantidadeSaida(Long quantidadeSaida) {
		this.quantidadeSaida = quantidadeSaida;
	}

	public Long getSaldoAtual() {
		return saldoAtual;
	}

	public void setSaldoAtual(Long saldoAtual) {
		this.saldoAtual = saldoAtual;
	}

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Long getSaldoMes() {
		return saldoMes;
	}

	public void setSaldoMes(Long saldoMes) {
		this.saldoMes = saldoMes;
	}

	public Date getUltimaModificacao() {
		return ultimaModificacao;
	}

	public void setUltimaModificacao(Date ultimaModificacao) {
		this.ultimaModificacao = ultimaModificacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ano == null) ? 0 : ano.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((mes == null) ? 0 : mes.hashCode());
		result = prime * result + ((paiol == null) ? 0 : paiol.hashCode());
		result = prime * result + ((produto == null) ? 0 : produto.hashCode());
		result = prime * result + ((quantidadeEntrada == null) ? 0 : quantidadeEntrada.hashCode());
		result = prime * result + ((quantidadeSaida == null) ? 0 : quantidadeSaida.hashCode());
		result = prime * result + ((saldoAnterior == null) ? 0 : saldoAnterior.hashCode());
		result = prime * result + ((saldoAtual == null) ? 0 : saldoAtual.hashCode());
		result = prime * result + ((saldoMes == null) ? 0 : saldoMes.hashCode());
		result = prime * result + ((ultimaModificacao == null) ? 0 : ultimaModificacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SaldoItens other = (SaldoItens) obj;
		if (ano == null) {
			if (other.ano != null)
				return false;
		} else if (!ano.equals(other.ano))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (mes == null) {
			if (other.mes != null)
				return false;
		} else if (!mes.equals(other.mes))
			return false;
		if (paiol == null) {
			if (other.paiol != null)
				return false;
		} else if (!paiol.equals(other.paiol))
			return false;
		if (produto == null) {
			if (other.produto != null)
				return false;
		} else if (!produto.equals(other.produto))
			return false;
		if (quantidadeEntrada == null) {
			if (other.quantidadeEntrada != null)
				return false;
		} else if (!quantidadeEntrada.equals(other.quantidadeEntrada))
			return false;
		if (quantidadeSaida == null) {
			if (other.quantidadeSaida != null)
				return false;
		} else if (!quantidadeSaida.equals(other.quantidadeSaida))
			return false;
		if (saldoAnterior == null) {
			if (other.saldoAnterior != null)
				return false;
		} else if (!saldoAnterior.equals(other.saldoAnterior))
			return false;
		if (saldoAtual == null) {
			if (other.saldoAtual != null)
				return false;
		} else if (!saldoAtual.equals(other.saldoAtual))
			return false;
		if (saldoMes == null) {
			if (other.saldoMes != null)
				return false;
		} else if (!saldoMes.equals(other.saldoMes))
			return false;
		if (ultimaModificacao == null) {
			if (other.ultimaModificacao != null)
				return false;
		} else if (!ultimaModificacao.equals(other.ultimaModificacao))
			return false;
		return true;
	}

}
