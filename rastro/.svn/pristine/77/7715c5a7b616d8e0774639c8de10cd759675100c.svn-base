package com.rasz.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.rasz.domain.OrdemTransferencia;
import com.rasz.domain.Paiol;
import com.rasz.domain.PaiolSaldo;
import com.rasz.domain.Produto;
import com.rasz.domain.StatusOrdemTransferenciaEnum;
import com.rasz.domain.TipoOrdemEnum;
import com.rasz.domain.UnidadeNegocio;
import com.rasz.service.MovimentacaoService;
import com.rasz.utils.ShowFacesMessage;
import com.rasz.utils.Utils;

@Controller
public class OrdemTransferenciaColetor {
 
	@Autowired private MovimentacaoService 	movimentacaoService;
	
	private StringBuffer sb;
	private Set<Paiol> paiois = new HashSet<>();
	
	public StreamedContent exportarColetor(List<OrdemTransferencia> ordens){
		sb = new StringBuffer();
		paiois = new HashSet<>();
		
		StreamedContent content = null;
		try {
			iniciarExportacao(ordens);
			content =  finalizarExportacao();
		} catch (IOException  | NullPointerException e) {
			e.printStackTrace();
			ShowFacesMessage.error("Erro na exportação", "Ocorreu um erro ao exportar o " + 
					"arquivo de Ordem de Transferência para o Coletor. Causa: " + e.getMessage());	
			return null;
		} 
		
		if(content != null)
			for(OrdemTransferencia ordem : ordens)
				movimentacaoService.updateStatus(StatusOrdemTransferenciaEnum.COLETA, ordem);
		
		return content;
	}
	
	private StreamedContent finalizarExportacao() throws IOException {
		InputStream stream = new ByteArrayInputStream(sb.toString().getBytes());
		StreamedContent ordemColetor = new DefaultStreamedContent(stream, "application/text",
				"OT - Exportado: " + Utils.getAtualTimeToFiles() + ".coletor");
		stream.close();
		return ordemColetor;
	}
	
	private void iniciarExportacao(List<OrdemTransferencia> ordens) throws NullPointerException{
		escreverOrdemCabecalho(ordens);
		escreverProdutos(ordens);
		escreverUnidadeNegocio(ordens);
		escreverPaiol(ordens);
		escreverItemProduto(ordens);
//		escreverRetornoColetor();
	}
	
	private void escreverOrdemCabecalho(List<OrdemTransferencia> ordens) throws NullPointerException{
		sb.append("<OT>");
		sb.append(System.getProperty("line.separator"));
		
		for(OrdemTransferencia ordem : ordens){
			UnidadeNegocio un = consultarUnidadeNegocio(ordem);
			
			if(un == null || un.getId() == null)
				throw new NullPointerException();
			
			sb.append(ordem.getId() + "|");
			sb.append(un.getId() + "|");
			sb.append(System.getProperty("line.separator"));
		}
		sb.append("</OT>");
		sb.append(System.getProperty("line.separator"));
		sb.append(System.getProperty("line.separator"));
	}
	
	private void escreverProdutos(List<OrdemTransferencia> ordens){
		sb.append("<PRODUTOS>");
		sb.append(System.getProperty("line.separator"));
		
		for(Produto p : consultarProdutosDistintos(ordens)){
			
			sb.append(p.getId() + "|");
			sb.append(p.getCodigo() + "|");
			sb.append(p.getNome() + "|");
			sb.append(System.getProperty("line.separator"));
		}
		
		sb.append("</PRODUTOS>");
		sb.append(System.getProperty("line.separator"));
		sb.append(System.getProperty("line.separator"));
	}
	
	private void escreverUnidadeNegocio(List<OrdemTransferencia> ordens){
		sb.append("<UNIDADE_NEGOCIO>");
		sb.append(System.getProperty("line.separator"));
		
		for(UnidadeNegocio un : consultarUnidadesDistintos(ordens)){
			
			sb.append(un.getId() + "|");
			sb.append(un.getPessoa().getDocumento() + "|");
			sb.append(un.getNome() + "|");
			sb.append(System.getProperty("line.separator"));
		}
		
		sb.append("</UNIDADE_NEGOCIO>");
		sb.append(System.getProperty("line.separator"));
		sb.append(System.getProperty("line.separator"));
	}
	
	private void escreverPaiol(List<OrdemTransferencia> ordens){
		sb.append("<PAIOLS>");
		sb.append(System.getProperty("line.separator"));
		
		for(Paiol p : paiois){
			
			sb.append(p.getId() + "|");
			sb.append(p.getCodigo() + "|");
			sb.append(p.getNome() + "|");
			sb.append(System.getProperty("line.separator"));
		}
		
		sb.append("</PAIOLS>");
		sb.append(System.getProperty("line.separator"));
		sb.append(System.getProperty("line.separator"));
	}
	
	private void escreverItemProduto(List<OrdemTransferencia> ordens){
		sb.append("<OT_ITENS_PRODUTO>");
		sb.append(System.getProperty("line.separator"));
		
		for(OrdemTransferencia ordem : ordens){
			for(PaiolSaldo ps : consultarProdutoPaiolOrdem(ordem)){
				
				sb.append(ordem.getId() + "|");
				sb.append(ps.getProduto().getId() + "|");
				sb.append(ps.getProduto().getCodigo() + "|");
				sb.append(ps.getQuantidadeProdutoCarregamento() + "|");
				sb.append(ordem.getPaiolOrigem().getId() + "|");
				sb.append(ordem.getPaiolDestino().getId() + "|");
				sb.append(System.getProperty("line.separator"));
			}
		}
		
		sb.append("</OT_ITENS_PRODUTO>");
		sb.append(System.getProperty("line.separator"));
		sb.append(System.getProperty("line.separator"));
	}
	
	
	//===================================================================================
	//Métodos para consultar dados
	
	private UnidadeNegocio consultarUnidadeNegocio(OrdemTransferencia ordem){
		return movimentacaoService.consultarUnidadeNegocio(ordem.getId(), TipoOrdemEnum.OT);
	}
	
	private Set<UnidadeNegocio> consultarUnidadesDistintos(List<OrdemTransferencia> ordens){
		Set<UnidadeNegocio> un = new HashSet<>();
		
		for(OrdemTransferencia ordem : ordens)
			un.add(ordem.getUnidadeNegocio());
		
		return un;
	}
	
	private Set<Produto> consultarProdutosDistintos(List<OrdemTransferencia> ordens){
		Set<Produto> produtos = new HashSet<>();
		for(OrdemTransferencia ordem : ordens){
			for(PaiolSaldo ps : consultarProdutoPaiolOrdem(ordem)){
				produtos.add(ps.getProduto());
				paiois.add(ordem.getPaiolOrigem());
				paiois.add(ordem.getPaiolDestino());
			}
		}
		
		return produtos;
	}
	
	private Set<PaiolSaldo> consultarProdutoPaiolOrdem(OrdemTransferencia ordem){
		Set<PaiolSaldo> paiolSaldoGrid = movimentacaoService.findPaiolSaldoFromOrdemTransferencia(ordem);
		
		return paiolSaldoGrid;
	}
}