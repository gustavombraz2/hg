package com.rasz.bean;

import java.io.InputStream;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.ArgInventario;
import com.rasz.domain.ArgInventarioItem;
import com.rasz.domain.ArgInventarioItemIis;
import com.rasz.domain.ArgStatusInventarioEnum;
import com.rasz.domain.ArgStatusInventarioItemEnum;
import com.rasz.domain.ArgTipoInventarioEnum;
import com.rasz.domain.Coletor;
import com.rasz.domain.FlowRuleEnum;
import com.rasz.domain.IisItem;
import com.rasz.domain.IisSubItem;
import com.rasz.domain.MovimentacaoIis;
import com.rasz.domain.MovimentacaoItens;
import com.rasz.domain.MovimentacaoSubIis;
import com.rasz.domain.Paiol;
import com.rasz.domain.Produto;
import com.rasz.domain.TipoOrdemEnum;
import com.rasz.service.ArgInventarioService;
import com.rasz.service.ColetorService;
import com.rasz.service.IisItemService;
import com.rasz.service.IisSubItemService;
import com.rasz.service.MovimentacaoIisService;
import com.rasz.service.MovimentacaoItensService;
import com.rasz.service.MovimentacaoService;
import com.rasz.service.MovimentacaoSubIisService;
import com.rasz.service.PaiolService;
import com.rasz.service.ProdutoService;
import com.rasz.utils.FlowUtilsFactory;
import com.rasz.utils.ShowFacesMessage;
import com.rasz.utils.Utils;
import com.rasz.webservice.LSTRECCONSULTACAIXA;
import com.rasz.webservice.RECCONSULTACAIXA;
import com.rasz.webservice.Webservice;

public class ArgInventarioImportarColetor implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired private transient IisItemService				 		iisItemService;
	@Autowired private transient IisSubItemService				 	iisSubItemService;
	@Autowired private transient PaiolService				 		paiolService;
	@Autowired private transient ColetorService				 		coletorService;
	@Autowired private transient ArgInventarioService				inventarioService;
	@Autowired private transient MovimentacaoItensService 	 		movimentacaoItensService;
	@Autowired private transient MovimentacaoIisService 			movimentacaoIisService;
	@Autowired private transient MovimentacaoSubIisService			movimentacaoSubIisService;
	@Autowired private transient MovimentacaoService 				movimentacaoService;
	@Autowired private transient ProdutoService				 		produtoService;
	@Autowired private transient Webservice 						webService;
	
	private UploadedFile fileUploaded;
	
	private List<ArgInventarioItem> listaInventario = new ArrayList<>();
	
	private ArgInventarioItem itemSelected = new ArgInventarioItem();
	private ArgInventarioItem itemEscolha;
	
	private ArgInventario argInventario;
	
	private boolean consultar;
	
	@Transactional(rollbackFor=Exception.class)
	public void listener(FileUploadEvent event) throws Exception {
		
		this.fileUploaded = event.getFile();

		if (fileUploaded == null)
			return;
		boolean retorno;
		
		try {
			retorno = listenerRaw();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} 
		
		if(!retorno){
			ShowFacesMessage.info("Sucesso!", "Arquivo inventário importado com sucesso!");
		}
	}
	
	private boolean listenerRaw() throws Exception{
		boolean encontrouArquivo = false;
		
		for (ArgInventarioItem argInventarioItem : listaInventario) {
			if(fileUploaded.getFileName().equals(argInventarioItem.getNomeArquivo())){
				encontrouArquivo = true;
				break;
			}
		}
		
		if(!encontrouArquivo){
			InputStream input = fileUploaded.getInputstream();
	
			StringWriter writer = new StringWriter();
			IOUtils.copy(input, writer, "utf-8");
			String arquivo = writer.toString();
	
			return iniciar(arquivo);
		}else{
			ShowFacesMessage.warn("Falha!", "O arquivo já foi importado anteriormente!");
			return true;
		}
	}
	
	private boolean iniciar(String conteudo) throws Exception{
		String mainTag = "<COLETOR_INVENTARIO_IIS>";
		int init = conteudo.indexOf(mainTag) + mainTag.length() ;
		int end  = conteudo.indexOf("</COLETOR_INVENTARIO_IIS>");
		String split[] = conteudo.substring(init, end).split("\n");
		
		return popularDados(split);
	}
	
	private boolean popularDados(String[] split) throws Exception{
		
		boolean falha = false;
		for(String s : split){
			
			if(s.trim().isEmpty()) continue;
			
			StringTokenizer st = new StringTokenizer(s, "|");
			
			//Codigo do Armazem| Id do Coletor| Data| Hora| IIS| Sequencia de Leitura| Codigo do Usuario
			while(st.hasMoreTokens()){
				ArgInventarioItem item = new ArgInventarioItem();
				item.setItemInventarioIis(new ArrayList<ArgInventarioItemIis>());
				ArgInventarioItemIis iisItem = new ArgInventarioItemIis();
				
				//Codigo do Armazem
				String paiolElement 		= (String) st.nextElement();
				Paiol paiol = new Paiol();
				paiol = paiolService.findById(Long.parseLong(paiolElement));
				item.setPaiol(paiol);
				
				try {

					String coletorElement		= (String) st.nextElement();
					Coletor coletor = new Coletor();
					coletor = coletorService.findOne(Long.parseLong(coletorElement));
					if(coletor != null) {
						item.setColetor(coletor);
					}else{
						falha = true;
						ShowFacesMessage.error("Erro", "ID do Coletor n�o encontrado.");
						break;
					}	
					String dataElement		= (String) st.nextElement();
					String horaElement		= (String) st.nextElement();
					item.setDataLeitura(Utils.formataData(dataElement+" "+horaElement));
					iisItem.setDataLeitura(Utils.formataData(dataElement+" "+horaElement));

					String iisElement		= (String) st.nextElement();
					iisItem.setIis(iisElement);

					String sequenciaLeituraElement		= (String) st.nextElement();
					item.setSequenciaLeitura(Long.parseLong(sequenciaLeituraElement));

					String codigoUsuarioElement		= (String) st.nextElement();
					item.setCodigoUsuario(codigoUsuarioElement);

					if(!inventarioService.localizarSequenciaPorData(item.getSequenciaLeitura(), item.getDataLeitura(), coletor)){
						montarInventarioPorSequenciaLeitura(item, iisItem);
					}else{
						falha = true;
						ShowFacesMessage.warn("Falha!", "Sequência do coletor já foi adicionado nesta data!");
						break;
					}

				} catch (NoSuchElementException e) {
					// TODO: handle exception
				}

			}
			
			if(falha){
				break;
			}
		}
		
		if(!falha){
			sinalizarInventario();
		}
		
		return falha;
		
		
	}
		

	private void montarInventarioPorSequenciaLeitura(ArgInventarioItem item, ArgInventarioItemIis iisItem){
		if(listaInventario == null){
			listaInventario = new ArrayList<>();
		}
		
		if(!listaInventario.contains(item)){
			item.getItemInventarioIis().add(iisItem);
			listaInventario.add(item);
		}else{
			for (ArgInventarioItem argInventarioItem : listaInventario) {
				if(argInventarioItem.getSequenciaLeitura().equals(item.getSequenciaLeitura())){
					argInventarioItem.getItemInventarioIis().add(iisItem);
					break;
				}
			}
		}
		
	}
	
	private void sinalizarInventario(){
		
		List<ArgInventarioItem> listaTemp = new ArrayList<ArgInventarioItem>();
		
		listaTemp.addAll(listaInventario);
		
		for (ArgInventarioItem item1 : listaInventario) {
			for (ArgInventarioItem item2 : listaInventario) {
				
				item1.setNomeArquivo(fileUploaded.getFileName());
				
				if(item1.getItemInventarioIis().containsAll(item2.getItemInventarioIis()) && 
						item1.getItemInventarioIis().size() == item2.getItemInventarioIis().size()){
					if(item1.getCountVerde() >=1){
						item1.setSinal("verde");
						item2.setSinal("verde");
					}
					item1.setCountVerde(item1.getCountVerde()+1);
				}else{
					Double percent = Double.parseDouble(String.valueOf(listaInventario.size()))/2;
					if(item2.getCountVerde() < percent){
						item2.setSinal("vermelho");
						item2.setCountVermelho(item2.getCountVermelho()+1);
					}
				}
			}
			
			Double percent = Double.parseDouble(String.valueOf(listaInventario.size()))/2;
			
			if(item1.getCountVerde() > percent){
				item1.setSinal("verde");
				break;
			}
		}
		
		boolean isVerde = true;
		for (ArgInventarioItem item3 : listaInventario) {
			if(item3.getSinal() == null) {
				item3.setSinal("vermelho");
			}else{
				if(!item3.getSinal().equals("verde")){
					isVerde = false;
					break;
				}
			}
		}
		
		if(isVerde){
			for (ArgInventarioItem item1 : listaInventario) {
				for (ArgInventarioItem item2 : listaInventario) {
					if(item1 != item2){
						if(item1.getItemInventarioIis().containsAll(item2.getItemInventarioIis()) && 
								item1.getItemInventarioIis().size() == item2.getItemInventarioIis().size()){
							isVerde = true;
						}else{
							isVerde = false;
							break;
						}
					}
				}
				break;
			}

			if(!isVerde){
				for (ArgInventarioItem item1 : listaInventario) {
					item1.setSinal("laranja");
				}
			}
		}
		
		
	}
	
	public boolean validarSalvar(){
		ArgInventario inventario = (ArgInventario) FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).get("scopeInventario");
		
		boolean encontrou = false;
		if(listaInventario != null && !listaInventario.isEmpty()){
			if(itemEscolha != null){
				for (ArgInventarioItem argInventarioItem : listaInventario) {
					if(itemEscolha == argInventarioItem){
						if(argInventarioItem.getStatus() == ArgStatusInventarioItemEnum.A || argInventarioItem.getStatus() == ArgStatusInventarioItemEnum.C ){
							argInventarioItem.setStatus(ArgStatusInventarioItemEnum.C);
							encontrou = true;
						}else{
							ShowFacesMessage.warn("Falha!", "O Item selecionado não pode ser escolhido para o inventário!");
							encontrou = false;
						}
					}else if(argInventarioItem.getStatus() == ArgStatusInventarioItemEnum.C){
						argInventarioItem.setStatus(ArgStatusInventarioItemEnum.A);
					}
				}
			}else{
				ShowFacesMessage.warn("Falha!", "Favor, selecionar pelo menos uma coleta!");
			}
		}else{
			ShowFacesMessage.warn("Falha!", "Favor, adicionar pelo menos uma coleta!");
		}
		
		if(encontrou){
			inventario.setItemInventario(listaInventario);
			FlowUtilsFactory.currentInstance(FlowRuleEnum.CONVERSATION_SCOPE).put("scopeInventario", inventario);
		}
		
		return encontrou;
	}
	
	public void limparRegistrosUnicos(){
		listaInventario = new ArrayList<>();
		itemEscolha = null;
	}
	
	public void popularRegistroColetor(){
		
		limparRegistrosUnicos();
		
		ArgInventario inventario = (ArgInventario) FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).get("scopeInventario");
		
		if(inventario != null && inventario.getItemInventario() != null){
			listaInventario = inventario.getItemInventario();
		}
		
		for (ArgInventarioItem argInventarioItem : listaInventario) {
			if(argInventarioItem.getStatus() == ArgStatusInventarioItemEnum.C){
				itemEscolha = argInventarioItem;
			}
			
		}
		
	}
	
	public void excluirItemInventario(){
		
		if(itemSelected != null){
			for (ArgInventarioItem argInventarioItem : listaInventario) {
				if(itemSelected == argInventarioItem){
					if(argInventarioItem == itemEscolha){
						ShowFacesMessage.warn("Falha!", "O item selecionado não pode ser excluído!");
					}else{
						argInventarioItem.setStatus(ArgStatusInventarioItemEnum.E);
						ShowFacesMessage.info("Sucesso!", "Leitura excluída com sucesso!");
					}
					
					break;
				}
			}
		}else{
			ShowFacesMessage.warn("Falha!", "Favor, selecionar o item para excluir!");
		}
		
	}
	
	@Transactional
	public void concluirArmazem(){
		
		ArgInventario inventario = argInventario;
		try {
		
			if(inventario.getTipoInventario() == ArgTipoInventarioEnum.B){
				
				concluirBalancoArmazem(inventario);
				
			}else{
				
				concluirInventarioArmazem(inventario);
				
			}
			inventario.setStatus(ArgStatusInventarioEnum.C);
			inventarioService.saveSimples(inventario);
			ShowFacesMessage.info("Sucesso!", inventario.getTipoInventario().getDescricao()+ " conclu�do com sucesso!");
		} catch (Exception e) {
			e.printStackTrace();
			ShowFacesMessage.warn("Falha!", "N�o foi poss�vel concluir o "+inventario.getTipoInventario().getDescricao());
			// TODO: handle exception
		}
		
	}
	
	public void concluirInventarioArmazem(ArgInventario inventario) throws NullPointerException{
		
		List<Object[]> resultados = new ArrayList<>();
		
		for (ArgInventarioItem argInventarioItem : inventario.getItemInventario()) {
			if(argInventarioItem.getStatus() == ArgStatusInventarioItemEnum.C){
				for (ArgInventarioItemIis iisItem : argInventarioItem.getItemInventarioIis()) {
					
					String identificadorIIS = iisItem.getIis().substring(0, 1);
					IisSubItem subIIS = null;
					IisItem IIS = null;
					
					if(identificadorIIS.equals("0")){
						subIIS = iisSubItemService.findByIis(iisItem.getIis());
					}else{
						IIS = iisItemService.findByIis(iisItem.getIis());
					}
					
					if(IIS == null && subIIS == null){
						Object[] resultado = webService.ConsultaItensCaixaIIS(iisItem.getIis());
						if(resultado == null){
							ShowFacesMessage.warn("Falha!", "Web Service indispon�vel, favor tente mais tarde!");
							throw new NullPointerException();
						}
						
						resultados.add(resultado);
					}
					
					for (Object[] objects : resultados) {
						if(objects.length == 0) {
							ShowFacesMessage.warn("Falha!", "Produtos n�o encontrados no Web Service");
							throw new NullPointerException();
						}
					}
				}
				break;
			}
		}
				
		/*PEGAR ARVORE RECUPERADO PELO WS E MONTAR IIS*/
		List<IisItem> listaIIs = new ArrayList<>();
		List<IisSubItem> listaSubIIs = new ArrayList<>();
		for (Object[] objects : resultados) {
			
			for (Object sequencia : objects) {
				
				LSTRECCONSULTACAIXA ITENS = (LSTRECCONSULTACAIXA) sequencia;
				
				for (RECCONSULTACAIXA item : ITENS.getITENS()) {
					String identificadorIIS = item.getIIS().substring(0, 1);
					
					 item.getCODEXTERNO().replaceAll("\n", "");
					 item.getCODEXTERNO().replace(System.getProperty("line.separator"), "");
					 item.getCODEXTERNO().replaceAll("\\n", "");
					 item.getCODEXTERNO().replaceAll("\\n$", "");
					 item.getCODEXTERNO().replaceAll("(\n|\r)+","");
					 item.getCODEXTERNO().replaceAll("-", "");
					 item.getCODEXTERNO().replaceAll("�", "C");
					 item.getCODEXTERNO().replaceAll("�", "");
					 Utils.retiraAcentos(item.getCODEXTERNO());
					
					String trim = item.getCODEXTERNO().trim();
					Produto produto = produtoService.findByCodigo(trim);
					if(!identificadorIIS.equals("0")){
						
						IisItem IIS = new IisItem();
						IIS.setMontado(true);
						IIS.setProduto(produto);
						
						if(IIS.getProduto() == null){
							ShowFacesMessage.warn("Falha!", "Não foi possível encontrar o produto "+trim);
							throw new NullPointerException();
						}
						
						IIS.setIis(item.getIIS());
						IIS.setIISPAI(item.getIISPAI());
						IIS.setNumLote(item.getLOTE());
						try {
							
							DateFormat dt = new SimpleDateFormat("yyyy-MM-dd", new Locale("pt", "br"));
							Date d = dt.parse(item.getVENCIMENTO());
							IIS.setDataVencimento(d);
						} catch (Exception e) {
							// TODO Auto-generated catch block
						}
						
						for (IisItem iisItem : listaIIs) {
							if(iisItem.getIis().equals(IIS.getIISPAI())){
								IIS.setNivelPai(iisItem);
								boolean caixaAberta = item.getSTATUSIISPAI().equals("A") ? true : false; 
								IIS.setCaixaAberta(caixaAberta);
								iisItem.setCaixaAberta(caixaAberta);
								break;
							}
						}
						
						listaIIs.add(IIS);
						
					}else{
						IisSubItem IIS = new IisSubItem();
						IIS.setMontado(true);
						IIS.setProduto(produto);
						
						if(IIS.getProduto() == null){
							ShowFacesMessage.warn("Falha!", "Não foi possível encontrar o produto "+trim);
							throw new NullPointerException();
						}
						
						IIS.setIis(item.getIIS());
						IIS.setIISPAI(item.getIISPAI());
						IIS.setNumeroLote(item.getLOTE());
						IIS.setIsCaixaAberta(false);
						try {
							DateFormat dt = new SimpleDateFormat("yyyy-MM-dd", new Locale("pt", "br"));
							Date d = dt.parse(item.getVENCIMENTO());
							IIS.setDataVencimento(d);
						} catch (Exception e) {
							// TODO Auto-generated catch block
						}
						
						for (IisItem iisItem : listaIIs) {
							if(iisItem.getIis().equals(IIS.getIISPAI())){
								IIS.setIisItem(iisItem);
								boolean caixaAberta = item.getSTATUSIISPAI().equals("A") ? true : false; 
								IIS.setIsCaixaAberta(caixaAberta);
								iisItem.setCaixaAberta(caixaAberta);
								break;
							}
						}
						
						listaSubIIs.add(IIS);
					}
				}
				
			}
		}
		
		/*SALVAR IIS MONTADO*/
		for (IisItem iisItem : listaIIs) {
			
			iisItemService.saveSimples(iisItem);
			
		}
		
		
		List<IisSubItem> unidades = new ArrayList<>();
		for (IisSubItem iisSubItem : listaSubIIs) {
			
			iisSubItemService.saveSimples(iisSubItem);
			
			/*RECUPERAR IIS TRATADO COMO UNIDADE PARA GERAR MOVIMENTACAO*/
			if(iisSubItem.getIisItem() == null){
				unidades.add(iisSubItem);
			}
			
		}
		if(!unidades.isEmpty()) {
			ajustarIisPaiolEmovimentacao(listaIIs, unidades, inventario, TipoOrdemEnum.IA);
		}else{
			ajustarIisPaiolEmovimentacao(listaIIs, listaSubIIs, inventario, TipoOrdemEnum.IA);
		}
	}
	
	
	public void concluirBalancoArmazem(ArgInventario inventario){

		List<IisItem> iisItemLista = new ArrayList<>();
		List<IisSubItem> iisSubItemLista = new ArrayList<>();
		
		String iisColetado="(";
		String iisItemColetado="(";
	
		for (ArgInventarioItem argInventarioItem : inventario.getItemInventario()) {
			if(argInventarioItem.getStatus() == ArgStatusInventarioItemEnum.C){
				for (ArgInventarioItemIis iisItem : argInventarioItem.getItemInventarioIis()) {
					if(iisItem.getIis().substring(0, 1).equals("0")){
						iisItemColetado += iisItemColetado.equals("(") ? "'"+iisItem.getIis()+"'" : ",'"+iisItem.getIis()+"'";
					}else{
						iisColetado += iisColetado.equals("(") ? "'"+iisItem.getIis()+"'" : ",'"+iisItem.getIis()+"'";
					}
				}
			}
		}
		
		iisColetado+=")";
		iisItemColetado+=")";
		
		if(!iisColetado.equals("()")){
			iisItemLista = iisItemService.listarTodosIis(iisColetado);
		}
		
		if(!iisItemColetado.equals("()")){
				iisSubItemLista = iisSubItemService.listarTodosIis(iisItemColetado);
		}
		
		ajustarIisPaiolEmovimentacao(iisItemLista, iisSubItemLista, inventario, TipoOrdemEnum.BA);
	}
	
	public void ajustarIisPaiolEmovimentacao(List<IisItem> iisItemLista, List<IisSubItem> iisSubItemLista, ArgInventario inventario, TipoOrdemEnum tipo){
		
		
		Paiol paiolDestino = null;
		if(tipo == TipoOrdemEnum.BA){
			paiolDestino = paiolService.localizarPaiolDivergenciaPorPlanta(inventario.getPaiol().getPlanta());
		}else{
			paiolDestino = inventario.getPaiol();
		}
		
		List<MovimentacaoIis> movimentacaoIis = new ArrayList<>();
		
		for (IisSubItem iisSubItem : iisSubItemLista) {
			MovimentacaoItens movimentacaoItens = new MovimentacaoItens();
			MovimentacaoSubIis moviSubIis = new MovimentacaoSubIis();
			MovimentacaoIis moviIis = new MovimentacaoIis();
			
			if(iisSubItem.getPaiol() != null && iisSubItem.getPaiol().equals(inventario.getPaiol()) && tipo == TipoOrdemEnum.BA){
				iisSubItem.setPaiol(inventario.getPaiol());
			}else{
				/*CREATE HEADER MOVIMENTACAO PARA INVENTARIO */
				movimentacaoItens.setDataMovimento(new Date());
				movimentacaoItens.setIdOrdem(inventario.getId());
				movimentacaoItens.setPaiolDestino(paiolDestino);
				movimentacaoItens.setPaiolOrigem(iisSubItem.getPaiol());
				movimentacaoItens.setProduto(iisSubItem.getProduto());
				movimentacaoItens.setUnidadeNegocioDestino(inventario.getUnidadeNegocio());
				
				if(tipo == TipoOrdemEnum.BA){
					movimentacaoItens.setUnidadeNegocioOrigem(inventario.getUnidadeNegocio());
				}
				movimentacaoItens.setTipoOrdemEnum(tipo);
				
				iisSubItem.setPaiol(paiolDestino);
				movimentacaoItens.setQuantidade(1L);
				moviSubIis.setIisItem(iisSubItem);
				moviSubIis.setMovimentacaoIis(moviIis);
				moviIis.setMovimentacaoItens(movimentacaoItens);
				
				movimentacaoItensService.save(movimentacaoItens);
				movimentacaoIisService.saveSimples(moviIis);
				movimentacaoSubIisService.saveSimples(moviSubIis);
			}
			
			
			iisSubItemService.saveSimples(iisSubItem);
			
			if(movimentacaoItens.getProduto() != null && movimentacaoItens.getProduto().getId() != null)
				movimentacaoService.processarSaldoItens(new Date(), movimentacaoItens.getProduto().getId(), tipo);
		}
		
		for (IisItem iisItem : iisItemLista) {
			if(iisItem.getPaiol() != null && iisItem.getPaiol().equals(inventario.getPaiol()) && tipo == TipoOrdemEnum.BA){
				iisItem.setPaiol(inventario.getPaiol());
			}else{
				
				movimentacaoIis = localizarPorNivelPai(iisItem, inventario, paiolDestino, tipo);
				iisItem.setPaiol(paiolDestino);
				
				for (MovimentacaoIis movimentacaoIis2 : movimentacaoIis) {
					movimentacaoItensService.save(movimentacaoIis2.getMovimentacaoItens());
					movimentacaoIisService.saveSimples(movimentacaoIis2);
					
					if(movimentacaoIis2.getMovimentacaoItens().getProduto() != null && movimentacaoIis2.getMovimentacaoItens().getProduto().getId() != null)
						movimentacaoService.processarSaldoItens(new Date(), movimentacaoIis2.getMovimentacaoItens().getProduto().getId(), tipo);
					
					for (MovimentacaoSubIis moviSub : movimentacaoIis2.getMovSubIis()) {
						moviSub.getIisItem().setPaiol(paiolDestino);
						iisSubItemService.saveSimples(moviSub.getIisItem());
						movimentacaoSubIisService.saveSimples(moviSub);
					}
					
				}
				
			}
			
			iisItemService.saveSimples(iisItem);
		}
		
	}
	
	public List<MovimentacaoIis> localizarPorNivelPai(IisItem iisItem, ArgInventario inventario, Paiol paiolDestino, TipoOrdemEnum tipo){
		List<MovimentacaoIis> moviIis = new ArrayList<>();
		List<IisSubItem> subItem = new ArrayList<>();
		
		List<IisItem> embalagens = iisItemService.listarIisInferiores(""+iisItem.getId()+"");
		
		for (IisItem iisItem2 : embalagens) {
			MovimentacaoIis subIis = new MovimentacaoIis();
			subIis.setIisItem(iisItem2);
			iisItem2.setPaiol(paiolDestino);
			moviIis.add(subIis);
			localizarPorNivelPai(iisItem2, inventario, paiolDestino, tipo);
			
		}
		
		MovimentacaoIis movIisPai = new MovimentacaoIis();
		movIisPai.setIisItem(iisItem);
		boolean encontrou = false;
		
		for (MovimentacaoIis movs : moviIis) {
			if(iisItem.equals(movs.getIisItem().getNivelPai())){
				movs.setMovimentacaoIisPai(movIisPai);
				encontrou = true;
			}
		}
		if(encontrou){
			moviIis.add(movIisPai);
		}
		
		MovimentacaoItens movimentacaoItens = new MovimentacaoItens();
		
		movimentacaoItens.setDataMovimento(new Date());
		movimentacaoItens.setIdOrdem(inventario.getId());
		movimentacaoItens.setPaiolDestino(paiolDestino);
		movimentacaoItens.setPaiolOrigem(iisItem.getPaiol());
		movimentacaoItens.setProduto(iisItem.getProduto());
		movimentacaoItens.setUnidadeNegocioDestino(inventario.getUnidadeNegocio());
		if(tipo == TipoOrdemEnum.BA){
			movimentacaoItens.setUnidadeNegocioOrigem(inventario.getUnidadeNegocio());
		}
		movimentacaoItens.setTipoOrdemEnum(tipo);
		
		for (MovimentacaoIis iis : moviIis) {
			List<MovimentacaoSubIis> subIis = new ArrayList<>();
			subItem = iisSubItemService.findByIisItem(iis.getIisItem());
			for (IisSubItem iisSubItem : subItem) {
				MovimentacaoSubIis subIisTemp = new MovimentacaoSubIis();
				movimentacaoItens.setQuantidade(movimentacaoItens.getQuantidade()+1);
				subIisTemp.setIisItem(iisSubItem);
				iisSubItem.setPaiol(paiolDestino);
				subIisTemp.setMovimentacaoIis(iis);
				subIis.add(subIisTemp);
			}
			iis.setMovSubIis(subIis);
			iis.setMovimentacaoItens(movimentacaoItens);
		}
		
		return moviIis;
	}
	
	public UploadedFile getFileUploaded() {
		return fileUploaded;
	}

	public void setFileUploaded(UploadedFile fileUploaded) {
		this.fileUploaded = fileUploaded;
	}

	public List<ArgInventarioItem> getListaInventario() {
		return listaInventario;
	}

	public void setListaInventario(List<ArgInventarioItem> listaInventario) {
		this.listaInventario = listaInventario;
	}

	public ArgInventarioItem getItemSelected() {
		return itemSelected;
	}

	public void setItemSelected(ArgInventarioItem itemSelected) {
		this.itemSelected = itemSelected;
	}

	public ArgInventarioItem getItemEscolha() {
		return itemEscolha;
	}

	public void setItemEscolha(ArgInventarioItem itemEscolha) {
		this.itemEscolha = itemEscolha;
	}

	public boolean isConsultar() {
		return consultar;
	}

	public void setConsultar(boolean consultar) {
		this.consultar = consultar;
	}

	public ArgInventario getArgInventario() {
		return argInventario;
	}

	public void setArgInventario(ArgInventario argInventario) {
		this.argInventario = argInventario;
	}
	

}
