package com.rasz.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.rasz.controller.ArgOemDevolucaoController;
import com.rasz.controller.MainController;
import com.rasz.domain.ArgHeaderItemOrdem;
import com.rasz.domain.ArgOemDevolucao;
import com.rasz.domain.Cliente;
import com.rasz.domain.FlowRuleEnum;
import com.rasz.domain.OrdemRecebimento;
import com.rasz.domain.Planta;
import com.rasz.domain.Produto;
import com.rasz.domain.StatusOrdemCarregamentoEnum;
import com.rasz.domain.UnidadeNegocio;
import com.rasz.domain.Usuario;
import com.rasz.repository.ArgOemDevolucaoRepository;
import com.rasz.repository.PlantaRepository;
import com.rasz.service.ClienteService;
import com.rasz.service.ItemProdutoOrdemCarregamentoService;
import com.rasz.service.MovimentacaoIisService;
import com.rasz.service.MovimentacaoItensService;
import com.rasz.service.ProdutoService;
import com.rasz.service.UnidadeNegocioService;
import com.rasz.utils.FlowUtilsFactory;

public class ArgOemDevolucaoCriteria extends AbstractCriteria<ArgOemDevolucao> {

	private static final long serialVersionUID = 1L;

	@Autowired
	private transient ArgOemDevolucaoRepository ocRepository;
	@Autowired
	private transient ProdutoService produtoService;
	@Autowired
	private transient ItemProdutoOrdemCarregamentoService itemProdutoService;
	@Autowired
	private transient ArgOemDevolucaoController ordemCarregamentoController;
	@Autowired
	private transient MovimentacaoItensService movimentacaoItensService;
	@Autowired
	private transient MovimentacaoIisService movimentacaoIisService;
	@Autowired
	private transient UnidadeNegocioService unidadeNegocioService;
	@Autowired
	private transient ClienteService clienteService;
	@Autowired
	private transient MainController mainController;
	@Autowired
	private transient PlantaRepository plantaRepository;
	
	private ArgOemDevolucao ocTemp;
	private List<StatusOrdemCarregamentoEnum> status;
	private Iterable<UnidadeNegocio> completeUn;
	private List<Planta> listPlanta = new ArrayList<>();
	private List<Produto> listaProduto = new ArrayList<>();
	private String idFiltro;
	private StatusOrdemCarregamentoEnum statusFiltro;
	private Cliente cliente = new Cliente();
	private Planta plantaFiltro = new Planta();
	private Cliente clienteDev = new Cliente();
	private Planta plantaFiltroDev = new Planta();
	private String vendaDevolucao = null;
	
	private String nRemito;
	
	private String transportista;
	private String patente;
	
	@PostConstruct
	public void inicializarAutoComplete(){
		completeUn =  unidadeNegocioService.findAll();
	}
	
	public ArgHeaderItemOrdem createHeader(){
		ArgHeaderItemOrdem header = new ArgHeaderItemOrdem();
		return header;
	}
	
	public void limparFiltro(){
		idFiltro = null;
		cliente = null;
		statusFiltro = null;
	}
	
	public ArgOemDevolucao createOC() {
		Boolean isTransitionPageFromOR = (Boolean) FlowUtilsFactory.currentInstance(
				FlowRuleEnum.CONVERSATION_SCOPE).get("isTransitionPageFromOR");
		
		//Cross-Docking:
		if(isTransitionPageFromOR != null && isTransitionPageFromOR){
			OrdemRecebimento ordemRecebimento =  (OrdemRecebimento) FlowUtilsFactory.currentInstance(
					FlowRuleEnum.CONVERSATION_SCOPE).get("ordemRecebimentoTransition");
			
			ArgOemDevolucao oc = new ArgOemDevolucao();
			oc.setUsuario(ordemRecebimento.getUsuario());
			oc.setStatus(StatusOrdemCarregamentoEnum.CRIADA);
			oc.setData(new Date());
			//ordemCarregamentoController.setOrdemCarregamento(oc);
			
			return oc;
		}
		
		ocTemp = new ArgOemDevolucao();
		Usuario usr = new Usuario();
		ocTemp.setUsuario(usr);

		if (status == null)
			status = new ArrayList<StatusOrdemCarregamentoEnum>();
		else
			status.clear();
		
		for (StatusOrdemCarregamentoEnum s : StatusOrdemCarregamentoEnum.values()) {
			if (!s.getCanonicalValue().toUpperCase().contains("TODOS"))
				status.add(s);
		}

		return ocTemp;
	}
	
	@Override
	public Page<ArgOemDevolucao> load(Pageable pageable) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			if(idFiltro != null && !idFiltro.equals("")){
				idFiltro = idFiltro.replace("_", "");
			}
			
			return ocRepository.filter(idFiltro, statusFiltro, cliente, nRemito, pageable);
		}
		return null;
	}
	
	public void setTodosStatus(){
		statusFiltro = null;
	}

	@Override
	public Object getRowKey(ArgOemDevolucao ordem) {
		return ordem != null ? ordem.getId() : null;
	}

	@Override
	public ArgOemDevolucao getRowData(String rowKey) {
		List<ArgOemDevolucao> list = (List<ArgOemDevolucao>) getWrappedData();

		for (ArgOemDevolucao p : list) {
			if (p.getId().toString().equals(rowKey)) {
				return p;
			}
		}

		return null;
	}

	public List<Produto> complete(String busca) {
		List<Produto> resultados = new ArrayList<Produto>();
		//ocTemp = ordemCarregamentoController.getOrdemCarregamento();

		for (Produto produto : listaProduto) {
			if (produto.getNome().trim().toLowerCase().contains(busca.trim().toLowerCase())) {
				resultados.add(produto);
			}
		}
		return resultados;
	}
	
	public List<Cliente> completeCliente(String busca) {
		List<Cliente> resultados = new ArrayList<Cliente>();
		//ocTemp = ordemCarregamentoController.getOrdemCarregamento();

		for (Cliente cliente : clienteService.findAll()) {
			if (cliente.getNome().trim().toLowerCase().contains(busca.trim().toLowerCase())) {
				resultados.add(cliente);
			}
		}
		return resultados;
	}
	
	public void listarPlantas(){
		listPlanta = plantaRepository.listarTodos();
	}

	public void listarProdutos(){
		listaProduto = produtoService.findAll();
	}
	
	public List<String> completePlanta(String busca) {
		List<String> resultado = new ArrayList<>();
		for (Planta planta : listPlanta) {
			if (planta.getId().toString().concat(" - ").concat(planta.getNome().toLowerCase()).contains(busca.toLowerCase())) {
    			resultado.add(planta.toString());
			}
		}
		return resultado;
	}
	
	public ArgOemDevolucao getOcTemp() {
		return ocTemp;
	}

	public void setOcTemp(ArgOemDevolucao ocTemp) {
		this.ocTemp = ocTemp;
	}

	public List<StatusOrdemCarregamentoEnum> getStatus() {
		return status;
	}

	public void setStatus(List<StatusOrdemCarregamentoEnum> status) {
		this.status = status;
	}

	public ItemProdutoOrdemCarregamentoService getItemProdutoService() {
		return itemProdutoService;
	}

	public void setItemProdutoService(ItemProdutoOrdemCarregamentoService itemProdutoService) {
		this.itemProdutoService = itemProdutoService;
	}

	public String getIdFiltro() {
		return idFiltro;
	}

	public void setIdFiltro(String idFiltro) {
		this.idFiltro = idFiltro;
	}

	public StatusOrdemCarregamentoEnum getStatusFiltro() {
		return statusFiltro;
	}

	public void setStatusFiltro(StatusOrdemCarregamentoEnum statusFiltro) {
		this.statusFiltro = statusFiltro;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Planta getPlantaFiltro() {
		return plantaFiltro;
	}

	public void setPlantaFiltro(Planta plantaFiltro) {
		this.plantaFiltro = plantaFiltro;
	}

	public String getnRemito() {
		return nRemito;
	}

	public void setnRemito(String nRemito) {
		this.nRemito = nRemito;
	}

	public String getTransportista() {
		return transportista;
	}

	public void setTransportista(String transportista) {
		this.transportista = transportista;
	}

	public String getPatente() {
		return patente;
	}

	public Cliente getClienteDev() {
		return clienteDev;
	}

	public void setClienteDev(Cliente clienteDev) {
		this.clienteDev = clienteDev;
	}

	public Planta getPlantaFiltroDev() {
		return plantaFiltroDev;
	}

	public void setPlantaFiltroDev(Planta plantaFiltroDev) {
		this.plantaFiltroDev = plantaFiltroDev;
	}

	public void setPatente(String patente) {
		this.patente = patente;
	}
	
	public String getVendaDevolucao() {
		return vendaDevolucao;
	}

	public void setVendaDevolucao(String vendaDevolucao) {
		this.vendaDevolucao = vendaDevolucao;
	}

	public void limparCamposPopUp() {
		transportista = new String();
		patente = new String();
	}
}
