package com.rasz.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;

import org.apache.commons.io.IOUtils;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.webflow.engine.RequestControlContext;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.RequestContextHolder;
import org.xml.sax.SAXException;

import com.rasz.controller.importacao.ItemType;
import com.rasz.controller.importacao.ItensType;
import com.rasz.controller.importacao.ProdutoType;
import com.rasz.controller.importacao.RecebimentoType;
import com.rasz.controller.importacao.SubItemType;
import com.rasz.domain.FlowRuleEnum;
import com.rasz.domain.HeaderItemOrdemRecebimento;
import com.rasz.domain.IisItem;
import com.rasz.domain.ItemOrdemRecebimento;
import com.rasz.domain.OrdemRecebimento;
import com.rasz.domain.Paiol;
import com.rasz.domain.Produto;
import com.rasz.domain.StatusOrdemRecebimentoEnum;
import com.rasz.domain.SubItemOrdemRecebimento;
import com.rasz.domain.UsuarioColetor;
import com.rasz.service.IisItemService;
import com.rasz.service.IisSimpleValidator;
import com.rasz.service.ItemOrdemRecebimentoService;
import com.rasz.service.MovimentacaoSaldoPaiolService;
import com.rasz.service.OrdemRecebimentoService;
import com.rasz.service.PaiolService;
import com.rasz.service.ProdutoService;
import com.rasz.service.SubItemOrdemRecebimentoService;
import com.rasz.utils.FlowUtilsFactory;
import com.rasz.utils.ShowFacesMessage;
import com.rasz.utils.Utils;

@Controller
public class OrdemRecebimentoController {

	/*
	 * S�o usadas para cadastrar/visualizar na ordem de recebimento:
	 */
	private OrdemRecebimento ordemRecebimento;
	private List<ItemOrdemRecebimento> itensOrdemRecebimentoTemp;
	private List<SubItemOrdemRecebimento> subItemParaCadastrar;

	private List<ItemOrdemRecebimento> itensProdutoView;
	private StreamedContent xmlContent;
	private int activeIndex;

	private List<OrdemRecebimento> ordensRecebimentoSelecionadoExportacao = new ArrayList<>();

	private IisItem iisItemSelecionadoGrid;
	
	private Integer incremento = new Integer(0);
	
	private ItemOrdemRecebimento itemOrdemRecebimento;
	
	@Resource(name = "iisValidatorOR")
	IisSimpleValidator<OrdemRecebimento> validator;

	//usado
	@Autowired private transient SubItemOrdemRecebimentoService subItemOrdemRecebimentoService;
	//usado
	@Autowired PaiolService paiolService;
	
	@Autowired
	MainController mainController;
	@Autowired
	OrdemRecebimentoService ordemRecebimentoService;
	@Autowired
	ItemOrdemRecebimentoService itemOrdemRecebimentoService;
	@Autowired
	ProdutoService produtoService;
	
	@Autowired
	IisItemService iisItemService;
	@Autowired 
	MovimentacaoSaldoPaiolService movimentacaoSaldoPaiolService;

	public List<SubItemOrdemRecebimento> createItensLote() {
		List<SubItemOrdemRecebimento> t = new ArrayList<>();
		SubItemOrdemRecebimento iLote = new SubItemOrdemRecebimento();
		t.add(iLote);
		return t;
	}

	
	public void incrementar(){
		++this.incremento;
	}
	
	public void definirParametrosFlow(){
		Boolean isNovoRegistro = (Boolean)FlowUtilsFactory.
				currentInstance(FlowRuleEnum.FLOW_SCOPE).get("isNovoRegistro");
		
		Boolean isTransitionPage = (Boolean)FlowUtilsFactory.
				currentInstance(FlowRuleEnum.CONVERSATION_SCOPE).get("isTransitionPage");
		
		if(( isTransitionPage == null || !isTransitionPage) && isNovoRegistro && ordemRecebimento != null)
			ordemRecebimento.setStatus(StatusOrdemRecebimentoEnum.CRIADA);
		
		if(isTransitionPage != null && isTransitionPage){
			FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).put("isImportar", false);
			FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).put("isConsultar", true);
			FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).put("isNovoRegistro", false);
			FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).put("isEditar", false);
			
			this.ordemRecebimento = (OrdemRecebimento) FlowUtilsFactory.currentInstance(FlowRuleEnum.CONVERSATION_SCOPE).get("ordem");
		}
	}

	public void remove(SubItemOrdemRecebimento item, List<SubItemOrdemRecebimento> itens) {
		if (item != null && itens != null && itens.size() > 0) {
			itens.remove(item);
		}
	}
	
	public boolean validateCNPJ(String cnpj) {

		try {
			if (ordemRecebimento != null && cnpj != null) {
				cnpj = Utils.retirarMascaras(cnpj);
				if (!cnpj.trim().isEmpty())
					if (!Utils.validaCNPJ(cnpj)) {
						ShowFacesMessage.warn("C.N.P.J inv�lido", "Verifique se o C.N.P.J digitado � valido.");
						clearObjectFields(ordemRecebimento);
						return false;
					}
			}
		} catch (NullPointerException npe) {
			ShowFacesMessage.warn("C.N.P.J inv�lido", "Verifique se o C.N.P.J digitado � valido.");
			clearObjectFields(ordemRecebimento);
			return false;
		}

		return true;
	}


	private void clearObjectFields(OrdemRecebimento ordemRecebimento) {
		if (ordemRecebimento == null)
			return;

		ordemRecebimento.setCnpjComprador(new String());
		ordemRecebimento.setCnpjVendedor(new String());
	}
 
	/*
	 * Realiza a convers�o de InputStream (vindo do UploadedFile do Primefaces)
	 * para um objeto do tipo File (para realizar a leitura do arquivo).
	 */
	public File inputStreamToFile(InputStream is) throws IOException {
		File tempFile = File.createTempFile("xml", ".xml");
		tempFile.deleteOnExit();

		try (FileOutputStream out = new FileOutputStream(tempFile)) {
			IOUtils.copy(is, out);
		}

		return tempFile;
	}

	public Object readXML(File file) throws JAXBException, SAXException, IOException, UnmarshalException {
		InputStream stream = null;
		try {
			stream = new FileInputStream(file);
			JAXBContext jc = JAXBContext.newInstance(RecebimentoType.class);
			Unmarshaller u = jc.createUnmarshaller();
			Object object = u.unmarshal(stream);

			return object;
		} finally {
			if (stream != null) {
				stream.close();
			}
		}
	}

	
	//Genérico
	public String formatarData(Date data) throws Exception {

		if (data == null)
			return new String();

		return Utils.formataDataString(data);
	}

	//Generico
	public String formatarData(OrdemRecebimento ordemRecebimento, Boolean isNovoRegistro) {
		if (isNovoRegistro == null || isNovoRegistro.equals(false)) {
			if (ordemRecebimento != null && ordemRecebimento.getData() != null)
				return Utils.formataDataString(ordemRecebimento.getData());
		} else {
			ordemRecebimento.setData(new Date());
			ordemRecebimento.setUsuario(mainController.getUsuarioLogado());
			return Utils.formataDataString(ordemRecebimento.getData());
		}

		return null;
	}

	public boolean isConfirmarButtonDisabled() {
		return ordemRecebimento == null;
	}
	
	public String returnResultsItensTitle() {
		String msg = "Nenhum item encontrado para o produto selecionado";

		return msg;
	}

	public String returnResultsSubItensTitle() {
		String msg = "Nenhum sub-item para o produto foi encontrado";

		return msg;
	}

	

	private long compareYearFromActualDate(Date date) {
		Calendar dataForCompare = Calendar.getInstance();
		dataForCompare.setTime(date);
		Calendar hoje = Calendar.getInstance();

		int diff = hoje.get(Calendar.YEAR) - dataForCompare.get(Calendar.YEAR);

		if (hoje.get(Calendar.MONTH) < dataForCompare.get(Calendar.MONTH))
			diff--;
		else if (hoje.get(Calendar.MONTH) == dataForCompare.get(Calendar.MONTH)
				&& hoje.get(Calendar.DAY_OF_MONTH) < dataForCompare.get(Calendar.DAY_OF_MONTH))
			diff--;

		return diff;
	}
	
	public HeaderItemOrdemRecebimento inserirHeader(OrdemRecebimento ordem, Produto produto,
			String quantidade, UsuarioColetor usuario, Date data) throws NullPointerException{
		
		/*
		 * TODO validar se o header n�o existe
		 */
		HeaderItemOrdemRecebimento header = new HeaderItemOrdemRecebimento();
		header.setOrdemRecebimento(ordem);
		header.setProduto(produto);
		header.setUsuario(usuario); //Usuario que fez coleta, como � importaÇÃO, ficar� em branco
		header.setDataColeta(data);
//			header.setQuantidadeTotalCarregamento(quantidadeTotal.get(produto.getId().toString()).longValue())
		
		return header;
	}

	public IisItem consultarIisCaixa(String iis){
		return iisItemService.findByIis(iis);
	}
	
	/****** EXPORTAÇÃO ******/
	private String readObjectXML(OrdemRecebimento ordemRecebimento)
			throws JAXBException, DatatypeConfigurationException {
		StringWriter sw = new StringWriter();
		JAXBContext jc = JAXBContext.newInstance(RecebimentoType.class);

		RecebimentoType rec = new RecebimentoType();
		objectToXML(ordemRecebimento, rec);

		Marshaller ms = jc.createMarshaller();
		ms.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		ms.marshal(rec, sw);
		String xmlString = sw.toString();
		// System.out.println(xmlString);

		return xmlString;
	}

	public StreamedContent exportToDisk(OrdemRecebimento ordemRecebimento)
			throws JAXBException, DatatypeConfigurationException, IOException {
		this.ordemRecebimento = ordemRecebimento;
		String xmlObjectContent = readObjectXML(ordemRecebimento);
		InputStream stream = new ByteArrayInputStream(xmlObjectContent.getBytes());
		this.xmlContent = new DefaultStreamedContent(stream, "application/xml",
				"OR - " + ordemRecebimento.getNotaFiscal().replaceFirst("^0+(?!$)", "") + "/"
						+ ordemRecebimento.getSerieNotaFiscal().replaceFirst("^0+(?!$)", "") + " - Exported: "
						+ Utils.getAtualTimeToFiles() + ".xml");
		stream.close();

		return xmlContent;
	}

	private void objectToXML(OrdemRecebimento or, RecebimentoType rt) throws DatatypeConfigurationException {
//		rt.setCnpjDeComprador(or.getCnpjComprador());
//		rt.setCnpjDeVendedor(or.getCnpjVendedor());
		rt.setDataExportacao(Utils.dateToXmlGregorianCalendar(or.getData()));
//		rt.setGuiaDeTrafego(or.getGuiaTrafego());
//		rt.setNumeroNotaFiscal(or.getNotaFiscal());
//		rt.setOrderDeVendedor(or.getOrderDeVendedor());
//		rt.setSerieNotaFiscal(or.getSerieNotaFiscal());

		List<Produto> produtos = produtoService.findByOrdemRecebimentoNative(or.getId());
		List<ProdutoType> produtosType = new ArrayList<>();

		for (Produto produto : produtos) {
			ProdutoType produtoType = new ProdutoType();
//			produtoType.setCodigoPais(produto.getCodigoPais());

			if (produto.getMetros() != null)
//				produtoType.setComprimento(produto.getMetros().intValue());

//			produtoType.setCodigoProduto(Integer.parseInt(produto.getCodigo()));
//			produtoType.setCodigoProdutor(Integer.parseInt(produto.getCodigoFabricante()));
			produtoType.setCodigoR105(produto.getR105());
//			produtoType.setTipoDeEmbalagem(readEmbalagensObjectToXmlBean(produto));
			produtoType.setNome(produto.getNome());
			produtoType.setPesoBruto(produto.getPesoBruto());
			produtoType.setPesoLiquido(produto.getPesoLiquido());
			produtoType.setPesoNec(produto.getPesoNec());
			produtoType.setItens(readItensObjectToXmlBean(produto, or));
			produtosType.add(produtoType);
		}

//		rt.setProduto(produtosType);
	}

	private ItensType readItensObjectToXmlBean(Produto produto, OrdemRecebimento ordemRecebimento)
			throws DatatypeConfigurationException {
		List<ItemType> itens = new ArrayList<ItemType>();
		List<ItemOrdemRecebimento> itensProduto = itemOrdemRecebimentoService
				.buscarLotesExportacaoXml(ordemRecebimento, produto);
		ItensType itensType = new ItensType();

		for (ItemOrdemRecebimento itemProduto : itensProduto) {
			ItemType itemType = new ItemType();
			itemType.setIis(itemProduto.getIisItem().getIis());
			itemType.setLote(itemProduto.getLoteItemOrdemRecebimento().getNumeroLote());
//			itemType.setMetroFinal(itemProduto.getIisItem().getMetroInicial());
//			itemType.setMetroInicial(itemProduto.getIisItem().getMetroInicial());
			itemType.setProduzido(Utils.dateToXmlGregorianCalendar(itemProduto.getIisItem().getProduzido()));


			List<SubItemOrdemRecebimento> itensLote = subItemOrdemRecebimentoService.findByItem(itemProduto);
			
			List<SubItemType> itensLoteType = new ArrayList<>();

			for (SubItemOrdemRecebimento itemLote : itensLote) {
				SubItemType subItemTemp = new SubItemType();
				subItemTemp.setIis(itemLote.getIisSubItem().getIis());
				subItemTemp.setLote(itemLote.getIisSubItem().getIisItem().getNumeroLote());
//				subItemTemp.setMetroFinal(itemLote.getIisSubItem().getMetroFinal());
//				subItemTemp.setMetroInicial(itemLote.getIisSubItem().getMetroFinal());
				subItemTemp.setProduzido(Utils.dateToXmlGregorianCalendar(itemLote.getIisSubItem().getProduzido()));
				itensLoteType.add(subItemTemp);
			}

//			itemType.setItem(itensLoteType);
			itens.add(itemType);
		}

		itensType.setItem(itens);
		return itensType;
	}

//	private EmbalagemType readEmbalagensObjectToXmlBean(Produto p) {
//		List<EmbalagemNivelType> niveisEmbalagemType = new ArrayList<>();
//		for (Embalagem e : p.getComposicaoEmbalagem().getEmbalagens()) {
//			EmbalagemNivelType eNivel = new EmbalagemNivelType();
//			eNivel.setNivel(e.getNivel());
//			eNivel.setTipoEmbalagem(e.getTipoEmbalagem());
//			eNivel.setQuantidadeDeSubniveis(e.getSubNiveis());
//			niveisEmbalagemType.add(eNivel);
//		}
//
//		EmbalagemType embalagem = new EmbalagemType();
//		embalagem.setEmbalagem(niveisEmbalagemType);
//
//		return embalagem;
//	}
	
	/****** FIM - EXPORTAÇÃO ******/

	/****** IMPORTAÇÃO *********/
	public OrdemRecebimento insertOrdemRecebimentoToDomain(RecebimentoType rt) {
		OrdemRecebimento or = new OrdemRecebimento();
//		or.setCnpjComprador(rt.getCnpjDeComprador());
//		or.setCnpjVendedor(rt.getCnpjDeVendedor());
//		or.setGuiaTrafego(rt.getGuiaDeTrafego());
//		or.setNotaFiscal(rt.getNumeroNotaFiscal());
//		or.setOrderDeVendedor(rt.getOrderDeVendedor());
//		or.setSerieNotaFiscal(rt.getSerieNotaFiscal());
		or.setStatus(StatusOrdemRecebimentoEnum.IMPORTADA); // Verificar o
		or.setUsuario(mainController.getUsuarioLogado());
		or.setData(new Date());
		return or;

	}

	public Produto insertProdutoControllerToDomain(ProdutoType produtoType) {
		Produto produto = new Produto();
		produto.setCodigo(String.valueOf(produtoType.getCodigoProduto()));
//		produto.setCodigoFabricante(String.valueOf(produtoType.getCodigoProdutor()));
//		produto.setCodigoPais(produtoType.getCodigoPais());
		produto.setNome(produtoType.getNome());
		produto.setPesoBruto(produtoType.getPesoBruto());
		produto.setPesoLiquido(produtoType.getPesoLiquido());
		produto.setPesoNec(produtoType.getPesoNec());
		produto.setR105(produtoType.getCodigoR105());
		produto.setImportado(true);
		return produto;
	}

	public void executeSelectedItemDataTableList(SelectEvent selectEvent) {
		if(!Utils.isMobileDevice())
			return;
		
		ordemRecebimento = (OrdemRecebimento) selectEvent.getObject();
		RequestContext requestContext = RequestContextHolder.getRequestContext();
		RequestControlContext rec = (RequestControlContext) requestContext;
		rec.handleEvent(new Event(this, "consultar"));
	}

//	private List<Embalagem> insertEmbalagemControllerToDomain(EmbalagemType embalagemType) {
//		List<Embalagem> embalagens = new ArrayList<>();
//
//		for (EmbalagemNivelType embalagensDomainController : embalagemType.getEmbalagem()) {
//			Embalagem embalagemOC = new Embalagem();
//			embalagemOC.setNivel(embalagensDomainController.getNivel());
//			embalagemOC.setTipoEmbalagem(embalagensDomainController.getTipoEmbalagem());
//			embalagemOC.setSubNiveis(embalagensDomainController.getQuantidadeDeSubniveis());
//			embalagens.add(embalagemOC);
//		}
//
//		return embalagens;
//	}

	public String removeFirstZero(String str) {
		if (str == null || str.isEmpty())
			return new String();	

		return str.replaceFirst("^0+(?!$)", "");
	}
	

	public Date getMaxDateCalendar() {
		return new Date();
	}
	
	public void setUnidadeDoUsuario(){
		ordemRecebimento.setUnidadeNegocio(mainController.getUsuarioLogado().getUnidadeNegocio());
	}

	/****** !IMPORTAÇÃO *********/

	public List<ItemOrdemRecebimento> getItensProdutoView() {
		return itensProdutoView;
	}

	public void setItensProdutoView(List<ItemOrdemRecebimento> itensProdutoView) {
		this.itensProdutoView = itensProdutoView;
	}

	public StreamedContent getXmlContent() {
		return xmlContent;
	}

	public void setXmlContent(StreamedContent xmlContent) {
		this.xmlContent = xmlContent;
	}

	public int getActiveIndex() {
		return activeIndex;
	}

	public void setActiveIndex(int activeIndex) {
		this.activeIndex = activeIndex;
	}

	public List<SubItemOrdemRecebimento> getSubItemParaCadastrar() {
		return subItemParaCadastrar;
	}

	public void setSubItemParaCadastrar(List<SubItemOrdemRecebimento> subItemParaCadastrar) {
		this.subItemParaCadastrar = subItemParaCadastrar;
	}

	public OrdemRecebimento getOrdemRecebimento() {
		return ordemRecebimento;
	}

	public void setOrdemRecebimento(OrdemRecebimento ordemRecebimento) {
		this.ordemRecebimento = ordemRecebimento;
	}

	public List<ItemOrdemRecebimento> getItensOrdemRecebimentoTemp() {
		return itensOrdemRecebimentoTemp;
	}

	public void setItensOrdemRecebimentoTemp(List<ItemOrdemRecebimento> itensOrdemRecebimentoTemp) {
		this.itensOrdemRecebimentoTemp = itensOrdemRecebimentoTemp;
	}

	public List<OrdemRecebimento> getOrdensRecebimentoSelecionadoExportacao() {
		return ordensRecebimentoSelecionadoExportacao;
	}

	public void setOrdensRecebimentoSelecionadoExportacao(List<OrdemRecebimento> ordensRecebimentoSelecionadoExportacao) {
		this.ordensRecebimentoSelecionadoExportacao = ordensRecebimentoSelecionadoExportacao;
	}

	public IisItem getIisItemSelecionadoGrid() {
		return iisItemSelecionadoGrid;
	}

	public void setIisItemSelecionadoGrid(IisItem iisItemSelecionadoGrid) {
		this.iisItemSelecionadoGrid = iisItemSelecionadoGrid;
	}

	public Integer getIncremento() {
		return incremento;
	}

	public void setIncremento(Integer incremento) {
		this.incremento = incremento;
	}
	
	
	
	public Produto findByCodigo(String codigo) {
		return produtoService.findByCodigo(codigo);
	}
	
	public OrdemRecebimento findByNotaFiscalAndSerieNotaFiscal(String notaFiscal, String serieNotaFiscal) {
		return ordemRecebimentoService.findByNotaFiscalAndSerieNotaFiscal(notaFiscal, serieNotaFiscal);
	}
 
	public boolean isFileAlreadyImported(OrdemRecebimento or) {
		OrdemRecebimento ordemTemp = findByNotaFiscalAndSerieNotaFiscal(or.getNotaFiscal(),
				or.getSerieNotaFiscal());

		return ordemTemp != null && ordemTemp.getId() != null && !ordemTemp.getId().equals(0);
	}
	
	public Paiol findTop1ByVirtual(Boolean isVirtual) {
		return paiolService.findTop1ByVirtual(isVirtual);
	}

	public ItemOrdemRecebimento getItemOrdemRecebimento() {
		return itemOrdemRecebimento;
	}

	public void setItemOrdemRecebimento(ItemOrdemRecebimento itemOrdemRecebimento) {
		this.itemOrdemRecebimento = itemOrdemRecebimento;
	}
	
	/*public void popularDados(){
		ArgOrdemProducao ordemProducao = (ArgOrdemProducao) FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).get("scopeOrdemRecebimento");
		
		if(ordemProducao.getItemProdutoOrdemProducao() != null && !ordemProducao.getItemProdutoOrdemProducao().isEmpty()){
			ordensProducaoItemProduto = ordemProducao.getItemProdutoOrdemProducao();
			itemOrdemProducao = new ArgOrdemProducaoItemProduto();
		}else{
			ordensProducaoItemProduto = new ArrayList<>();
			itemOrdemProducao = new ArgOrdemProducaoItemProduto();
			itemOPSelected = null;
		}
	}*/
	
	
}