package com.rasz.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.rasz.domain.Coletor;
import com.rasz.domain.HeaderItemOrdemTransferencia;
import com.rasz.domain.IisItem;
import com.rasz.domain.IisSubItem;
import com.rasz.domain.ItemOrdemTransferencia;
import com.rasz.domain.LoteItemOrdemTransferencia;
import com.rasz.domain.MovimentacaoIis;
import com.rasz.domain.MovimentacaoItens;
import com.rasz.domain.MovimentacaoSaldoPaiol;
import com.rasz.domain.MovimentacaoSubIis;
import com.rasz.domain.OrdemTransferencia;
import com.rasz.domain.Paiol;
import com.rasz.domain.PaiolItemOrdemTransferencia;
import com.rasz.domain.Produto;
import com.rasz.domain.StatusOrdemTransferenciaEnum;
import com.rasz.domain.SubItemOrdemTransferencia;
import com.rasz.domain.TipoOrdemEnum;
import com.rasz.domain.UnidadeNegocio;
import com.rasz.domain.UsuarioColetor;
import com.rasz.repository.ItemLoteOrdemTransferenciaRepository;
import com.rasz.service.ColetorService;
import com.rasz.service.HeaderItemOrdemTransferenciaService;
import com.rasz.service.IisItemService;
import com.rasz.service.IisSubItemService;
import com.rasz.service.MovimentacaoItensService;
import com.rasz.service.MovimentacaoSaldoPaiolService;
import com.rasz.service.OrdemTransferenciaService;
import com.rasz.service.PaiolService;
import com.rasz.service.ProdutoService;
import com.rasz.service.UsuarioColetorService;
import com.rasz.utils.ShowFacesMessage;

@Controller
public class OrdemTransferenciaImportarColetor {
	
	private UploadedFile fileUploaded;
	
	@Autowired private HeaderItemOrdemTransferenciaService 	headerService;	
	@Autowired private OrdemTransferenciaService 	 ordemService;
	@Autowired private UsuarioColetorService 		 usuarioColetorService;
	@Autowired private ProdutoService 				 produtoService;
	@Autowired private ColetorService 				 coletorService;
	@Autowired private PaiolService 				 paiolService;
	@Autowired private IisItemService				 iisItemService;
	@Autowired private IisSubItemService			 iisSubItemService;
	@Autowired private MovimentacaoSaldoPaiolService movimentacaoSaldoPaiolService;
	@Autowired private ItemLoteOrdemTransferenciaRepository subItemRepository;
	@Autowired private MovimentacaoItensService            	movimentacaoItensService; 
	@PersistenceContext private EntityManager em;
	
	private Set<String> ordensId 		    = new HashSet<>();
	private Set<String> paiolsId 		    = new HashSet<>();
	private Set<String> usuariosId 		    = new HashSet<>();
	private Set<String> produtosId 		    = new HashSet<>();
	private Set<String> coletoresId			= new HashSet<>();
	
	private Set<OrdemTransferencia> ordens 	= new HashSet<>();
	private Set<Paiol> paiois 				= new HashSet<>();
	private Set<UsuarioColetor> usuarios 		= new HashSet<>();
	private Set<Coletor> coletores				= new HashSet<>();
	private Set<Produto> produtos 			= new HashSet<>();
	private Map<String, Long> quantidadeTotal = new Hashtable<>();
	
	private boolean isShowFacesMessageShow = false;
	
	public void listener(FileUploadEvent event) throws IOException {
		ordensId 		    = new HashSet<>();
		paiolsId 		    = new HashSet<>();
		usuariosId 		    = new HashSet<>();
		produtosId 		    = new HashSet<>();
		coletoresId 		= new HashSet<>();
		
		ordens 				= new HashSet<>();
		paiois 				= new HashSet<>();
		usuarios 			= new HashSet<>();
		coletores 			= new HashSet<>();
		produtos 			= new HashSet<>();
		quantidadeTotal 	= new Hashtable<>();
		
		this.fileUploaded = event.getFile();

		if (fileUploaded == null)
			return;
		
		try {
			InputStream input = fileUploaded.getInputstream();

			StringWriter writer = new StringWriter();
			IOUtils.copy(input, writer, "utf-8");
			String arquivo = writer.toString();

			iniciar(arquivo);
		} catch (NumberFormatException | ParseException | NullPointerException e) {
			e.printStackTrace();
			ShowFacesMessage.error("Erro na importa��o", e.getMessage());
			return;
		} catch(StringIndexOutOfBoundsException e){
			e.printStackTrace();
			ShowFacesMessage.error("Erro na importa��o", "Arquivo com o cabe�alho inv�lido.");
			return;
		}
		
		
		finalizarStatus();
		
		if(!isShowFacesMessageShow){
			ShowFacesMessage.info("Sucesso!", "Informa��es importadas para a O.T com sucesso!");
			isShowFacesMessageShow = true;
		}
	}
	
	private void iniciar(String conteudo) throws NullPointerException, NumberFormatException, ParseException, StringIndexOutOfBoundsException{
		String mainTag = "<COLETOR_OT_IIS>";
		int init = conteudo.indexOf(mainTag) + mainTag.length() ;
		int end  = conteudo.indexOf("</COLETOR_OT_IIS>");
		String split[] = conteudo.substring(init, end).split("\n");

		popularHashIds(split);
		
		popularTodosObjetos();
		
		unirOrdem(split);
		
		
	}
	
	private void unirOrdem(String split[]) throws ParseException{
		//atualizar saldo
		//atualizar movimentacao_iis movimentacao_sub_iis
		Map<IisItem, UnidadeNegocio> iisParaAlterar = new Hashtable<>();
		Map<IisItem, Paiol> 			iisParaAlterarPaiolDestino = new Hashtable<>();
		List<IisItem> caixasAbertas = new ArrayList<>();
		
		for(String s : split){
			if(s.trim().isEmpty()) continue;
			
			StringTokenizer st = new StringTokenizer(s, "|");
			
			//ID_OT|ID_USUARIO|ID_COLETOR|ID_PRODUTO|QUANTIDADE|ID_PAIOL_ORIGEM|ID_PAIOL_DESTINO|DATA_COLETA|HORA_COLETA|IIS|
			while(st.hasMoreTokens()){
				String idOrdem 		    = st.nextToken(); //ID-O.T
				String idUsuario 	    = st.nextToken(); //ID_USUARIO
				String idColetor 	    = st.nextToken(); //ID_COLETOR
				String idProduto 	    = st.nextToken(); //ID_PRODUTO
				String quantidade 		= st.nextToken(); //QUANTIDADE
				String idPaiolOrigem   	= st.nextToken(); //ID PAIOL ORIGEM
				String idPaiolDestino  	= st.nextToken(); //ID_PAIOL DESTINO
				String dataColeta 	   	= st.nextToken(); //DATA_COLETA
				String horaColeta 	   	= st.nextToken(); //HORA_COLETA
				String iis			   	= st.nextToken(); //IIS
				st.nextToken(); //\r   \n
				
				OrdemTransferencia ordem = getOrdemFromHashSet(idOrdem);
				Produto produto 		= getProdutoFromHashSet(idProduto);
				Paiol paiolOrigem 			= getPaiolFromHashSet(idPaiolOrigem);
				Paiol paiolDestino 			= getPaiolFromHashSet(idPaiolDestino);
				UsuarioColetor usuario  = getUsuarioColetorFromHash(idUsuario);
				Coletor coletor 		= getColetorFromHash(idColetor);
				
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				Date data = null;
				
				try {
					data = sdf.parse(dataColeta + " " + horaColeta);
				} catch (ParseException e) {
					throw new ParseException("Formato da data inv�lida.", 0);
				}
				
				
				//HEADER ORDEM
				HeaderItemOrdemTransferencia header = inserirHeader(ordem, produto, quantidade, usuario, data, coletor);
				//PAIOL ORDEM
				PaiolItemOrdemTransferencia  headerPaiol = new PaiolItemOrdemTransferencia();
				headerPaiol.setPaiolOrigem(paiolOrigem);
				headerPaiol.setPaiolDestino(paiolDestino);
				headerPaiol.setHeaderItemOrdemTransferencia(header);
				
				MovimentacaoSaldoPaiol saldoPaiol = movimentacaoSaldoPaiolService.findByPaiolAndProduto(paiolOrigem, produto);
				
				if(saldoPaiol == null)
					throw new NullPointerException("Nenhum saldo para o paiol foi encontrado.");
				
				headerPaiol.setQuantidadePaiol(saldoPaiol.getSaldoAtual());
				
				//LOTE ITEM ORDEM - SEPARAR POR LOTE:
				IisItem iisCaixa = consultarIisCaixa(iis);
				LoteItemOrdemTransferencia caixa = new LoteItemOrdemTransferencia();
				IisSubItem iisSubItem = null;
				Boolean isCaixaAberta = false;
				
				//Se iisCaixa for nulo e possuir o IIS que era pra ser caixa no sub-item, ent�o � caixa aberta:
				if(iisCaixa == null && (iisSubItem = consultarIisSubItem(iis)) != null) {
					iisSubItem.setIsCaixaAberta(true);
					isCaixaAberta = true;
					em.merge(iisSubItem);
					iisCaixa = iisSubItem.getIisItem();
					
					//Se caixa aberta, n�o poder� alterar a IIS, somente a unidade de neg�cio
					iisParaAlterar.put(iisCaixa, paiolOrigem.getUnidadeNegocio());
					iisParaAlterarPaiolDestino.put(iisCaixa, paiolDestino);
					caixasAbertas.add(iisCaixa);
				}
				
				caixa.setNumeroLote(iisCaixa.getNumeroLote());
				caixa.setPaiolItemOrdemTransferencia(headerPaiol);
				caixa.setQuantidadeLote(Long.parseLong(quantidade)); //Averiguar
				
				//ITEM ORDEM
				ItemOrdemTransferencia itemOrdem = new ItemOrdemTransferencia();
				itemOrdem.setIisItem(iisCaixa);
				itemOrdem.setLoteItemOrdemTransferencia(caixa);
				
				//INSERE NA TABELA DE MOVIMENTA��ES:
				MovimentacaoIis movimentacaoIis = new MovimentacaoIis();
				movimentacaoIis.setIisItem(iisCaixa);
				
				MovimentacaoItens movimentacaoItens = movimentacaoItensService.findByTipoOrdemEnumAndProdutoAndIdOrdem(
						TipoOrdemEnum.OT, produto, ordem.getId());
				movimentacaoIis.setMovimentacaoItens(movimentacaoItens);
				
				
				//SUB-ITENS
				List<SubItemOrdemTransferencia> subItensOrdem = new ArrayList<>();
				List<MovimentacaoSubIis> movimentacoesSubIis 		 = new ArrayList<>();
				
				if (!isCaixaAberta) {
					List<IisSubItem> subItens = iisSubItemService.findByIisItem(iisCaixa);
					
					for(IisSubItem subitem : subItens){
						SubItemOrdemTransferencia subItemOrdem = new SubItemOrdemTransferencia();
						subItemOrdem.setIisSubItem(subitem);
						subItemOrdem.setItem(itemOrdem);
						subItensOrdem.add(subItemOrdem);
						
						MovimentacaoSubIis subIis = new MovimentacaoSubIis();
						subIis.setIisItem(subitem);
						subIis.setMovimentacaoIis(movimentacaoIis);
						movimentacoesSubIis.add(subIis);
					}
					
					iisParaAlterar.put(iisCaixa, ordem.getUnidadeNegocio());
					iisParaAlterarPaiolDestino.put(iisCaixa, paiolDestino);
				}
				
				subItemRepository.persistirObjetos(subItensOrdem, itemOrdem, caixa, 
						headerPaiol, header, movimentacaoIis, movimentacoesSubIis);
			}
		} 
		
		Set<IisItem> t = iisParaAlterar.keySet();
		for(IisItem key : t){
			if(caixasAbertas.contains(key)) {
				iisItemService.updateIisItemCaixaAbertaOrdemFinalizar(key, iisParaAlterar.get(key));
				iisItemService.updateIisItemOrdemFinalizarOtCaixaAberta(key, iisParaAlterar.get(key));
				continue;
			}
			//Altera a UN e depois o paiol
			iisItemService.updateIisItemOrdemFinalizar(key, iisParaAlterar.get(key));
			iisItemService.updateIisItemOrdemFinalizar(key, iisParaAlterarPaiolDestino.get(key));
			
		}
	}
	
	private void finalizarStatus(){
		for(OrdemTransferencia ordem : ordens){
			ordem.setStatus(StatusOrdemTransferenciaEnum.COLETADA);
			ordemService.save(ordem);
		}
	}
	
	private IisItem consultarIisCaixa(String iis){
		return iisItemService.findByIis(iis);
	}

	private IisSubItem consultarIisSubItem(String iis){
		return iisSubItemService.findByIis(iis);
	}
	
	private HeaderItemOrdemTransferencia inserirHeader(OrdemTransferencia ordem, Produto produto,
			String quantidade, UsuarioColetor usuario, Date data, Coletor coletor){
		HeaderItemOrdemTransferencia header =  
				findHeaderItemOrdemTransferenciaByOrdemTransferencia(ordem, produto);
		
		if(header == null){
			header = new HeaderItemOrdemTransferencia();
			header.setOrdemTransferencia(ordem);
			header.setProduto(produto);
			header.setUsuarioColetor(usuario);
			header.setColetor(coletor);
			header.setDataColeta(data);
			header.setQuantidadeTotalTransferencia(quantidadeTotal.get(produto.getId().toString()).longValue());
		}
		
		return header;
	}

	
	//===================================================================================
	//M�todos para consultar e validar dados
	
	public HeaderItemOrdemTransferencia findHeaderItemOrdemTransferenciaByOrdemTransferencia(
			OrdemTransferencia ordem, Produto produto){
		return  headerService.findByOrdemTransferenciaAndProduto(ordem, produto);
	}
	
	public Paiol getPaiolFromHashSet(String id){
		Long key = Long.parseLong(id);
		
		for(Paiol paiol : paiois){
			if(paiol.getId().equals(key))
				return paiol;
		}
		
		return null;
	}
	
	public Produto getProdutoFromHashSet(String id){
		Long key = Long.parseLong(id);
		
		for(Produto produto : produtos){
			if(produto.getId().equals(key))
				return produto;
		}
		
		return null;
	}
	
	public UsuarioColetor getUsuarioColetorFromHash(String id){
		Long key = Long.parseLong(id);
		
		for(UsuarioColetor usuario : usuarios){
			if(usuario.getId().equals(key))
				return usuario;
		}
		
		return null;
	}
	
	public Coletor getColetorFromHash(String id){
		Long key = Long.parseLong(id);
		
		for(Coletor coletor: coletores){
			if(coletor.getId().equals(key))
				return coletor;
		}
		
		return null;
	}
	
	public OrdemTransferencia getOrdemFromHashSet(String id){
		Long key = Long.parseLong(id);
		
		for(OrdemTransferencia ordem : ordens){
			if(ordem.getId().equals(key))
				return ordem;
		}
		
		return null;
	}
	
	private void popularHashIds(String[] split) throws NullPointerException, NumberFormatException{
		for(String s : split){
			if(s.trim().isEmpty()) continue;
			
			StringTokenizer st = new StringTokenizer(s, "|");
			
			//ID_OC|ID_USUARIO|ID_PRODUTO|QUANTIDADE|ID_PAIOL|DATA_COLETA|HORA_COLETA|IIS|
			while(st.hasMoreTokens()){
				String idOrdem 		= (String) st.nextElement();//st.nextToken("|"); //ID-O.T
				validar(idOrdem, "ID O.T", true);
				
				String idUsuario 	= (String) st.nextElement();// st.nextToken("|"); //ID_USUARIO
				validar(idUsuario, "ID USU�RIO", true);
				
				String idColetor 	= (String) st.nextElement();//st.nextToken("|"); //ID_COLETOR
				validar(idColetor, "ID COLETOR", true);
				
				String idProduto 	= (String) st.nextElement();//st.nextToken("|"); //ID_PRODUTO
				validar(idProduto, "ID PRODUTO", true);
				
				String quantidade 	= (String) st.nextElement();//st.nextToken("|"); //QUANTIDADE
				validar(quantidade, "QUANTIDADE", true);
				
				String idPaiolOrigem   	= (String) st.nextElement();//st.nextToken("|"); //ID_PAIOL
				validar(idPaiolOrigem, "ID PAIOL", true);
				
				String idPaiolDestino   	= (String) st.nextElement();//st.nextToken("|"); //ID_PAIOL
				validar(idPaiolDestino, "ID PAIOL", true);
				
				String dataColeta 	= (String) st.nextElement();//st.nextToken("|"); //DATA_COLETA
				validar(dataColeta, "DATA COLETA", false);
				
				String horaColeta 	= (String) st.nextElement();//st.nextToken("|"); //HORA_COLETA
				validar(horaColeta, "HORA COLETA", false);
				
				String iis			= (String) st.nextElement();//st.nextToken("|"); //IIS
				validar(iis, "IIS", false);
				
				st.nextElement();//\r\n
				
				ordensId.add(idOrdem);
				usuariosId.add(idUsuario);
				coletoresId.add(idColetor);
				produtosId.add(idProduto);
				paiolsId.add(idPaiolOrigem);
				paiolsId.add(idPaiolDestino);
				
				//Insere no hashtable a quantidade total de transferencia para UM produto:
				try{
					if(quantidadeTotal.get(idProduto) == null){
						quantidadeTotal.put(idProduto, Long.parseLong(quantidade));
					}else{
						quantidadeTotal.put(idProduto, quantidadeTotal.get(idProduto).longValue()+Long.parseLong(quantidade));
					}
				}catch(NumberFormatException nfe){
					throw new NumberFormatException("Quantidade informada para o produto ID " + 
							idProduto + " e ordem ID " + idOrdem + " � inv�lida.");
				}
				
			}
		}
	}
	
	private void popularTodosObjetos() throws NullPointerException{
		
		//O.C
		for(String id : ordensId){
			OrdemTransferencia oc = ordemService.findById(Long.parseLong(id));
			if(oc == null)
				throw new NullPointerException("O Campo para a O.T est� vazio ou n�o existe nenhuma Ordem com este ID (" + id+"), fa�a uma O.T com os produtos.");
			
			ordens.add(oc);
		}
		
		// USUARIOS
		for (String id : usuariosId) {
			UsuarioColetor usuario = usuarioColetorService.findOne(Long.parseLong(id));
			if (usuario == null)
				throw new NullPointerException("O Campo para o Usu�rio est� vazio.");

			usuarios.add(usuario);
		}

		// COLETORES
		for (String id : coletoresId) {
			Coletor coletor = coletorService.findOne(Long.parseLong(id));
			if (coletor == null)
				throw new NullPointerException("O Campo para o Usu�rio est� vazio.");

			coletores.add(coletor);
		}
		
		//PRODUTOS
		for(String id : produtosId){
			Produto produto = produtoService.findById(Long.parseLong(id));
			if(produto == null)
				throw new NullPointerException("O Campo para o Produto est� vazio.");
			
			produtos.add(produto);
		}
		
		//PAIOIS
		for(String id : paiolsId){
			Paiol paiol = paiolService.findById(Long.parseLong(id));
			
			if(paiol == null)
				throw new NullPointerException("O Campo para a Paiol est� vazio.");
			
			paiois.add(paiol);
		}
	}
	
	private void validar(String valor, String descricao, Boolean isNumber) throws NullPointerException, NumberFormatException{
		if(valor == null || valor.isEmpty()){
			String mensagem = "O atributo referente a(o) " + 
					descricao + " est� vazio.";
			
			ShowFacesMessage.error("Erro na leitura do arquivo.", mensagem);
			throw new NullPointerException(mensagem);
		}
		
		try{
			if(isNumber)
				Long.parseLong(valor);
		}catch(NumberFormatException nfe){
			throw new NumberFormatException("O atributo referente a(o) " + descricao + 
						" n�o � um caractere num�rico v�lido (ID).");
		}
	}


	public boolean isShowFacesMessageShow() {
		return isShowFacesMessageShow;
	}


	public void setShowFacesMessageShow(boolean isShowFacesMessageShow) {
		this.isShowFacesMessageShow = isShowFacesMessageShow;
	}
	
}
