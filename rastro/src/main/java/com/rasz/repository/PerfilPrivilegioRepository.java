package com.rasz.repository;

import org.springframework.data.repository.CrudRepository;

import com.rasz.domain.PerfilPrivilegio;

public interface PerfilPrivilegioRepository extends CrudRepository<PerfilPrivilegio, Long>, PerfilPrivilegioRepositoryCustom {
	
	
}
