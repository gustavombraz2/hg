package com.rasz.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.rasz.domain.Department;
import com.rasz.domain.Employee;
import com.rasz.domain.Role;

public interface EmployeeRepository extends CrudRepository<Employee, Long>{
	
	public Employee salvar(Employee employee);
	
	public Employee findOne(Long id);
	
	public void delete(Long id);
	
	public Page<Employee> listarTodos(String name, String eeNumber, Long idRole, Pageable pageable);
	
	public List<Employee> managerList();
	
	public List<Employee> hrManagerList();
	
	public Employee findByName(String employee);
	
	public List<Employee> listManagerByDepartament(Long id);
	
	public List<Employee> getAll();
	
	public void insertEmployeeDepartment(List<Department> departments, Long id);
	
	public void deleteEmployeeDepartment(Long id);
	
	public List<Employee> listEmployeeContainsEH();
	
	public Employee findByUsuario(Long id);
	
}
