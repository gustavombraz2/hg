package com.rasz.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.rasz.domain.Perfil;
import com.rasz.domain.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long>{
	
	Usuario findByEmail(String email);
	
	Usuario findByLogin(String login);
	
	public void salvar(Usuario usuario);
	
	public Usuario findOne(Long id);
	
	Page<Usuario> listarTodos(String nmUsuario, String login, String email, Integer code, Boolean active, Pageable pageable);
	
	@Transactional
	@Modifying
	void excluirPerfilUsuario(Usuario usuario, Perfil perfil);
	
//	@Query("select u from Usuario u where (upper(u.email) like upper(?1) or upper(u.login) like upper(?1) or upper(u.nmUsuario) like upper(?1))")
	Page<Usuario> searchUsuarios(String nome, String login, String cpf, Pageable pageable);
	
//	@Query("select u from Usuario u where (upper(u.email) like upper(?1) or upper(u.login) like upper(?1) or upper(u.nmUsuario) like upper(?1)) and boAtivo = true")
	Page<Usuario> searchAtivadoUsuarios(String nome, String login, String cpf, Pageable pageable);
		
	@Query("select u from Usuario u where u.id not in (select distinct u.id from Perfil p join p.usuarios u where ativado = true) and boAtivo = true")
	List<Usuario> listarDisponiveis();

}
