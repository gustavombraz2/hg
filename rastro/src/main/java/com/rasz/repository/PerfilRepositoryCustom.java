package com.rasz.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.rasz.domain.Perfil;
import com.rasz.domain.PerfilPrivilegio;
import com.rasz.domain.Usuario;

public interface PerfilRepositoryCustom  {

	Page<Perfil> filter(String descricao, Long codigo, Boolean ativo, Pageable pageable);
	List<Perfil> findByUsuario(Usuario usuario);
	List<PerfilPrivilegio> findPrivilegioByPerfil(Perfil perfil);
}
