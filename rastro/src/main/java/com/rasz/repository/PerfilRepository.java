package com.rasz.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.rasz.domain.Perfil;

public interface PerfilRepository extends CrudRepository<Perfil, Long>, PerfilRepositoryCustom {
	
	Perfil findByNome(String nome);
	
	List<Perfil> findByAtivadoTrue();

	@Query("select p from Perfil p where (upper(p.descricao) like upper(?1) or upper(p.nome) like upper(?1))")
	Page<Perfil> searchPerfils(String searchString, Pageable pageable);
	
	@Query("select p from Perfil p where (upper(p.descricao) like upper(?1) or upper(p.nome) like upper(?1)) and ativado = ?2")
	List<Perfil> listarPorParametro(String searchString, boolean ativo);
	
	@Query("select distinct p from Perfil p join p.usuarios u where u.id = ?1 and ativado = true")
	Perfil localizarPorUsuario(Long idUsuario);

	Perfil findById(Long id);
}
