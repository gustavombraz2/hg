package com.rasz.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.rasz.domain.Contract;

public interface ContractRepository extends CrudRepository<Contract, Long>{
	
	public Page<Contract> listarTodos(Long code, Long idAllowance, Long idEmployee, Pageable pageable);
	
	public void salvar(Contract contract);
	
	public Contract findOne(Long id);
	
	public Contract findByEmployee(Long id);
	
}
