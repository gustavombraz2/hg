package com.rasz.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.rasz.domain.Privilegio;

public interface PrivilegioRepository extends CrudRepository<Privilegio, Long> {
	
	Privilegio findByNome(String nome); 
	
	Privilegio findByDescricao(String descricao);
	
	@Query("select p from Privilegio p where (upper(p.descricao) like upper(?1) or upper(p.nome) like upper(?1))")
	Page<Privilegio> searchAutorizacoes(String searchString, Pageable pageable);

}
