package com.rasz.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.rasz.domain.Vehicle;

public interface VehicleRepository extends CrudRepository<Vehicle, Long>{
	
	public void salvar(Vehicle vehicle);
	
	public Vehicle findOne(Long id);
	
	public void delete(Long id);
	
	Page<Vehicle> listarTodos(String registration, Integer code, Pageable pageable);

}
