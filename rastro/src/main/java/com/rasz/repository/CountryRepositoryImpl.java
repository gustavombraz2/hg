package com.rasz.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.Country;

public class CountryRepositoryImpl implements CountryRepository {
	
	@PersistenceContext private EntityManager em;

	@Override
	public Page<Country> listarTodos(String description, Integer code, Pageable pageable){
		String where = "";
		
		if(description != null && !description.equals("")) {
			where += " and UPPER(a.name) like '%"+description.toUpperCase()+"%' ";
		}
		
		if(code != null && code > 0) {
			where += " and a.idCountry = "+code.toString();
		}
		
		String select = " select a from Country a where 1=1 ";
		String selectCount = " select count(a) from Country a where 1=1 ";

		Order order = pageable.getSort().iterator().next();
		String orderby = " order by a." + order.getProperty() + " " + order.getDirection();

		Query query = em.createQuery(select + where + orderby);
		Query queryCount = em.createQuery(selectCount + where);

		query.setMaxResults(pageable.getPageSize());
		query.setFirstResult(pageable.getOffset());

		List<Country> lista = query.getResultList();

		int count = 0;
		count = ((Number) queryCount.getSingleResult()).intValue();

		return new PageImpl<Country>(lista, pageable, count);
	}
	
	@Override
	public Country findByName(String country) {
		String where = "";
		
		if(country != null && !country.equals("")) {
			where += " and UPPER(a.name) = '"+country.toUpperCase()+"'";
		}
		
		String sql = " select a from Country a where 1=1" + where;
		Query query = em.createQuery(sql);

		try{
			return (Country)query.getSingleResult();
		}catch(NoResultException e){
			return null;	
		}
	}

	@Override
	public List<Country> listarTodos(String description, Integer code){
		String where = "";
		
		if(description != null && !description.equals("")) {
			where += " and UPPER(a.name) like '%"+description.toUpperCase()+"%' ";
		}
		
		if(code != null && code > 0) {
			where += " and a.idCountry = "+code.toString();
		}
		
		String select = " select a from Country a where 1=1 ";

		Query query = em.createQuery(select + where);
		List<Country> lista = query.getResultList();

		return lista;
	}
	
	@Transactional
	public void salvar(Country country) {
		if(country.getIdCountry() != null && country.getIdCountry() > 0){
			em.merge(country);
		}else{
			em.persist(country);
		}
	}
	
	@Override
	public Country findOne(Long id) {
		if(id != null && id > 0){
			String sql = "SELECT u FROM Country u WHERE u.idCountry = "+id.toString();
			Query query = em.createQuery(sql);
			
			try {
				return (Country) query.getSingleResult();
			
			} catch (Exception e) {
				// TODO: handle exception
				throw e;
			}
		}
		
		return null;
	}
	
	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(Country arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Iterable<? extends Country> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean exists(Long arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<Country> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<Country> findAll(Iterable<Long> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Country> S save(S arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Country> Iterable<S> save(Iterable<S> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Long arg0) {
		// TODO Auto-generated method stub
		
	}
}

