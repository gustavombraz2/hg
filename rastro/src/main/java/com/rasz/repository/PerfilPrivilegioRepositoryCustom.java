package com.rasz.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;

import com.rasz.domain.Perfil;
import com.rasz.domain.Privilegio;

public interface PerfilPrivilegioRepositoryCustom {
	
	@Transactional
	@Modifying
	void excluirPerfilPrivilegio(Perfil perfil, Privilegio privilegio);
	
}
