package com.rasz.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.rasz.domain.HolidayDay;

public interface HolidayDayRepository extends CrudRepository<HolidayDay, Long>{
	
	public void salvar(HolidayDay holidayDay);
	
	public HolidayDay findOne(Long id);
	
	public List<HolidayDay> findByEmployeeYear(Long id, Integer stDate, Integer endDate);
	
	public List<HolidayDay> findByEmployeeHolidayDay(Long id);
	
	public long countDaysByEmployee(Long id, Long year);
	
	public long countHoursByEmployee(Long id, Long year);
	
	public long getSaveCode(Long id);
	
	public void delete(Long saveCode);
	
	public List<HolidayDay> countDaysByEmployeeHoliday(String ids, Long year);
	
	public List<HolidayDay> countDaysByEmployeeYear(String ids, Long year);
	
	public long countHolidayByDayDepartment(Long id, Date day);
	
	public List<HolidayDay> countHoursDaysByEmployeeYear(String ids, Long year);
		
}
