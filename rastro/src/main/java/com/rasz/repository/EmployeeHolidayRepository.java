package com.rasz.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.rasz.domain.EmployeeHoliday;

public interface EmployeeHolidayRepository extends CrudRepository<EmployeeHoliday, Long>{
	
	public EmployeeHoliday salvar(EmployeeHoliday employeeHoliday);
	
	public EmployeeHoliday findOne(Long id);
	
	public Page<EmployeeHoliday> listarTodos(Long idEmployee, Long idHolidayCategory, Pageable pageable);
	
	public Page<EmployeeHoliday> listaByManager(Long idManager, Long idEmployee, Long idHolidayCategory, Pageable pageable) throws Exception;

	public List<EmployeeHoliday> listarTodos(Integer code);
	
	public List<Long> listEmployeeWithHoliday();
	
}
