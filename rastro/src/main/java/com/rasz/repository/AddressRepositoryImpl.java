package com.rasz.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.Address;

public class AddressRepositoryImpl implements AddressRepository {
	
	@PersistenceContext private EntityManager em;

	@Override
	public Page<Address> listarTodos(String description, Integer code, Pageable pageable){
		String where = "";
		
		if(description != null && !description.equals("")) {
			where += " and UPPER(a.description) like '%"+description.toUpperCase()+"%' ";
		}
		
		if(code != null && code > 0) {
			where += " and a.idAddress = "+code.toString();
		}
		
		String select = " select a from Address a where 1=1 ";
		String selectCount = " select count(a) from Address a where 1=1 ";

		Order order = pageable.getSort().iterator().next();
		String orderby = " order by a." + order.getProperty() + " " + order.getDirection();

		Query query = em.createQuery(select + where + orderby);
		Query queryCount = em.createQuery(selectCount + where);

		query.setMaxResults(pageable.getPageSize());
		query.setFirstResult(pageable.getOffset());

		List<Address> lista = query.getResultList();

		int count = 0;
		count = ((Number) queryCount.getSingleResult()).intValue();

		return new PageImpl<Address>(lista, pageable, count);
	}
	
	@Override
	public Address findByName(String address) {
		String where = "";
		
		if(address != null && !address.equals("")) {
			where += " and UPPER(a.description) = '"+address.toUpperCase()+"'";
		}
		
		String sql = " select a from Address a where 1=1" + where;
		Query query = em.createQuery(sql);

		try{
			return (Address)query.getSingleResult();
		}catch(NoResultException e){
			return null;	
		}
	}

	@Override
	public List<Address> listarTodos(String description, Integer code){
		String where = "";
		
		if(description != null && !description.equals("")) {
			where += " and UPPER(a.description) like '%"+description.toUpperCase()+"%' ";
		}
		
		if(code != null && code > 0) {
			where += " and a.idAddress = "+code.toString();
		}
		
		String select = " select a from Address a where 1=1 ";

		Query query = em.createQuery(select + where);
		List<Address> lista = query.getResultList();

		return lista;
	}
	
	@Transactional
	public void salvar(Address address) {
		if(address.getIdAddress() != null && address.getIdAddress() > 0){
			em.merge(address);
		}else{
			em.persist(address);
		}
	}
	
	@Override
	public Address findOne(Long id) {
		if(id != null && id > 0){
			String sql = "SELECT u FROM Address u WHERE u.idAddress = "+id.toString();
			Query query = em.createQuery(sql);
			
			try {
				return (Address) query.getSingleResult();
			
			} catch (Exception e) {
				return null;
			}
		}
		
		return null;
	}
	
	@Override
	public Address findByEmployee(Long id) {
		if(id != null && id > 0){
			String sql = "SELECT u FROM Address u WHERE u.employee.idEmployee = "+id.toString();
			Query query = em.createQuery(sql);
			
			try {
				return (Address) query.getSingleResult();
			} catch (Exception e) {
				return null;
			}
		}
		
		return null;
	}
	
	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(Address arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Iterable<? extends Address> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean exists(Long arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<Address> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<Address> findAll(Iterable<Long> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Address> S save(S arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Address> Iterable<S> save(Iterable<S> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Long arg0) {
		// TODO Auto-generated method stub
		
	}
}

