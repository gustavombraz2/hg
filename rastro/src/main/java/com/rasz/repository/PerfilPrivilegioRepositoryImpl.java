package com.rasz.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.rasz.domain.Perfil;
import com.rasz.domain.Privilegio;

public class PerfilPrivilegioRepositoryImpl implements PerfilPrivilegioRepositoryCustom {

	@PersistenceContext EntityManager em;
	
	@Override
	public void excluirPerfilPrivilegio(Perfil perfil, Privilegio privilegio) {
		// TODO Auto-generated method stub
		if(perfil != null && privilegio != null) {
			String sql = "DELETE FROM perfil_privilegio pp "
					+ "WHERE pp.perfil_id = ANY (SELECT id FROM perfil p where p.id = :idPerfil) "
					+ "AND pp.privilegio_id = ANY (SELECT id FROM privilegio p where p.id = :idPrivilegio) ";

			try{
				
				Query query = em.createNativeQuery(sql);
				query.setParameter("idPerfil", perfil.getId());
				query.setParameter("idPrivilegio", privilegio.getId());

				query.executeUpdate();
								
			}catch (Exception e) {
				// TODO: handle exception
				
			}
		}
	}
		
}
