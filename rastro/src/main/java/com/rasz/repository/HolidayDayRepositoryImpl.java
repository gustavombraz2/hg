package com.rasz.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;

import com.rasz.domain.Contract;
import com.rasz.domain.Employee;
import com.rasz.domain.EmployeeHoliday;
import com.rasz.domain.HolidayDay;
import com.rasz.utils.Utils;

public class HolidayDayRepositoryImpl implements HolidayDayRepository {
	
	@PersistenceContext private EntityManager em;
	
	@Transactional
	public void salvar(HolidayDay employeeAllowance) {
		if(employeeAllowance.getIdHolidayDay() != null && employeeAllowance.getIdHolidayDay() > 0){
			em.merge(employeeAllowance);
		}else{
			em.persist(employeeAllowance);
		}
	}	
	
	@Override
	public List<HolidayDay> findByEmployeeYear(Long id, Integer stDate, Integer endDate) {
		String sql = " select ea.id_employee_allowance, ea.current_year, e.id_employee "+
					 " from employee_allowance as ea "+
					 " inner join contract as c on c.id_contract = ea.id_contract "+
					 " inner join employee as e on e.id_employee = c.id_employee "+
					 " where ea.current_year in ("+stDate.toString()+", "+endDate.toString()+")"+
					 " and e.id_employee =  "+id.toString();
	
		Query query = em.createNativeQuery(sql);
		List<HolidayDay> lista = new ArrayList<>();
		List<Object[]> list = query.getResultList();
		
		for (Object[] o : list) {
			HolidayDay e = new HolidayDay();
			Contract c = new Contract();
			
			if(o[0] != null){
				e.setIdHolidayDay(Long.parseLong(o[0].toString()));
			}
			
			if(o[1] != null){
//				e.setCurrentYear(Long.parseLong(o[1].toString()));
			}
			
			if(o[2] != null){
				c.setIdContract(Long.parseLong(o[2].toString()));
//				e.setContract(c);
			}
			
			lista.add(e);
		}
		
		return lista;
	}
	
	@Override
	public List<HolidayDay> countDaysByEmployeeHoliday(String ids, Long year) {
		String sql = " select eh.id_employee, eh.id_employeeholiday, hd.year,                                 "+
					 " (select count(hoda.id_holiday_day) from holiday_day hoda                               "+
					 "  inner join employee_holiday emho on emho.id_employeeholiday = hoda.id_employeeholiday "+
					 "  inner join employee em on em.id_employee = emho.id_employee                           "+
					 "  inner join contract con on con.id_employee = em.id_employee                           "+
					 "  inner join employee_allowance emal on emal.id_contract = con.id_contract              "+
					 "  where hoda.count = true                                                               "+
					 "  and emho.hr_permit = true                                                             "+
					 "  and hoda.year = emal.current_year                                                     "+
					 "  and emho.id_employee = eh.id_employee                                                 "+
					 "  and hoda.year = hd.year                                                               "+
					 "  and emho.id_employeeholiday = eh.id_employeeholiday                                   "+
					 "  group by emho.id_employeeholiday, hoda.year) as counted                               "+
					 " from holiday_day hd                                                                    "+
					 " inner join employee_holiday eh on eh.id_employeeholiday = hd.id_employeeholiday        "+
					 " where hd.count = true                                                                  "+
					 " and eh.hr_permit = true                                                                "+
					 " and hd.year = "+year.toString()+
					 " and eh.id_employee in ("+ids+")"+
					 " group by eh.id_employeeholiday, hd.year                                                ";
	
		Query query = em.createNativeQuery(sql);
		List<HolidayDay> lista = new ArrayList<>();
		List<Object[]> list = query.getResultList();
		
		for (Object[] o : list) {
			HolidayDay hd = new HolidayDay();
			EmployeeHoliday eh = new EmployeeHoliday();
			
			if(o[0] != null){
				Employee e = new Employee();
				e.setIdEmployee(Long.parseLong(o[0].toString()));
				eh.setEmployee(e);
			}
			
			if(o[1] != null){
				eh.setIdEmployeeHoliday(Long.parseLong(o[1].toString()));
			}
			
			if(o[2] != null){
				hd.setYear(Long.parseLong(o[2].toString()));
			}
			
			if(o[3] != null){
				eh.setCountedDays(Long.parseLong(o[3].toString()));
			}
			
			hd.setEmployeeHoliday(eh);
			lista.add(hd);
		}
		
		return lista;
	}
	
	@Override
	public List<HolidayDay> countDaysByEmployeeYear(String ids, Long year) {
		String sql = " select eh.id_employee, hd.year, ea.available_days, count(hd.id_holiday_day), (ea.available_days - count(hd.id_holiday_day)) as balance "+
					 " from holiday_day hd                                                                                                                    "+
					 " inner join employee_holiday eh on eh.id_employeeholiday = hd.id_employeeholiday                                                        "+
					 " inner join holiday_category hc on hc.id_holiday_category = eh.id_holiday_category                                                      "+
					 " inner join employee e on e.id_employee = eh.id_employee                                                                                "+
					 " inner join contract co on co.id_employee = e.id_employee                                                                               "+
					 " inner join employee_allowance ea on ea.id_contract = co.id_contract                                                                    "+
					 " where hd.count = true                                                                                                                  "+
					 " and eh.hr_permit = true                                                                                                                "+
					 " and hc.reckon = 'D'                                                                                                                    "+
					 " and ea.current_year = "+year.toString()+
					 " and hd.year = "+year.toString()+
					 " and eh.id_employee in ("+ids+")"+
					 " group by eh.id_employee, hd.year, ea.available_days                                                                                    ";
	
		Query query = em.createNativeQuery(sql);
		List<HolidayDay> lista = new ArrayList<>();
		List<Object[]> list = query.getResultList();
		
		for (Object[] o : list) {
			HolidayDay hd = new HolidayDay();
			EmployeeHoliday eh = new EmployeeHoliday();
			
			if(o[0] != null){
				Employee e = new Employee();
				e.setIdEmployee(Long.parseLong(o[0].toString()));
				eh.setEmployee(e);
			}
			
			if(o[1] != null){
				hd.setYear(Long.parseLong(o[1].toString()));
			}
			
			if(o[2] != null){
				eh.setAvailableDays((long)Double.parseDouble(o[2].toString()));
			}
			
			if(o[3] != null){
				eh.setCountedDays(Long.parseLong(o[3].toString()));
			}
			
			if(o[4] != null){
				eh.setBalance(Double.parseDouble(o[4].toString()));
			}
			
			hd.setEmployeeHoliday(eh);
			lista.add(hd);
		}
		
		return lista;
	}
	
	@Override
	public List<HolidayDay> countHoursDaysByEmployeeYear(String ids, Long year) {
		String sql = " select eh.id_employee, 				    												    "+
					 " hd.year, ea.available_days,                                                                   "+
					 " count(hd.id_holiday_day) as counted_days,                                                     "+
					 " (                                                                                             "+
					 "	 select sum(alal.working_hours)                                               "+
					 "	 from holiday_day hdhd                                                                      "+                                               
					 "	 inner join employee_holiday eheh on eheh.id_employeeholiday = hdhd.id_employeeholiday      "+                                                   
					 "	 inner join holiday_category hchc on hchc.id_holiday_category = eheh.id_holiday_category    "+                                                   
					 "	 inner join employee ee on ee.id_employee = eheh.id_employee                                "+                                                 
					 "	 inner join contract coco on coco.id_employee = ee.id_employee                              "+                                                  
					 "	 inner join employee_allowance eaea on eaea.id_contract = coco.id_contract                  "+
					 "	 inner join allowance alal on alal.id_allowance = coco.id_allowance                         "+
					 "	 where hdhd.count = true                                                                    "+                                               
					 "	 and eheh.hr_permit = true                                                                  "+                                               
					 "	 and hchc.reckon = 'H'                                                                      "+                                               
					 "	 and eaea.current_year = "+year.toString()+" and hdhd.year = "+year.toString()+" and eheh.id_employee in (eh.id_employee) "+
					 "	 group by eheh.id_employee, hdhd.year, eaea.available_days, alal.working_hours, hchc.reckon "+ 
					 " ) as counted_hours,                                                                            "+
					 " al.working_hours                                                                           "+
					 " from holiday_day hd                                                                           "+                                          
					 " inner join employee_holiday eh on eh.id_employeeholiday = hd.id_employeeholiday               "+                                          
					 " inner join holiday_category hc on hc.id_holiday_category = eh.id_holiday_category             "+                                          
					 " inner join employee e on e.id_employee = eh.id_employee                                       "+                                          
					 " inner join contract co on co.id_employee = e.id_employee                                      "+                                          
					 " inner join employee_allowance ea on ea.id_contract = co.id_contract                           "+
					 " inner join allowance al on al.id_allowance = co.id_allowance                                  "+
					 " where hd.count = true                                                                         "+                                          
					 " and eh.hr_permit = true                                                                       "+                                          
					 " and hc.reckon = 'D'                                                                           "+                                          
					 " and ea.current_year = "+year.toString()+" and hd.year = "+year.toString()+" and eh.id_employee in ("+ids+")                "+
					 " group by eh.id_employee, hd.year, ea.available_days, al.working_hours, hc.reckon              ";                                                                    
	
		Query query = em.createNativeQuery(sql);
		List<HolidayDay> lista = new ArrayList<>();
		List<Object[]> list = query.getResultList();
		
		for (Object[] o : list) {
			HolidayDay hd = new HolidayDay();
			EmployeeHoliday eh = new EmployeeHoliday();
			
			if(o[0] != null){
				Employee e = new Employee();
				e.setIdEmployee(Long.parseLong(o[0].toString()));
				eh.setEmployee(e);
			}
			
			if(o[1] != null){
				hd.setYear(Long.parseLong(o[1].toString()));
			}
			
			if(o[2] != null){
				eh.setAvailableDays((long)Double.parseDouble(o[2].toString()));
			}
			
			if(o[3] != null){
				eh.setCountedDays(Long.parseLong(o[3].toString()));
			}else{
				eh.setCountedDays(0L);
			}
			
			if(o[4] != null){
				eh.setCountedHours(Long.parseLong(o[4].toString()));
			}else{
				eh.setCountedHours(0L);
			}
			
			if(o[5] != null){
				eh.setWorkingHours(Long.parseLong(o[5].toString()));
			}
			
			Double equi = (double)(eh.getCountedHours() / eh.getWorkingHours())/2; 
			eh.setBalance((double)eh.getAvailableDays() - eh.getCountedDays() - equi);
			
			hd.setEmployeeHoliday(eh);
			lista.add(hd);
		}
		
		return lista;
	}
	
	@Override
	public long countDaysByEmployee(Long id, Long year) {
		String sql = " select count(hd.id_holiday_day) from holiday_day as hd "+
					 " inner join employee_holiday as eh on hd.id_employeeholiday = eh.id_employeeholiday "+
					 " inner join holiday_category as hc on eh.id_holiday_category = hc.id_holiday_category "+
					 " where hc.reckon = 'D' "+
					 " and hd.count = true "+
					 " and eh.hr_permit = true "+
					 " and eh.id_employee = "+id.toString()+
					 " and hd.day between '01/01/"+year.toString()+"' and '31/12/"+year.toString()+"' ";	
				
		Query query = em.createNativeQuery(sql);
		long r;
		
		try {
			r = Long.valueOf(query.getSingleResult().toString());
		} catch (Exception e) {
			r = 0;
			System.out.println(e.getMessage());
		}
		
		return r;
	}
	
	@Override
	public long countHolidayByDayDepartment(Long id, Date day) {
		String sql = " select count(hd.id_holiday_day)												   "+
					 " from holiday_day hd                                                             "+
					 " inner join employee_holiday eh on eh.id_employeeholiday = hd.id_employeeholiday "+
					 " inner join employee e on e.id_employee = eh.id_employee                         "+
					 " inner join employee_department ed on ed.id_employee = e.id_employee             "+
					 " inner join department d on d.id_department = ed.id_department                   "+
					 " where hd.count = true                                                           "+
					 " and eh.hr_permit = true                                                         "+
					 " and d.id_department = "+id.toString()+
					 " and hd.day = '"+Utils.formataDataStringSemHora(day)+"';";                                                       
				
		Query query = em.createNativeQuery(sql);
		long r;
		
		try {
			r = Long.valueOf(query.getSingleResult().toString());
		} catch (Exception e) {
			r = 0;
			System.out.println(e.getMessage());
		}
		
		return r;
	}
	
	@Override
	public long countHoursByEmployee(Long id, Long year) {
		String sql = " select count(hd.id_holiday_day) from holiday_day as hd "+
					 " inner join employee_holiday as eh on hd.id_employeeholiday = eh.id_employeeholiday "+
					 " inner join holiday_category as hc on eh.id_holiday_category = hc.id_holiday_category "+
					 " where hc.reckon = 'H' "+
					 " and hd.count = true "+
					 " and eh.hr_permit = true "+
					 " and eh.id_employee = "+id.toString()+
					 " and hd.day between '01/01/"+year.toString()+"' and '31/12/"+year.toString()+"' ";	
				
		Query query = em.createNativeQuery(sql);
		long r;
		
		try {
			r = Long.valueOf(query.getSingleResult().toString());
		} catch (Exception e) {
			r = 0;
			System.out.println(e.getMessage());
		}
		
		return r;
	}
	
	@Override
	public long getSaveCode(Long id) {
		String sql = " select hd.save_code from holiday_day as hd "+
					 " where hd.id_employeeholiday = "+id.toString()+
					 " group by hd.save_code ";
				
		Query query = em.createNativeQuery(sql);
		long r;
		
		try {
			r = Long.valueOf(query.getSingleResult().toString());
		} catch (Exception e) {
			r = 0;
			System.out.println(e.getMessage());
		}
		
		return r;
	}

	@Override
	public List<HolidayDay> findByEmployeeHolidayDay(Long id) {
		String sql = " select o from HolidayDay o "+
					 " where o.employeeHoliday.idEmployeeHoliday = "+id.toString()+
					 " order by o.day ";
	
		Query query = em.createQuery(sql);
		List<HolidayDay> list = query.getResultList();
		return list;
	}
	
	@Override
	public void delete(Long saveCode) {
		String sql = " delete from HolidayDay o where o.saveCode = "+saveCode.toString();
		Query query = em.createQuery(sql);
		
		try {
			query.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.getMessage();
			e.getLocalizedMessage();
		}
	}
	
	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(HolidayDay arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Iterable<? extends HolidayDay> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean exists(Long arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<HolidayDay> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<HolidayDay> findAll(Iterable<Long> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HolidayDay findOne(Long id) {
		if(id != null && id > 0){
			String sql = "SELECT u FROM HolidayDay u WHERE u.idHolidayDay = "+id.toString();
			Query query = em.createQuery(sql);
			
			try {
				return (HolidayDay) query.getSingleResult();
			
			} catch (Exception e) {
				// TODO: handle exception
				throw e;
			}
		}
		
		return null;
	}

	@Override
	public <S extends HolidayDay> S save(S arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends HolidayDay> Iterable<S> save(Iterable<S> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

}

