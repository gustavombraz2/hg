package com.rasz.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.BankHoliday;

public class BankHolidayRepositoryImpl implements BankHolidayRepository {
	
	@PersistenceContext private EntityManager em;

	@Override
	public Page<BankHoliday> listarTodos(String description, Integer code, Pageable pageable){
		String where = "";
		
		if(description != null && !description.equals("")) {
			where += " and UPPER(a.description) like '%"+description.toUpperCase()+"%' ";
		}
		
		if(code != null && code > 0) {
			where += " and a.idBankHoliday = "+code.toString();
		}
		
		String select = " select a from BankHoliday a where 1=1 ";
		String selectCount = " select count(a) from BankHoliday a where 1=1 ";

		Order order = pageable.getSort().iterator().next();
		String orderby = " order by a." + order.getProperty() + " " + order.getDirection();

		Query query = em.createQuery(select + where + orderby);
		Query queryCount = em.createQuery(selectCount + where);

		query.setMaxResults(pageable.getPageSize());
		query.setFirstResult(pageable.getOffset());

		List<BankHoliday> lista = query.getResultList();

		int count = 0;
		count = ((Number) queryCount.getSingleResult()).intValue();

		return new PageImpl<BankHoliday>(lista, pageable, count);
	}
	
	@Override
	public BankHoliday findByName(String bankHoliday) {
		String where = "";
		
		if(bankHoliday != null && !bankHoliday.equals("")) {
			where += " and UPPER(a.description) = '"+bankHoliday.toUpperCase()+"'";
		}
		
		String sql = " select a from BankHoliday a where 1=1" + where;
		Query query = em.createQuery(sql);

		try{
			return (BankHoliday)query.getSingleResult();
		}catch(NoResultException e){
			return null;	
		}
	}
	
	@Override
	public BankHoliday findByDate(String date) {
		String where = "";
		
		if(date != null && !date.equals("")) {
			where += " and a.date = '"+date.toUpperCase()+"'";
		}
		
		String sql = " select a from BankHoliday a where 1=1" + where;
		Query query = em.createQuery(sql);

		try{
			return (BankHoliday)query.getSingleResult();
		}catch(NoResultException e){
			return null;	
		}
	}

	@Override
	public List<BankHoliday> listarTodos(String description, Integer code){
		String where = "";
		
		if(description != null && !description.equals("")) {
			where += " and UPPER(a.description) like '%"+description.toUpperCase()+"%' ";
		}
		
		if(code != null && code > 0) {
			where += " and a.idBankHoliday = "+code.toString();
		}
		
		String select = " select a from BankHoliday a where 1=1 ";

		Query query = em.createQuery(select + where);
		List<BankHoliday> lista = query.getResultList();

		return lista;
	}
	
	@Transactional
	public void salvar(BankHoliday bankHoliday) {
		if(bankHoliday.getIdBankHoliday() != null && bankHoliday.getIdBankHoliday() > 0){
			em.merge(bankHoliday);
		}else{
			em.persist(bankHoliday);
		}
	}
	
	@Override
	public BankHoliday findOne(Long id) {
		if(id != null && id > 0){
			String sql = "SELECT u FROM BankHoliday u WHERE u.idBankHoliday = "+id.toString();
			Query query = em.createQuery(sql);
			
			try {
				return (BankHoliday) query.getSingleResult();
			
			} catch (Exception e) {
				// TODO: handle exception
				throw e;
			}
		}
		
		return null;
	}
	
	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(BankHoliday arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Iterable<? extends BankHoliday> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean exists(Long arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<BankHoliday> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<BankHoliday> findAll(Iterable<Long> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends BankHoliday> S save(S arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends BankHoliday> Iterable<S> save(Iterable<S> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}
}

