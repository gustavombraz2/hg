package com.rasz.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.HolidayCategory;

public class HolidayCategoryRepositoryImpl implements HolidayCategoryRepository {
	
	@PersistenceContext private EntityManager em;

	@Override
	public Page<HolidayCategory> listarTodos(String description, Integer code, Pageable pageable){
		String where = "";
		
		if(description != null && !description.equals("")) {
			where += " and UPPER(a.description) like '%"+description.toUpperCase()+"%' ";
		}
		
		if(code != null && code > 0) {
			where += " and a.idHolidayCategory = "+code.toString();
		}
		
		String select = " select a from HolidayCategory a where 1=1 and a.activate = true";
		String selectCount = " select count(a) from HolidayCategory a where 1=1 and a.activate = true";

		Order order = pageable.getSort().iterator().next();
		String orderby = " order by a." + order.getProperty() + " " + order.getDirection();

		Query query = em.createQuery(select + where + orderby);
		Query queryCount = em.createQuery(selectCount + where);

		query.setMaxResults(pageable.getPageSize());
		query.setFirstResult(pageable.getOffset());

		List<HolidayCategory> lista = query.getResultList();

		int count = 0;
		count = ((Number) queryCount.getSingleResult()).intValue();

		return new PageImpl<HolidayCategory>(lista, pageable, count);
	}
	
	@Override
	public HolidayCategory findByName(String holidayCategory) {
		String where = "";
		
		if(holidayCategory != null && !holidayCategory.equals("")) {
			where += " and UPPER(a.description) = '"+holidayCategory.toUpperCase()+"'";
		}
		
		String sql = " select a from HolidayCategory a where 1=1" + where;
		Query query = em.createQuery(sql);

		try{
			return (HolidayCategory)query.getSingleResult();
		}catch(NoResultException e){
			return null;	
		}
	}

	@Override
	public List<HolidayCategory> listarTodos(String description, Integer code){
		String where = "";
		
		if(description != null && !description.equals("")) {
			where += " and UPPER(a.description) like '%"+description.toUpperCase()+"%' ";
		}
		
		if(code != null && code > 0) {
			where += " and a.idHolidayCategory = "+code.toString();
		}
		
		String select = " select a from HolidayCategory a where 1=1 and a.activate = true";

		Query query = em.createQuery(select + where);
		List<HolidayCategory> lista = query.getResultList();

		return lista;
	}
	
	@Transactional
	public void salvar(HolidayCategory holidayCategory) {
		if(holidayCategory.getIdHolidayCategory() != null && holidayCategory.getIdHolidayCategory() > 0){
			em.merge(holidayCategory);
		}else{
			em.persist(holidayCategory);
		}
	}
	
	@Override
	public HolidayCategory findOne(Long id) {
		if(id != null && id > 0){
			String sql = "SELECT u FROM HolidayCategory u WHERE u.idHolidayCategory = "+id.toString();
			Query query = em.createQuery(sql);
			
			try {
				return (HolidayCategory) query.getSingleResult();
			} catch (Exception e) {
				throw e;
			}
		}
		
		return null;
	}
	
	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(Long id) {
		if(id != null && id > 0){
			String sql = "UPDATE holiday_category SET activate = false WHERE id_holiday_category = "+id.toString();
			Query query = em.createNativeQuery(sql);
			
			try {
				query.executeUpdate();
			
			} catch (Exception e) {
				throw e;
			}
		}
	}

	@Override
	public void delete(HolidayCategory arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Iterable<? extends HolidayCategory> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean exists(Long arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<HolidayCategory> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<HolidayCategory> findAll(Iterable<Long> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends HolidayCategory> S save(S arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends HolidayCategory> Iterable<S> save(Iterable<S> arg0) {
		// TODO Auto-generated method stub
		return null;
	}
}

