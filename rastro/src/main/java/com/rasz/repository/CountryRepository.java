package com.rasz.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.rasz.domain.Country;

public interface CountryRepository extends CrudRepository<Country, Long>{
	
	public void salvar(Country country);
	
	public Country findOne(Long id);
	
	Page<Country> listarTodos(String description, Integer code, Pageable pageable);

	public List<Country> listarTodos(String description, Integer code);
	
	public Country findByName(String country);

}
