package com.rasz.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.Allowance;

public class AllowanceRepositoryImpl implements AllowanceRepository {
	
	@PersistenceContext private EntityManager em;

	@Override
	public Page<Allowance> listarTodos(String description, Integer code, Pageable pageable){
		String where = "";
		
		if(description != null && !description.equals("")) {
			where += " and UPPER(a.description) like '%"+description.toUpperCase()+"%' ";
		}
		
		if(code != null && code > 0) {
			where += " and a.idAllowance = "+code.toString();
		}
		
		String select = " select a from Allowance a where 1=1 ";
		String selectCount = " select count(a) from Allowance a where 1=1 ";

		Order order = pageable.getSort().iterator().next();
		String orderby = " order by a." + order.getProperty() + " " + order.getDirection();

		Query query = em.createQuery(select + where + orderby);
		Query queryCount = em.createQuery(selectCount + where);

		query.setMaxResults(pageable.getPageSize());
		query.setFirstResult(pageable.getOffset());

		List<Allowance> lista = query.getResultList();

		int count = 0;
		count = ((Number) queryCount.getSingleResult()).intValue();

		return new PageImpl<Allowance>(lista, pageable, count);
	}
	
	@Override
	public Allowance findByName(String allowance) {
		String where = "";
		
		if(allowance != null && !allowance.equals("")) {
			where += " and UPPER(a.description) = '"+allowance.toUpperCase()+"'";
		}
		
		String sql = " select a from Allowance a where 1=1" + where;
		Query query = em.createQuery(sql);

		try{
			return (Allowance)query.getSingleResult();
		}catch(NoResultException e){
			return null;	
		}
	}

	@Override
	public List<Allowance> listarTodos(String description, Integer code){
		String where = "";
		
		if(description != null && !description.equals("")) {
			where += " and UPPER(a.description) like '%"+description.toUpperCase()+"%' ";
		}
		
		if(code != null && code > 0) {
			where += " and a.idAllowance = "+code.toString();
		}
		
		String select = " select a from Allowance a where 1=1 ";

		Query query = em.createQuery(select + where);
		List<Allowance> lista = query.getResultList();

		return lista;
	}
	
	@Transactional
	public void salvar(Allowance allowance) {
		if(allowance.getIdAllowance() != null && allowance.getIdAllowance() > 0){
			em.merge(allowance);
		}else{
			em.persist(allowance);
		}
	}
	
	@Override
	public Allowance findOne(Long id) {
		if(id != null && id > 0){
			String sql = "SELECT u FROM Allowance u WHERE u.idAllowance = "+id.toString();
			Query query = em.createQuery(sql);
			
			try {
				return (Allowance) query.getSingleResult();
			
			} catch (Exception e) {
				// TODO: handle exception
				throw e;
			}
		}
		
		return null;
	}
	
	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(Long id) {
		if(id != null && id > 0){
			String sql = "UPDATE allowance SET activate = false WHERE id_allowance = "+id.toString();
			Query query = em.createNativeQuery(sql);
			
			try {
				query.executeUpdate();
			
			} catch (Exception e) {
				throw e;
			}
		}
	}

	@Override
	public void delete(Allowance arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Iterable<? extends Allowance> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean exists(Long arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<Allowance> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<Allowance> findAll(Iterable<Long> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Allowance> S save(S arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Allowance> Iterable<S> save(Iterable<S> arg0) {
		// TODO Auto-generated method stub
		return null;
	}
}

