package com.rasz.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.rasz.domain.Role;

public interface RoleRepository extends CrudRepository<Role, Long>{
	
	public void salvar(Role role);
	
	public Role findOne(Long id);
	
	public void delete(Long id);
	
	Page<Role> listarTodos(String description, Integer code, Pageable pageable);

	public List<Role> listarTodos(String description, Integer code);
	
	public Role findByName(String role);

}
