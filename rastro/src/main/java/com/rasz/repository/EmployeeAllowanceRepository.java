package com.rasz.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.rasz.domain.EmployeeAllowance;

public interface EmployeeAllowanceRepository extends CrudRepository<EmployeeAllowance, Long>{
	
	public Page<EmployeeAllowance> listarTodos(Long code, Long idAllowance, Long idEmployee, Pageable pageable);
	
	public void salvar(EmployeeAllowance employeeAllowance);
	
	public EmployeeAllowance findOne(Long id);
	
	public List<EmployeeAllowance> findByEmployeeYear(Long id, Integer stDate, Integer endDate);
	
	public void updateDays(Long id, Long year, Long days, Long hours);
	
	public EmployeeAllowance findByEmployeeYear(Long id, Long year);
	
	public List<EmployeeAllowance> listEmployeeAllowance();
	
}
