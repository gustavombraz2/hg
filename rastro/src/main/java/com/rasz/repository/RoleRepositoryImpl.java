package com.rasz.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.Role;

public class RoleRepositoryImpl implements RoleRepository {
	
	@PersistenceContext private EntityManager em;

	@Override
	public Page<Role> listarTodos(String description, Integer code, Pageable pageable){
		String where = "";
		
		if(description != null && !description.equals("")) {
			where += " and UPPER(a.description) like '%"+description.toUpperCase()+"%' ";
		}
		
		if(code != null && code > 0) {
			where += " and a.idRole = "+code.toString();
		}
		
		String select = " select a from Role a where 1=1 and a.activate = true";
		String selectCount = " select count(a) from Role a where 1=1 and a.activate = true";

		Order order = pageable.getSort().iterator().next();
		String orderby = " order by a." + order.getProperty() + " " + order.getDirection();

		Query query = em.createQuery(select + where + orderby);
		Query queryCount = em.createQuery(selectCount + where);

		query.setMaxResults(pageable.getPageSize());
		query.setFirstResult(pageable.getOffset());

		List<Role> lista = query.getResultList();

		int count = 0;
		count = ((Number) queryCount.getSingleResult()).intValue();

		return new PageImpl<Role>(lista, pageable, count);
	}
	
	@Override
	public Role findByName(String role) {
		String where = "";
		
		if(role != null && !role.equals("")) {
			where += " and UPPER(a.description) = '"+role.toUpperCase()+"'";
		}
		
		String sql = " select a from Role a where 1=1" + where;
		Query query = em.createQuery(sql);

		try{
			return (Role)query.getSingleResult();
		}catch(NoResultException e){
			return null;	
		}
	}

	@Override
	public List<Role> listarTodos(String description, Integer code){
		String where = "";
		
		if(description != null && !description.equals("")) {
			where += " and UPPER(a.description) like '%"+description.toUpperCase()+"%' ";
		}
		
		if(code != null && code > 0) {
			where += " and a.idRole = "+code.toString();
		}
		
		String select = " select a from Role a where 1=1 and a.activate = true";

		Query query = em.createQuery(select + where);
		List<Role> lista = query.getResultList();

		return lista;
	}
	
	@Transactional
	public void salvar(Role role) {
		if(role.getIdRole() != null && role.getIdRole() > 0){
			em.merge(role);
		}else{
			em.persist(role);
		}
	}
	
	@Override
	public Role findOne(Long id) {
		if(id != null && id > 0){
			String sql = "SELECT u FROM Role u WHERE u.idRole = "+id.toString();
			Query query = em.createQuery(sql);
			
			try {
				return (Role) query.getSingleResult();
			
			} catch (Exception e) {
				// TODO: handle exception
				throw e;
			}
		}
		
		return null;
	}
	
	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(Long id) {
		if(id != null && id > 0){
			String sql = "UPDATE role SET activate = false WHERE id_role = "+id.toString();
			Query query = em.createNativeQuery(sql);
			
			try {
				query.executeUpdate();
			
			} catch (Exception e) {
				throw e;
			}
		}
	}

	@Override
	public void delete(Role arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Iterable<? extends Role> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean exists(Long arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<Role> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<Role> findAll(Iterable<Long> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Role> S save(S arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Role> Iterable<S> save(Iterable<S> arg0) {
		// TODO Auto-generated method stub
		return null;
	}
}

