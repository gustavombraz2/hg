package com.rasz.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.Employee;
import com.rasz.domain.EmployeeHoliday;
import com.rasz.domain.HolidayCategory;
import com.rasz.utils.Utils;

public class EmployeeHolidayRepositoryImpl implements EmployeeHolidayRepository {
	
	@PersistenceContext private EntityManager em;
	
	@Autowired private transient EmployeeRepository employeeRepository;
	@Autowired private transient HolidayCategoryRepository holidayCategoryRepository;

	@Override
	public Page<EmployeeHoliday> listarTodos(Long idEmployee, Long idHolidayCategory, Pageable pageable){
		String where = "";
		
		if(idEmployee != null && idEmployee > 0){
			where += " AND a.employee.idEmployee = "+idEmployee.toString();
		}
		
		if(idHolidayCategory != null && idHolidayCategory > 0){
			where += " AND a.holidayCategory.idHolidayCategory = "+idHolidayCategory.toString();
		}
		
		String select = " select a from EmployeeHoliday a where 1=1 ";
		String selectCount = " select count(a) from EmployeeHoliday a where 1=1 ";

		Order order = pageable.getSort().iterator().next();
		String orderby = " order by a." + order.getProperty() + " " + order.getDirection();

		Query query = em.createQuery(select + where + orderby);
		Query queryCount = em.createQuery(selectCount + where);

		query.setMaxResults(pageable.getPageSize());
		query.setFirstResult(pageable.getOffset());

		List<EmployeeHoliday> lista = query.getResultList();

		int count = 0;
		count = ((Number) queryCount.getSingleResult()).intValue();

		return new PageImpl<EmployeeHoliday>(lista, pageable, count);
	}
	
	@Override
	public Page<EmployeeHoliday> listaByManager(Long idManager, Long idEmployee, Long idHolidayCategory, Pageable pageable) throws Exception{
		String sql = " select eh.id_employeeHoliday,	 "+ //0
					 " eh.requisition_date,      		"+ //1
					 " eh.start_date,            "+ //2
					 " eh.end_date,              "+ //3
					 " eh.reason,                "+ //4
					 " eh.day_counted,           "+ //5
					 " eh.hour_counted,          "+ //6
					 " eh.id_employee,           "+ //7
					 " eh.id_holiday_category,   "+ //8
					 " eh.id_manager_permission, "+ //9
					 " eh.id_hr_permission,      "+ //10
					 " eh.manager_permit,        "+ //11
					 " eh.hr_permit,              "+ //12
					 " eh.manager_permission_date, "+ //13
					 " eh.hr_permission_date       "+ //14
					 " from employee_holiday eh									    "+ 	
				     " inner join employee e on e.id_employee = eh.id_employee                  "+
					 " left outer join employee_department ed on ed.id_employee = e.id_employee "+
					 " left outer join department d on d.id_department = ed.id_department       "+
					 " inner join manager_department md on md.id_department = d.id_department   "+
					 " inner join employee ma on ma.id_employee = md.id_employee                "+
					 " and ma.id_employee = "+idManager.toString()+" and ma.manager = true      "+
					 " inner join holiday_category hc on hc.id_holiday_category = eh.id_holiday_category   "+
					 " where 1=1 ";
		
		String where = "";
		
		if(idEmployee != null && idEmployee > 0){
			where += " AND eh.id_employee = "+idEmployee.toString();
		}
		
		if(idHolidayCategory != null && idHolidayCategory > 0){
			where += " AND eh.id_holiday_category = "+idHolidayCategory.toString();
		}

		Query query = em.createNativeQuery(sql + where + " order by eh.id_employeeHoliday DESC ");
		
		List<Object[]> listCount = query.getResultList();

		query.setMaxResults(pageable.getPageSize());
		query.setFirstResult(pageable.getOffset());

		List<EmployeeHoliday> lista = new ArrayList<>();
		List<Object[]> list = query.getResultList();
		
		for (Object[] o : list) {
			EmployeeHoliday eh = new EmployeeHoliday();
			Employee e = new Employee();
			Employee ma = new Employee();
			Employee hr = new Employee();
			HolidayCategory hc = new HolidayCategory();
			
			if(o[0] != null){
				eh.setIdEmployeeHoliday(Long.parseLong(o[0].toString()));
			}
			
			if(o[1] != null){
				eh.setRequisitionDate(Utils.formataData(o[1].toString()));
			}
			
			if(o[2] != null){
				eh.setStartDate(Utils.formataData(o[2].toString()));
			}
			
			if(o[3] != null){
				eh.setEndDate(Utils.formataData(o[3].toString()));
			}
			
			if(o[4] != null){
				eh.setReason(o[4].toString());
			}
			
			if(o[5] != null){
				eh.setDayCounted(Integer.parseInt(o[5].toString()));
			}
			
			if(o[6] != null){
				eh.setHourCounted(Integer.parseInt(o[6].toString()));
			}

			if(o[7] != null){
				e = employeeRepository.findOne(Long.parseLong(o[7].toString()));
			}
			
			if(o[8] != null){
				hc = holidayCategoryRepository.findOne(Long.parseLong(o[8].toString()));
			}
			
			if(o[9] != null){
				ma = employeeRepository.findOne(Long.parseLong(o[9].toString()));
			}
			
			if(o[10] != null){
				hr = employeeRepository.findOne(Long.parseLong(o[10].toString()));
			}
			
			if(o[11] != null){
				eh.setManagerPermit(Boolean.parseBoolean(o[11].toString()));
			}
			
			if(o[12] != null){
				eh.setHrPermit(Boolean.parseBoolean(o[12].toString()));
			}
			
			if(o[13] != null){
				eh.setManagerPermissionDate(Utils.formataData(o[13].toString()));
			}
			
			if(o[14] != null){
				eh.setHrPermissionDate(Utils.formataData(o[14].toString()));
			}
			
			eh.setEmployee(e);
			eh.setManagerPermission(ma);
			eh.setHrPermission(hr);
			eh.setHolidayCategory(hc);
			lista.add(eh);
		}
		

		return new PageImpl<EmployeeHoliday>(lista, pageable, listCount.size());
	}
	
	@Override
	public List<EmployeeHoliday> listarTodos(Integer code){
		String where = "";
		
		if(code != null && code > 0) {
			where += " and a.idEmployeeHoliday = "+code.toString();
		}
		
		String select = " select a from EmployeeHoliday a where 1=1 ";

		Query query = em.createQuery(select + where);
		List<EmployeeHoliday> lista = query.getResultList();

		return lista;
	}
	
	@Override
	public List<Long> listEmployeeWithHoliday(){
		String select = " SELECT a.employee.idEmployee FROM EmployeeHoliday a "+
					    " WHERE a.hrPermit = true GROUP BY a.employee.idEmployee";

		Query query = em.createQuery(select);
		List<Long> lista = new ArrayList<>();
		
		try {
			lista = query.getResultList();
			return lista;
		} catch (Exception e) {
			// TODO: handle exception
			return lista;
		}
	}
	
	@Transactional
	public EmployeeHoliday salvar(EmployeeHoliday employeeHoliday) {
		if(employeeHoliday.getIdEmployeeHoliday() != null && employeeHoliday.getIdEmployeeHoliday() > 0){
			return em.merge(employeeHoliday);
		}else{
			em.persist(employeeHoliday);
			return employeeHoliday;
		}
	}
	
	@Override
	public EmployeeHoliday findOne(Long id) {
		if(id != null && id > 0){
			String sql = "SELECT u FROM EmployeeHoliday u WHERE u.idEmployeeHoliday = "+id.toString();
			Query query = em.createQuery(sql);
			
			try {
				return (EmployeeHoliday) query.getSingleResult();
			
			} catch (Exception e) {
				// TODO: handle exception
				Utils.getStackTrace(e);
			}
		}
		
		return null;
	}
	
	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(EmployeeHoliday arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Iterable<? extends EmployeeHoliday> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean exists(Long arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<EmployeeHoliday> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<EmployeeHoliday> findAll(Iterable<Long> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends EmployeeHoliday> S save(S arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends EmployeeHoliday> Iterable<S> save(Iterable<S> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Long arg0) {
		// TODO Auto-generated method stub
		
	}
}

