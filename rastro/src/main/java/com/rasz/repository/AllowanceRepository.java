package com.rasz.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.rasz.domain.Allowance;

public interface AllowanceRepository extends CrudRepository<Allowance, Long>{
	
	public void salvar(Allowance allowance);
	
	public Allowance findOne(Long id);
	
	public void delete(Long id);
	
	Page<Allowance> listarTodos(String description, Integer code, Pageable pageable);

	public List<Allowance> listarTodos(String description, Integer code);
	
	public Allowance findByName(String allowance);

}
