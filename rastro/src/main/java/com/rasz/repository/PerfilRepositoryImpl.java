package com.rasz.repository;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;

import com.rasz.domain.Perfil;
import com.rasz.domain.PerfilPrivilegio;
import com.rasz.domain.Privilegio;
import com.rasz.domain.Usuario;

public class PerfilRepositoryImpl implements PerfilRepositoryCustom {
	
	
	@PersistenceContext EntityManager em;
	

	public Page<Perfil> filter(String descricao, Long codigo, Boolean ativo, Pageable pageable) {
		String filtro = "where 1=1";
		String select = "SELECT p FROM Perfil p ";
		String selectCount = " SELECT count(p) FROM Perfil p ";
		
		Order order = pageable.getSort().iterator().next();
		String orderby = " order by p." + order.getProperty() + " " + order.getDirection();
		
		if(descricao != null && !descricao.equals("")) {
			filtro += " AND UPPER(p.descricao) LIKE '%" +descricao.toUpperCase()+ "%'";
		}
		
		if(codigo != null && codigo.longValue() > 0) {
			filtro += " AND p.id = " + codigo;
		}
		
		if(ativo != null) {
			filtro += " AND p.ativado = " + ativo;
		}
		
		Query query = em.createQuery(select + filtro + orderby);
		Query queryCount = em.createQuery(selectCount + filtro);

		

		query.setMaxResults(pageable.getPageSize());
		query.setFirstResult(pageable.getOffset());

		List<Perfil> uns = query.getResultList();

		int count = ((Number) queryCount.getSingleResult()).intValue();
		return new PageImpl<Perfil>(uns, pageable, count);
		
	}


	@Override
	public List<Perfil> findByUsuario(Usuario usuario) {
		if(usuario != null) {
			String sql = "SELECT * FROM perfil p "
						+ "LEFT JOIN perfil_usuario pu on pu.id_perfil = p.id "
						+ "LEFT JOIN usuario u ON u.id = pu.id_usuario "
						+ "WHERE u.id = :idUser ";
			
			Query query = em.createNativeQuery(sql);
			query.setParameter("idUser", usuario.getId());
			
			List<Object[]> lista = query.getResultList();
			List<Perfil> perfis = new ArrayList<>();
			List<Usuario> usuarios = new ArrayList<>();
			
			for (Object[] obj : lista) {
				Perfil p = new Perfil();
				p.setId(Long.valueOf(obj[0].toString()));
				p.setAdministrador(Boolean.valueOf(obj[1].toString()));
				p.setAtivado(Boolean.valueOf(obj[2].toString()));
				p.setDescricao(obj[3].toString());
				p.setNome(obj[4].toString());
				

				Usuario u = new Usuario();
				
				u.setId(Long.valueOf(obj[6].toString()));
				u.setEmail(obj[8].toString());
				u.setLogin(obj[9].toString());
				u.setSenha(obj[10].toString());
//				u.setDocumento(obj[12].toString());
				u.setNmUsuario(obj[13].toString());
				u.setBoAtivo(Boolean.valueOf(obj[16].toString()));
				
				usuarios.add(u);
				
				p.setUsuarios(usuarios);
				
				perfis.add(p);
				
			}
			return perfis;
		}		
		// TODO Auto-generated method stub
		return null;
	}

	
	public List<PerfilPrivilegio> findPrivilegioByPerfil(Perfil perfil) {
		
		String sql = "SELECT * FROM privilegio p "
				+ "LEFT JOIN perfil_privilegio pri on pri.privilegio_id = p.id "
				+ "WHERE pri.perfil_id = :idPerfil";
		
		Query query = em.createNativeQuery(sql);
		query.setParameter("idPerfil", perfil.getId());
		
		List<Object[]> lista = new ArrayList<>();
		
		List<PerfilPrivilegio> privilegios = new ArrayList<>();
		
		lista = query.getResultList();
		
		for (Object[] obj : lista) {
			Privilegio p = new Privilegio();
			p.setId(Long.valueOf(obj[0].toString()));
			p.setDescricao(obj[1].toString());
			p.setNome(obj[2].toString());
			
			
			if(obj[3] != null) {
				Privilegio pai = new Privilegio();
				pai.setId(Long.valueOf(obj[3].toString()));
				p.setPrivilegioPai(pai);
			}
					
			if(obj[4] != null) {		
				p.setOrdem(Integer.valueOf(obj[4].toString()));
			}
			
			PerfilPrivilegio perfilPrivilegio = new PerfilPrivilegio();
			
			perfilPrivilegio.setId(Long.valueOf(obj[5].toString()));
			perfilPrivilegio.setAlterar(Boolean.valueOf(obj[6].toString()));
			perfilPrivilegio.setConsultar(Boolean.valueOf(Boolean.valueOf(obj[7].toString())));
			perfilPrivilegio.setIncluir(Boolean.valueOf(obj[8].toString()));
			perfilPrivilegio.setRemover(Boolean.valueOf(obj[9].toString()));
			perfilPrivilegio.setPerfil(perfil);
			perfilPrivilegio.setPrivilegio(p);
			perfilPrivilegio.setImprimir(Boolean.valueOf(obj[12].toString()));
			perfilPrivilegio.setColetor(Boolean.valueOf(obj[13].toString()));
			
			privilegios.add(perfilPrivilegio);
		}
		
		return privilegios;
	}
}
