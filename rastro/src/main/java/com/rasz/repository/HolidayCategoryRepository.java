package com.rasz.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.rasz.domain.HolidayCategory;

public interface HolidayCategoryRepository extends CrudRepository<HolidayCategory, Long>{
	
	public void salvar(HolidayCategory holidayCategory);
	
	public HolidayCategory findOne(Long id);
	
	public void delete(Long id);
	
	Page<HolidayCategory> listarTodos(String description, Integer code, Pageable pageable);

	public List<HolidayCategory> listarTodos(String description, Integer code);
	
	public HolidayCategory findByName(String holidayCategory);

}
