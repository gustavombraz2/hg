package com.rasz.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.City;

public class CityRepositoryImpl implements CityRepository {
	
	@PersistenceContext private EntityManager em;

	@Override
	public Page<City> listarTodos(String description, Integer code, Pageable pageable){
		String where = "";
		
		if(description != null && !description.equals("")) {
			where += " and UPPER(a.name) like '%"+description.toUpperCase()+"%' ";
		}
		
		if(code != null && code > 0) {
			where += " and a.idCity = "+code.toString();
		}
		
		String select = " select a from City a where 1=1 ";
		String selectCount = " select count(a) from City a where 1=1 ";

		Order order = pageable.getSort().iterator().next();
		String orderby = " order by a." + order.getProperty() + " " + order.getDirection();

		Query query = em.createQuery(select + where + orderby);
		Query queryCount = em.createQuery(selectCount + where);

		query.setMaxResults(pageable.getPageSize());
		query.setFirstResult(pageable.getOffset());

		List<City> lista = query.getResultList();

		int count = 0;
		count = ((Number) queryCount.getSingleResult()).intValue();

		return new PageImpl<City>(lista, pageable, count);
	}
	
	@Override
	public City findByName(String city) {
		String where = "";
		
		if(city != null && !city.equals("")) {
			where += " and UPPER(a.name) = '"+city.toUpperCase()+"'";
		}
		
		String sql = " select a from City a where 1=1" + where;
		Query query = em.createQuery(sql);

		try{
			return (City)query.getSingleResult();
		}catch(NoResultException e){
			return null;	
		}
	}

	@Override
	public List<City> listarTodos(String description, Integer code){
		String where = "";
		
		if(description != null && !description.equals("")) {
			where += " and UPPER(a.name) like '%"+description.toUpperCase()+"%' ";
		}
		
		if(code != null && code > 0) {
			where += " and a.idCity = "+code.toString();
		}
		
		String select = " select a from City a where 1=1 ";

		Query query = em.createQuery(select + where);
		List<City> lista = query.getResultList();

		return lista;
	}
	
	@Transactional
	public void salvar(City city) {
		if(city.getIdCity() != null && city.getIdCity() > 0){
			em.merge(city);
		}else{
			em.persist(city);
		}
	}
	
	@Override
	public City findOne(Long id) {
		if(id != null && id > 0){
			String sql = "SELECT u FROM City u WHERE u.idCity = "+id.toString();
			Query query = em.createQuery(sql);
			
			try {
				return (City) query.getSingleResult();
			
			} catch (Exception e) {
				// TODO: handle exception
				throw e;
			}
		}
		
		return null;
	}
	
	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(City arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Iterable<? extends City> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean exists(Long arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<City> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<City> findAll(Iterable<Long> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends City> S save(S arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends City> Iterable<S> save(Iterable<S> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Long arg0) {
		// TODO Auto-generated method stub
		
	}
}

