package com.rasz.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;

import com.rasz.domain.Contract;
import com.rasz.utils.Utils;

public class ContractRepositoryImpl implements ContractRepository {
	
	@PersistenceContext private EntityManager em;

	@Override
	public Page<Contract> listarTodos(Long code, Long idAllowance, Long idEmployee, Pageable pageable){
		String where = "";
		
		if(code != null && code > 0) {
			where += " and a.idContract = "+code.toString();
		}
		
		if(idAllowance != null && idAllowance > 0) {
			where += " and a.allowance.idAllowance = "+idAllowance.toString();
		}
		
		if(idEmployee != null && idEmployee > 0) {
			where += " and a.employee.idEmployee = "+idEmployee.toString();
		}
		
		String select = " select a from Contract a where 1=1";
		String selectCount = " select count(a) from Contract a where 1=1";

		Order order = pageable.getSort().iterator().next();
		String orderby = " order by a." + order.getProperty() + " " + order.getDirection();

		Query query = em.createQuery(select + where + orderby);
		Query queryCount = em.createQuery(selectCount + where);

		query.setMaxResults(pageable.getPageSize());
		query.setFirstResult(pageable.getOffset());

		List<Contract> lista = query.getResultList();

		int count = 0;
		count = ((Number) queryCount.getSingleResult()).intValue();

		return new PageImpl<Contract>(lista, pageable, count);
	}
	
	@Transactional
	public void salvar(Contract contract) {
		if(contract.getIdContract() != null && contract.getIdContract() > 0){
			em.merge(contract);
		}else{
			em.persist(contract);
		}
	}	

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(Long arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Contract arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Iterable<? extends Contract> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean exists(Long arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<Contract> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<Contract> findAll(Iterable<Long> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Contract findOne(Long id) {
		if(id != null && id > 0){
			String sql = "SELECT u FROM Contract u WHERE u.idContract = "+id.toString();
			Query query = em.createQuery(sql);
			
			try {
				return (Contract) query.getSingleResult();
			} catch (Exception e) {
				// TODO: handle exception
				throw e;
			}
		}
		
		return null;
	}
	
	@Override
	public Contract findByEmployee(Long id) {
		if(id != null && id > 0){
			String sql = "SELECT u FROM Contract u WHERE u.employee.idEmployee = "+id.toString();
			Query query = em.createQuery(sql);
			
			try {
				return (Contract) query.getSingleResult();
			} catch (Exception e) {
				// TODO: handle exception
				Utils.getStackTrace(e);
			}
		}
		
		return null;
	}

	@Override
	public <S extends Contract> S save(S arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Contract> Iterable<S> save(Iterable<S> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

}

