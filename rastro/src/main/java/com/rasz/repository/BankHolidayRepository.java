package com.rasz.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.rasz.domain.BankHoliday;

public interface BankHolidayRepository extends CrudRepository<BankHoliday, Long>{
	
	public void salvar(BankHoliday bankHoliday);
	
	public BankHoliday findOne(Long id);
	
	Page<BankHoliday> listarTodos(String description, Integer code, Pageable pageable);

	public List<BankHoliday> listarTodos(String description, Integer code);
	
	public BankHoliday findByName(String bankHoliday);
	
	public BankHoliday findByDate(String date);

}
