package com.rasz.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.Perfil;
import com.rasz.domain.Usuario;

public class UsuarioRepositoryImpl implements UsuarioRepository {
	
	@PersistenceContext private EntityManager em;

	@Override
	public Page<Usuario> listarTodos(String nmUsuario, String login, String email, Integer code, Boolean active, Pageable pageable){
		String where = "";
		
		if(nmUsuario != null && !nmUsuario.equals("")) {
			where += " and UPPER(a.nmUsuario) like '%"+nmUsuario.toUpperCase()+"%' ";
		}
		
		if(login != null && !login.equals("")) {
			where += " and UPPER(a.login) like '%"+login.toUpperCase()+"%' ";
		}
		
		if(email != null && !email.equals("")) {
			where += " and UPPER(a.email) like '%"+email.toUpperCase()+"%' ";
		}
		
		if(code != null && code > 0) {
			where += " and a.id = "+code.toString();
		}
		
		if(active != null) {
			where += " and a.boAtivo = "+active.toString();
		}
		
		String select = " select a from Usuario a where 1=1";
		String selectCount = " select count(a) from Usuario a where 1=1";

		Order order = pageable.getSort().iterator().next();
		String orderby = " order by a." + order.getProperty() + " " + order.getDirection();

		Query query = em.createQuery(select + where + orderby);
		Query queryCount = em.createQuery(selectCount + where);

		query.setMaxResults(pageable.getPageSize());
		query.setFirstResult(pageable.getOffset());

		List<Usuario> lista = query.getResultList();

		int count = 0;
		count = ((Number) queryCount.getSingleResult()).intValue();

		return new PageImpl<Usuario>(lista, pageable, count);
	}

	public Usuario findByLogin(String login){
		if(login != null && !login.equals("")){
			String sql = "SELECT u FROM Usuario u WHERE UPPER(u.login) = '"+login.toUpperCase()+"'";

			Query query = em.createQuery(sql);
			try{
				return (Usuario) query.getSingleResult();
			}catch (Exception e) {
				// TODO: handle exception
				return null;
			}
		}else{
			return null;
		}
	}
	
	@Transactional
	public void salvar(Usuario usuario) {
		if(usuario.getId() != null && usuario.getId() > 0){
			em.merge(usuario);
		}else{
			em.persist(usuario);
		}
	}
	
	public void excluirPerfilUsuario(Usuario usuario, Perfil perfil) {
		String sql = "DELETE FROM perfil_usuario pu WHERE pu.id_usuario = "
						+ "ANY (SELECT id FROM usuario p where p.id = :idUsuario) "
						+ "AND pu.id_perfil = ANY (SELECT id FROM perfil p where p.id = :idPerfil)";
		Query query = em.createNativeQuery(sql);
		query.setParameter("idUsuario", usuario.getId());
		query.setParameter("idPerfil", perfil.getId());
		
		try {
			query.executeUpdate();

		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
	}
	
	@Override
	public Usuario findOne(Long id) {
		if(id != null && id > 0){
			String sql = "SELECT u FROM Usuario u WHERE u.id = "+id.toString();
			Query query = em.createQuery(sql);
			
			try {
				return (Usuario) query.getSingleResult();
			
			} catch (Exception e) {
				// TODO: handle exception
				throw e;
			}
		}
		
		return null;
	}
	
	@Override
	public Usuario findByEmail(String email) {
		if(email != null && !email.equals("")){
			String sql = "SELECT u FROM Usuario u WHERE UPPER(u.email) = '"+email.toUpperCase()+"'";
			Query query = em.createQuery(sql);
			
			try {
				return (Usuario) query.getSingleResult();
			
			} catch (Exception e) {
				return null;
			}
		}else{
			return null;
		}
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(Long arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Usuario arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Iterable<? extends Usuario> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean exists(Long arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<Usuario> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<Usuario> findAll(Iterable<Long> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Usuario> S save(S arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Usuario> Iterable<S> save(Iterable<S> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Usuario> searchUsuarios(String nome, String login, String cpf, Pageable pageable) {
		String where = "";
		
		if(nome != null && !nome.equals("")) {
			where += " and (upper(u.email) like '"+nome.toUpperCase()+"' or upper(u.login) like '"+nome.toUpperCase()+"' or upper(u.nmUsuario) like '"+nome.toUpperCase()+"')";
		}
		
		String select = " select a from Usuario a where 1=1";
		String selectCount = " select count(a) from Usuario a where 1=1";

		Order order = pageable.getSort().iterator().next();
		String orderby = " order by a." + order.getProperty() + " " + order.getDirection();

		Query query = em.createQuery(select + where + orderby);
		Query queryCount = em.createQuery(selectCount + where);

		query.setMaxResults(pageable.getPageSize());
		query.setFirstResult(pageable.getOffset());

		List<Usuario> lista = query.getResultList();

		int count = 0;
		count = ((Number) queryCount.getSingleResult()).intValue();

		return new PageImpl<Usuario>(lista, pageable, count);
	}

	@Override
	public Page<Usuario> searchAtivadoUsuarios(String nome, String login, String cpf, Pageable pageable) {
		String where = "";
		
		if(nome != null && !nome.equals("")) {
			where += " and (upper(u.email) like '"+nome.toUpperCase()+"' or upper(u.login) like '"+nome.toUpperCase()+"' or upper(u.nmUsuario) like '"+nome.toUpperCase()+"' and boAtivo = true";
		}
		
		String select = " select a from Usuario a where 1=1";
		String selectCount = " select count(a) from Usuario a where 1=1";

		Order order = pageable.getSort().iterator().next();
		String orderby = " order by a." + order.getProperty() + " " + order.getDirection();

		Query query = em.createQuery(select + where + orderby);
		Query queryCount = em.createQuery(selectCount + where);

		query.setMaxResults(pageable.getPageSize());
		query.setFirstResult(pageable.getOffset());

		List<Usuario> lista = query.getResultList();

		int count = 0;
		count = ((Number) queryCount.getSingleResult()).intValue();

		return new PageImpl<Usuario>(lista, pageable, count);
	}

	@Override
	public List<Usuario> listarDisponiveis() {
		String sql = "select u from Usuario u where u.id not in (select distinct u.id from Perfil p join p.usuarios u where ativado = true) and boAtivo = true";
		
		Query query = em.createQuery(sql);

		List<Usuario> lista = query.getResultList();

		return lista;
	}
}

