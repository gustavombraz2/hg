package com.rasz.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.postgresql.core.NativeQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.Department;
import com.rasz.domain.Employee;
import com.rasz.domain.Role;
import com.rasz.utils.Utils;

public class EmployeeRepositoryImpl implements EmployeeRepository {
	
	@PersistenceContext private EntityManager em;

	@Transactional
	public Employee salvar(Employee employee) {
		if(employee.getIdEmployee() != null && employee.getIdEmployee() > 0){
			em.merge(employee);
		}else{
			em.persist(employee);
		}
		
		return employee;
	}
	
	@Override
	public Employee findOne(Long id) {
		if(id != null && id > 0){
			String sql = "SELECT u FROM Employee u WHERE u.idEmployee = "+id.toString();
			Query query = em.createQuery(sql);
			
			try {
				return (Employee) query.getSingleResult();
			
			} catch (Exception e) {
				// TODO: handle exception
				throw e;
			}
		}
		
		return null;
	}
	
	@Override
	public Employee findByUsuario(Long id) {
		String sql = "SELECT u FROM Employee u WHERE u.usuario.id = "+id.toString();
		Query query = em.createQuery(sql);
		
		try {
			return (Employee) query.getSingleResult();
		
		} catch (Exception e) {
			Utils.getStackTrace(e);
			return null;
		}
	}
	
	public Page<Employee> listarTodos(String name, String eeNumber, Long idRole, Pageable pageable){
		String where = "";
		
		if(name != null && !name.equals("")) {
			where += " and UPPER(a.name) like '%"+name.toUpperCase()+"%' or UPPER(a.surname) like '%"+name.toUpperCase()+"%'";
		}
		
		if(eeNumber != null && !eeNumber.equals("")) {
			where += " and a.eeNumber = '"+eeNumber.toString()+"'";
		}
		
		if(idRole != null && idRole > 0){
			where += " and a.role.idRole = '"+idRole.toString()+"'";
		}
		
		String select = " select a from Employee a where 1=1 ";
		String selectCount = " select count(a) from Employee a where 1=1 ";

		Order order = pageable.getSort().iterator().next();
		String orderby = " order by a." + order.getProperty() + " " + order.getDirection();

		Query query = em.createQuery(select + where + orderby);
		Query queryCount = em.createQuery(selectCount + where);

		query.setMaxResults(pageable.getPageSize());
		query.setFirstResult(pageable.getOffset());

		List<Employee> lista = query.getResultList();

		int count = 0;
		count = ((Number) queryCount.getSingleResult()).intValue();

		return new PageImpl<Employee>(lista, pageable, count);
	}
	
	public List<Employee> managerList(){
		Query query = em.createQuery("select a from Employee a where a.role.manager = true");
		List<Employee> lista = query.getResultList();
		return lista;
	}
	
	public List<Employee> listEmployeeContainsEH(){
		String sql = " select e.id_employee, e.name from employee e "+
					 " inner join employee_holiday eh on e.id_employee = eh.id_employee "+
					 " where eh.hr_permit = true "+
					 " group by e.id_employee ";
		
		Query query = em.createNativeQuery(sql);
		List<Employee> lista = new ArrayList<>();
		List<Object[]> list = query.getResultList();
		
		for (Object[] o : list) {
			Employee e = new Employee();
			
			if(o[0] != null){
				e.setIdEmployee(Long.parseLong(o[0].toString()));
			}
			
			if(o[1] != null){
				e.setName(o[1].toString());
			}
			
			lista.add(e);
		}
		
		return lista;
	}
	
	public List<Employee> hrManagerList(){
		Query query = em.createQuery("select a from Employee a where a.role.hrManager = true");
		List<Employee> lista = query.getResultList();
		return lista;
	}
	
	public List<Employee> getAll(){
		Query query = em.createQuery("select a from Employee a where 1=1 ");
		List<Employee> lista = query.getResultList();
		return lista;
	}
	
	public List<Employee> listManagerByDepartament(Long id){
		String sql = "select e.id_employee, e.name, e.surname from employee as e "+
					 "inner join manager_department as md on md.id_employee = e.id_employee "+
					 "where e.manager = true and md.id_department = "+id.toString();
		
		Query query = em.createNativeQuery(sql);
		List<Employee> lista = new ArrayList<>();
		List<Object[]> list = query.getResultList();
		
		for (Object[] o : list) {
			Employee e = new Employee();
			
			if(o[0] != null){
				e.setIdEmployee(Long.parseLong(o[0].toString()));
			}
			
			if(o[1] != null){
				e.setName((o[1].toString()));
			}
			
			if(o[2] != null){
				e.setSurname((o[2].toString()));
			}
			
			lista.add(e);
		}
		
		return lista;
	}
	
	@Override
	public void insertEmployeeDepartment(List<Department> departments, Long id) {
		if(id != null && id > 0){
			for (Department d : departments) {
				String sql = "INSERT INTO employee_department(id_employee, id_department) "
						   + "VALUES ("+id.toString()+", "+d.getIdDepartment().toString()+")";
				Query query = em.createNativeQuery(sql);
				
				try {
					query.executeUpdate();
				} catch (Exception e) {
					// TODO: handle exception
					throw e;
				}
			}
		}
	}
	
	@Override
	public void deleteEmployeeDepartment(Long id) {
		if(id != null && id > 0){
			String sql = "DELETE FROM employee_department WHERE id_employee = "+id.toString();
			Query query = em.createNativeQuery(sql);
			
			try {
				query.executeUpdate();
			} catch (Exception e) {
				throw e;
			}
		}
	}
	
	@Override
	public Employee findByName(String employee) {
		String where = "";
		
		if(employee != null && !employee.equals("")) {
			where += " and UPPER(a.description) = '"+employee.toUpperCase()+"'";
		}
		
		String sql = " select a from Employee a where 1=1" + where;
		Query query = em.createQuery(sql);

		try{
			return (Employee)query.getSingleResult();
		}catch(NoResultException e){
			return null;	
		}
	}
	
	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(Long id) {
		if(id != null && id > 0){
			String sql = "UPDATE employee SET activate = false WHERE id_employee = "+id.toString();
			Query query = em.createNativeQuery(sql);
			
			try {
				query.executeUpdate();
			
			} catch (Exception e) {
				throw e;
			}
		}
	}

	@Override
	public void delete(Employee arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Iterable<? extends Employee> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean exists(Long arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<Employee> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<Employee> findAll(Iterable<Long> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Employee> S save(S arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Employee> Iterable<S> save(Iterable<S> arg0) {
		// TODO Auto-generated method stub
		return null;
	}
}

