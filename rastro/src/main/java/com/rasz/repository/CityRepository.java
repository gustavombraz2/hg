package com.rasz.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.rasz.domain.City;

public interface CityRepository extends CrudRepository<City, Long>{
	
	public void salvar(City city);
	
	public City findOne(Long id);
	
	Page<City> listarTodos(String description, Integer code, Pageable pageable);

	public List<City> listarTodos(String description, Integer code);
	
	public City findByName(String city);

}
