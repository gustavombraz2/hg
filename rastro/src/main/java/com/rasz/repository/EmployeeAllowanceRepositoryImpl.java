package com.rasz.repository;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.Contract;
import com.rasz.domain.Employee;
import com.rasz.domain.EmployeeAllowance;
import com.rasz.domain.EmployeeHoliday;
import com.rasz.utils.Utils;

public class EmployeeAllowanceRepositoryImpl implements EmployeeAllowanceRepository {
	
	@PersistenceContext private EntityManager em;

	@Override
	public Page<EmployeeAllowance> listarTodos(Long code, Long idAllowance, Long idEmployee, Pageable pageable){
		String where = "";
		
		if(code != null && code > 0) {
			where += " and a.idEmployeeAllowance = "+code.toString();
		}
		
		String select = " select a from EmployeeAllowance a where 1=1";
		String selectCount = " select count(a) from EmployeeAllowance a where 1=1";

		Order order = pageable.getSort().iterator().next();
		String orderby = " order by a." + order.getProperty() + " " + order.getDirection();

		Query query = em.createQuery(select + where + orderby);
		Query queryCount = em.createQuery(selectCount + where);

		query.setMaxResults(pageable.getPageSize());
		query.setFirstResult(pageable.getOffset());

		List<EmployeeAllowance> lista = query.getResultList();

		int count = 0;
		count = ((Number) queryCount.getSingleResult()).intValue();

		return new PageImpl<EmployeeAllowance>(lista, pageable, count);
	}
	
	@Transactional
	public void salvar(EmployeeAllowance employeeAllowance) {
		if(employeeAllowance.getIdEmployeeAllowance() != null && employeeAllowance.getIdEmployeeAllowance() > 0){
			em.merge(employeeAllowance);
		}else{
			em.persist(employeeAllowance);
		}
	}	
	
	@Override
	public List<EmployeeAllowance> findByEmployeeYear(Long id, Integer stDate, Integer endDate) {
		String sql = " select ea.id_employee_allowance, ea.current_year, e.id_employee "+
					 " from employee_allowance as ea "+
					 " inner join contract as c on c.id_contract = ea.id_contract "+
					 " inner join employee as e on e.id_employee = c.id_employee "+
					 " where ea.current_year in ("+stDate.toString()+", "+endDate.toString()+")"+
					 " and e.id_employee =  "+id.toString();
	
		Query query = em.createNativeQuery(sql);
		List<EmployeeAllowance> lista = new ArrayList<>();
		List<Object[]> list = query.getResultList();
		
		for (Object[] o : list) {
			EmployeeAllowance e = new EmployeeAllowance();
			Contract c = new Contract();
			
			if(o[0] != null){
				e.setIdEmployeeAllowance(Long.parseLong(o[0].toString()));
			}
			
			if(o[1] != null){
				e.setCurrentYear(Long.parseLong(o[1].toString()));
			}
			
			if(o[2] != null){
				c.setIdContract(Long.parseLong(o[2].toString()));
				e.setContract(c);
			}
			
			lista.add(e);
		}
		
		return lista;
	}
	
	@Override
	public List<EmployeeAllowance> listEmployeeAllowance(){
		String sql = " SELECT e.id_employee, "+    //00
					 " e.name, "+				   //01
					 " ea.current_year, "+		   //02
					 " ea.available_days, "+	   //03
					 " ea.total_days_counted, "+   //04
					 " ea.total_hours_counted, "+  //05
					 " eh.id_employeeHoliday, "+   //06
					 " eh.hr_permit, "+			   //07
					 " eh.day_counted, "+		   //08
					 " eh.hour_counted, "+		   //09
					 " co.id_contract "+		   //10
					 
					 " FROM employee_allowance ea "+
					 " INNER JOIN contract co ON co.id_contract = ea.id_contract "+
					 " INNER JOIN employee e ON e.id_employee = co.id_employee "+
					 " INNER JOIN employee_holiday eh ON eh.id_employee = e.id_employee "+
					 " WHERE eh.hr_permit = true "+
					 " GROUP BY e.id_employee, ea.current_year, ea.available_days, ea.total_days_counted, ea.total_hours_counted, eh.id_employeeHoliday, eh.hr_permit, eh.day_counted, eh.hour_counted, co.id_contract ";
	
		Query query = em.createNativeQuery(sql);
		List<EmployeeAllowance> lista = new ArrayList<>();
		List<Object[]> list = query.getResultList();
		
		for (Object[] o : list){
			EmployeeAllowance ea = new EmployeeAllowance();
			Contract c = new Contract();
			EmployeeHoliday eh = new EmployeeHoliday();
			Employee e = new Employee();
			
			if(o[0] != null){
				e.setIdEmployee(Long.parseLong(o[0].toString()));
			}
			
			if(o[1] != null){
				e.setName(o[1].toString());
			}
			
			if(o[2] != null){
				ea.setCurrentYear(Long.parseLong(o[2].toString()));
			}
			
			if(o[3] != null){
				ea.setAvailableDays(Double.parseDouble(o[3].toString()));
			}
			
			if(o[4] != null){
				ea.setTotalDaysCounted(Long.parseLong(o[4].toString()));
			}
			
			if(o[5] != null){
				ea.setTotalHoursCounted(Long.parseLong(o[5].toString()));
			}
			
			if(o[6] != null){
				eh.setIdEmployeeHoliday(Long.parseLong(o[6].toString()));
			}
			
			if(o[7] != null){
				eh.setHrPermit(Boolean.parseBoolean(o[7].toString()));
			}
			
			if(o[8] != null){
				eh.setDayCounted(Integer.parseInt(o[8].toString()));
			}
			
			if(o[9] != null){
				eh.setHourCounted(Integer.parseInt(o[9].toString()));
			}
			
			if(o[10] != null){
				c.setIdContract(Long.parseLong(o[10].toString()));
			}
			
			c.setEmployee(e);
			eh.setEmployee(e);
			ea.setContract(c);
			ea.setEmployeeHoliday(eh);
			
			lista.add(ea);
		}
		
		return lista;
	}
	
	@Override
	public EmployeeAllowance findByEmployeeYear(Long id, Long year) {
		String sql = " select ea from EmployeeAllowance ea "+
					 " inner join ea.contract c "+
					 " where ea.currentYear = "+year.toString()+
					 //" and ea.currentYear = "+year.toString()+
					 " and c.employee.idEmployee = "+id.toString();
	
		Query query = em.createQuery(sql);
		
		try {
			return (EmployeeAllowance) query.getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	@Transactional
	public void updateDays(Long id, Long year, Long days, Long hours) {
		String sql = " UPDATE employee_allowance "+
					 " SET total_days_counted = "+days.toString()+", total_hours_counted = "+hours.toString()+
					 " FROM contract, employee "+
					 " WHERE employee_allowance.id_contract = contract.id_contract "+
					 " AND contract.id_employee = employee.id_employee "+
					 " AND employee_allowance.current_year = "+year.toString()+
					 " AND employee.id_employee = "+id.toString()+";";
	
		Query query = em.createNativeQuery(sql);
		
		try {
			query.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(Utils.getStackTrace(e));
		}
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(Long arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(EmployeeAllowance arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Iterable<? extends EmployeeAllowance> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean exists(Long arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<EmployeeAllowance> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<EmployeeAllowance> findAll(Iterable<Long> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EmployeeAllowance findOne(Long id) {
		if(id != null && id > 0){
			String sql = "SELECT u FROM EmployeeAllowance u WHERE u.idEmployeeAllowance = "+id.toString();
			Query query = em.createQuery(sql);
			
			try {
				return (EmployeeAllowance) query.getSingleResult();
			
			} catch (Exception e) {
				// TODO: handle exception
				throw e;
			}
		}
		
		return null;
	}

	@Override
	public <S extends EmployeeAllowance> S save(S arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends EmployeeAllowance> Iterable<S> save(Iterable<S> arg0) {
		// TODO Auto-generated method stub
		return null;
	}
}

