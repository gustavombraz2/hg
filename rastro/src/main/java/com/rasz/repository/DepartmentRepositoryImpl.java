package com.rasz.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.Contract;
import com.rasz.domain.Department;
import com.rasz.domain.Employee;
import com.rasz.domain.EmployeeHoliday;
import com.rasz.domain.HolidayDay;
import com.rasz.domain.Role;
import com.rasz.utils.Utils;

public class DepartmentRepositoryImpl implements DepartmentRepository {
	
	@PersistenceContext private EntityManager em;

	@Override
	public Page<Department> listarTodos(String description, Integer code, Pageable pageable){
		String where = "";
		
		if(description != null && !description.equals("")) {
			where += " and UPPER(a.description) like '%"+description.toUpperCase()+"%' ";
		}
		
		if(code != null && code > 0) {
			where += " and a.idDepartment = "+code.toString();
		}
		
		String select = " select a from Department a where 1=1 and a.activate = true";
		String selectCount = " select count(a) from Department a where 1=1 and a.activate = true";

		Order order = pageable.getSort().iterator().next();
		String orderby = " order by a." + order.getProperty() + " " + order.getDirection();

		Query query = em.createQuery(select + where + orderby);
		Query queryCount = em.createQuery(selectCount + where);

		query.setMaxResults(pageable.getPageSize());
		query.setFirstResult(pageable.getOffset());

		List<Department> lista = query.getResultList();

		int count = 0;
		count = ((Number) queryCount.getSingleResult()).intValue();

		return new PageImpl<Department>(lista, pageable, count);
	}
	
	@Override
	public List<Department> listarTodos(String description, Integer code){
		String where = "";
		
		if(description != null && !description.equals("")) {
			where += " and UPPER(a.description) like '%"+description.toUpperCase()+"%' ";
		}
		
		if(code != null && code > 0) {
			where += " and a.idDepartment = "+code.toString();
		}
		
		String select = " select a from Department a where 1=1 and a.activate = true";

		Query query = em.createQuery(select + where);
		List<Department> lista = query.getResultList();

		return lista;
	}
	
	@Override
	public List<Department> listaByEmployee(Long id){
		String select = " select d.id_department, d.activate, d.description, d.maximum_days, d.maximum_employees from department d "+
						" inner join employee_department ed on ed.id_department = d.id_department "+
						" where d.activate = true and ed.id_employee = "+id.toString();

		Query query = em.createNativeQuery(select);
		List<Department> lista = new ArrayList<>();
		List<Object[]> list = query.getResultList();
		
		for (Object[] o : list) {
			Department d = new Department();
			
			if(o[0] != null){
				d.setIdDepartment(Long.parseLong(o[0].toString()));
			}
			
			if(o[1] != null){
				d.setActivate(Boolean.parseBoolean(o[1].toString()));
			}
			
			if(o[2] != null){
				d.setDescription(o[2].toString());
			}
			
			if(o[3] != null){
				d.setMaximumDays(Long.parseLong(o[3].toString()));
			}
			
			if(o[4] != null){
				d.setMaximumEmployees(Long.parseLong(o[4].toString()));
			}
			
			lista.add(d);
		}
		
		return lista;
	}

	@Transactional
	public Department salvar(Department department) {
		if(department.getIdDepartment() != null && department.getIdDepartment() > 0){
			em.merge(department);
		}else{
			em.persist(department);
		}
		return department;
	}
	
	@Override
	public Department findOne(Long id) {
		if(id != null && id > 0){
			String sql = "SELECT u FROM Department u WHERE u.idDepartment = "+id.toString();
			Query query = em.createQuery(sql);
			
			try {
				return (Department) query.getSingleResult();
			} catch (Exception e) {
				// TODO: handle exception
				throw e;
			}
		}
		
		return null;
	}
	
	@Override
	public void insertManagerDepartment(List<Employee> managers, Long id) {
		if(id != null && id > 0){
			for (Employee emp : managers) {
				String sql = "INSERT INTO manager_department(id_employee, id_department) "
						   + "VALUES ("+emp.getIdEmployee().toString()+", "+id.toString()+")";
				Query query = em.createNativeQuery(sql);
				
				try {
					query.executeUpdate();
				} catch (Exception e) {
					// TODO: handle exception
					throw e;
				}
			}
		}
	}
	
	@Override
	public void deleteManagerDepartment(Long id) {
		if(id != null && id > 0){
			String sql = "DELETE FROM manager_department WHERE id_department = "+id.toString();
			Query query = em.createNativeQuery(sql);
			
			try {
				query.executeUpdate();
			} catch (Exception e) {
				throw e;
			}
		}
	}
	
	@Override
	public Department findByName(String department) {
		String where = "";
		
		if(department != null && !department.equals("")) {
			where += " and UPPER(a.description) = '"+department.toUpperCase()+"'";
		}
		
		String sql = " select a from Department a where 1=1" + where;
		Query query = em.createQuery(sql);

		try{
			return (Department)query.getSingleResult();
		}catch(NoResultException e){
			return null;	
		}
	}
	
	@Override
	public List<Department> listHolidayByDayDepartment(Date day, String ids) {
		String sql = " select d.id_department, d.description, count(hd.id_holiday_day) as holiday_per_day, d.maximum_days, d.maximum_employees "+
					 " from holiday_day hd                                                                                    "+
					 " inner join employee_holiday eh on eh.id_employeeholiday = hd.id_employeeholiday                        "+
					 " inner join employee e on e.id_employee = eh.id_employee                                                "+
					 " inner join employee_department ed on ed.id_employee = e.id_employee                                    "+
					 " inner join department d on d.id_department = ed.id_department                                          "+
					 " where hd.count = true                                                                                  "+
					 " and eh.hr_permit = true                                                                                "+
					 " and hd.day = '"+Utils.formataDataStringSemHora(day)+"'"+
					 " and d.id_department in ("+ids+")"+
					 " group by d.id_department, d.description, d.maximum_days, d.maximum_employees                                            ";
				
		Query query = em.createNativeQuery(sql);
		List<Department> lista = new ArrayList<>();
		List<Object[]> list = query.getResultList();
		
		for (Object[] o : list) {
			Department d = new Department();
			
			if(o[0] != null){
				d.setIdDepartment(Long.parseLong(o[0].toString()));
			}
			
			if(o[1] != null){
				d.setDescription(o[1].toString());
			}
			
			if(o[2] != null){
				d.setHolidayPerDay(Long.parseLong(o[2].toString()));
			}
			
			if(o[3] != null){
				d.setMaximumDays(Long.parseLong(o[3].toString()));
			}
			
			if(o[4] != null){
				d.setMaximumEmployees(Long.parseLong(o[4].toString()));
			}
			
			lista.add(d);
		}
		
		return lista;
	}
	
	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(Long id) {
		if(id != null && id > 0){
			String sql = "UPDATE department SET activate = false WHERE id_department = "+id.toString();
			Query query = em.createNativeQuery(sql);
			
			try {
				query.executeUpdate();
			
			} catch (Exception e) {
				throw e;
			}
		}
	}

	@Override
	public void delete(Department arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Iterable<? extends Department> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean exists(Long arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<Department> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<Department> findAll(Iterable<Long> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Department> S save(S arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Department> Iterable<S> save(Iterable<S> arg0) {
		// TODO Auto-generated method stub
		return null;
	}
}

