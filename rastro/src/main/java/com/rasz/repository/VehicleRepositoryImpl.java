package com.rasz.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.Vehicle;

public class VehicleRepositoryImpl implements VehicleRepository {
	
	@PersistenceContext private EntityManager em;

	@Override
	public Page<Vehicle> listarTodos(String registration, Integer code, Pageable pageable){
		String where = "";
		
		if(registration != null && !registration.equals("")) {
			where += " and UPPER(a.registration) like '%"+registration.toUpperCase()+"%' ";
		}
		
		if(code != null && code > 0) {
			where += " and a.idVehicle = "+code.toString();
		}
		
		String select = " select a from Vehicle a where 1=1 and a.activate = true";
		String selectCount = " select count(a) from Vehicle a where 1=1 and a.activate = true";

		Order order = pageable.getSort().iterator().next();
		String orderby = " order by a." + order.getProperty() + " " + order.getDirection();

		Query query = em.createQuery(select + where + orderby);
		Query queryCount = em.createQuery(selectCount + where);

		query.setMaxResults(pageable.getPageSize());
		query.setFirstResult(pageable.getOffset());

		List<Vehicle> lista = query.getResultList();

		int count = 0;
		count = ((Number) queryCount.getSingleResult()).intValue();

		return new PageImpl<Vehicle>(lista, pageable, count);
	}

	@Transactional
	public void salvar(Vehicle vehicle) {
		if(vehicle.getIdVehicle() != null && vehicle.getIdVehicle() > 0){
			em.merge(vehicle);
		}else{
			em.persist(vehicle);
		}
	}
	
	@Override
	public Vehicle findOne(Long id) {
		if(id != null && id > 0){
			String sql = "SELECT u FROM Vehicle u WHERE u.idVehicle = "+id.toString();
			Query query = em.createQuery(sql);
			
			try {
				return (Vehicle) query.getSingleResult();
			
			} catch (Exception e) {
				// TODO: handle exception
				throw e;
			}
		}
		
		return null;
	}
	
	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(Long id) {
		if(id != null && id > 0){
			String sql = "UPDATE vehicle SET activate = false WHERE id_vehicle = "+id.toString();
			Query query = em.createNativeQuery(sql);
			
			try {
				query.executeUpdate();
			
			} catch (Exception e) {
				throw e;
			}
		}
	}

	@Override
	public void delete(Vehicle arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Iterable<? extends Vehicle> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean exists(Long arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<Vehicle> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<Vehicle> findAll(Iterable<Long> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Vehicle> S save(S arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Vehicle> Iterable<S> save(Iterable<S> arg0) {
		// TODO Auto-generated method stub
		return null;
	}
}

