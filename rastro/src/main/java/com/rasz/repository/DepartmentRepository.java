package com.rasz.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.rasz.domain.Department;
import com.rasz.domain.Employee;

public interface DepartmentRepository extends CrudRepository<Department, Long>{
	
	public Department salvar(Department department);
	
	public Department findOne(Long id);
	
	public void delete(Long id);
	
	Page<Department> listarTodos(String description, Integer code, Pageable pageable);
	
	public Department findByName(String department);
	
	public List<Department> listarTodos(String description, Integer code);
	
	public void insertManagerDepartment(List<Employee> managers, Long id);
	
	public void deleteManagerDepartment(Long id);
	
	public List<Department> listHolidayByDayDepartment(Date day, String ids);
	
	public List<Department> listaByEmployee(Long id);

}
