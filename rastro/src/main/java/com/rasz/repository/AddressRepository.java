package com.rasz.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.rasz.domain.Address;

public interface AddressRepository extends CrudRepository<Address, Long>{
	
	public void salvar(Address address);
	
	public Address findOne(Long id);
	
	Page<Address> listarTodos(String description, Integer code, Pageable pageable);

	public List<Address> listarTodos(String description, Integer code);
	
	public Address findByName(String address);
	
	public Address findByEmployee(Long id);

}
