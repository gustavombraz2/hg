package com.rasz.service;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.Allowance;
import com.rasz.repository.AllowanceRepository;

	
@Service
public class AllowanceService {

	@Autowired private AllowanceRepository allowanceRepository;
	@PersistenceContext EntityManager em;
		
	boolean permissaoSalvar;
	private String description;
	private List<String> weekDayList = new ArrayList<>();
	private Allowance allowance = new Allowance();
	
	public Allowance findOne(Long id) {
		return allowanceRepository.findOne(id);
	}
	
	public void loadWDList() {
		weekDayList = new ArrayList<>();
		weekDayList.add(DayOfWeek.MONDAY.toString());
		weekDayList.add(DayOfWeek.TUESDAY.toString());
		weekDayList.add(DayOfWeek.WEDNESDAY.toString());
		weekDayList.add(DayOfWeek.THURSDAY.toString());
		weekDayList.add(DayOfWeek.FRIDAY.toString());
		weekDayList.add(DayOfWeek.SATURDAY.toString());
		weekDayList.add(DayOfWeek.SUNDAY.toString());
	}
	
	public Allowance editar(long id) {
		permissaoSalvar = true;
		allowance = allowanceRepository.findOne(id);
		loadWDList();
		return allowance;
	}
	public Allowance consultar(long id) {
		permissaoSalvar = false;
		allowance = allowanceRepository.findOne(id);
		loadWDList();
		return allowance;
	}

	public List<Allowance> getAll() {
		return (List<Allowance>) allowanceRepository.findAll();
	}
	
	public List<Allowance> listarPorTipoAllowance() {
		return (List<Allowance>) allowanceRepository.findAll();
	}
	
	public Allowance create() {
		permissaoSalvar = true;
		allowance = new Allowance();
		loadWDList();
		return allowance;
	}

	@Transactional
	public void remove(Long id) {
		if(id != null && id > 0){
			allowanceRepository.delete(id);
		}
	}

	@Transactional
	public boolean save(Allowance allowance) {
		allowanceRepository.salvar(allowance);
		return true;
	}

	public boolean isPermissaoSalvar() {
		return permissaoSalvar;
	}

	public void setPermissaoSalvar(boolean permissaoSalvar) {
		this.permissaoSalvar = permissaoSalvar;
	}

	public Allowance getAllowance() {
		return allowance;
	}

	public void setAllowance(Allowance allowance) {
		this.allowance = allowance;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getWeekDayList() {
		return weekDayList;
	}

	public void setWeekDayList(List<String> weekDayList) {
		this.weekDayList = weekDayList;
	}

}
