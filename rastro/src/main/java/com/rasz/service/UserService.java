package com.rasz.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.rasz.domain.Perfil;
import com.rasz.domain.Usuario;
import com.rasz.repository.PerfilRepository;
import com.rasz.utils.TipoUsuarioAuthenticationFilter;

// End of user code

	
@Service
public class UserService implements UserDetailsService {

	@Autowired private PerfilRepository perfilRepository;
	@Autowired private UsuarioService usuarioService;
	@Autowired private TipoUsuarioAuthenticationFilter authenticationFilter;

	public UserDetails loadUserByUsername(String arg0) throws UsernameNotFoundException {
		String tipoUsuario = authenticationFilter.getTipoUsuario();
		Usuario user = usuarioService.findByLogin(arg0);
		if (user == null) {
			throw new UsernameNotFoundException("Usu�rio n�o encontrado.");
		}
		
		Perfil perfil = perfilRepository.localizarPorUsuario(user.getId());
		user.setPerfil(perfil);
		
		return new com.rasz.bean.UserDetails(user);
	}

}
