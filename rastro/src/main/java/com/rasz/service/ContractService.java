package com.rasz.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.Allowance;
import com.rasz.domain.Contract;
import com.rasz.domain.Employee;
import com.rasz.repository.AllowanceRepository;
import com.rasz.repository.ContractRepository;
import com.rasz.repository.EmployeeRepository;

	
@Service
public class ContractService {

	@Autowired private ContractRepository contractRepository;
	@Autowired private EmployeeRepository employeeRepository;
	@Autowired private AllowanceRepository allowanceRepository;
	@PersistenceContext EntityManager em;
	
	Contract contract = new Contract();
	Employee employee = new Employee();
	Allowance allowance = new Allowance();
	List<Employee> employeeList = new ArrayList<>();
	List<Allowance> allowanceList = new ArrayList<>();
		
	boolean permissaoSalvar;
	
	public List<String> completeEmployee(String busca) {
		List<String> resultados = new ArrayList<String>();
		
		for (Employee employee : employeeList) {
			if (employee.getFullName().toLowerCase().contains(busca.toLowerCase())) {
    			resultados.add(employee.toString());
			}
		}
		return resultados;
	}
	
	public List<String> completeAllowance(String busca) {
		List<String> resultados = new ArrayList<String>();
		
		for (Allowance allowance : allowanceList) {
			if (allowance.getDescription().toLowerCase().contains(busca.toLowerCase())) {
    			resultados.add(allowance.toString());
			}
		}
		return resultados;
	}

	public Contract findOne(Long id) {
		return contractRepository.findOne(id);
	}

	public Contract load(){
		permissaoSalvar = true;
		employeeList = employeeRepository.getAll();
		allowanceList = allowanceRepository.listarTodos("", 0);
		employee = contract.getEmployee();
		allowance = contract.getAllowance();
		
		return contract;
	}
	
	public Contract create(){
		contract = new Contract();
		load();
		return contract;
	}

	public Contract editar(long id){
		contract = contractRepository.findOne(id);
		load();
		return contract;
	}
	public Contract consultar(long id) {
		permissaoSalvar = false;
		contract = contractRepository.findOne(id);
		return contract;
	}

	public List<Contract> getAll() {
		return (List<Contract>) contractRepository.findAll();
	}
	
	
	public List<Contract> listarPorTipoContract() {
		return (List<Contract>) contractRepository.findAll();
	}
	
	@Transactional
	public boolean save(Contract contract){
		if(allowance != null){
			contract.setAllowance(allowance);
		}
		
		if(employee != null){
			contract.setEmployee(employee);
		}
		
		contractRepository.salvar(contract);
		return true;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Allowance getAllowance() {
		return allowance;
	}

	public void setAllowance(Allowance allowance) {
		this.allowance = allowance;
	}

	public boolean isPermissaoSalvar() {
		return permissaoSalvar;
	}

	public void setPermissaoSalvar(boolean permissaoSalvar) {
		this.permissaoSalvar = permissaoSalvar;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}


}
