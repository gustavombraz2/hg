package com.rasz.service;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.Department;
import com.rasz.domain.Employee;
import com.rasz.domain.Role;
import com.rasz.repository.DepartmentRepository;
import com.rasz.repository.EmployeeRepository;

	
@Service
public class DepartmentService {

	@Autowired private DepartmentRepository departmentRepository;
	@Autowired private EmployeeRepository employeeRepository;
	@PersistenceContext EntityManager em;
		
	boolean permissaoSalvar;
	private String description;
	private Department department = new Department();
	private Employee manager = new Employee();
	private List<Employee> managerList = new ArrayList<>();
	private List<Employee> managerListAC = new ArrayList<>();
	
	public Department findOne(Long id) {
		return departmentRepository.findOne(id);
	}
	
	public Department editar(long id) {
		managerList = new ArrayList<>();
		manager = new Employee();
		listarManagers();
		permissaoSalvar = true;
		department = departmentRepository.findOne(id);
		managerList = employeeRepository.listManagerByDepartament(id);
		return department;
	}
	public Department consultar(long id) {
		manager = new Employee();
		managerList = new ArrayList<>();
		permissaoSalvar = false;
		department = departmentRepository.findOne(id);
		managerList = employeeRepository.listManagerByDepartament(id);
		return department;
	}

	public List<Department> getAll() {
		return (List<Department>) departmentRepository.findAll();
	}
	
	public List<Department> listarPorTipoDepartment() {
		return (List<Department>) departmentRepository.findAll();
	}
	
	public Department create() {
		managerList = new ArrayList<>();
		listarManagers();
		permissaoSalvar = true;
		department = new Department();
		department.setActivate(true);
		return department;
	}

	@Transactional
	public boolean remove(Long id) {
		if(id != null && id > 0){
			departmentRepository.delete(id);
			return true;
		}else{
			return false;
		}
	}
	
	public List<String> completeEmployee(String busca) {
		List<String> resultados = new ArrayList<String>();
		
		for (Employee e : managerListAC) {
			if (e.getFullName().toLowerCase().contains(busca.toLowerCase())) {
    			resultados.add(e.toString());
			}
		}
		return resultados;
	}
	
	public List<Employee> listarManagers() {
		managerListAC = employeeRepository.managerList();
		return managerListAC;
	}
	
	public void addManager() {
		if(manager != null){
			if(manager.getIdEmployee() != null && manager.getIdEmployee() > 0){
				if(managerList.size() > 0){
					boolean e = false;
					for (Employee em : managerList) {
						if(manager.getIdEmployee() == em.getIdEmployee()){
							e = true;
						}
					}
					
					if(e){
						FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Warn!", "This Manager has already been selected.");
						FacesContext.getCurrentInstance().addMessage(null, message);
					}else{
						managerList.add(manager);
					}
				}else{
					managerList.add(manager);
				}
			}
 		}else{
 			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Warn!", "This Manager has already been selected.");
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		
		setManager(null);
	}
	
	public void removeManager(Employee manager) {
		if(manager != null){
			if(manager.getIdEmployee() != null && manager.getIdEmployee() > 0){
				if(managerList.size() > 0){
					for (Employee e : managerList) {
						if(manager.getIdEmployee() == e.getIdEmployee()){
							managerList.remove(e);
							break;
						}
					}
				}
			}
		}
	}

	@Transactional
	public boolean save(Department department) {
		if(department.getIdDepartment() == null || department.getIdDepartment() == 0){
			department.setActivate(true);
		}
		
		department = departmentRepository.salvar(department);
		departmentRepository.deleteManagerDepartment(department.getIdDepartment());
		if(managerList.size() > 0){
			departmentRepository.insertManagerDepartment(managerList, department.getIdDepartment());
		}
		
		return true;
	}

	public boolean isPermissaoSalvar() {
		return permissaoSalvar;
	}

	public void setPermissaoSalvar(boolean permissaoSalvar) {
		this.permissaoSalvar = permissaoSalvar;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Employee getManager() {
		return manager;
	}

	public void setManager(Employee manager) {
		this.manager = manager;
	}

	public List<Employee> getManagerList() {
		return managerList;
	}

	public void setManagerList(List<Employee> managerList) {
		this.managerList = managerList;
	}

	public List<Employee> getManagerListAC() {
		return managerListAC;
	}

	public void setManagerListAC(List<Employee> managerListAC) {
		this.managerListAC = managerListAC;
	}
	
}
