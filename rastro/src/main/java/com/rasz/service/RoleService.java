package com.rasz.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.Role;
import com.rasz.repository.RoleRepository;

	
@Service
public class RoleService {

	@Autowired private RoleRepository roleRepository;
	@PersistenceContext EntityManager em;
		
	boolean permissaoSalvar;
	private String description;
	private Role role = new Role();
	
	public Role findOne(Long id) {
		return roleRepository.findOne(id);
	}
	
	public Role editar(long id) {
		permissaoSalvar = true;
		role = roleRepository.findOne(id);
		return role;
	}
	public Role consultar(long id) {
		permissaoSalvar = false;
		role = roleRepository.findOne(id);
		return role;
	}

	public List<Role> getAll() {
		return (List<Role>) roleRepository.findAll();
	}
	
	public List<Role> listarPorTipoRole() {
		return (List<Role>) roleRepository.findAll();
	}
	
	public Role create() {
		permissaoSalvar = true;
		role = new Role();
		role.setActivate(true);
		return role;
	}

	@Transactional
	public boolean remove(Long id) {
		if(id != null && id > 0){
			roleRepository.delete(id);
			return true;
		}else{
			return false;
		}
	}

	@Transactional
	public boolean save(Role role) {
		roleRepository.salvar(role);
		return true;
	}

	public boolean isPermissaoSalvar() {
		return permissaoSalvar;
	}

	public void setPermissaoSalvar(boolean permissaoSalvar) {
		this.permissaoSalvar = permissaoSalvar;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
