package com.rasz.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.Privilegio;
import com.rasz.repository.PrivilegioRepository;

	
@Service
public class PrivilegioService {

	@Autowired private PrivilegioRepository autorizacaoRepository;

	public Privilegio findByNome(String nome) {
		return autorizacaoRepository.findByNome(nome);
	}

	public Privilegio findByDescricao(String descricao) {
		return autorizacaoRepository.findByDescricao(descricao);
	}

	public Privilegio findOne(Long id) {
		return autorizacaoRepository.findOne(id);
	}

	public List<Privilegio> getAll() {
		return (List<Privilegio>)autorizacaoRepository.findAll()	;
	}

	public Privilegio create() {
		Privilegio autorizacao = new Privilegio();
		return autorizacao;
	}

	public Page<Privilegio> searchAutorizacoes(String searchString, Pageable pageable) {
		if (searchString != null) {
			searchString = "%" + searchString + "%";
		}
		return autorizacaoRepository.searchAutorizacoes(searchString, pageable);
	}

	public void remove(Privilegio autorizacao) {
		Privilegio autorizacaoBd = autorizacaoRepository.findOne(autorizacao.getId());
		autorizacaoRepository.delete(autorizacaoBd);
	}	
	
	@Transactional
	public void save(Privilegio autorizacao) {
		autorizacaoRepository.save(autorizacao);
	}


}
