package com.rasz.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.BankHoliday;
import com.rasz.domain.Contract;
import com.rasz.domain.Department;
import com.rasz.domain.Employee;
import com.rasz.domain.EmployeeAllowance;
import com.rasz.domain.EmployeeHoliday;
import com.rasz.domain.HolidayCategory;
import com.rasz.domain.HolidayDay;
import com.rasz.repository.BankHolidayRepository;
import com.rasz.repository.ContractRepository;
import com.rasz.repository.DepartmentRepository;
import com.rasz.repository.EmployeeAllowanceRepository;
import com.rasz.repository.EmployeeHolidayRepository;
import com.rasz.repository.EmployeeRepository;
import com.rasz.repository.HolidayCategoryRepository;
import com.rasz.repository.HolidayDayRepository;
import com.rasz.utils.Utils;

	
@Service
public class EmployeeHolidayService {

	@Autowired private EmployeeHolidayRepository employeeHolidayRepository;
	@Autowired private EmployeeAllowanceRepository employeeAllowanceRepository;
	@Autowired private EmployeeRepository employeeRepository;
	@Autowired private HolidayCategoryRepository holidayCategoryRepository;
	@Autowired private ContractRepository contractRepository;
	@Autowired private HolidayDayRepository holidayDayRepository;
	@Autowired private DepartmentRepository departmentRepository;
	@Autowired private BankHolidayRepository bankHolidayRepository;
	@PersistenceContext EntityManager em;
		
	boolean permissaoSalvar;
	boolean disabledDaysHours;
	private EmployeeHoliday employeeHoliday = new EmployeeHoliday();
	private Employee employee = new Employee();
	List<Employee> employeeList = new ArrayList<>();
	private HolidayCategory holidayCategory = new HolidayCategory();
	List<HolidayCategory> holidayCategoryList = new ArrayList<>();
	private Employee manager = new Employee();
	List<Employee> managerList = new ArrayList<>();
	private Employee hrManager = new Employee();
	List<Employee> hrManagerList = new ArrayList<>();
	List<HolidayDay> listHolidayDay = new ArrayList<>(); 
	long currentSaveCode = 0;
	
	boolean disabledEndDate;
	boolean disabledStartDate;
	boolean disabledEmployee;
	boolean disabledHC;
	boolean disabledSetDayOff;
	
	public EmployeeHoliday findOne(Long id) {
		return employeeHolidayRepository.findOne(id);
	}
	
	public void load(long id){
		employeeHoliday = employeeHolidayRepository.findOne(id);
		setHolidayCategory(employeeHoliday.getHolidayCategory());
		setEmployee(employeeHoliday.getEmployee());
		setManager(employeeHoliday.getManagerPermission());
		setHrManager(employeeHoliday.getHrPermission());
		employeeList = employeeRepository.getAll();
		holidayCategoryList = holidayCategoryRepository.listarTodos("", 0);
		managerList = employeeRepository.managerList();
		hrManagerList = employeeRepository.hrManagerList();
		currentSaveCode = holidayDayRepository.getSaveCode(id);
		getBalance(false);
		disabledControl(false);
		loadListHolidayDay(id);
	}
	
	public EmployeeHoliday editar(long id) {
		permissaoSalvar = true;
		disabledControl(false);
		clean();
		load(id);
		updateEmployeeAllowance();
		return employeeHoliday;
	}
	
	public EmployeeHoliday consultar(long id) {
		permissaoSalvar = false;
		disabledControl(false);
		clean();
		load(id);
		updateEmployeeAllowance();
		return employeeHoliday;
	}
	
	public EmployeeHoliday create() {
		permissaoSalvar = true;
		clean();
		disabledControl(false);
		employeeList = employeeRepository.getAll();
		managerList = employeeRepository.managerList();
		hrManagerList = employeeRepository.hrManagerList();
		holidayCategoryList = holidayCategoryRepository.listarTodos("", 0);
		listHolidayDay = new ArrayList<>();
		updateEmployeeAllowance();
		
		return employeeHoliday;
	}
	
	public void updateEmployeeAllowance() {
		if(employeeHoliday != null){
			if(employeeHoliday.getEmployee() != null){
				List<Long> years = new ArrayList<>();
				years.add((long)Utils.getDateYear(new Date()) - 1);
				years.add((long)Utils.getDateYear(new Date()));
				years.add((long)Utils.getDateYear(new Date()) + 1);
				for (Long y : years) {
					List<HolidayDay> listDaysByEmployeeYear = holidayDayRepository.countHoursDaysByEmployeeYear(employeeHoliday.getEmployee().getIdEmployee().toString(), y);
					for (HolidayDay hd : listDaysByEmployeeYear) {
						long d = 0;
						long h = 0;
						if(hd.getEmployeeHoliday().getCountedDays() != null){
							d = hd.getEmployeeHoliday().getCountedDays();
						}
						
						if(hd.getEmployeeHoliday().getCountedHours() != null){
							h = hd.getEmployeeHoliday().getCountedHours()/2;
						}
						employeeAllowanceRepository.updateDays(employeeHoliday.getEmployee().getIdEmployee(), y, d, h);
					}
				}
			}
		}
	}
	
	public void getBalance(boolean validadePeriod) {
		disabledControl(validadePeriod);
		setDisabledDaysHours();
		
		if(employee != null){
			if(employee.getIdEmployee() != null){
				Contract c = contractRepository.findByEmployee(employee.getIdEmployee());
				if(c == null){
					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Warn!", "This Employee has no contract.");
					FacesContext.getCurrentInstance().addMessage(null, message);
					setEmployee(null);
				}else{
					List<HolidayDay> hd = holidayDayRepository.countHoursDaysByEmployeeYear(employee.getIdEmployee().toString(), (long)Utils.getDateYear(new Date()));
					for (HolidayDay holidayDay : hd) {
						employeeHoliday.setBalance(holidayDay.getEmployeeHoliday().getBalance());
						employeeHoliday.setWorkingHours(holidayDay.getEmployeeHoliday().getWorkingHours());
						break;
					}
				}
			}
		}
		
		if(validadePeriod)
			validatePeriod(employeeHoliday);
	}
	
	public void setDisabledDaysHours(){
		setDisabledDaysHours(true);
		if(holidayCategory != null){
			if(holidayCategory.getIdHolidayCategory() != null && holidayCategory.getIdHolidayCategory() > 0){
				if(holidayCategory.getReckon().equals("H")){
					setDisabledDaysHours(false);
				}
			}
		}
	}
	
	public void clean() {
		employeeHoliday = new EmployeeHoliday();
		employee = new Employee();
		holidayCategory = new HolidayCategory();
		manager = new Employee();
		hrManager = new Employee();
	}
	
	public void partTime(boolean validatePeriod){
		if(holidayCategory != null){
			if(holidayCategory.getReckon() != null && !holidayCategory.getReckon().equals("")){
				if(holidayCategory.getReckon().equals("H")){
					setDisabledEndDate(true);
					employeeHoliday.setEndDate(employeeHoliday.getStartDate());
				}else{
					setDisabledEndDate(false);
					listHolidayDay = new ArrayList<>();
				}
				
				if(validatePeriod)
					validatePeriod(employeeHoliday);
			}
		}
		
		setDisabledDaysHours();
	}
	
	public void hourDayControl() {
		if(holidayCategory != null){
			if(holidayCategory.getIdHolidayCategory() != null && holidayCategory.getIdHolidayCategory() > 0){
				if(holidayCategory.getReckon() != null && !holidayCategory.getReckon().equals("")){
					if(holidayCategory.getReckon().equals("H")){
						
					}
					
					if(holidayCategory.getReckon().equals("D")){
						
					}
				}
			}
		}
	}

	public List<EmployeeHoliday> getAll() {
		return (List<EmployeeHoliday>) employeeHolidayRepository.findAll();
	}
	
	public List<EmployeeHoliday> listarPorTipoEmployeeHoliday() {
		return (List<EmployeeHoliday>) employeeHolidayRepository.findAll();
	}
	
	

	@Transactional
	public void remove(Long id) {
		if(id != null && id > 0){
			employeeHolidayRepository.delete(id);
		}
	}
	
	public List<String> completeEmployee(String busca) {
		List<String> resultados = new ArrayList<String>();
		
		for (Employee employee : employeeList) {
			if (employee.getFullName().toLowerCase().contains(busca.toLowerCase())) {
    			resultados.add(employee.toString());
			}
		}
		return resultados;
	}
	
	public boolean requiredManagerAC(){
		if(employeeHoliday.getManagerPermit() != null){
			if(employeeHoliday.getManagerPermissionDate() == null){
				employeeHoliday.setManagerPermissionDate(new Date());
			}
			
			return true;
		}else{
			return false;
		}
	}
	
	public boolean requiredHrManagerAC(){
		if(employeeHoliday.getHrPermit() != null){
			if(employeeHoliday.getHrPermissionDate() == null){
				employeeHoliday.setHrPermissionDate(new Date());
			}
			
			return true;
		}else{
			return false;
		}
	}
	
	public void validatePeriod(EmployeeHoliday eh){
		if(holidayCategory != null){
			if(holidayCategory.getIdHolidayCategory() != null && holidayCategory.getIdHolidayCategory() > 0){
				if(holidayCategory.getReckon().equals("D") || holidayCategory.getReckon().equals("N")){
					if(eh.getStartDate() != null && eh.getEndDate() != null){
						if(eh.getStartDate().getTime() > eh.getEndDate().getTime()){
							FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Warn!", "End Date must be equal to or later than Start Date.");
							FacesContext.getCurrentInstance().addMessage(null, message);
							eh.setEndDate(null);
						}else{
							if(employee != null){
								createHolidayDay();
							}
							
							int x = 0;
							for (HolidayDay hd : listHolidayDay) {
								if(hd.isCount()){
									x += 1;
								}
							}
							employeeHoliday.setDayCounted(x);
						}
					}
				}
				
				if(holidayCategory.getReckon().equals("H")){
					employeeHoliday.setEndDate(employeeHoliday.getStartDate());
					
					if(employee != null){
						createHolidayDay();
					}
					
					Contract c = contractRepository.findByEmployee(employee.getIdEmployee());
					long x = c.getAllowance().getWorkingHours() / 2; 
					
					employeeHoliday.setHourCounted((int)x);
				}
			}
		}
	}
	
	public void loadListHolidayDay(Long id){
		listHolidayDay = holidayDayRepository.findByEmployeeHolidayDay(id);
		
		if(holidayCategory.getReckon().equals("D") || holidayCategory.getReckon().equals("N")){
			int x = 0;
			for (HolidayDay hd : listHolidayDay) {
				if(hd.isCount()){
					x += 1;
				}
			}
			employeeHoliday.setDayCounted(x);
		}
		
		if(holidayCategory.getReckon().equals("H")){
			employeeHoliday.setEndDate(employeeHoliday.getStartDate());
			
			Contract c = contractRepository.findByEmployee(employee.getIdEmployee());
			long x = c.getAllowance().getWorkingHours() / 2; 
			
			employeeHoliday.setHourCounted((int)x);
		}
	}
	
	public String StringDayHour(HolidayDay hd){
		String s = "";
		
		if(holidayCategory != null){
			if(holidayCategory.getReckon().equals("H")){
				if(hd != null){
					if(hd.isCount()){
						if(employeeHoliday.getHourCounted() != null){
							if(employeeHoliday.getHourCounted() == 1){
								s = "1 hour";
							}else{
								s = employeeHoliday.getHourCounted().toString()+" hours";
							}
						}
					}else{
						s = "0 hour";
					}
				}
			}else{
				if(hd != null){
					if(hd.isCount()){
						s = "1 day";
					}else{
						s = "0 day";
					}
				}
			}
		}
		
		return s;
	}
	
	public void setTrue(){
		setDisabledEmployee(true);
		setDisabledStartDate(true);
		setDisabledEndDate(true);
		setDisabledHC(true);
		setDisabledSetDayOff(true);
	}
	
	public void setFalse(){
		setDisabledEmployee(false);
		setDisabledStartDate(false);
		setDisabledEndDate(false);
		setDisabledHC(false);
		setDisabledSetDayOff(false);
	}
	
	public boolean renderedSetDayOff(Long status){
		if(status != null){
			if(status == 3 || status == 6){
				return false;
			}else{
				return true;
			}
		}else{
			return false;
		}
	}
	
	public void disabledControl(boolean validatePeriod){
		if(permissaoSalvar){
			setFalse();
			partTime(validatePeriod);
			
			if(holidayCategory == null){
				setTrue();
				setDisabledHC(false);
			}else{
				if(holidayCategory.getIdHolidayCategory() == null){
					setTrue();
					setDisabledHC(false);
				}else{
					if(employee == null){
						setTrue();
						setDisabledEmployee(false);
					}else{
						if(employee.getIdEmployee() == null){
							setTrue();
							setDisabledEmployee(false);
						}
					}
				}
			}
		}else{
			setTrue();
		}
	}
	
	public boolean getImageStatus(long status, long fixo){
		if(status == fixo){
			return true;
		}else{
			return false;
		}
	}
	
	public void setDayOff(HolidayDay hd){
		for (HolidayDay hDay : listHolidayDay) {
			hDay.setSaveCode(Utils.getTimeInMillis(new Date()));
			if(hd != null && hDay != null){
				if(hd.getHashCode() == hDay.getHashCode()){
					if(hd.isCount()){
						hDay.setStatus(hDay.getStatusLog());
					}else{
						hDay.setStatus(4L);
					}
					
//					Long lastStatus = arraysLastStatus.get(hDay.hashCode());
//					
//					if(hd.isCount()){
//						if(lastStatus != null){
//							hDay.setStatus(lastStatus);
//						}
//					}else{
//						arraysLastStatus.put(hDay.hashCode(), hDay.getStatus());
//						hDay.setStatus(4L);
//					}
					
					hDay.setCount(hd.isCount());
				}
			}
		}
		
		if(holidayCategory.getReckon().equals("D") || holidayCategory.getReckon().equals("N")){
			int x = 0;
			for (HolidayDay hoDay : listHolidayDay) {
				if(hoDay.isCount()){
					x += 1;
				}
			}
			employeeHoliday.setDayCounted(x);
		}
		
		if(holidayCategory.getReckon().equals("H")){
			employeeHoliday.setEndDate(employeeHoliday.getStartDate());
			
			Contract c = contractRepository.findByEmployee(employee.getIdEmployee());
			long x = c.getAllowance().getWorkingHours() / 2; 
			
			employeeHoliday.setHourCounted((int)x);
		}
	}
	
	public String StringDescription(HolidayDay hd){
		if(hd != null){
			if(hd.isCount()){
				if(holidayCategory != null){
					return holidayCategory.getDescription();
				}else{
					return "";
				}
			}else{
				if(hd.getStatus() == 3){
					return "NOT COUNTED";
				}else if(hd.getStatus() == 6){
					return "BANK HOLIDAY";
				}else{
					return "DAY OFF";
				}
			}
		}else{
			return "";
		}
	}
	
	public void createHolidayDay(){
		//CREATE holiday_day
		listHolidayDay = new ArrayList<>();
		long stDt = employeeHoliday.getStartDate().getTime();
		
		if(holidayCategory != null){
			List<Department> departments = new ArrayList<>();
			long saveCode = Utils.getTimeInMillis(new Date());
			long hashCode = saveCode;
			
			if(holidayCategory.getReckon() != null && holidayCategory.getReckon().equals("H")){
				HolidayDay day = new HolidayDay();
				Calendar c = Calendar.getInstance();
				c.setTimeInMillis(stDt);
				
				day.setHolidayPerDay(null);
				if(employee != null){
					departments = departmentRepository.listaByEmployee(employee.getIdEmployee());
					if(departments.size() > 1){
						//5-MANY DEPARTMENTS (SEE DETAILS)
						day.setStatus(5L);
						day.setStatusLog(5L);
					}else{
						for (Department d : departments) {
							List<Department> holidayByDayDepartments = departmentRepository.listHolidayByDayDepartment(c.getTime(), d.getIdDepartment().toString());
							if(holidayByDayDepartments.size() == 0){
								//0-NO ONE SLOT BOOKED
								day.setStatus(0L);
								day.setStatusLog(0L);
							}else{
								for (Department dd : holidayByDayDepartments) {
									//2-SLOT FULLY BOOKED
									if(dd.getMaximumEmployees() <= dd.getHolidayPerDay()){
										day.setStatus(2L);
										day.setStatusLog(2L);
									}
									
									//1-SOME SLOTS AVAILABLE
									if(dd.getMaximumEmployees() > dd.getHolidayPerDay() && dd.getHolidayPerDay() > 0){
										day.setStatus(1L);
										day.setStatusLog(1L);
									}
								}
							}
						}
					}
				}
				
				day.setDay(c.getTime());
				day.setYear((long) Utils.getDateYear(c.getTime()));
				day.setEmployeeHoliday(employeeHoliday);
				day.setSaveCode(saveCode);
				day.setHashCode(hashCode+1);
				hashCode++;
				
				//SE DIA DA SEMANA NAO ESTIVER NO ALLOWANCE SETAR FALSE E TBM SE FOR BANK HOLIDAY
				BankHoliday bh = bankHolidayRepository.findByDate(Utils.formataDataStringSemHora(c.getTime()));
				if(bh != null){
					day.setCount(false);
					day.setStatus(6L);
					day.setStatusLog(6L);
				}else{
					if(employee != null){
						Contract contract = contractRepository.findByEmployee(employee.getIdEmployee());
						int init = Utils.WeekDayToInt(contract.getAllowance().getStartDayWorking());
						int end = Utils.WeekDayToInt(contract.getAllowance().getEndDayWorking());
						if(c.get(Calendar.DAY_OF_WEEK) >= init && c.get(Calendar.DAY_OF_WEEK) <= end){
							day.setCount(true);
						}else{
							day.setCount(false);
							day.setStatus(3L);
						}
					}
				}
				
				if(day.getIdHolidayDay() == null || day.getIdHolidayDay() > 0){
					listHolidayDay.add(day);
				}
			}else{
				while(stDt <= employeeHoliday.getEndDate().getTime()){
					HolidayDay day = new HolidayDay();
					Calendar c = Calendar.getInstance();
					c.setTimeInMillis(stDt);
					
					day.setHolidayPerDay(null);
					if(employee != null){
						departments = departmentRepository.listaByEmployee(employee.getIdEmployee());
						if(departments.size() > 1){
							//5-MANY DEPARTMENTS (SEE DETAILS)
							day.setStatus(5L);
							day.setStatusLog(5L);
						}else{
							for (Department d : departments) {
								List<Department> holidayByDayDepartments = departmentRepository.listHolidayByDayDepartment(c.getTime(), d.getIdDepartment().toString());
								if(holidayByDayDepartments.size() == 0){
									//0-NO ONE SLOT BOOKED
									day.setStatus(0L);
									day.setStatusLog(0L);
								}else{
									for (Department dd : holidayByDayDepartments) {
										//2-SLOT FULLY BOOKED
										if(dd.getMaximumEmployees() <= dd.getHolidayPerDay()){
											day.setStatus(2L);
											day.setStatusLog(2L);
										}
										
										//1-SOME SLOTS AVAILABLE
										if(dd.getMaximumEmployees() > dd.getHolidayPerDay() && dd.getHolidayPerDay() > 0){
											day.setStatus(1L);
											day.setStatusLog(1L);
										}
									}
								}
							}
						}
					}
					
					day.setDay(c.getTime());
					day.setYear((long) Utils.getDateYear(c.getTime()));
					day.setEmployeeHoliday(employeeHoliday);
					day.setSaveCode(saveCode);
					day.setHashCode(hashCode+1);
					hashCode++;
					
					//BANK HOLIDAY
					//	WEEKEND
					//		COUNTED HOLIDAY
					
					//SE DIA DA SEMANA NAO ESTIVER NO ALLOWANCE SETAR FALSE E TBM SE FOR BANK HOLIDAY
					BankHoliday bh = bankHolidayRepository.findByDate(Utils.formataDataStringSemHora(c.getTime()));
					if(bh != null){
						day.setCount(false);
						day.setStatus(6L);
						day.setStatusLog(6L);
					}else{
						if(employee != null){
							Contract contract = contractRepository.findByEmployee(employee.getIdEmployee());
							int init = Utils.WeekDayToInt(contract.getAllowance().getStartDayWorking());
							int end = Utils.WeekDayToInt(contract.getAllowance().getEndDayWorking());
							if(c.get(Calendar.DAY_OF_WEEK) >= init && c.get(Calendar.DAY_OF_WEEK) <= end){
								if(holidayCategory.getReckon().equals("N")){
									day.setCount(false);
									day.setStatus(3L);
								}else{
									day.setCount(true);
								}
							}else{
								day.setCount(false);
								day.setStatus(3L);
								day.setStatusLog(3L);
							}
						}
					}
					
					if(day.getIdHolidayDay() == null || day.getIdHolidayDay() > 0){
						listHolidayDay.add(day);
					}
					
					stDt = Utils.addDias(c.getTime(), 1).getTime();
				}
			}
		}
	}
	
	
	public List<String> completeManager(String busca) {
		List<String> resultados = new ArrayList<String>();
		
		for (Employee employee : managerList) {
			if (employee.getFullName().toLowerCase().contains(busca.toLowerCase())) {
    			resultados.add(employee.toString());
			}
		}
		return resultados;
	}
	
	public List<String> completeHrManager(String busca) {
		List<String> resultados = new ArrayList<String>();
		
		for (Employee employee : hrManagerList) {
			if (employee.getFullName().toLowerCase().contains(busca.toLowerCase())) {
    			resultados.add(employee.toString());
			}
		}
		return resultados;
	}
	
	public List<String> completeHolidayCategory(String busca) {
		List<String> resultados = new ArrayList<String>();
		
		for (HolidayCategory holidayCategory : holidayCategoryList) {
			if (holidayCategory.getDescription().toLowerCase().contains(busca.toLowerCase())) {
    			resultados.add(holidayCategory.toString());
			}
		} 
		return resultados;
	}

	@Transactional
	public boolean save(EmployeeHoliday employeeHoliday) {
		List<EmployeeAllowance> listEmployeeAllowance = new ArrayList<>();
		
		if(employeeHoliday.getIdEmployeeHoliday() != null && employeeHoliday.getIdEmployeeHoliday() > 0){
			
		}else{
			employeeHoliday.setRequisitionDate(new Date());
		}
		
		if(employee != null){
			if(employee.getIdEmployee() != null && employee.getIdEmployee() > 0){
				employeeHoliday.setEmployee(employee);
				listEmployeeAllowance = employeeAllowanceRepository.findByEmployeeYear(employee.getIdEmployee(), Utils.getDateYear(employeeHoliday.getStartDate()), Utils.getDateYear(employeeHoliday.getEndDate()));
			}
		}
		
		if(holidayCategory != null){
			if(holidayCategory.getIdHolidayCategory() != null && holidayCategory.getIdHolidayCategory() > 0){
				employeeHoliday.setHolidayCategory(holidayCategory);
			}
		}
		
		if(manager != null){
			if(manager.getIdEmployee() != null && manager.getIdEmployee() > 0){
				employeeHoliday.setManagerPermission(manager);
			}
		}
		
		if(hrManager != null){
			if(hrManager.getIdEmployee() != null && hrManager.getIdEmployee() > 0){
				employeeHoliday.setHrPermission(hrManager);
			}
		}
		
		employeeHoliday = employeeHolidayRepository.salvar(employeeHoliday);
		
		long code = 0;
		for (HolidayDay hd : listHolidayDay) {
			if(hd.getSaveCode() != null){
				code = hd.getSaveCode();
				break;
			}else{
				break;
			}
		}
		
		if(code != currentSaveCode){
			if(currentSaveCode > 0){
				holidayDayRepository.delete(currentSaveCode);
			}
			
			for (HolidayDay hd : listHolidayDay) {
				holidayDayRepository.salvar(hd);
			}
		}
		
		//CRIAR OU ATUALIZAR employee_allowance
		if(listEmployeeAllowance.size() > 0){
//			//UPDATE
//			
		}else{
			//CREATE
			saveEmployeeAllowance(employeeHoliday.getEmployee().getIdEmployee(), employeeHoliday);
		}
		
		return true;
	}
	
	public void saveEmployeeAllowance(Long idEmployee, EmployeeHoliday eHoliday){
		List<Long> years = new ArrayList<>();
		//VERIFICANDO SE O PERIDO DE FERIAS COMECA E TERMINA E ANOS DIFERENTES
		years.add(Long.valueOf(Utils.getDateYear(eHoliday.getStartDate())));
		if(Utils.getDateYear(eHoliday.getStartDate()) != Utils.getDateYear(eHoliday.getEndDate())){
			years.add(Long.valueOf(Utils.getDateYear(eHoliday.getEndDate())));
		}
		
		//PARA CADA ANO CRIAR EMPLOYEE_ALLOWANCE
		for (Long y : years) {
			EmployeeAllowance eAllowance = new EmployeeAllowance();
			Contract contract = contractRepository.findByEmployee(idEmployee);
			eAllowance.setContract(contract);
			eAllowance.setCurrentYear(y);
			
			//SE ANO ANO DO PERIODO DE FERIAS > QUE ANO DO CONTRATO, USAR TOTAL DE DIAS DISPONIVEIS DO CONTRATO
			if(y > Long.valueOf(Utils.getDateYear(contract.getStartDate()))){
				eAllowance.setAvailableDays(Double.valueOf(contract.getAllowance().getAllowance()));
			}else{//SE NAO, CALCULAR DIAS DISPONIVEIS USANDO DATA INICIAL DO CONTRATO E ULTIMO DIA DO ANO
				Calendar last = Calendar.getInstance();
				last.set(Utils.getDateYear(contract.getStartDate()), 12, 0);
				
				long dif = Utils.retornarDiferencaEmDiasEntreDatas(contract.getStartDate(), last.getTime());
				long v1 = dif * contract.getAllowance().getAllowance();
				eAllowance.setAvailableDays((double)Math.round((double)v1 / 365));
			}
			
			eAllowance.setTotalDaysCounted(holidayDayRepository.countDaysByEmployee(idEmployee, y));
			eAllowance.setTotalHoursCounted(holidayDayRepository.countHoursByEmployee(idEmployee, y));
			employeeAllowanceRepository.salvar(eAllowance);
			
		}
	}

	public boolean isPermissaoSalvar() {
		return permissaoSalvar;
	}

	public void setPermissaoSalvar(boolean permissaoSalvar) {
		this.permissaoSalvar = permissaoSalvar;
	}

	public EmployeeHoliday getEmployeeHoliday() {
		return employeeHoliday;
	}

	public void setEmployeeHoliday(EmployeeHoliday employeeHoliday) {
		this.employeeHoliday = employeeHoliday;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	public void setEmployeeDisabledControl(Employee employee) {
		this.employee = employee;
		disabledControl(true);
	}
	
	public void setHCDisabledControl(HolidayCategory holidayCategory) {
		this.holidayCategory = holidayCategory;
		disabledControl(true);
	}

	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}

	public HolidayCategory getHolidayCategory() {
		return holidayCategory;
	}

	public void setHolidayCategory(HolidayCategory holidayCategory) {
		this.holidayCategory = holidayCategory;
	}

	public List<HolidayCategory> getHolidayCategoryList() {
		return holidayCategoryList;
	}

	public void setHolidayCategoryList(List<HolidayCategory> holidayCategoryList) {
		this.holidayCategoryList = holidayCategoryList;
	}

	public Employee getManager() {
		return manager;
	}

	public void setManager(Employee manager) {
		this.manager = manager;
	}

	public List<Employee> getManagerList() {
		return managerList;
	}

	public void setManagerList(List<Employee> managerList) {
		this.managerList = managerList;
	}

	public Employee getHrManager() {
		return hrManager;
	}

	public void setHrManager(Employee hrManager) {
		this.hrManager = hrManager;
	}

	public List<Employee> getHrManagerList() {
		return hrManagerList;
	}

	public void setHrManagerList(List<Employee> hrManagerList) {
		this.hrManagerList = hrManagerList;
	}
	
	public Date getNewDate() {
		return new Date();
	}

	public boolean isDisabledStartDate() {
		return disabledStartDate;
	}

	public void setDisabledStartDate(boolean disabledStartDate) {
		this.disabledStartDate = disabledStartDate;
	}

	public boolean isDisabledHC() {
		return disabledHC;
	}

	public void setDisabledHC(boolean disabledHC) {
		this.disabledHC = disabledHC;
	}

	public boolean isDisabledEmployee() {
		return disabledEmployee;
	}

	public void setDisabledEmployee(boolean disabledEmployee) {
		this.disabledEmployee = disabledEmployee;
	}

	public boolean isDisabledEndDate() {
		return disabledEndDate;
	}

	public void setDisabledEndDate(boolean disabledEndDate) {
		this.disabledEndDate = disabledEndDate;
	}

	public List<HolidayDay> getListHolidayDay() {
		return listHolidayDay;
	}

	public void setListHolidayDay(List<HolidayDay> listHolidayDay) {
		this.listHolidayDay = listHolidayDay;
	}

	public long getCurrentSavedCode() {
		return currentSaveCode;
	}

	public void setCurrentSavedCode(long currentSavedCode) {
		this.currentSaveCode = currentSavedCode;
	}

	public boolean isDisabledDaysHours() {
		return disabledDaysHours;
	}

	public void setDisabledDaysHours(boolean disabledDaysHours) {
		this.disabledDaysHours = disabledDaysHours;
	}

	public boolean isDisabledSetDayOff() {
		return disabledSetDayOff;
	}

	public void setDisabledSetDayOff(boolean disabledSetDayOff) {
		this.disabledSetDayOff = disabledSetDayOff;
	}

}
