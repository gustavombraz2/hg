package com.rasz.service;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.Perfil;
import com.rasz.domain.PerfilPrivilegio;
import com.rasz.domain.Usuario;
import com.rasz.repository.PerfilPrivilegioRepository;
import com.rasz.repository.PerfilRepository;
import com.rasz.repository.UsuarioRepository;
import com.rasz.utils.ShowFacesMessage;

	
@Service
public class UsuarioService {

	@Autowired private UsuarioRepository usuarioRepository;
	@Autowired private PerfilRepository perfilRepository;
	@Autowired private PerfilPrivilegioRepository perfilPrivilegioRepository;
	@PersistenceContext EntityManager em;
		
	boolean permissaoSalvar;
	boolean disabledUserFound;
	boolean disabledUserNotFound;
	boolean emailFound;
	boolean emailNotFound;
	boolean renderedPassword;
	boolean foundUser;
	private String senha;
	private String nome;
	private Usuario usuario = new Usuario();
	
	public Usuario findByEmail(String email) {
		return usuarioRepository.findByEmail(email);
	}

	public Usuario findByLogin(String login) {
		return usuarioRepository.findByLogin(login);
	}

	public Usuario findOne(Long id) {
		return usuarioRepository.findOne(id);
	}
	
	public Usuario editar(long id) {
		permissaoSalvar = true;
		renderedPassword = false;
		usuario = usuarioRepository.findOne(id);
		return usuario;
	}
	public Usuario consultar(long id) {
		renderedPassword = false;
		permissaoSalvar = false;
		usuario = usuarioRepository.findOne(id);
		return usuario;
	}

	public List<Usuario> getAll() {
		return (List<Usuario>) usuarioRepository.findAll();
	}
	
	public List<Usuario> listarPorTipoUsuario() {
		return (List<Usuario>) usuarioRepository.findAll();
	}
	
	public boolean verificarEmail(Usuario usuario){
		if(!usuario.getEmail().equals("")) {
			Usuario u = usuarioRepository.findByEmail(usuario.getEmail());
			
			if(u != null){
				if(usuario.getId() == null && usuario.getEmail().toUpperCase().equals(u.getEmail().toUpperCase())) {
					ShowFacesMessage.warn("Warn!", "Email already exists.");
					setEmailFound(true);
					setEmailNotFound(false);
					return false;
				}
				
				if(usuario.getId() != null && !u.getId().equals(usuario.getId())) {
					if(u.getEmail().equals(usuario.getEmail())) {
						ShowFacesMessage.warn("Warn!", "Email already exists.");
						setEmailFound(true);
						setEmailNotFound(false);
						return false;
					}
				}
			}
		}
		this.usuario.setEmail(usuario.getEmail().toLowerCase());
		setEmailFound(false);
		setEmailNotFound(true);
		return true;
	}
	
	public boolean verificarLogin(Usuario usuario){
		Usuario u = findByLogin(usuario.getLogin());
		
		if(u != null && u.getId() > 0){
			if(usuario != null){
				if(usuario.getId() != null && usuario.getId() > 0){
					if(u.getId() != usuario.getId()){
						FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Warn!", "Username already exists.");
						FacesContext.getCurrentInstance().addMessage(null, message);
						setDisabledUserFound(true);
						setDisabledUserNotFound(false);
						return false;
					}
				}else{
					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Warn!", "Username already exists.");
					FacesContext context = FacesContext.getCurrentInstance();
					context.addMessage(null, message);
					context.getExternalContext().getFlash().setKeepMessages(true);
					setDisabledUserFound(true);
					setDisabledUserNotFound(false);
					return false;
				}
			}
		}
		
		setDisabledUserFound(false);
		setDisabledUserNotFound(true);
		return true;
	}

	public Usuario create() {
		usuario = new Usuario();
		permissaoSalvar = true;
		renderedPassword = true;
		usuario.setBoAtivo(true);
		return usuario;
	}

	public boolean inativar(Usuario usuario) {
		if(usuario != null && usuario.getBoAtivo()) {
			usuario.setBoAtivo(false);
			usuarioRepository.salvar(usuario);
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Message", "User deactivated successfully.");
			FacesContext.getCurrentInstance().addMessage(null, message);
			return true;
		}
		return false;
	}
	
	public boolean ativar(Usuario usuario) {
		if(usuario != null && !usuario.getBoAtivo()) {
			usuario.setBoAtivo(true);
			usuarioRepository.salvar(usuario);
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Message", "User successfully activated.");
			FacesContext.getCurrentInstance().addMessage(null, message);
			return true;
		}
		return false;
	}
	
	public boolean validate() {
		Md5PasswordEncoder encoder = new Md5PasswordEncoder();
		String s = encoder.encodePassword(senha, null);
		
		if(s.equals(usuario.getSenha())){
			usuario.setSenha("");
			usuario.setSenhaNova("");
			return true;
		}else{
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Warn!", "Incorrect password.");
			FacesContext.getCurrentInstance().addMessage(null, message);
			return false;
		}
	}
	
	public void load() {
		usuario = getCurrentLoggedUser();
	}
	
	@Transactional
	public boolean remove(Usuario usuario) {
		
		Usuario usuarioBd = usuarioRepository.findOne(usuario.getId());
		List<Perfil> perfilUser = perfilRepository.findByUsuario(usuario);
		if(perfilUser != null && !perfilUser.isEmpty()) {
			for (Perfil perfil : perfilUser) {
				List<PerfilPrivilegio> privilegios = perfilRepository.findPrivilegioByPerfil(perfil);
				perfil.setPrivilegios(privilegios);

				if(perfil.getUsuarios()!= null && !perfil.getUsuarios().isEmpty() && !usuarioBd.getBoAtivo()) {

					for (PerfilPrivilegio p : perfil.getPrivilegios()) {
						perfilPrivilegioRepository.excluirPerfilPrivilegio(p.getPerfil(), p.getPrivilegio());
					}

					for (Usuario u : perfil.getUsuarios()) {
						List<Perfil> perfis = perfilRepository.findByUsuario(u);
						if(perfis != null && !perfis.isEmpty()) {
							for (Perfil p : perfis) {
								usuarioRepository.excluirPerfilUsuario(u, p);
							}	
						}
					}
					usuarioRepository.delete(usuarioBd.getId());
					perfilRepository.delete(perfil.getId());

					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Opera��o Realizada", 
							"O registro foi excluido com sucesso.");
					FacesContext.getCurrentInstance().addMessage(null, message);
					return true;						

				}else if(usuarioBd.getBoAtivo()) {
					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao Excluir", "O usu�rio "
							+ usuarioBd.getNmUsuario() + " est� com status ativo. Favor inativar o usu�rio antes de realizar a exclus�o.");
					FacesContext.getCurrentInstance().addMessage(null, message);
					return false;
				}else{
					usuarioRepository.delete(usuarioBd);
					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Opera��o realizada", "O registro foi excluido com sucesso.");
					FacesContext.getCurrentInstance().addMessage(null, message);
					return true;
				}
			}
			}else{
				if(usuarioBd.getBoAtivo()) {
					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro", "O usu�rio "
							+ usuarioBd.getNmUsuario() + " est� com status ativo. Favor inativar o usu�rio antes de realizar a exclus�o.");
					FacesContext.getCurrentInstance().addMessage(null, message);
					return false;
				}else{
					usuarioRepository.delete(usuarioBd);
					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Opera��o realizada", "O registro foi excluido com sucesso.");
					FacesContext.getCurrentInstance().addMessage(null, message);
					return true;
				}
			}
			
		return false;
	}
	
	public List<Usuario> listarDisponiveis(Long idPerfil){
		return usuarioRepository.listarDisponiveis();
	}

	
	@Transactional
	public boolean save(Usuario usuario) {
		boolean save = false;
		
		if(verificarLogin(usuario)){
			save = true;
			if(verificarEmail(usuario)) {
				save = true;
			}else{
				save = false;
			}
		}else{
			save = false;
		}
			
		if(save){
			if (usuario.getSenhaNova() != null && usuario.getSenhaNova().length() > 0) {
				Md5PasswordEncoder encoder = new Md5PasswordEncoder();
				usuario.setSenha(encoder.encodePassword(usuario.getSenhaNova(), null));
			}else{
				Md5PasswordEncoder encoder = new Md5PasswordEncoder();
				usuario.setSenha(encoder.encodePassword(usuario.getSenha(), null));
			}
			
			usuarioRepository.salvar(usuario);
			return true;
		}else{
			return false;
		}
	}
	
	@Transactional
	public void changePassword() {
		if (usuario.getSenhaNova() != null && usuario.getSenhaNova().length() > 0) {
			Md5PasswordEncoder encoder = new Md5PasswordEncoder();
			usuario.setSenha(encoder.encodePassword(usuario.getSenhaNova(), null));
		}else{
			Md5PasswordEncoder encoder = new Md5PasswordEncoder();
			usuario.setSenha(encoder.encodePassword(usuario.getSenha(), null));
		}
		
		usuarioRepository.salvar(usuario);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Usuario getCurrentLoggedUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario currentUser = null;
		if (auth != null) {
			currentUser = (Usuario) auth.getPrincipal();
			currentUser = usuarioRepository.findByLogin(currentUser.getLogin());
		}
		return currentUser;
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String getLoginUserCorrente() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario currentUser = null;
		if (auth != null) {
			currentUser = (Usuario) auth.getPrincipal();
			return currentUser.getLogin();
		}
		return "";
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void atualizarNotificacoes() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario currentUser = null;
		if (auth != null) {
			currentUser = (Usuario) auth.getPrincipal();
		}
	}
	
	public boolean isPermissaoSalvar() {
		return permissaoSalvar;
	}

	public void setPermissaoSalvar(boolean permissaoSalvar) {
		this.permissaoSalvar = permissaoSalvar;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public boolean isDisabledUserFound() {
		return disabledUserFound;
	}

	public void setDisabledUserFound(boolean disabledUserFound) {
		this.disabledUserFound = disabledUserFound;
	}

	public boolean isDisabledUserNotFound() {
		return disabledUserNotFound;
	}

	public void setDisabledUserNotFound(boolean disabledUserNotFound) {
		this.disabledUserNotFound = disabledUserNotFound;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public boolean isRenderedPassword() {
		return renderedPassword;
	}

	public void setRenderedPassword(boolean renderedPassword) {
		this.renderedPassword = renderedPassword;
	}

	public boolean isEmailFound() {
		return emailFound;
	}

	public void setEmailFound(boolean emailFound) {
		this.emailFound = emailFound;
	}

	public boolean isEmailNotFound() {
		return emailNotFound;
	}

	public void setEmailNotFound(boolean emailNotFound) {
		this.emailNotFound = emailNotFound;
	}

}
