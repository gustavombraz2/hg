package com.rasz.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.Vehicle;
import com.rasz.repository.VehicleRepository;

	
@Service
public class VehicleService {

	@Autowired private VehicleRepository vehicleRepository;
	@PersistenceContext EntityManager em;
		
	boolean permissaoSalvar;
	private Vehicle vehicle = new Vehicle();
	
	public Vehicle findOne(Long id) {
		return vehicleRepository.findOne(id);
	}
	
	public Vehicle editar(long id) {
		permissaoSalvar = true;
		vehicle = vehicleRepository.findOne(id);
		return vehicle;
	}
	public Vehicle consultar(long id) {
		permissaoSalvar = false;
		vehicle = vehicleRepository.findOne(id);
		return vehicle;
	}

	public List<Vehicle> getAll() {
		return (List<Vehicle>) vehicleRepository.findAll();
	}
	
	public Vehicle create() {
		permissaoSalvar = true;
		vehicle = new Vehicle();
		vehicle.setActivate(true);
		return vehicle;
	}

	@Transactional
	public boolean remove(Long id) {
		if(id != null && id > 0){
			vehicleRepository.delete(id);
			return true;
		}else{
			return false;
		}
	}

	@Transactional
	public boolean save(Vehicle vehicle) {
		vehicleRepository.salvar(vehicle);
		return true;
	}

	public boolean isPermissaoSalvar() {
		return permissaoSalvar;
	}

	public void setPermissaoSalvar(boolean permissaoSalvar) {
		this.permissaoSalvar = permissaoSalvar;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	
}
