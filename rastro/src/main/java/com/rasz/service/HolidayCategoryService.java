package com.rasz.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.HolidayCategory;
import com.rasz.repository.HolidayCategoryRepository;

	
@Service
public class HolidayCategoryService {

	@Autowired private HolidayCategoryRepository holidayCategoryRepository;
	@PersistenceContext EntityManager em;
		
	boolean permissaoSalvar;
	private String description;
	private HolidayCategory holidayCategory = new HolidayCategory();
	
	public HolidayCategory findOne(Long id) {
		return holidayCategoryRepository.findOne(id);
	}
	
	public HolidayCategory editar(long id) {
		permissaoSalvar = true;
		holidayCategory = holidayCategoryRepository.findOne(id);
		return holidayCategory;
	}
	public HolidayCategory consultar(long id) {
		permissaoSalvar = false;
		holidayCategory = holidayCategoryRepository.findOne(id);
		return holidayCategory;
	}

	public List<HolidayCategory> getAll() {
		return (List<HolidayCategory>) holidayCategoryRepository.findAll();
	}
	
	public List<HolidayCategory> listarPorTipoHolidayCategory() {
		return (List<HolidayCategory>) holidayCategoryRepository.findAll();
	}
	
	public HolidayCategory create() {
		permissaoSalvar = true;
		holidayCategory = new HolidayCategory();
		holidayCategory.setActivate(true);
		return holidayCategory;
	}

	@Transactional
	public boolean remove(Long id) {
		if(id != null && id > 0){
			holidayCategoryRepository.delete(id);
			return true;
		}else{
			return false;
		}
	}

	@Transactional
	public boolean save(HolidayCategory holidayCategory) {
		holidayCategoryRepository.salvar(holidayCategory);
		return true;
	}

	public boolean isPermissaoSalvar() {
		return permissaoSalvar;
	}

	public void setPermissaoSalvar(boolean permissaoSalvar) {
		this.permissaoSalvar = permissaoSalvar;
	}

	public HolidayCategory getHolidayCategory() {
		return holidayCategory;
	}

	public void setHolidayCategory(HolidayCategory holidayCategory) {
		this.holidayCategory = holidayCategory;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
