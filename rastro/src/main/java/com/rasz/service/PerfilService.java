package com.rasz.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.component.tabview.TabView;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.webflow.engine.RequestControlContext;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.RequestContextHolder;

import com.rasz.domain.FlowRuleEnum;
import com.rasz.domain.Perfil;
import com.rasz.domain.PerfilPrivilegio;
import com.rasz.domain.Privilegio;
import com.rasz.domain.Usuario;
import com.rasz.repository.PerfilRepository;
import com.rasz.repository.PrivilegioRepository;
import com.rasz.utils.FlowUtilsFactory;
import com.rasz.utils.ShowFacesMessage;
import com.rasz.utils.Utils;

	
@Service
public class PerfilService {

	@Autowired private PrivilegioRepository privilegioRepository;
	@Autowired private PerfilRepository perfilRepository;
	
	int activeTabIndex = 0;
	

	public int getActiveTabIndex() {
		return activeTabIndex;
	}
	boolean permissaoSalvar;

	public void setActiveTabIndex(int activeTabIndex) {
		this.activeTabIndex = activeTabIndex;
	}

	public boolean isPermissaoSalvar() {
		return permissaoSalvar;
	}

	public void setPermissaoSalvar(boolean permissaoSalvar) {
		this.permissaoSalvar = permissaoSalvar;
	}

	public Perfil consultar(Perfil perfil) {
		if(perfil != null){
			permissaoSalvar = false;
			return perfilRepository.findOne(perfil.getId());
		}else{
			return null;
		}
	}
	
	public Perfil replicar(Perfil perfil) {
		perfil.setId(null);
		perfil.setDescricao("");
		perfil.setUsuarios(null);
		
		for(PerfilPrivilegio p : perfil.getPrivilegios()) {
			p.setId(null);
			perfil.getPrivilegios().set(perfil.getPrivilegios().indexOf(p), p);
		}
		
		return perfil;
	}
	
//	public Perfil editar(long id) {
//		permissaoSalvar = false;
//		return perfilRepository.findOne(id);
//	}
	public Perfil findByNome(String nome) {
		return perfilRepository.findByNome(nome);
	}

	public Perfil findOne(Long id) {
		Perfil perfil = perfilRepository.findById(id);
		permissaoSalvar = true;
		if(perfil.getPrivilegios() != null){
			perfil.getPrivilegios().size();
			perfil = gerarNovosPrivilegios(perfil);
		}else{
			perfil = gerarPrivilegios(perfil);
		}
		
		if(perfil.getUsuarios() != null){
			perfil.getUsuarios().size();
		}
		
		return perfil;
	}
	
	public Perfil findOne(Perfil p) {
		if(p != null){
			Perfil perfil = perfilRepository.findById(p.getId());
			permissaoSalvar = true;
			if(perfil.getPrivilegios() != null){
				perfil.getPrivilegios().size();
				perfil = gerarNovosPrivilegios(perfil);
			}else{
				perfil = gerarPrivilegios(perfil);
			}
			
			if(perfil.getUsuarios() != null){
				perfil.getUsuarios().size();
			}
			
			return perfil;
		}else{
			return null;
		}
	}
	
	public void executeSelectedItemDataTableList(SelectEvent selectEvent) {
		if(!Utils.isMobileDevice())
			return;
		
		if(selectEvent != null){
			Perfil perfil = (Perfil) selectEvent.getObject();
			RequestContext requestContext = RequestContextHolder.getRequestContext();
			RequestControlContext rec = (RequestControlContext) requestContext;
			
			if(perfil != null && perfil.getId() != null)
				perfil = findOne(perfil.getId());
			FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).put("perfil", perfil);
			rec.handleEvent(new Event(this, "listarAgenteClickGrid"));
		}
	}
	
	public String retornarTituloPagina() {
		if ((Boolean) FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).get("isNovoRegistro"))
			return "CADASTRO DE";
		else if ((Boolean) FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).get("isConsultar"))
			return "CONSULTA DE";
		else if ((Boolean) FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).get("isEditar"))
			return "ALTERA��O DE";
		else
			return "CONSULTA DE";
	}
	
	private Perfil gerarNovosPrivilegios(Perfil perfil){	
		List<Privilegio> privilegios = (List<Privilegio>) privilegioRepository.findAll();
		
		boolean encontrou = false;
		for(Privilegio privilegio : privilegios){
				for(PerfilPrivilegio perfilPrivilegio : perfil.getPrivilegios()){			
					if(perfilPrivilegio.getPrivilegio().getId() == privilegio.getId()){
						encontrou = true;
						break;
					}
				}
				
				if(!encontrou){
					PerfilPrivilegio pp = new PerfilPrivilegio();
					pp.setPrivilegio(privilegio);
					pp.setPerfil(perfil);
					perfil.getPrivilegios().add(pp);
					encontrou = false;
				}else{
					encontrou = false;
				}
		}
		return perfil;
	}
	
	public final void onTabChange(final TabChangeEvent event){
		TabView tv = (TabView) event.getComponent();
		this.activeTabIndex = tv.getActiveIndex();
	}
	
	private Perfil gerarPrivilegios(Perfil perfil){
		perfil.setPrivilegios(new ArrayList<PerfilPrivilegio>());		
		List<Privilegio> privilegios = (List<Privilegio>) privilegioRepository.findAll();
		for(Privilegio privilegio : privilegios){
				PerfilPrivilegio perfilPrivilegio = new PerfilPrivilegio();
				perfilPrivilegio.setPerfil(perfil);
				perfilPrivilegio.setPrivilegio(privilegio);
				perfil.getPrivilegios().add(perfilPrivilegio);
		}
		return perfil;
	}
	
	public List<Perfil> getAll(Perfil perfilFiltro) {
		String searchString = "%%";
		if (perfilFiltro.getNome() != null) {
			searchString = "%" + perfilFiltro.getNome() + "%";
		}
		return perfilRepository.listarPorParametro(searchString, perfilFiltro.isAtivado());
	}

	public Perfil create() {
		permissaoSalvar = true;
		Perfil perfil = new Perfil();
		perfil.setDescricao(new String());
		perfil.setAtivado(true);
		perfil.setPrivilegios(new ArrayList<PerfilPrivilegio>());	
		perfil.setUsuarios(new ArrayList<Usuario>());
		perfil = gerarPrivilegios(perfil);
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.HOUR_OF_DAY, 12);
		cal.set(Calendar.MINUTE, 00);
		return perfil;
	}
	
	public Perfil createPerfilFiltro() {
		Perfil perfil = new Perfil();
		perfil.setAtivado(true);
		perfil.setNome("");
		return perfil;
	}

	
	public Page<Perfil> searchPerfils(String searchString, boolean searchAll, Pageable pageable) {
		if (searchString != null) {
			searchString = "%" + searchString + "%";
		}
		if (searchAll) {
			return perfilRepository.searchPerfils(searchString, pageable);
		} else {
			return perfilRepository.searchPerfils(searchString, pageable);
		}
	}
	

	public void remove(Perfil perfil) {
		if(perfil != null) {
			if(perfil.getUsuarios() != null && perfil.getUsuarios().isEmpty()){
				if (perfil.isAtivado()) {
					perfil.setAtivado(false);
					perfilRepository.save(perfil);
				} else {
					Perfil perfilBd = perfilRepository.findOne(perfil.getId());
					perfilRepository.delete(perfilBd);
					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Opera��o realizada com sucesso", 
							"O registro foi exclu�do com sucesso.");
					FacesContext.getCurrentInstance().addMessage(null, message);
				}
			}else{
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Opera��o n�o permitida", 
						"Existe um ou mais usu�rios vinculados a este perfil e n�o pode ser exclu�do!");
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}
		}else{
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro", "Selecione um perfil.");
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
	}	
	
	@Transactional
	public boolean save(Perfil perfil) {
		if(perfil.getPrivilegios() != null){
			List<PerfilPrivilegio> lista = new ArrayList<PerfilPrivilegio>();
			for(PerfilPrivilegio pp : perfil.getPrivilegios()){
				if(pp.isAlterar() || pp.isConsultar() || pp.isIncluir() || pp.isRemover()){
					lista.add(pp);
				}
			}
			perfil.setPrivilegios(lista);
		}
		
		perfil.setNome(perfil.getDescricao());
		
		if(findByNome(perfil.getNome()) != null && perfil.getId() == null){
			ShowFacesMessage.error("N�o foi poss�vel inserir/alterar", "Nome do perfil j� existe");
			return false;
		}else{
			perfilRepository.save(perfil);
			return true;
		}
	}
	
	public Perfil localizarPorUsuario(Long id){
		
		Perfil p = perfilRepository.localizarPorUsuario(id);
		return p;
	}
	
}
