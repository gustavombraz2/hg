package com.rasz.service;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.Address;
import com.rasz.domain.City;
import com.rasz.domain.Department;
import com.rasz.domain.Employee;
import com.rasz.domain.Role;
import com.rasz.repository.AddressRepository;
import com.rasz.repository.CityRepository;
import com.rasz.repository.DepartmentRepository;
import com.rasz.repository.EmployeeRepository;
import com.rasz.repository.RoleRepository;

	
@Service
public class EmployeeService {

	@Autowired private EmployeeRepository employeeRepository;
	@Autowired private CityRepository cityRepository;
	@Autowired private RoleRepository roleRepository;
	@Autowired private DepartmentRepository departmentRepository;
	@Autowired private AddressRepository addressRepository;
	@PersistenceContext EntityManager em;
		
	boolean permissaoSalvar;
	private String description;
	private Employee employee = new Employee();
	private Role role = new Role();
	private Department department = new Department();
	private Address address = new Address();
	private City city = new City();
	private List<City> cityList = new ArrayList<>();
	private List<Role> roleList = new ArrayList<>();
	private List<Department> departmentList = new ArrayList<>();
	private List<Department> departmentListAC = new ArrayList<>();
	
	public Employee findOne(Long id) {
		return employeeRepository.findOne(id);
	}
	
	public void clear() {
		department = new Department();
		city = new City();
		address = new Address();
		departmentList = new ArrayList<>();
	}
	
	public Employee load(long id) {
		clear();
		
		employee = employeeRepository.findOne(id);
		address = addressRepository.findByEmployee(id);
		cityList = cityRepository.listarTodos(null, null);
		roleList = roleRepository.listarTodos("", 0);
		departmentListAC = departmentRepository.listarTodos("", 0);
		
		if(employee.getRole() != null){
			setRole(employee.getRole());
		}
		
		if(address != null){
			if(address.getIdAddress() != null && address.getIdAddress() > 0){
				city = address.getCity();
			}
		}else{
			address = new Address();
		}
		
		if(employee.getDepartments() != null && employee.getDepartments().size() > 0){
			departmentList = employee.getDepartments();
		}
		
		return employee;
	}
	
	public Employee editar(long id) {
		permissaoSalvar = true;
		return load(id);
	}
	
	public Employee consultar(long id) {
		permissaoSalvar = false;
		return load(id);
	}
	
	public void addDepartment() {
		if(department != null){
			if(department.getIdDepartment() != null && department.getIdDepartment() > 0){
				if(departmentList.size() > 0){
					boolean e = false;
					for (Department d : departmentList) {
						if(department.getIdDepartment() == d.getIdDepartment()){
							e = true;
						}
					}
					
					if(e){
						FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Warn!", "This Department has already been selected.");
						FacesContext.getCurrentInstance().addMessage(null, message);
					}else{
						departmentList.add(department);
					}
				}else{
					departmentList.add(department);
				}
			}
 		}else{
 			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Warn!", "This Department has already been selected.");
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		
		setDepartment(null);
	}
	
	public List<String> completeRole(String busca) {
		List<String> resultados = new ArrayList<String>();
		
		for (Role role : roleList) {
			if (role.getDescription().toLowerCase().contains(busca.toLowerCase())) {
    			resultados.add(role.toString());
			}
		}
		return resultados;
	}
	
	public List<String> completeCity(String busca) {
		List<String> resultados = new ArrayList<String>();
		
		for (City city : cityList) {
			if (city.getName().toLowerCase().contains(busca.toLowerCase())) {
    			resultados.add(city.toString());
			}
		}
		return resultados;
	}
	
	public List<String> completeDepartment(String busca) {
		List<String> resultados = new ArrayList<String>();
		
		for (Department department : departmentListAC) {
			if (department.getDescription().toLowerCase().contains(busca.toLowerCase())) {
    			resultados.add(department.toString());
			}
		}
		return resultados;
	}
	
	public void removeDepartment(Department department) {
		if(department != null){
			if(department.getIdDepartment() != null && department.getIdDepartment() > 0){
				if(departmentList.size() > 0){
					for (Department d : departmentList) {
						if(department.getIdDepartment() == d.getIdDepartment()){
							departmentList.remove(d);
							break;
						}
					}
				}
			}
		}
	}

	public List<Employee> getAll() {
		return (List<Employee>) employeeRepository.findAll();
	}
	
	public List<Employee> listarPorTipoEmployee() {
		return (List<Employee>) employeeRepository.findAll();
	}
	
	public Employee create() {
		clear();
		cityList = cityRepository.listarTodos(null, null);
		roleList = roleRepository.listarTodos("", 0);
		departmentListAC = departmentRepository.listarTodos("", 0);
		permissaoSalvar = true;
		employee = new Employee();
		role = new Role();
		return employee;
	}

	@Transactional
	public boolean remove(Long id) {
		if(id != null && id > 0){
			employeeRepository.delete(id);
			return true;
		}else{
			return false;
		}
	}

	@Transactional
	public boolean save(Employee employee) {
		if(employee.getIdEmployee() == null || employee.getIdEmployee() == 0){
			employee.setActivate(true);
		}
		
		if(role != null){
			employee.setRole(role);
		}
		
		if(city != null){
			address.setCity(city);
		}
		
		if(departmentList.size() <= 0){
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Warn!", "There is no Department selected.");
			FacesContext.getCurrentInstance().addMessage(null, message);
			return false;
		}
		
		if(address != null){
			address.setEmployee(employee);
			addressRepository.salvar(address);
		}
		
		employee = employeeRepository.salvar(employee);
		employeeRepository.deleteEmployeeDepartment(employee.getIdEmployee());
		if(departmentList.size() > 0){
			employeeRepository.insertEmployeeDepartment(departmentList, employee.getIdEmployee());
		}
		
		return true;
	}

	public boolean isPermissaoSalvar() {
		return permissaoSalvar;
	}

	public void setPermissaoSalvar(boolean permissaoSalvar) {
		this.permissaoSalvar = permissaoSalvar;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public List<Department> getDepartmentList() {
		return departmentList;
	}

	public void setDepartmentList(List<Department> departmentList) {
		this.departmentList = departmentList;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public List<City> getCityList() {
		return cityList;
	}

	public void setCityList(List<City> cityList) {
		this.cityList = cityList;
	}

	public List<Role> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<Role> roleList) {
		this.roleList = roleList;
	}

	public List<Department> getDepartmentListAC() {
		return departmentListAC;
	}

	public void setDepartmentListAC(List<Department> departmentListAC) {
		this.departmentListAC = departmentListAC;
	}

}
