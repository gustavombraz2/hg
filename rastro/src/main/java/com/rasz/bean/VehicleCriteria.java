package com.rasz.bean;

import java.util.ArrayList;
import java.util.List;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.rasz.domain.FlowRuleEnum;
import com.rasz.domain.Vehicle;
import com.rasz.repository.VehicleRepository;
import com.rasz.utils.FlowUtilsFactory;
import com.rasz.utils.Utils;

public class VehicleCriteria extends AbstractCriteria<Vehicle> {

	private static final long serialVersionUID = 1L;
	
	@Autowired private transient VehicleRepository vehicleRepository;
	
	private Vehicle vehicle;
	private String registration;
	private Integer code;
	private Long milage;
	
	@Override
	public Page<Vehicle> load(Pageable pageable) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {	
			return vehicleRepository.listarTodos(registration, code, pageable);		
		}else{
			return null;
		}
	}
	
	public void someListenerSelecionarVehicle(SelectEvent event){
		if(!Utils.isMobileDevice())
			return;
		
		if(event != null){
			vehicle  = (Vehicle) event.getObject();
			FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).put("vehicle", vehicle);
		}
	}
	
	public Vehicle retornaVehicle(){
		return vehicleRepository.findOne(((Vehicle) this.vehicle).getIdVehicle());
	}
	
	@Override
    public Object getRowKey(Vehicle vehicle) {
        return vehicle != null ? vehicle.getIdVehicle() : null;
    }

    @Override
    public Vehicle getRowData(String rowKey) {
		List<Vehicle> list = (List<Vehicle>) getWrappedData();

        for (Vehicle p : list) {
            if (p.getIdVehicle().toString().equals(rowKey)) {
                return p;
            }
        }

        return null;
    }
    
    public List<String> complete(String busca) {
		List<String> resultados = new ArrayList<String>();
		for (Vehicle o : vehicleRepository.findAll()) {
			if ((o.getRegistration().toLowerCase()).contains(busca.toLowerCase())) {
    			resultados.add(o.toString());
			}
		}
		return resultados;
	}
    
    public void limparCampos() {
    	registration = new String();
    	code = null;
    	vehicle = null;
    }
    
    public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getRegistration() {
		return registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

	public Long getMilage() {
		return milage;
	}

	public void setMilage(Long milage) {
		this.milage = milage;
	}

}
