package com.rasz.bean;

import java.util.ArrayList;
import java.util.List;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.rasz.domain.FlowRuleEnum;
import com.rasz.domain.Allowance;
import com.rasz.repository.AllowanceRepository;
import com.rasz.utils.FlowUtilsFactory;
import com.rasz.utils.Utils;

public class AllowanceCriteria extends AbstractCriteria<Allowance> {

	private static final long serialVersionUID = 1L;
	
	@Autowired private transient AllowanceRepository allowanceRepository;
	
	private Allowance allowance;
	private String description;
	private Integer code;
	
	@Override
	public Page<Allowance> load(Pageable pageable) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {	
			return allowanceRepository.listarTodos(description, code, pageable);		
		}else{
			return null;
		}
	}
	
	public void someListenerSelecionarAllowance(SelectEvent event){
		if(!Utils.isMobileDevice())
			return;
		
		if(event != null){
			allowance  = (Allowance) event.getObject();
			FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).put("allowance", allowance);
		}
	}
	
	public Allowance retornaAllowance(){
		return allowanceRepository.findOne(((Allowance) this.allowance).getIdAllowance());
	}
	
	@Override
    public Object getRowKey(Allowance allowance) {
        return allowance != null ? allowance.getIdAllowance() : null;
    }

    @Override
    public Allowance getRowData(String rowKey) {
		List<Allowance> list = (List<Allowance>) getWrappedData();

        for (Allowance p : list) {
            if (p.getIdAllowance().toString().equals(rowKey)) {
                return p;
            }
        }

        return null;
    }
    
    public List<String> complete(String busca) {
		List<String> resultados = new ArrayList<String>();
		for (Allowance o : allowanceRepository.findAll()) {
			if ((o.getDescription().toLowerCase()).contains(busca.toLowerCase())) {
    			resultados.add(o.toString());
			}
		}
		return resultados;
	}
    
    public void limparCampos() {
    	description = new String();
    	code = null;
    	allowance = null;
    }
    
    public Allowance getAllowance() {
		return allowance;
	}

	public void setAllowance(Allowance allowance) {
		this.allowance = allowance;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
