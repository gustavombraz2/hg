package com.rasz.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;

import com.rasz.domain.Contract;
import com.rasz.domain.Employee;
import com.rasz.domain.EmployeeAllowance;
import com.rasz.domain.EmployeeHoliday;
import com.rasz.domain.FlowRuleEnum;
import com.rasz.domain.HolidayCategory;
import com.rasz.domain.HolidayDay;
import com.rasz.domain.Usuario;
import com.rasz.repository.ContractRepository;
import com.rasz.repository.EmployeeAllowanceRepository;
import com.rasz.repository.EmployeeHolidayRepository;
import com.rasz.repository.EmployeeRepository;
import com.rasz.repository.HolidayCategoryRepository;
import com.rasz.repository.HolidayDayRepository;
import com.rasz.repository.UsuarioRepository;
import com.rasz.utils.FlowUtilsFactory;
import com.rasz.utils.Utils;

public class MyHolidayCriteria extends AbstractCriteria<EmployeeHoliday> {

	private static final long serialVersionUID = 1L;
	
	@Autowired private transient EmployeeHolidayRepository employeeHolidayRepository;
	@Autowired private transient EmployeeRepository employeeRepository;
	@Autowired private transient HolidayCategoryRepository holidayCategoryRepository;
	@Autowired private transient HolidayDayRepository holidayDayRepository;
	@Autowired private transient EmployeeAllowanceRepository employeeAllowanceRepository;
	@Autowired private transient UsuarioRepository usuarioRepository;
	@Autowired private transient ContractRepository contractRepository;
	
	private EmployeeHoliday employeeHoliday;
	private Employee employee = new Employee();
	private EmployeeAllowance employeeAllowance = new EmployeeAllowance();
	private Contract contract = new Contract();
	private HolidayCategory holidayCategory = new HolidayCategory();
	private Long selectedYear = (long)Utils.getDateYear(new Date());
	List<HolidayCategory> holidayCategoryList = new ArrayList<>();
	private String text = "";
	private String requestedDays= "";
	private String balance = "";
	private String allowance = "";
	boolean init = true;
	
	@Override
	public Page<EmployeeHoliday> load(Pageable pageable) {
		if (employee != null) {
			holidayCategoryList = holidayCategoryRepository.listarTodos("", 0);
			updateEmployeeAllowance();
			
			Long idHolidayCategory = 0L;
			if(holidayCategory != null){
				if(holidayCategory.getIdHolidayCategory() != null && holidayCategory.getIdHolidayCategory() > 0){
					idHolidayCategory = holidayCategory.getIdHolidayCategory();
				}
			}
			
			PageImpl<EmployeeHoliday> page = (PageImpl<EmployeeHoliday>) employeeHolidayRepository.listarTodos(employee.getIdEmployee(), idHolidayCategory, pageable);
			
			List<EmployeeHoliday> listPage = (List<EmployeeHoliday>)page.getContent();
			
			getBalanceMath();
			
			return new PageImpl<EmployeeHoliday>(listPage, pageable, page.getTotalElements());
		}else{
			return null;
		}
	}
	
	@Transactional
	public void updateEmployeeAllowance() {
		List<Long> years = new ArrayList<>();
		years.add((long)Utils.getDateYear(new Date()) - 1);
		years.add((long)Utils.getDateYear(new Date()));
		years.add((long)Utils.getDateYear(new Date()) + 1);
		for (Long y : years) {
			List<HolidayDay> listDaysByEmployeeYear = holidayDayRepository.countHoursDaysByEmployeeYear(employee.getIdEmployee().toString(), y);
			for (HolidayDay hd : listDaysByEmployeeYear) {
				long d = 0;
				long h = 0;
				if(hd.getEmployeeHoliday().getCountedDays() != null){
					d = hd.getEmployeeHoliday().getCountedDays();
				}
				
				if(hd.getEmployeeHoliday().getCountedHours() != null){
					h = hd.getEmployeeHoliday().getCountedHours()/2;
				}
				employeeAllowanceRepository.updateDays(employee.getIdEmployee(), y, d, h);
			}
		}
	}
	
	public void getBalanceMath(){
		EmployeeHoliday eh = new EmployeeHoliday();
		employeeAllowance = employeeAllowanceRepository.findByEmployeeYear(employee.getIdEmployee(), selectedYear);
		
		if(employeeAllowance != null){
			long x = employeeAllowance.getTotalHoursCounted();
		    long y = contract.getAllowance().getWorkingHours();
		    double equi = (double)x/y;
		    Double balance = (double)employeeAllowance.getAvailableDays() - employeeAllowance.getTotalDaysCounted() - equi;
		    
		    setAllowance(String.valueOf((long)(double)employeeAllowance.getAvailableDays()));
		    setRequestedDays(eh.getUsedDayString((employeeAllowance.getTotalDaysCounted() + equi), y));
		    setBalance(eh.getBalanceString(balance, y));
		}else{
			setAllowance("-");
		    setRequestedDays("-");
		    setBalance("-");
		}
	}
	
	public void loading(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario u = (Usuario) auth.getPrincipal();
		u = usuarioRepository.findByLogin(u.getLogin());
		employee = employeeRepository.findByUsuario(u.getId());
		
		setInit(true);
		if (employee != null) {
			contract = contractRepository.findByEmployee(employee.getIdEmployee());
			if(contract == null){
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Warn!", "This Employee has no contract.");
				FacesContext.getCurrentInstance().addMessage(null, message);
				setEmployee(null);
			}else{
				setText("Hello "+employee.getName());
				getBalanceMath();
			}
		}else{
			setText("Please link User to an Employee!");
			setInit(false);
		}
	}
	
	public void someListenerSelecionarEmployeeHoliday(SelectEvent event){
		if(!Utils.isMobileDevice())
			return;
		
		if(event != null){
			employeeHoliday = (EmployeeHoliday) event.getObject();
			FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).put("employeeHoliday", employeeHoliday);
		}
	}
	
	public List<String> completeHolidayCategory(String busca) {
		List<String> resultados = new ArrayList<String>();
		
		for (HolidayCategory holidayCategory : holidayCategoryList) {
			if (holidayCategory.getDescription().toLowerCase().contains(busca.toLowerCase())){
    			resultados.add(holidayCategory.toString());
			}
		}
		
		return resultados;
	}
	
	public EmployeeHoliday retornaEmployeeHoliday(){
		return employeeHolidayRepository.findOne(((EmployeeHoliday) this.employeeHoliday).getIdEmployeeHoliday());
	}
	
	@Override
    public Object getRowKey(EmployeeHoliday employeeHoliday) {
        return employeeHoliday != null ? employeeHoliday.getIdEmployeeHoliday() : null;
    }

    @Override
    public EmployeeHoliday getRowData(String rowKey) {
		List<EmployeeHoliday> list = (List<EmployeeHoliday>) getWrappedData();

        for (EmployeeHoliday p : list) {
            if (p.getIdEmployeeHoliday().toString().equals(rowKey)) {
                return p;
            }
        }

        return null;
    }
    
    
    public void limparCampos() {
    	employee = new Employee();
    	holidayCategory = new HolidayCategory();
    	employeeHoliday = null;
    }
    
    public EmployeeHoliday getEmployeeHoliday() {
		return employeeHoliday;
	}

	public void setEmployeeHoliday(EmployeeHoliday employeeHoliday) {
		this.employeeHoliday = employeeHoliday;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public HolidayCategory getHolidayCategory() {
		return holidayCategory;
	}

	public void setHolidayCategory(HolidayCategory holidayCategory) {
		this.holidayCategory = holidayCategory;
	}

	public List<HolidayCategory> getHolidayCategoryList() {
		return holidayCategoryList;
	}

	public void setHolidayCategoryList(List<HolidayCategory> holidayCategoryList) {
		this.holidayCategoryList = holidayCategoryList;
	}
	
	public String getDate(Date date) {
		return Utils.formataDataStringSemHora(date);
	}
	
	public String toStringHeaderBalance(){
		if(selectedYear != null){
			return "Balance " + selectedYear.toString();
		}else{
			return "";
		}
	}

	public Long getSelectedYear() {
		return selectedYear;
	}

	public void setSelectedYear(Long selectedYear) {
		this.selectedYear = selectedYear;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isInit() {
		return init;
	}

	public void setInit(boolean init) {
		this.init = init;
	}

	public EmployeeAllowance getEmployeeAllowance() {
		return employeeAllowance;
	}

	public void setEmployeeAllowance(EmployeeAllowance employeeAllowance) {
		this.employeeAllowance = employeeAllowance;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public String getRequestedDays() {
		return requestedDays;
	}

	public void setRequestedDays(String requestedDays) {
		this.requestedDays = requestedDays;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getAllowance() {
		return allowance;
	}

	public void setAllowance(String allowance) {
		this.allowance = allowance;
	}
	
}
