package com.rasz.bean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.webflow.engine.RequestControlContext;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.RequestContextHolder;

import com.rasz.controller.MainController;
import com.rasz.domain.Allowance;
import com.rasz.domain.Contract;
import com.rasz.domain.Employee;
import com.rasz.domain.FlowRuleEnum;
import com.rasz.report.RepHolidayForm;
import com.rasz.repository.AllowanceRepository;
import com.rasz.repository.ContractRepository;
import com.rasz.repository.EmployeeRepository;
import com.rasz.utils.FlowUtilsFactory;
import com.rasz.utils.Utils;

public class ContractCriteria extends AbstractCriteria<Contract> {

	private static final long serialVersionUID = 1L;
	
	@Autowired private transient ContractRepository contractRepository;
	@Autowired private transient EmployeeRepository employeeRepository;
	@Autowired private transient AllowanceRepository allowanceRepository;
	
	private Contract contract;
	private Long code;
	private Allowance allowance;
	private Employee employee;
	List<Employee> employeeList = new ArrayList<>();
	List<Allowance> allowanceList = new ArrayList<>();
	
	@Override
	public Page<Contract> load(Pageable pageable) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		long idAllowance = 0;
		long idEmployee = 0;
		employeeList = employeeRepository.getAll();
		allowanceList = allowanceRepository.listarTodos("", 0);
		
		if(allowance != null){
			if(allowance.getIdAllowance() != null && allowance.getIdAllowance() > 0){
				idAllowance = allowance.getIdAllowance();
			}
		}
		
		if(employee != null){
			if(employee.getIdEmployee() != null && employee.getIdEmployee() > 0){
				idEmployee = employee.getIdEmployee();
			}
		}
		
		if (auth != null) {	
			return contractRepository.listarTodos(code, idAllowance, idEmployee, pageable);
		}else{
			return null;
		}
	}
	
	public List<String> completeEmployee(String busca) {
		List<String> resultados = new ArrayList<String>();
		
		for (Employee employee : employeeList) {
			if (employee.getFullName().toLowerCase().contains(busca.toLowerCase())) {
    			resultados.add(employee.toString());
			}
		}
		return resultados;
	}
	
	public List<String> completeAllowance(String busca) {
		List<String> resultados = new ArrayList<String>();
		
		for (Allowance allowance : allowanceList) {
			if (allowance.getDescription().toLowerCase().contains(busca.toLowerCase())) {
    			resultados.add(allowance.toString());
			}
		}
		return resultados;
	}
	
	public void someListenerSelecionarContract(SelectEvent event){
		if(!Utils.isMobileDevice())
			return;
		
		if(event != null){
			contract  = (Contract) event.getObject();
			RequestContext requestContext = RequestContextHolder.getRequestContext();
			RequestControlContext rec = (RequestControlContext) requestContext;
			rec.handleEvent(new Event(this, "listarProdutos"));
			FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).put("contract", contract);
		}
	}
	
	public Contract retornacontract(){
		return contractRepository.findOne(((Contract) this.contract).getIdContract());
	}
	
	@Override
    public Object getRowKey(Contract contract) {
        return contract != null ? contract.getIdContract() : null;
    }

    @Override
    public Contract getRowData(String rowKey) {
		List<Contract> list = (List<Contract>) getWrappedData();

        for (Contract p : list) {
            if (p.getIdContract().toString().equals(rowKey)) {
                return p;
            }
        }

        return null;
    }
    
    
    public void limparCampos() {
    	code = null;
    	setAllowance(null);
    	setEmployee(null);
    	contract = null;
    }
    
    public Contract getContract() {
		return contract;
	}


	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public Allowance getAllowance() {
		return allowance;
	}

	public void setAllowance(Allowance allowance) {
		this.allowance = allowance;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}

	public List<Allowance> getAllowanceList() {
		return allowanceList;
	}

	public void setAllowanceList(List<Allowance> allowanceList) {
		this.allowanceList = allowanceList;
	}

}
