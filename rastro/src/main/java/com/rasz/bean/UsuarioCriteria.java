package com.rasz.bean;

import java.util.ArrayList;
import java.util.List;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.webflow.engine.RequestControlContext;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.RequestContextHolder;

import com.rasz.domain.FlowRuleEnum;
import com.rasz.domain.Usuario;
import com.rasz.repository.UsuarioRepository;
import com.rasz.utils.FlowUtilsFactory;
import com.rasz.utils.Utils;

public class UsuarioCriteria extends AbstractCriteria<Usuario> {

	private static final long serialVersionUID = 1L;
	
	@Autowired private transient UsuarioRepository usuarioRepository;
	
	private Usuario usuario;
	private Boolean ativo;	
	private String nome;
	private String login;
	private String email;
	private Integer code;
	
	@Override
	public Page<Usuario> load(Pageable pageable) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {	
			return usuarioRepository.listarTodos(nome, login, email, code, ativo, pageable);		
		}else{
			return null;
		}
	}
	
	public void someListenerSelecionarUsuario(SelectEvent event){
		if(!Utils.isMobileDevice())
			return;
		
		if(event != null){
			usuario  = (Usuario) event.getObject();
			RequestContext requestContext = RequestContextHolder.getRequestContext();
			RequestControlContext rec = (RequestControlContext) requestContext;
			rec.handleEvent(new Event(this, "listarProdutos"));
			FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).put("usuario", usuario);
		}
	}
	
	public Usuario retornausuario(){
		return usuarioRepository.findOne(((Usuario) this.usuario).getId());
	}
	
	@Override
    public Object getRowKey(Usuario usuario) {
        return usuario != null ? usuario.getId() : null;
    }

    @Override
    public Usuario getRowData(String rowKey) {
		List<Usuario> list = (List<Usuario>) getWrappedData();

        for (Usuario p : list) {
            if (p.getId().toString().equals(rowKey)) {
                return p;
            }
        }

        return null;
    }
    
    public List<String> complete(String busca) {
		List<String> resultados = new ArrayList<String>();
		for (Usuario o : usuarioRepository.findAll()) {
			if ((o.getNmUsuario().toLowerCase().concat(" - ").concat(o.getLogin().toLowerCase())).contains(busca.toLowerCase())) {
    			resultados.add(o.toString());
			}
		}
		return resultados;
	}
    
    public void limparCampos() {
    	ativo = null;
    	nome = new String();
    	login = new String();
    	email = new String();
    	code = null;
    	usuario = null;
    }
    
    public Usuario getUsuario() {
		return usuario;
	}


	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public String getDescricao() {
		return nome;
	}

	public void setDescricao(String nome) {
		this.nome = nome;
	}
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

}
