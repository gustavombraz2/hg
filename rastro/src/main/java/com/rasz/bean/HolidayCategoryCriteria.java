package com.rasz.bean;

import java.util.ArrayList;
import java.util.List;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.rasz.domain.FlowRuleEnum;
import com.rasz.domain.HolidayCategory;
import com.rasz.repository.HolidayCategoryRepository;
import com.rasz.utils.FlowUtilsFactory;
import com.rasz.utils.Utils;

public class HolidayCategoryCriteria extends AbstractCriteria<HolidayCategory> {

	private static final long serialVersionUID = 1L;
	
	@Autowired private transient HolidayCategoryRepository holidayCategoryRepository;
	
	private HolidayCategory holidayCategory;
	private String description;
	private Integer code;
	
	@Override
	public Page<HolidayCategory> load(Pageable pageable) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {	
			return holidayCategoryRepository.listarTodos(description, code, pageable);		
		}else{
			return null;
		}
	}
	
	public void someListenerSelecionarHolidayCategory(SelectEvent event){
		if(!Utils.isMobileDevice())
			return;
		
		if(event != null){
			holidayCategory  = (HolidayCategory) event.getObject();
			FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).put("holidayCategory", holidayCategory);
		}
	}
	
	public HolidayCategory retornaHolidayCategory(){
		return holidayCategoryRepository.findOne(((HolidayCategory) this.holidayCategory).getIdHolidayCategory());
	}
	
	@Override
    public Object getRowKey(HolidayCategory holidayCategory) {
        return holidayCategory != null ? holidayCategory.getIdHolidayCategory() : null;
    }

    @Override
    public HolidayCategory getRowData(String rowKey) {
		List<HolidayCategory> list = (List<HolidayCategory>) getWrappedData();

        for (HolidayCategory p : list) {
            if (p.getIdHolidayCategory().toString().equals(rowKey)) {
                return p;
            }
        }

        return null;
    }
    
    public List<String> complete(String busca) {
		List<String> resultados = new ArrayList<String>();
		for (HolidayCategory o : holidayCategoryRepository.findAll()) {
			if ((o.getDescription().toLowerCase()).contains(busca.toLowerCase())) {
    			resultados.add(o.toString());
			}
		}
		return resultados;
	}
    
    public void limparCampos() {
    	description = new String();
    	code = null;
    	holidayCategory = null;
    }
    
    public HolidayCategory getHolidayCategory() {
		return holidayCategory;
	}

	public void setHolidayCategory(HolidayCategory holidayCategory) {
		this.holidayCategory = holidayCategory;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
