package com.rasz.bean;

import java.util.ArrayList;
import java.util.List;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.webflow.engine.RequestControlContext;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.RequestContextHolder;

import com.rasz.domain.FlowRuleEnum;
import com.rasz.domain.Role;
import com.rasz.repository.RoleRepository;
import com.rasz.utils.FlowUtilsFactory;
import com.rasz.utils.Utils;

public class RoleCriteria extends AbstractCriteria<Role> {

	private static final long serialVersionUID = 1L;
	
	@Autowired private transient RoleRepository roleRepository;
	
	private Role role;
	private String description;
	private Integer code;
	
	@Override
	public Page<Role> load(Pageable pageable) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {	
			return roleRepository.listarTodos(description, code, pageable);		
		}else{
			return null;
		}
	}
	
	public void someListenerSelecionarRole(SelectEvent event){
		if(!Utils.isMobileDevice())
			return;
		
		if(event != null){
			role  = (Role) event.getObject();
			FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).put("role", role);
		}
	}
	
	public Role retornaRole(){
		return roleRepository.findOne(((Role) this.role).getIdRole());
	}
	
	@Override
    public Object getRowKey(Role role) {
        return role != null ? role.getIdRole() : null;
    }

    @Override
    public Role getRowData(String rowKey) {
		List<Role> list = (List<Role>) getWrappedData();

        for (Role p : list) {
            if (p.getIdRole().toString().equals(rowKey)) {
                return p;
            }
        }

        return null;
    }
    
    public List<String> complete(String busca) {
		List<String> resultados = new ArrayList<String>();
		for (Role o : roleRepository.findAll()) {
			if ((o.getDescription().toLowerCase()).contains(busca.toLowerCase())) {
    			resultados.add(o.toString());
			}
		}
		return resultados;
	}
    
    public void limparCampos() {
    	description = new String();
    	code = null;
    	role = null;
    }
    
    public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
