package com.rasz.bean;

import java.util.List;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.rasz.domain.FlowRuleEnum;
import com.rasz.domain.Perfil;
import com.rasz.repository.PerfilRepository;
import com.rasz.utils.FlowUtilsFactory;
import com.rasz.utils.Utils;

public class PerfilCriteria extends AbstractCriteria<Perfil> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String descricao = "";
	Long codigo = null;
	Boolean ativo = null;
	Perfil perfil;	
	
	@Autowired private transient PerfilRepository perfilRepository;

	@Override
	public Page<Perfil> load(Pageable pageable) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			return perfilRepository.filter(descricao, codigo, ativo, pageable);
		}
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Object getRowKey(Perfil perfil) {
		return perfil != null ? perfil.getId() : null;
	}
	
	public void someListenerSelecionarPerfil(SelectEvent event){
		if(!Utils.isMobileDevice())
			return;
		
		if(event != null){
			perfil  = (Perfil) event.getObject();
			FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).put("perfil", perfil);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Perfil getRowData(String rowKey) {
		List<Perfil> list = (List<Perfil>) getWrappedData();

		for (Perfil p : list) {
			if (p.getId().toString().equals(rowKey)) {
				return p;
			}
		}

		return null;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}
	
	public void limparCampos() {
		ativo = null;
		codigo = null;
		descricao = null;
	}
	
}
