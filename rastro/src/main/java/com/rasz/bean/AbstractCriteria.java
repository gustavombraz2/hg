package com.rasz.bean;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

public abstract class AbstractCriteria<T> extends LazyDataModel<T> {	
	private static final long serialVersionUID = -4020095057483426977L;

	private T selected;
	
	public T getSelected() {
		return selected;
	}

	public void setSelected(T selected) {
		this.selected = selected;
	}

	@Override
	public List<T> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		PageRequest request = null;
		if (sortOrder == SortOrder.UNSORTED || sortField == null) {
			request = new PageRequest(first/pageSize, pageSize);
		} else {
			Direction dir = sortOrder == SortOrder.ASCENDING ? Direction.ASC : Direction.DESC;
			Sort sort = new Sort(dir, sortField);
			request = new PageRequest(first/pageSize, pageSize, sort);
		}
		Page<T> page = this.load(request);
		this.setRowCount((int) page.getTotalElements());
		return page.getContent();
	}
	
	public abstract Page<T> load(Pageable pageable);

}
