package com.rasz.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.rasz.domain.PerfilPrivilegio;
import com.rasz.domain.Usuario;

public class UserDetails extends Usuario implements org.springframework.security.core.userdetails.UserDetails {
	private static final long serialVersionUID = 3412954666382722061L;
	
	private List<GrantedAuthority> authorities;

	public UserDetails(Usuario usuario) {
		this.setLogin(usuario.getLogin());
		this.setNmUsuario(usuario.getNmUsuario());
		this.setEmail(usuario.getEmail());
		this.setSenha(usuario.getSenha());
		this.setPerfil(usuario.getPerfil());
		this.setBoAtivo(usuario.getBoAtivo());
//		this.setUnidadeNegocio(usuario.getUnidadeNegocio());
		this.setId(usuario.getId());
	}

	public Collection<? extends GrantedAuthority> getAuthorities() {
		if (authorities == null) {
			authorities = new ArrayList<GrantedAuthority>();
			authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
			
			if (this.getPerfil() != null && this.getPerfil().getPrivilegios() != null) {
				for (PerfilPrivilegio perfilPrivilegio : this.getPerfil().getPrivilegios()) {
					if(perfilPrivilegio.getId() == 53){
						System.out.println(" ");
					}
					
					if(!perfilPrivilegio.getPrivilegio().getNome().equals("ROLE_USER")){
						authorities.add(new SimpleGrantedAuthority(perfilPrivilegio.getPrivilegio().getNome().toUpperCase()));
						
						if (perfilPrivilegio.isConsultar()) {
							authorities.add(new SimpleGrantedAuthority(perfilPrivilegio.getPrivilegio().getNome().toUpperCase() + 
																	   "_CONSULTAR"));
						}
						if (perfilPrivilegio.isIncluir()) {
							authorities.add(new SimpleGrantedAuthority(perfilPrivilegio.getPrivilegio().getNome().toUpperCase() + 
																	   "_INCLUIR"));
						}
						if (perfilPrivilegio.isAlterar()) {
							authorities.add(new SimpleGrantedAuthority(perfilPrivilegio.getPrivilegio().getNome().toUpperCase() + 
																	   "_ALTERAR"));
						}
						if (perfilPrivilegio.isRemover()) {
							authorities.add(new SimpleGrantedAuthority(perfilPrivilegio.getPrivilegio().getNome().toUpperCase() + 
																	   "_REMOVER"));
						}
						if (perfilPrivilegio.isImprimir()) {
							authorities.add(new SimpleGrantedAuthority(perfilPrivilegio.getPrivilegio().getNome().toUpperCase() + 
																	   "_IMPRIMIR"));
						}
						if (perfilPrivilegio.isColetor()) {
							authorities.add(new SimpleGrantedAuthority(perfilPrivilegio.getPrivilegio().getNome().toUpperCase() + 
																	   "_COLETOR"));
						}
					}
				}
			}
		}
		return authorities;
	}

	public String getUsername() {
		return this.getLogin();
	}

	public boolean isAccountNonExpired() {
		return true;
	}

	public boolean isAccountNonLocked() {
		return true;
	}

	public boolean isCredentialsNonExpired() {
		return true;
	}

	public boolean isEnabled() {
		return this.getBoAtivo();
	}

	public String getPassword() {
		return this.getSenha();
	}
	
}