package com.rasz.bean;

import java.util.ArrayList;
import java.util.List;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.rasz.domain.Department;
import com.rasz.domain.Employee;
import com.rasz.domain.FlowRuleEnum;
import com.rasz.domain.Role;
import com.rasz.repository.DepartmentRepository;
import com.rasz.repository.EmployeeRepository;
import com.rasz.repository.RoleRepository;
import com.rasz.utils.FlowUtilsFactory;
import com.rasz.utils.Utils;

public class EmployeeCriteria extends AbstractCriteria<Employee> {

	private static final long serialVersionUID = 1L;
	
	@Autowired private transient EmployeeRepository employeeRepository;
	@Autowired private transient RoleRepository roleRepository;
	@Autowired private transient DepartmentRepository departmentRepository;
	
	private Employee employee;
	private Role role;
	private String name;
	private String eeNumber;
	private List<Role> roleList = new ArrayList<>();
	private List<Department> departmentList = new ArrayList<>();
	
	@Override
	public Page<Employee> load(Pageable pageable) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Long idRole = 0L;
		if(role != null){
			idRole = role.getIdRole();
		}
		
		roleList = roleRepository.listarTodos(null, null);
		departmentList = departmentRepository.listarTodos(null, null);
		
		if (auth != null) {	
			return employeeRepository.listarTodos(name, eeNumber, idRole, pageable);
		}else{
			return null;
		}
	}
	
	public List<String> completeRole(String busca) {
		List<String> resultados = new ArrayList<String>();
		
		for (Role role : roleList) {
			if (role.getDescription().toLowerCase().contains(busca.toLowerCase())) {
    			resultados.add(role.toString());
			}
		}
		return resultados;
	}
	
	public List<String> completeDepartment(String busca) {
		List<String> resultados = new ArrayList<String>();
		
		for (Department department : departmentList) {
			if (department.getDescription().toLowerCase().contains(busca.toLowerCase())) {
    			resultados.add(department.toString());
			}
		}
		return resultados;
	}
	
	public void someListenerSelecionarEmployee(SelectEvent event){
		if(!Utils.isMobileDevice())
			return;
		
		if(event != null){
			employee  = (Employee) event.getObject();
			FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).put("employee", employee);
		}
	}
	
	public Employee retornaEmployee(){
		return employeeRepository.findOne(((Employee) this.employee).getIdEmployee());
	}
	
	@Override
    public Object getRowKey(Employee employee) {
        return employee != null ? employee.getIdEmployee() : null;
    }

    @Override
    public Employee getRowData(String rowKey) {
		List<Employee> list = (List<Employee>) getWrappedData();

        for (Employee p : list) {
            if (p.getIdEmployee().toString().equals(rowKey)) {
                return p;
            }
        }

        return null;
    }
    
    public void limparCampos() {
    	name = new String();
    	eeNumber = null;
    	employee = null;
    	role = new Role();
    }
    
    public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public List<Role> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<Role> roleList) {
		this.roleList = roleList;
	}

	public String getEeNumber() {
		return eeNumber;
	}

	public void setEeNumber(String eeNumber) {
		this.eeNumber = eeNumber;
	}

	public List<Department> getDepartmentList() {
		return departmentList;
	}

	public void setDepartmentList(List<Department> departmentList) {
		this.departmentList = departmentList;
	}

}
