package com.rasz.bean;

import java.util.ArrayList;
import java.util.List;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.rasz.domain.Department;
import com.rasz.domain.FlowRuleEnum;
import com.rasz.repository.DepartmentRepository;
import com.rasz.utils.FlowUtilsFactory;
import com.rasz.utils.Utils;

public class DepartmentCriteria extends AbstractCriteria<Department> {

	private static final long serialVersionUID = 1L;
	
	@Autowired private transient DepartmentRepository departmentRepository;
	
	private Department department;
	private String description;
	private Integer code;
	
	@Override
	public Page<Department> load(Pageable pageable) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {	
			return departmentRepository.listarTodos(description, code, pageable);		
		}else{
			return null;
		}
	}
	
	public void someListenerSelecionarDepartment(SelectEvent event){
		if(!Utils.isMobileDevice())
			return;
		
		if(event != null){
			department  = (Department) event.getObject();
			FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).put("department", department);
		}
	}
	
	public Department retornaDepartment(){
		return departmentRepository.findOne(((Department) this.department).getIdDepartment());
	}
	
	@Override
    public Object getRowKey(Department department) {
        return department != null ? department.getIdDepartment() : null;
    }

    @Override
    public Department getRowData(String rowKey) {
		List<Department> list = (List<Department>) getWrappedData();

        for (Department p : list) {
            if (p.getIdDepartment().toString().equals(rowKey)) {
                return p;
            }
        }

        return null;
    }
    
    public List<String> complete(String busca) {
		List<String> resultados = new ArrayList<String>();
		for (Department o : departmentRepository.findAll()) {
			if ((o.getDescription().toLowerCase()).contains(busca.toLowerCase())) {
    			resultados.add(o.toString());
			}
		}
		return resultados;
	}
    
    public void limparCampos() {
    	description = new String();
    	code = null;
    	department = null;
    }
    
    public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
