package com.rasz.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rasz.domain.Perfil;
import com.rasz.service.PrivilegioService;

@Component
public class PrivilegioConverterByDescricao implements Converter {
 
     @Autowired private PrivilegioService privilegioService;
 
     public Object getAsObject(FacesContext context, UIComponent component, String value) {
         if (value != null) {
             return privilegioService.findByDescricao(value);
         }
         return null;
     }
 
     public String getAsString(FacesContext context, UIComponent component, Object value) {
         if (value instanceof Perfil) {
			 return ((Perfil) value).getDescricao();
		 }
         return null;
    }
}
