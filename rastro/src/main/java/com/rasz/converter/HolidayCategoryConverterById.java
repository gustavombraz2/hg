package com.rasz.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rasz.domain.HolidayCategory;
import com.rasz.repository.HolidayCategoryRepository;

@Component
public class HolidayCategoryConverterById implements Converter {

	@Autowired
	private HolidayCategoryRepository holidayCategoryRepository;

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		try {
			String[] obj = value.replaceAll(" - ", " ").split(" ");
			HolidayCategory holidayCategory=  holidayCategoryRepository.findOne(Long.parseLong(obj[0]));
			 if(holidayCategory == null)
				 return new HolidayCategory();
			 
            return holidayCategory;
		} catch (NumberFormatException e) {
			return validateValue(value);
		}
	}
	
	private Object validateValue(String value) {
		//String valores[] = value.replace(" ", "").split("-");
		String valores[] = value.split(" - ");

		if(valores.length > 1){
			return holidayCategoryRepository.findByName(valores[0]);
		}
		
		return null;
	}


	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof HolidayCategory) {
			if (((HolidayCategory) value).getIdHolidayCategory() != null) {
				return String.valueOf(((HolidayCategory) value).toString());
			}  
		}
		return null;
	}
}

