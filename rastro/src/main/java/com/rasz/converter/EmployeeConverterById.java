package com.rasz.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rasz.domain.Employee;
import com.rasz.repository.EmployeeRepository;

@Component
public class EmployeeConverterById implements Converter {

	@Autowired
	private EmployeeRepository employeeRepository;

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		try {
			String[] obj = value.replaceAll(" - ", " ").split(" ");
			Employee employee=  employeeRepository.findOne(Long.parseLong(obj[0]));
			 if(employee == null)
				 return new Employee();
			 
            return employee;
		} catch (NumberFormatException e) {
			return validateValue(value);
		}
	}
	
	private Object validateValue(String value) {
		//String valores[] = value.replace(" ", "").split("-");
		String valores[] = value.split(" - ");

		if(valores.length > 1){
			return employeeRepository.findByName(valores[0]);
		}
		
		return null;
	}


	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof Employee) {
			if (((Employee) value).getIdEmployee() != null) {
				return String.valueOf(((Employee) value).toString());
			}  
		}
		return null;
	}
}

