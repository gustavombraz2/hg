package com.rasz.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rasz.domain.Usuario;
import com.rasz.service.UsuarioService;

@Component
public class UsuarioConverterById implements Converter {

	@Autowired private UsuarioService usuarioService;

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		try {
			if(value != null && !value.toUpperCase().contains("SELECIONE")) {
	            return usuarioService.findOne(Long.parseLong(value));
	        }else{
	        	return null;
	        }
	         
		} catch (NumberFormatException e) {
			//Separa o Codigo do Nome, para Fazer a Busca no Banco de Dados
			String valores[] = value.split(" - ");
			 
			return usuarioService.findOne(Long.parseLong(valores[0]));
		}
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof Usuario) {
			if(((Usuario) value).getId() != null && ((Usuario) value).getId() != 0){
				return String.valueOf(((Usuario) value).toString());
			}else{
				return "";
			}
		}
		return null;
	}

}
