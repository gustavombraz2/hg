package com.rasz.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.rasz.domain.Allowance;
import com.rasz.repository.AllowanceRepository;

@Component
public class AllowanceConverterById implements Converter {

	@Autowired
	private AllowanceRepository allowanceRepository;

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		try {
			String[] obj = value.replaceAll(" - ", " ").split(" ");
			Allowance allowance=  allowanceRepository.findOne(Long.parseLong(obj[0]));
			 if(allowance == null)
				 return new Allowance();
			 
            return allowance;
		} catch (NumberFormatException e) {
			return validateValue(value);
		}
	}
	
	private Object validateValue(String value) {
		//String valores[] = value.replace(" ", "").split("-");
		String valores[] = value.split(" - ");

		if(valores.length > 1){
			return allowanceRepository.findByName(valores[0]);
		}
		
		return null;
	}


	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof Allowance) {
			if (((Allowance) value).getIdAllowance() != null) {
				return String.valueOf(((Allowance) value).toString());
			}  
		}
		return null;
	}
}

