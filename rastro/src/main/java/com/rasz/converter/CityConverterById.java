package com.rasz.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rasz.domain.City;
import com.rasz.repository.CityRepository;

@Component
public class CityConverterById implements Converter {

	@Autowired
	private CityRepository cityRepository;

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		try {
			String[] obj = value.replaceAll(" - ", " ").split(" ");
			City city=  cityRepository.findOne(Long.parseLong(obj[0]));
			 if(city == null)
				 return new City();
			 
            return city;
		} catch (NumberFormatException e) {
			return validateValue(value);
		}
	}
	
	private Object validateValue(String value) {
		//String valores[] = value.replace(" ", "").split("-");
		String valores[] = value.split(" - ");

		if(valores.length > 1){
			return cityRepository.findByName(valores[0]);
		}
		
		return null;
	}


	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof City) {
			if (((City) value).getIdCity() != null) {
				return String.valueOf(((City) value).toString());
			}  
		}
		return null;
	}
}

