package com.rasz.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rasz.domain.Department;
import com.rasz.repository.DepartmentRepository;

@Component
public class DepartmentConverterById implements Converter {

	@Autowired
	private DepartmentRepository departmentRepository;

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		try {
			String[] obj = value.replaceAll(" - ", " ").split(" ");
			Department department=  departmentRepository.findOne(Long.parseLong(obj[0]));
			 if(department == null)
				 return new Department();
			 
            return department;
		} catch (NumberFormatException e) {
			return validateValue(value);
		}
	}
	
	private Object validateValue(String value) {
		//String valores[] = value.replace(" ", "").split("-");
		String valores[] = value.split(" - ");

		if(valores.length > 1){
			return departmentRepository.findByName(valores[0]);
		}
		
		return null;
	}


	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof Department) {
			if (((Department) value).getIdDepartment() != null) {
				return String.valueOf(((Department) value).toString());
			}  
		}
		return null;
	}
}

