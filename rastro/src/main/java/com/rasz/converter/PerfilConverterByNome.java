package com.rasz.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rasz.domain.Perfil;
import com.rasz.service.PerfilService;

@Component
public class PerfilConverterByNome implements Converter {
 
     @Autowired private PerfilService perfilService;
 
     public Object getAsObject(FacesContext context, UIComponent component, String value) {
    	 if (value != null && !value.toUpperCase().contains("SELECIONE")) {
             return perfilService.findByNome(value);
         }
         return null;
     }
 
     public String getAsString(FacesContext context, UIComponent component, Object value) {
         if (value instanceof Perfil) {
			 return ((Perfil) value).getNome();
		 }
         return null;
    }
}
