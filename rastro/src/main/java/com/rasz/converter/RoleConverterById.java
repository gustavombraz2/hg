package com.rasz.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rasz.domain.Role;
import com.rasz.repository.RoleRepository;

@Component
public class RoleConverterById implements Converter {

	@Autowired
	private RoleRepository roleRepository;

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		try {
			String[] obj = value.replaceAll(" - ", " ").split(" ");
			Role role=  roleRepository.findOne(Long.parseLong(obj[0]));
			 if(role == null)
				 return new Role();
			 
            return role;
		} catch (NumberFormatException e) {
			return validateValue(value);
		}
	}
	
	private Object validateValue(String value) {
		//String valores[] = value.replace(" ", "").split("-");
		String valores[] = value.split(" - ");

		if(valores.length > 1){
			return roleRepository.findByName(valores[0]);
		}
		
		return null;
	}


	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof Role) {
			if (((Role) value).getIdRole() != null) {
				return String.valueOf(((Role) value).toString());
			}  
		}
		return null;
	}
}

