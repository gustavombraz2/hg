package com.rasz.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rasz.utils.Utils;

@Entity
@Table(name = "usuario")
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;


	@Id
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "pk_usuario")
//	@SequenceGenerator(name = "pk_usuario", sequenceName = "usuario_id_seq")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;	

	@Column(name = "nm_usuario",length = 80)
	private String nmUsuario;

	@Column(name = "bo_ativo",  columnDefinition="boolean not null default true")
	private boolean boAtivo;
	
	@Column(length = 25, unique = true)
	private String login; 
	
	@Column(length = 50)
	private String senha;
	
	@Column(length = 50, unique = true)
	private String email;
	
	@Transient
	private Perfil perfil;
			
	@Column(length = 50)
	@Transient
	private String senhaNova;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmUsuario() {
		return nmUsuario;
	}

	public void setNmUsuario(String nmUsuario) {
		this.nmUsuario = Utils.validaToUpperCase(nmUsuario);
	}

	public boolean getBoAtivo() {
		return boAtivo;
	}
	
	public String getBoAtivoDescricao() {
		return boAtivo == true ? "ativo" : "inativo";
	}

	public void setBoAtivo(boolean boAtivo) {
		this.boAtivo = boAtivo;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = Utils.validaToUpperCase(email);
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public String getSenhaNova() {
		return senhaNova;
	}

	public void setSenhaNova(String senhaNova) {
		this.senhaNova = senhaNova;
	}

	public boolean isNew() {
		return id == null;
	}
	
	@Override
	public String toString() {
		return this.id.toString() +" - "+ this.nmUsuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		return true; 
	}

}
