package com.rasz.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "bank_holiday")
public class BankHoliday implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id_bank_holiday")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idBankHoliday;	

	@Column(name = "description")
	private String description;
	
	@Column(name = "date")
	private Date date;
	
	public Long getIdBankHoliday() {
		return idBankHoliday;
	}

	public void setIdBankHoliday(Long idBankHoliday) {
		this.idBankHoliday = idBankHoliday;
	}

	public String getDescription() {
		if(description != null ){
			return description.toUpperCase();
		}else{
			return null;
		}
	}

	public void setDescription(String description) {
		if(description != null ){
			this.description = description.toUpperCase();
		}else{
			this.description = null;
		}
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return this.idBankHoliday.toString() +" - "+ this.description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idBankHoliday == null) ? 0 : idBankHoliday.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BankHoliday other = (BankHoliday) obj;
		if (idBankHoliday == null) {
			if (other.idBankHoliday != null)
				return false;
		} else if (!idBankHoliday.equals(other.idBankHoliday))
			return false;
		return true; 
	}
}
