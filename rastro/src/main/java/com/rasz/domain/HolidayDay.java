package com.rasz.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.data.annotation.Transient;

import com.rasz.utils.Utils;

@Entity
@Table(name = "holiday_day")
public class HolidayDay implements Serializable {
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_holiday_day")
	private Long idHolidayDay;
	
	@OneToOne
	@JoinColumn(name = "id_employeeHoliday", referencedColumnName = "id_employeeHoliday")
	private EmployeeHoliday employeeHoliday;
	
	@Column(name="day")
	private Date day;
	
	@Column(name="year")
	private Long year;
	
	@Column(name="save_code")
	private Long saveCode;
	
	@Column(name="count")
	private boolean count;
	
	@Transient
	private Long holidayPerDay;
	
	@Transient
	private Long hashCode;
	
	//0-NO ONE SLOT BOOKED
	//1-SOME SLOTS AVAILABLE
	//2-SLOT FULLY BOOKED
	//3-DAY NOT COUNTED
	//4-DAY OFF
	//5-MANY DEPARTMENTS (SEE DETAILS)
	//6-BANK HOLIDAY
	@Column(name="status")
	private Long status;
	
	@Column(name="status_log")
	private Long statusLog;
	
	public Long getStatusLog() {
		return statusLog;
	}

	public void setStatusLog(Long statusLog) {
		this.statusLog = statusLog;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Long getIdAllowanceDay() {
		return idHolidayDay;
	}

	public void setIdAllowanceDay(Long idHolidayDay) {
		this.idHolidayDay = idHolidayDay;
	}

	public Long getIdHolidayDay() {
		return idHolidayDay;
	}

	public void setIdHolidayDay(Long idHolidayDay) {
		this.idHolidayDay = idHolidayDay;
	}

	public EmployeeHoliday getEmployeeHoliday() {
		return employeeHoliday;
	}

	public void setEmployeeHoliday(EmployeeHoliday employeeHoliday) {
		this.employeeHoliday = employeeHoliday;
	}

	public Date getDay() {
		return day;
	}

	public void setDay(Date day) {
		this.day = day;
	}

	public Long getYear() {
		return year;
	}

	public void setYear(Long year) {
		this.year = year;
	}

	public Long getSaveCode() {
		return saveCode;
	}

	public void setSaveCode(Long saveCode) {
		this.saveCode = saveCode;
	}

	public boolean isCount() {
		return count;
	}

	public void setCount(boolean count) {
		this.count = count;
	}

	public Long getHolidayPerDay() {
		return holidayPerDay;
	}

	public void setHolidayPerDay(Long holidayPerDay) {
		this.holidayPerDay = holidayPerDay;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idHolidayDay == null) ? 0 : idHolidayDay.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HolidayDay other = (HolidayDay) obj;
		if (idHolidayDay == null) {
			if (other.idHolidayDay != null)
				return false;
		} else if (!idHolidayDay.equals(other.idHolidayDay))
			return false;
		return true; 
	}

	public Long getHashCode() {
		return hashCode;
	}

	public void setHashCode(Long hashCode) {
		this.hashCode = hashCode;
	}

}
