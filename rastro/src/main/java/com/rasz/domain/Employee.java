package com.rasz.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "employee")
public class Employee implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@ManyToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="employee_department", joinColumns = {@JoinColumn(name="id_employee")}, inverseJoinColumns = {@JoinColumn(name = "id_department")})
	List<Department> departments = new ArrayList<>(); 
	
	@ManyToMany(cascade = {CascadeType.ALL})
	@JoinTable(name="manager_department", joinColumns = {@JoinColumn(name="id_employee")}, inverseJoinColumns = {@JoinColumn(name = "id_department")})
	List<Department> departmentsManager = new ArrayList<>(); 

	@Id
	@Column(name = "id_employee")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idEmployee;	
	
	@OneToOne
	@JoinColumn(name = "id_role", referencedColumnName = "id_role")
	private Role role;
	
	@Column(name = "ee_number")
	private Long eeNumber;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "surname")
	private String surname;
	
	@Column(name = "gender", columnDefinition="char(1)")
	private String gender;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "national_insurance")
	private String nationalInsurance;
	
	@Column(name = "date_birth")
	private Date dateBirth;
	
	@Column(name = "activate", columnDefinition="boolean not null default true")
	private boolean activate;
	
	@OneToOne
	@JoinColumn(name = "id_usuario", referencedColumnName = "id", unique=true)
	private Usuario usuario;
	
	public Date getDateBirth() {
		return dateBirth;
	}

	public void setDateBirth(Date dateBirth) {
		this.dateBirth = dateBirth;
	}

	public Long getIdEmployee() {
		return idEmployee;
	}

	public void setIdEmployee(Long idEmployee) {
		this.idEmployee = idEmployee;
	}

	public String getName() {
		if(name != null){
			return name.toUpperCase();
		}else{
			return null;
		}
	}
	
	public String getFullName(){
		String n = "";
		
		if(name != null && !name.equals("")){
			n = name.toUpperCase();
		}
		
		if(surname != null && !surname.equals("")){
			n += " "+surname.toUpperCase();
		}
		
		return n;
	}
	
	public String getCompleteName() {
		return name+" "+surname;
	}

	public void setName(String name) {
		if(name != null){
			this.name = name.toUpperCase();
		}else{
			this.name = null;
		}
	}

	public String getSurname() {
		if(surname != null){
			return surname.toUpperCase();
		}else{
			return null;
		}
	}

	public void setSurname(String surname) {
		if(surname != null){
			this.surname = surname.toUpperCase();
		}else{
			this.surname = null;
		}	
	}

	public boolean isActivate() {
		return activate;
	}

	public void setActivate(boolean activate) {
		this.activate = activate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getNationalInsurance() {
		return nationalInsurance;
	}

	public void setNationalInsurance(String nationalInsurance) {
		this.nationalInsurance = nationalInsurance;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getEeNumber() {
		return eeNumber;
	}

	public void setEeNumber(Long eeNumber) {
		this.eeNumber = eeNumber;
	}


	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public List<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}

	public List<Department> getDepartmentsManager() {
		return departmentsManager;
	}

	public void setDepartmentsManager(List<Department> departmentsManager) {
		this.departmentsManager = departmentsManager;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idEmployee == null) ? 0 : idEmployee.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (idEmployee == null) {
			if (other.idEmployee != null)
				return false;
		} else if (!idEmployee.equals(other.idEmployee))
			return false;
		return true; 
	}
	
	@Override
	public String toString() {
		return this.idEmployee.toString() +" - "+ getFullName();
	}
	
}
