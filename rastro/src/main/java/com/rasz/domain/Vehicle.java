package com.rasz.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "vehicle")
public class Vehicle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_vehicle")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idVehicle;	

	@Column(name = "registration")
	private String registration;
	
	@Column(name = "milage")
	private Long milage;

	@Column(name = "activate",  columnDefinition="boolean not null default true")
	private boolean activate;
	
	public Long getIdVehicle() {
		return idVehicle;
	}

	public void setIdVehicle(Long idVehicle) {
		this.idVehicle = idVehicle;
	}

	public String getRegistration() {
		return registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

	public boolean isActivate() {
		return activate;
	}

	public void setActivate(boolean activate) {
		this.activate = activate;
	}

	public Long getMilage() {
		return milage;
	}

	public void setMilage(Long milage) {
		this.milage = milage;
	}

	@Override
	public String toString() {
		return this.idVehicle.toString() +" - "+ this.registration;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idVehicle == null) ? 0 : idVehicle.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vehicle other = (Vehicle) obj;
		if (idVehicle == null) {
			if (other.idVehicle != null)
				return false;
		} else if (!idVehicle.equals(other.idVehicle))
			return false;
		return true; 
	}
}
