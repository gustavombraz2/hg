package com.rasz.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "role")
public class Role implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id_role")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idRole;	

	@Column(name = "description")
	private String description;
	
	@Column(name = "manager", columnDefinition="boolean not null default false")
	private boolean manager;
	
	@Column(name = "hr_manager", columnDefinition="boolean not null default false")
	private boolean hrManager;

	@Column(name = "activate",  columnDefinition="boolean not null default true")
	private boolean activate;
	
	public Long getIdRole() {
		return idRole;
	}

	public void setIdRole(Long idRole) {
		this.idRole = idRole;
	}

	public String getDescription() {
		if(description != null ){
			return description.toUpperCase();
		}else{
			return null;
		}
	}

	public void setDescription(String description) {
		if(description != null ){
			this.description = description.toUpperCase();
		}else{
			this.description = null;
		}
	}

	public boolean isActivate() {
		return activate;
	}

	public void setActivate(boolean activate) {
		this.activate = activate;
	}

	public boolean isManager() {
		return manager;
	}

	public void setManager(boolean manager) {
		this.manager = manager;
	}

	public boolean isHrManager() {
		return hrManager;
	}

	public void setHrManager(boolean hrManager) {
		this.hrManager = hrManager;
	}

	@Override
	public String toString() {
		return this.idRole.toString() +" - "+ this.description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idRole == null) ? 0 : idRole.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Role other = (Role) obj;
		if (idRole == null) {
			if (other.idRole != null)
				return false;
		} else if (!idRole.equals(other.idRole))
			return false;
		return true; 
	}
}
