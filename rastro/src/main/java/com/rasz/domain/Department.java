package com.rasz.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.data.annotation.Transient;

@Entity
@Table(name = "department")
public class Department implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@ManyToMany(mappedBy = "departments")
	private List<Employee> employeeList = new ArrayList<>();
	
	@ManyToMany(mappedBy = "departmentsManager")
	private List<Employee> managerList = new ArrayList<>();

	@Id
	@Column(name = "id_department")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idDepartment;	

	@Column(name = "description")
	private String description;
	
	@Column(name = "maximum_days")
	private Long maximumDays;
	
	@Column(name = "maximum_employees")
	private Long maximumEmployees;

	@Column(name = "activate",  columnDefinition="boolean not null default true")
	private boolean activate;
	
	@Transient
	private Long holidayPerDay;
	
	public Long getHolidayPerDay() {
		return holidayPerDay;
	}

	public void setHolidayPerDay(Long holidayPerDay) {
		this.holidayPerDay = holidayPerDay;
	}

	public Long getIdDepartment() {
		return idDepartment;
	}

	public void setIdDepartment(Long idDepartment) {
		this.idDepartment = idDepartment;
	}

	public String getDescription() {
		if(description != null ){
			return description.toUpperCase();
		}else{
			return null;
		}
	}

	public void setDescription(String description) {
		if(description != null ){
			this.description = description.toUpperCase();
		}else{
			this.description = null;
		}
	}

	public Long getMaximumDays() {
		return maximumDays;
	}

	public void setMaximumDays(Long maximumDays) {
		this.maximumDays = maximumDays;
	}

	public Long getMaximumEmployees() {
		return maximumEmployees;
	}

	public void setMaximumEmployees(Long maximumEmployees) {
		this.maximumEmployees = maximumEmployees;
	}

	public boolean isActivate() {
		return activate;
	}

	public void setActivate(boolean activate) {
		this.activate = activate;
	}

	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}

	public List<Employee> getManagerList() {
		return managerList;
	}

	public void setManagerList(List<Employee> managerList) {
		this.managerList = managerList;
	}

	@Override
	public String toString() {
		return this.idDepartment.toString() +" - "+ this.description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idDepartment == null) ? 0 : idDepartment.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Department other = (Department) obj;
		if (idDepartment == null) {
			if (other.idDepartment != null)
				return false;
		} else if (!idDepartment.equals(other.idDepartment))
			return false;
		return true; 
	}
}
