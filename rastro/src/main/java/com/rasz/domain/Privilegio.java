package com.rasz.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="privilegio")
public class Privilegio implements Serializable {
	private static final long serialVersionUID = -7116019631034061705L;

	@Id
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "Privilegio")
//	@SequenceGenerator(name = "Privilegio", sequenceName = "Privilegio")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(length = 100, unique = true)
	
	private String nome;
	
	private Integer ordem;

	public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	@ManyToOne
	@JoinColumn(name = "id_privilegio_pai")
	private Privilegio privilegioPai;
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	private String descricao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Privilegio getPrivilegioPai() {
		return privilegioPai;
	}

	public void setPrivilegioPai(Privilegio privilegioPai) {
		this.privilegioPai = privilegioPai;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((privilegioPai == null) ? 0 : privilegioPai.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Privilegio other = (Privilegio) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (privilegioPai == null) {
			if (other.privilegioPai != null)
				return false;
		} else if (!privilegioPai.equals(other.privilegioPai))
			return false;
		return true;
	}
	
}
