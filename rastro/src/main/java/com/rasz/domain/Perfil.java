package com.rasz.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="perfil")
public class Perfil implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2576082157218759433L;
//	@SequenceGenerator(name="pk_perfil",sequenceName="perfil_id_seq", allocationSize=1)
//	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="pk_perfil")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Id
	private Long id;
	
	@Column(length = 100)
	private String descricao;
	
	@Column(length = 50, unique = true)
	private String nome;
	
	@Column(name = "ativado")
	private boolean ativado;
	
	@Column(name = "administrador")
	private boolean administrador;

	@OneToMany(mappedBy = "perfil", cascade = {CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
	private List<PerfilPrivilegio> privilegios;
		
	@JoinTable(name = "perfil_usuario", joinColumns = @JoinColumn(name = "id_perfil"),
	           inverseJoinColumns = @JoinColumn(name = "id_usuario"))
	@ManyToMany(fetch=FetchType.EAGER, cascade = {CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.MERGE})
	private List<Usuario> usuarios;
	
	@Transient
	private String descrAtivado;
	
	@Transient
	private String descrAdministrador;
	
	public String getDescricao() {
	   return this.descricao;
	}
	
	public void setDescricao(String descricao){
		this.descricao = descricao.toUpperCase();		
	}
	
	public String getNome() {
	   return this.nome;
	}
	
	public void setNome(String nome){
		this.nome = nome.toUpperCase();		
	}
	
	public boolean isAtivado() {
	   return this.ativado;
	}
	
	public void setAtivado(boolean ativado){
		this.ativado = ativado;		
	}

	public List<PerfilPrivilegio> getPrivilegios() {
		return privilegios;
	}
	
	public List<PerfilPrivilegio> getPrivilegiosSemUser() {
		List<PerfilPrivilegio> lista = new ArrayList<PerfilPrivilegio>();
		if(privilegios != null){
			for(PerfilPrivilegio p : privilegios){
				if(p.getPrivilegio() != null && p.getPrivilegio().getId() != 1){
					lista.add(p);
				}
			}
		}
		return lista;
	}

	public void setPrivilegios(List<PerfilPrivilegio> privilegios) {
		this.privilegios = privilegios;
	}
	
	public void addPrivilegio(PerfilPrivilegio privilegio) {
		if (privilegios == null) {
			privilegios = new ArrayList<PerfilPrivilegio>();
		}
		if (!privilegios.contains(privilegio)) {
			privilegio.setPerfil(this);
			privilegios.add(privilegio);
		}
	}
	
	public void addUsuario(Usuario usuario) {
		if (usuarios == null) {
			usuarios = new ArrayList<Usuario>();
		}
		if (!usuarios.contains(usuario)) {
			usuarios.add(usuario);
		}
	}
	
	public void removerUsuario(Usuario usuario) {
		if (usuarios != null) {
			usuarios.remove(usuario);
		}
	}
	
	public void marcarPrivilegios (PerfilPrivilegio pp){
		if(pp.isConsultar() == false || pp.isAlterar() == false 
				|| pp.isIncluir() == false || pp.isRemover() == false || !pp.isImprimir()){
			pp.setAlterar(true);
			pp.setConsultar(true);
			pp.setIncluir(true);
			pp.setRemover(true);
			pp.setImprimir(true);
		}else{
			pp.setAlterar(false);
			pp.setConsultar(false);
			pp.setIncluir(false);
			pp.setRemover(false);
			pp.setImprimir(false);
		}
		
	}
	
	public void marcarFilhosDoPai (PerfilPrivilegio pp){
		if(pp.isConsultar() == false || pp.isAlterar() == false 
				|| pp.isIncluir() == false || pp.isRemover() == false || !pp.isImprimir()){
			
			pp.setAlterar(true);
			pp.setConsultar(true);
			pp.setIncluir(true);
			pp.setRemover(true);
			pp.setImprimir(true);
			
			if(privilegios != null){
				for(PerfilPrivilegio p : privilegios){
					if(p.getPrivilegio().getPrivilegioPai() != null && p.getPrivilegio().getPrivilegioPai().getId().equals(pp.getPrivilegio().getId())){
						p.setAlterar(true);
						p.setConsultar(true);
						p.setIncluir(true);
						p.setRemover(true);
						pp.setImprimir(true);
					}
				}
			}
		}else{
			pp.setAlterar(false);
			pp.setConsultar(false);
			pp.setIncluir(false);
			pp.setRemover(false);
			pp.setImprimir(false);
			
			if(privilegios != null){
				for(PerfilPrivilegio p : privilegios){
					if(p.getPrivilegio().getPrivilegioPai() != null && p.getPrivilegio().getPrivilegioPai().getId().equals(pp.getPrivilegio().getId())){
						p.setAlterar(false);
						p.setConsultar(false);
						p.setIncluir(false);
						p.setRemover(false);
						p.setImprimir(false);
					}
				}
			}
		}
		
	}

	public String getDescrAtivado() {
		descrAtivado = ativado ? "Ativo" : "Inativo";
		return descrAtivado;
	}

	public void setDescrAtivado(String descrAtivado) {
		this.descrAtivado = descrAtivado.toUpperCase();
	}
		
	public String getDescrAdministrador() {
		descrAdministrador = administrador ? "Sim" : "N�o";
		return descrAdministrador;
	}

	public void setDescrAdministrador(String descrAdministrador) {
		this.descrAdministrador = descrAdministrador;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public boolean isNew() {
		return id == null;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Perfil [id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Perfil other = (Perfil) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true; 
	}

	public boolean getAdministrador() {
		return administrador;
	}

	public void setAdministrador(boolean administrador) {
		this.administrador = administrador;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
