package com.rasz.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rasz.utils.Utils;

@Entity
@Table(name = "employee_holiday")
public class EmployeeHoliday implements Serializable {
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_employeeHoliday")
	private Long idEmployeeHoliday;
	
	@Column(name="requisition_date")
	private Date requisitionDate;
	
	@Column(name="start_date")
	private Date startDate;
	
	@Column(name="end_date")
	private Date endDate;
	
	@Column(name="reason")
	private String reason;
	
	@Column(name="day_counted")
	private Integer dayCounted;
	
	@Column(name="hour_counted")
	private Integer hourCounted;
	
	@OneToOne
	@JoinColumn(name = "id_employee", referencedColumnName = "id_employee")
	private Employee employee;
	
	@OneToOne
	@JoinColumn(name = "id_holiday_category", referencedColumnName = "id_holiday_category")
	private HolidayCategory holidayCategory;
	
	@OneToOne
	@JoinColumn(name = "id_manager_permission", referencedColumnName = "id_employee")
	private Employee managerPermission;
	
	@OneToOne
	@JoinColumn(name = "id_hr_permission", referencedColumnName = "id_employee")
	private Employee hrPermission;
	
	@Column(name="manager_permit")
	private Boolean managerPermit;
	
	@Column(name="hr_permit")
	private Boolean hrPermit;

	@Column(name="manager_permission_date")
	private Date managerPermissionDate;
	
	@Column(name="hr_permission_date")
	private Date hrPermissionDate;
	
	//total holiday_day by employee and year
	@Transient
	private Long countedDays;
	
	//total holiday_day by employee and year
	@Transient
	private Long countedHours;
	
	//from employee_allowance
	@Transient
	private Long availableDays;
	
	//availableDays - countedDays - countedHours
	@Transient
	private Double balance;
	
	//from allowance
	@Transient
	private Long workingHours;
	
	public Long getIdEmployeeHoliday() {
		return idEmployeeHoliday;
	}

	public void setIdEmployeeHoliday(Long idEmployeeHoliday) {
		this.idEmployeeHoliday = idEmployeeHoliday;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Employee getManagerPermission() {
		return managerPermission;
	}

	public void setManagerPermission(Employee managerPermission) {
		this.managerPermission = managerPermission;
	}

	public Employee getHrPermission() {
		return hrPermission;
	}

	public void setHrPermission(Employee hrPermission) {
		this.hrPermission = hrPermission;
	}

	public Boolean getManagerPermit() {
		return managerPermit;
	}

	public void setManagerPermit(Boolean managerPermit) {
		this.managerPermit = managerPermit;
	}

	public Boolean getHrPermit() {
		return hrPermit;
	}

	public void setHrPermit(Boolean hrPermit) {
		this.hrPermit = hrPermit;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

//	@Override
//	public String toString() {
//		return this.id.toString() +" - "+ this.nmUsuario;
//	}

	public Date getRequisitionDate() {
		return requisitionDate;
	}

	public void setRequisitionDate(Date requisitionDate) {
		this.requisitionDate = requisitionDate;
	}

	public Date getManagerPermissionDate() {
		return managerPermissionDate;
	}

	public void setManagerPermissionDate(Date managerPermissionDate) {
		this.managerPermissionDate = managerPermissionDate;
	}

	public Date getHrPermissionDate() {
		return hrPermissionDate;
	}

	public void setHrPermissionDate(Date hrPermissionDate) {
		this.hrPermissionDate = hrPermissionDate;
	}

	public HolidayCategory getHolidayCategory() {
		return holidayCategory;
	}

	public void setHolidayCategory(HolidayCategory holidayCategory) {
		this.holidayCategory = holidayCategory;
	}
	
	public Integer getDayCounted() {
		return dayCounted;
	}

	public void setDayCounted(Integer dayCounted) {
		this.dayCounted = dayCounted;
	}

	public Integer getHourCounted() {
		return hourCounted;
	}

	public void setHourCounted(Integer hourCounted) {
		this.hourCounted = hourCounted;
	}

	public Long getCountedDays() {
		return countedDays;
	}

	public void setCountedDays(Long countedDays) {
		this.countedDays = countedDays;
	}

	public Long getCountedHours() {
		return countedHours;
	}

	public void setCountedHours(Long countedHours) {
		this.countedHours = countedHours;
	}

	public Long getAvailableDays() {
		return availableDays;
	}

	public void setAvailableDays(Long availableDays) {
		this.availableDays = availableDays;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public Long getWorkingHours() {
		return workingHours;
	}

	public void setWorkingHours(Long workingHours) {
		this.workingHours = workingHours;
	}

	public String getBalanceString() {
		String s = "";
		
		if(balance != null){
			Integer i = (int)(double)balance;
			Double d = balance - i;
			
			if(i == 1){
				s = i.toString()+" DAY";			
			}else{
				s = i.toString()+" DAYS";
			}
			
			if(d > 0){
				if(workingHours != null){
					s += " "+String.valueOf(workingHours/2)+" HOURS";
				}
			}
		}else{
			s = "NO BALANCE";
		}
		
		return s;
	}
	
	public String getBalanceString(Double balance, Long workingHours) {
		String s = "";
		
		if(balance != null){
			Integer i = (int)(double)balance;
			Double d = balance - i;
			
			if(i == 1){
				s = i.toString()+" DAY";			
			}else{
				s = i.toString()+" DAYS";
			}
			
			if(d > 0){
				if(workingHours != null){
					s += " "+String.valueOf(workingHours/2)+" HOURS";
				}
			}
		}else{
			s = "NO BALANCE";
		}
		
		return s;
	}
	
	public String getUsedDayString(Double value, Long workingHours) {
		String s = "";
		
		if(value != null){
			Integer i = (int)(double)value;
			Double d = value - i;
			
			if(i == 1){
				s = i.toString()+" DAY";			
			}else{
				s = i.toString()+" DAYS";
			}
			
			if(d > 0){
				if(workingHours != null){
					s += " "+String.valueOf(workingHours/2)+" HOURS";
				}
			}
		}else{
			s = "";
		}
		
		return s;
	}

	public String getManagerPermissionString() {
		String s = "";
		
		if(managerPermit != null){
			if(managerPermit){
				s += "YES";
			}else{
				s += "NOT";
			}
		}
		
		if(managerPermission != null){
			if(managerPermission.getIdEmployee() != null && managerPermission.getIdEmployee() > 0){
				s += " - by "+managerPermission.getName();
			}
		}
		
		if(managerPermissionDate != null){
			s += " on "+Utils.formataDataStringSemHora(managerPermissionDate);
		}
		
		return s;
	}
	
	public String getHrManagerPermissionString() {
		String s = "";
		
		if(hrPermit != null){
			if(hrPermit){
				s += "YES";
			}else{
				s += "NOT";
			}
		}
		
		if(hrPermission != null){
			s += " - by "+hrPermission.getName();
		}
		
		if(hrPermissionDate != null){
			s += " on "+Utils.formataDataStringSemHora(hrPermissionDate);
		}
		
		return s;
	}
	
	public String getAmountString(){
		String s = "";
		
		if(holidayCategory != null){
			if(holidayCategory.getReckon().equals("N")){
				s = "NOT COUNTED";
			}
			
			if(holidayCategory.getReckon().equals("D")){
				if(dayCounted == 1){
					s = "1 DAY";
				}else{
					s = dayCounted.toString() + " DAYS";
				}
				
				return s;
			}
			
			if(holidayCategory.getReckon().equals("H")){
				if(hourCounted == 1){
					s = "1 HOUR";
				}else{
					s = hourCounted.toString() + " HOURS";
				}
				
				return s;
			}
		}
		
		return s;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idEmployeeHoliday == null) ? 0 : idEmployeeHoliday.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeeHoliday other = (EmployeeHoliday) obj;
		if (idEmployeeHoliday == null) {
			if (other.idEmployeeHoliday != null)
				return false;
		} else if (!idEmployeeHoliday.equals(other.idEmployeeHoliday))
			return false;
		return true; 
	}

}
