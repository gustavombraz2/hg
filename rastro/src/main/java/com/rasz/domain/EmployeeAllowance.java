package com.rasz.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "employee_allowance")
public class EmployeeAllowance implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id_employee_allowance")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idEmployeeAllowance;	

	@OneToOne
	@JoinColumn(name = "id_contract", referencedColumnName = "id_contract")
	private Contract contract;
	
	@Column(name = "current_year")
	private Long currentYear;
	
	@Column(name = "available_days")
	private Double availableDays;
	
	@Column(name = "total_days_counted")
	private Long totalDaysCounted;
	
	@Column(name = "total_hours_counted")
	private Long totalHoursCounted;
	
	@Transient
	private EmployeeHoliday employeeHoliday;
	
	@Transient
	private Long balance;

	public Long getIdEmployeeAllowance() {
		return idEmployeeAllowance;
	}

	public void setIdEmployeeAllowance(Long idEmployeeAllowance) {
		this.idEmployeeAllowance = idEmployeeAllowance;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public Long getCurrentYear() {
		return currentYear;
	}

	public void setCurrentYear(Long currentYear) {
		this.currentYear = currentYear;
	}

	public Double getAvailableDays() {
		return availableDays;
	}

	public void setAvailableDays(Double availableDays) {
		this.availableDays = availableDays;
	}

	public Long getTotalDaysCounted() {
		return totalDaysCounted;
	}

	public void setTotalDaysCounted(Long totalDaysCounted) {
		this.totalDaysCounted = totalDaysCounted;
	}

	public Long getTotalHoursCounted() {
		return totalHoursCounted;
	}

	public void setTotalHoursCounted(Long totalHoursCounted) {
		this.totalHoursCounted = totalHoursCounted;
	}

	public EmployeeHoliday getEmployeeHoliday() {
		return employeeHoliday;
	}

	public void setEmployeeHoliday(EmployeeHoliday employeeHoliday) {
		this.employeeHoliday = employeeHoliday;
	}

	public Long getBalance() {
		return balance;
	}

	public void setBalance(Long balance) {
		this.balance = balance;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idEmployeeAllowance == null) ? 0 : idEmployeeAllowance.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeeAllowance other = (EmployeeAllowance) obj;
		if (idEmployeeAllowance == null) {
			if (other.idEmployeeAllowance != null)
				return false;
		} else if (!idEmployeeAllowance.equals(other.idEmployeeAllowance))
			return false;
		return true; 
	}
}
