package com.rasz.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "allowance")
public class Allowance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_allowance")
	private Long idAllowance;
	
	@Column(name="description")
	private String description;
	
	@Column(name="working_day")
	private Long workingDay;
	
	@Column(name="working_hours")
	private Long workingHours;
	
	@Column(name="allowance")
	private Long allowance;
	
	@Column(name="start_day_working")
	private String startDayWorking;
	
	@Column(name="end_day_working")
	private String endDayWorking;
	
	@Column(name = "activate",  columnDefinition="boolean not null default true")
	private boolean activate;
	
	public Long getIdAllowance() {
		return idAllowance;
	}
	
	public void setIdAllowance(Long idAllowance) {
		this.idAllowance = idAllowance;
	}

	public String getDescription() {
		if(description != null){
			return description.toUpperCase();
		}else{
			return null;
		}
	}

	public void setDescription(String description) {
		if(description != null){
			this.description = description.toUpperCase();
		}else{
			this.description = null;
		}
	}

	public Long getWorkingDay() {
		return workingDay;
	}

	public void setWorkingDay(Long workingDay) {
		this.workingDay = workingDay;
	}

	public Long getWorkingHours() {
		return workingHours;
	}

	public void setWorkingHours(Long workingHours) {
		this.workingHours = workingHours;
	}

	public Long getAllowance() {
		return allowance;
	}

	public void setAllowance(Long allowance) {
		this.allowance = allowance;
	}

	public String getStartDayWorking() {
		return startDayWorking;
	}

	public void setStartDayWorking(String startDayWorking) {
		this.startDayWorking = startDayWorking;
	}

	public String getEndDayWorking() {
		return endDayWorking;
	}

	public void setEndDayWorking(String endDayWorking) {
		this.endDayWorking = endDayWorking;
	}

	public boolean isActivate() {
		return activate;
	}

	public void setActivate(boolean activate) {
		this.activate = activate;
	}

	public String getDayWorking() {
		return startDayWorking +" - "+ endDayWorking;
	}

	@Override
	public String toString() {
		return this.idAllowance.toString() +" - "+ this.description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idAllowance == null) ? 0 : idAllowance.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Allowance other = (Allowance) obj;
		if (idAllowance == null) {
			if (other.idAllowance != null)
				return false;
		} else if (!idAllowance.equals(other.idAllowance))
			return false;
		return true; 
	}

}
