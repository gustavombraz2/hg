package com.rasz.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "holiday_category")
public class HolidayCategory implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id_holiday_category")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idHolidayCategory;	

	@Column(name = "description")
	private String description;
	
	@Column(name = "reckon")
	private String reckon;

	@Column(name = "activate",  columnDefinition="boolean not null default true")
	private boolean activate;
	
	public Long getIdHolidayCategory() {
		return idHolidayCategory;
	}

	public void setIdHolidayCategory(Long idHolidayCategory) {
		this.idHolidayCategory = idHolidayCategory;
	}
	
	public String getReckonDescription() {
		String s = "";
		
		if(reckon != null && !reckon.equals("")){
			if(reckon.equals("N")){
				s = "Not";
			}
		}
			
		if(reckon != null && !reckon.equals("")){
			if(reckon.equals("H")){
				s = "by Hour";
			}
		}
		
		if(reckon != null && !reckon.equals("")){
			if(reckon.equals("D")){
				s = "by Day";
			}
		}
		
		return s;
	}

	public String getReckon() {
		return reckon;
	}

	public void setReckon(String reckon) {
		this.reckon = reckon;
	}

	public String getDescription() {
		if(description != null ){
			return description.toUpperCase();
		}else{
			return null;
		}
	}

	public void setDescription(String description) {
		if(description != null ){
			this.description = description.toUpperCase();
		}else{
			this.description = null;
		}
	}

	public boolean isActivate() {
		return activate;
	}

	public void setActivate(boolean activate) {
		this.activate = activate;
	}

	@Override
	public String toString() {
		return this.idHolidayCategory.toString() +" - "+ this.description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idHolidayCategory == null) ? 0 : idHolidayCategory.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HolidayCategory other = (HolidayCategory) obj;
		if (idHolidayCategory == null) {
			if (other.idHolidayCategory != null)
				return false;
		} else if (!idHolidayCategory.equals(other.idHolidayCategory))
			return false;
		return true; 
	}
}
