package com.rasz.report;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.mail.util.ByteArrayDataSource;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.ServletContextAware;

import com.rasz.domain.Contract;
import com.rasz.domain.EmployeeAllowance;
import com.rasz.domain.EmployeeHoliday;
import com.rasz.repository.ContractRepository;
import com.rasz.repository.EmployeeAllowanceRepository;
import com.rasz.repository.EmployeeHolidayRepository;
import com.rasz.repository.HolidayDayRepository;
import com.rasz.utils.Utils;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

@Controller
public class RepHolidayForm implements ServletContextAware {

	@Autowired private DataSource dataSource;
	@Autowired private ServletContext servletContext;
	@Autowired private EmployeeHolidayRepository employeeHolidayRepository;
	@Autowired private EmployeeAllowanceRepository employeeAllowanceRepository;
	@Autowired private ContractRepository contractRepository;
	@Autowired private HolidayDayRepository holidayDayRepository;

	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}
	
	public DataSource getDataSource() {
		return dataSource;
	}
	
	public Connection getConnection() throws SQLException {
		return this.getDataSource().getConnection();
	}

	
	@RequestMapping(value = "/rep_holiday_form/{key}/{id}/{email}")
	protected void doPost(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, @PathVariable boolean email) throws ServletException, IOException {
		Connection conn = null;
		
		try {
			String path = "D:/WORK/HG/rastro/src/main/java/com/rasz/report/jasper/rep_holiday_form.jrxml";
			Map<String, Object> parameters = new HashMap<String, Object>();
		    parameters.put("REPORT_LOCALE", new Locale("en","GB"));
		    parameters.put("caminhoImagem", "D:/WORK/HG/rastro/src/main/webapp/resources/images/logo_home_white.png");
			
		    EmployeeHoliday eh = employeeHolidayRepository.findOne(id);
		    EmployeeAllowance ea = employeeAllowanceRepository.findByEmployeeYear(eh.getEmployee().getIdEmployee(), (long)Utils.getDateYear(eh.getEndDate()));
		    Contract c = contractRepository.findByEmployee(eh.getEmployee().getIdEmployee());
		    
		    parameters.put("idEH", eh.getIdEmployeeHoliday());
		    parameters.put("name", eh.getEmployee().getFullName().toUpperCase());
		    parameters.put("reqDate", eh.getRequisitionDate());
		    parameters.put("stDate", eh.getStartDate());
		    parameters.put("endDate", eh.getEndDate());
		    parameters.put("reason", eh.getReason().toUpperCase());
		    parameters.put("dayCounted", eh.getDayCounted());
		    parameters.put("hourCounted", eh.getHourCounted());
		    
		    parameters.put("descriptionHC", eh.getHolidayCategory().getDescription().toUpperCase());
		    parameters.put("reckonHC", eh.getHolidayCategory().getReckon());
		    
		    eh.setWorkingHours(c.getAllowance().getWorkingHours());
		    eh.setBalance(ea.getAvailableDays());
		    
		    
		    long x = ea.getTotalHoursCounted();
		    long y = eh.getWorkingHours();
		    double equi = (double)x/y; 
			eh.setBalance((double)ea.getAvailableDays() - ea.getTotalDaysCounted() - equi);
		    
		    parameters.put("availableDays", ea.getAvailableDays());
		    parameters.put("usedDays", eh.getUsedDayString((ea.getTotalDaysCounted() + equi), eh.getWorkingHours()));
		    parameters.put("balance", eh.getBalanceString());
		    parameters.put("year", ea.getCurrentYear());
		    
		    if(eh.getHrPermit() != null){
		    	parameters.put("hrPermit", eh.getHrPermit());
		    	parameters.put("hrPermitName", eh.getHrPermission().getFullName());
		    }else{
		    	parameters.put("hrPermit", null);
		    	parameters.put("hrPermitName", null);
		    }
		    
		    conn = getConnection();
		    parameters.put("conexao", conn);
            
			System.out.println(path);
			JasperDesign jasperDesign = JRXmlLoader.load(path);
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
	    	JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, conn);
	    	ServletOutputStream stream = response.getOutputStream();
		    JasperExportManager.exportReportToPdfStream(jasperPrint, stream);
		    
		    if(email){
		    	byte[] pdfBytes = JasperExportManager.exportReportToPdf(jasperPrint);
			    ByteArrayInputStream bais = new ByteArrayInputStream(pdfBytes);
			    javax.activation.DataSource ds =  (javax.activation.DataSource) new ByteArrayDataSource(bais, "application/pdf");
			    Utils.sendEmailWithAttachment("gustavombraz2@gmail.com", "!Corinthians1910@", "gustavo.mafrabraz@hotmail.com", "HG WALTER - Holiday Request", "", ds);
		    }
		    
		} catch (JRException e) {
		    System.out.println(Utils.getStackTrace(e));  
			//e.printStackTrace();
	    } catch (IOException e) {
	      e.printStackTrace();
	    } catch (Exception e) {
	     System.out.println(
	          "N�o foi possivel gerar o relat�rio. Problemas ao executar tarefas ligadas ao Jasper Reports.\n"+
	          e.getCause());
	      e.printStackTrace();
	    } finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
		
	public void sendEmailWithReport(EmployeeHoliday eh) throws ServletException, IOException {
		Connection conn = null;
		
		try {
			String path = "D:/WORK/HG/rastro/src/main/java/com/rasz/report/jasper/rep_holiday_form.jrxml";
			Map<String, Object> parameters = new HashMap<String, Object>();
		    parameters.put("REPORT_LOCALE", new Locale("en","GB"));
		    parameters.put("caminhoImagem", "D:/WORK/HG/rastro/src/main/webapp/resources/images/logo_home_white.png");
			
		    //EmployeeHoliday eh = employeeHolidayRepository.findOne(id);
		    EmployeeAllowance ea = employeeAllowanceRepository.findByEmployeeYear(eh.getEmployee().getIdEmployee(), (long)Utils.getDateYear(eh.getEndDate()));
		    Contract c = contractRepository.findByEmployee(eh.getEmployee().getIdEmployee());
		    
		    parameters.put("idEH", eh.getIdEmployeeHoliday());
		    parameters.put("name", eh.getEmployee().getFullName().toUpperCase());
		    parameters.put("reqDate", eh.getRequisitionDate());
		    parameters.put("stDate", eh.getStartDate());
		    parameters.put("endDate", eh.getEndDate());
		    parameters.put("reason", eh.getReason().toUpperCase());
		    parameters.put("dayCounted", eh.getDayCounted());
		    parameters.put("hourCounted", eh.getHourCounted());
		    
		    parameters.put("descriptionHC", eh.getHolidayCategory().getDescription().toUpperCase());
		    parameters.put("reckonHC", eh.getHolidayCategory().getReckon());
		    
		    eh.setWorkingHours(c.getAllowance().getWorkingHours());
		    eh.setBalance(ea.getAvailableDays());
		    
		    
		    long x = ea.getTotalHoursCounted();
		    long y = eh.getWorkingHours();
		    double equi = (double)x/y; 
			eh.setBalance((double)ea.getAvailableDays() - ea.getTotalDaysCounted() - equi);
		    
		    parameters.put("availableDays", ea.getAvailableDays());
		    parameters.put("usedDays", eh.getUsedDayString((ea.getTotalDaysCounted() + equi), eh.getWorkingHours()));
		    parameters.put("balance", eh.getBalanceString());
		    parameters.put("year", ea.getCurrentYear());
		    
		    if(eh.getHrPermit() != null){
		    	parameters.put("hrPermit", eh.getHrPermit());
		    	parameters.put("hrPermitName", eh.getHrPermission().getFullName());
		    }else{
		    	parameters.put("hrPermit", null);
		    	parameters.put("hrPermitName", null);
		    }
		    
		    conn = getConnection();
		    parameters.put("conexao", conn);
            
			System.out.println(path);
			JasperDesign jasperDesign = JRXmlLoader.load(path);
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
	    	JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, conn);
//	    	ServletOutputStream stream = response.getOutputStream();
//		    JasperExportManager.exportReportToPdfStream(jasperPrint, stream);
		    
		    byte[] pdfBytes = JasperExportManager.exportReportToPdf(jasperPrint);
		    ByteArrayInputStream bais = new ByteArrayInputStream(pdfBytes);
		    javax.activation.DataSource ds =  (javax.activation.DataSource) new ByteArrayDataSource(bais, "application/pdf");
		    
		    //List<HolidayDay> listHolidayDay = holidayDayRepository.findByEmployeeHolidayDay(eh.getIdEmployeeHoliday());
		    String subject = "HG WALTER - Holiday Request";
			String text = "Your Holiday Request has been made!\n\n"+
						  "Requested days:\n";
			
//			for (HolidayDay holidayDay : listHolidayDay) {
//				text += Utils.formataDataStringSemHora(holidayDay.getDay())+"\n";
//			}
			
		    text += "\nAwait for your manager's response.\n\n"+
				    "Kind Regards \n"+
				    "HG WALTER";
		    Utils.sendEmailWithAttachment("gustavombraz2@gmail.com", "!Corinthians1910@", "gustavo.mafrabraz@hotmail.com", subject, text, ds);
		    
		} catch (JRException e) {
		    System.out.println(Utils.getStackTrace(e));  
			//e.printStackTrace();
	    } catch (IOException e) {
	      e.printStackTrace();
	    } catch (Exception e) {
	     System.out.println(
	          "N�o foi possivel gerar o relat�rio. Problemas ao executar tarefas ligadas ao Jasper Reports.\n"+
	          e.getCause());
	      e.printStackTrace();
	    } finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}	
}
