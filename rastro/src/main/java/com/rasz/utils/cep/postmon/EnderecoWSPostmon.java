package com.rasz.utils.cep.postmon;

/**
 * Entidade baseada nos dados do WS do postmon.com
 */
public class EnderecoWSPostmon {
    private String bairro;
    private String cidade;
    private String cep;
    private String logradouro;
    private EstadoInfoWSPostmon estadoInfo;
    private CidadeInfo cidadeInfo;
    private String estado;

    public String getBairro() {
        return bairro;
    }

    public EnderecoWSPostmon setBairro(String bairro) {
        this.bairro = bairro;
        return this;
    }

    public String getCidade() {
        return cidade;
    }

    public EnderecoWSPostmon setCidade(String cidade) {
        this.cidade = cidade;
        return this;
    }

    public String getCep() {
        return cep;
    }

    public EnderecoWSPostmon setCep(String cep) {
        this.cep = cep;
        return this;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public EnderecoWSPostmon setLogradouro(String logradouro) {
        this.logradouro = logradouro;
        return this;
    }

    public EstadoInfoWSPostmon getEstadoInfo() {
        return estadoInfo;
    }

    public EnderecoWSPostmon setEstadoInfo(EstadoInfoWSPostmon estadoInfo) {
        this.estadoInfo = estadoInfo;
        return this;
    }

    public CidadeInfo getCidadeInfo() {
        return cidadeInfo;
    }

    public EnderecoWSPostmon setCidadeInfo(CidadeInfo cidadeInfo) {
        this.cidadeInfo = cidadeInfo;
        return this;
    }

    public String getEstado() {
        return estado;
    }

    public EnderecoWSPostmon setEstado(String estado) {
        this.estado = estado;
        return this;
    }

    @Override
    public String toString() {
        return "Endereco{" +
                "bairro='" + bairro + '\'' +
                ", cidade='" + cidade + '\'' +
                ", cep='" + cep + '\'' +
                ", logradouro='" + logradouro + '\'' +
                ", estadoInfo=" + estadoInfo +
                ", cidadeInfo=" + cidadeInfo +
                ", estado='" + estado + '\'' +
                '}';
    }
}
