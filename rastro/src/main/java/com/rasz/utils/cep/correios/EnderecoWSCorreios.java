package com.rasz.utils.cep.correios;


/**
 * Entidade baseada nos dados do WS dos correios
 */
public class EnderecoWSCorreios {

    private String cep;
    private String uf;
    private String cidade;
    private String logradouro;
    private String complemento;
    private String complemento2;

    public String getCep() {
        return cep;
    }

    public String getUf() {
        return uf;
    }

    public String getCidade() {
        return cidade;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public String getComplemento() {
        return complemento;
    }

    public String getComplemento2() {
        return complemento2;
    }

    public EnderecoWSCorreios setCep(String cep) {
        this.cep = cep;
        return this;
    }

    public EnderecoWSCorreios setUf(String uf) {
        this.uf = uf;
        return this;
    }

    public EnderecoWSCorreios setCidade(String cidade) {
        this.cidade = cidade;
        return this;
    }

    public EnderecoWSCorreios setLogradouro(String logradouro) {
        this.logradouro = logradouro;
        return this;
    }

    public EnderecoWSCorreios setComplemento(String complemento) {
        this.complemento = complemento;
        return this;
    }

    public EnderecoWSCorreios setComplemento2(String complemento2) {
        this.complemento2 = complemento2;
        return this;
    }

    @Override
    public String toString() {
        return "Endereco{" +
                "cep='" + cep + '\'' +
                ", uf='" + uf + '\'' +
                ", cidade='" + cidade + '\'' +
                ", logradouro='" + logradouro + '\'' +
                ", complemento='" + complemento + '\'' +
                ", complemento2='" + complemento2 + '\'' +
                '}';
    }
}
