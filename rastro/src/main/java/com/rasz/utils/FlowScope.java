package com.rasz.utils;

import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.RequestContextHolder;

import com.rasz.utils.FlowUtilsFactory.FlowUtils;

public class FlowScope implements FlowUtils {

	@Override
	public void put(String attrInScope, Object object) {
		RequestContext rc = RequestContextHolder.getRequestContext();
		rc.getFlowScope().put(attrInScope, object);
		RequestContextHolder.setRequestContext(rc);
	}

	@Override
	public Object get(String attrInScope) {
		RequestContext rc = RequestContextHolder.getRequestContext();
		return rc.getFlowScope().get(attrInScope);
	}

}
