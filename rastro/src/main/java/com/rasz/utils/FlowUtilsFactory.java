package com.rasz.utils;

import com.rasz.domain.FlowRuleEnum;

/**
 * 
 * @author rodrigo
 * 
 * Utilit�rio para inserir e recuperar objetos no flow. Atualmente,
 * possui somente flowScope e conversationScope.
 * 
 * Como utilizar:
 * 
 *   - Inserindo objeto no escopo do flow (flowScope):
		FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).put("blaster", blasterReset);
	 - Recuperando o mesmo objeto no flow (flowScope):
	    Blaster b = (Blaster)FlowUtilsFactory.currentInstance(FlowRuleEnum.FLOW_SCOPE).get("blaster");
	    
	 O que identifica se ir� ser flowScope ou conversationScope 
	 � o Enum passado por par�metro do m�todo currentInstance 
 *
 */
public class FlowUtilsFactory {

	public interface FlowUtils {
		public void put(String attrInScope, Object object);

		public Object get(String attrInScope);
	}

	public static FlowUtils currentInstance(FlowRuleEnum flowScope) {

		switch (flowScope) {
		case CONVERSATION_SCOPE:
			return new ConversationScope();
		case FLOW_SCOPE:
			return new FlowScope();
		default:
			return new FlowScope();
		}
	}

}
