package com.rasz.utils;

public enum EmbalagemEnum {
	CX("CAIXA"),
	SC("SACO"),
	UN("UNIDADE");
	 
	private String value;
	 
	private EmbalagemEnum(String value){
		this.value = value;
	}
    
	public String getValue() {
		return value;
	}
}
