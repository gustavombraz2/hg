package com.rasz.utils.cnpj;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 
 * @author Rodrigo Utiyama
 * @since 2017-02-09
 *
 */
public class CnpjUtils {
	
	private static String WEB_SERVICE_PRINCIPAL = "https://www.receitaws.com.br/v1/cnpj/";
	
	/*
	 * Nenhuma exceção foi tratada nesta classe, pois o intuito é ser uma classe genérica, 
	 * sendo tratada nas classes de Controller. Portanto, ao invés de tratar em um try/catch,
	 * esta classe delega as exceções para o método mandatório (classe que o chamou):
	 */
	public static CnpjDomain readCnpj(String cnpj) throws IOException, JSONException, ParseException{
		// Caso o CNPJ informado estiver nulo, nenhuma verificação será realizada:
		if(cnpj == null)
			throw new NullPointerException("Verifique o CNPJ informado");
		
		
		JSONObject res = inicializarConsulta(cnpj);
		
	    
	    return popularCnpjDomain(res);
	}
	
	private static CnpjDomain popularCnpjDomain(JSONObject res) throws JSONException, ParseException, NullPointerException{
		
		CnpjDomain cnpjDomain = new CnpjDomain();
	    SimpleDateFormat sdfEspecial = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	    
	    if(res.getString("data_situacao") != null && !res.getString("data_situacao").isEmpty())
	    	cnpjDomain.setDataSituacao(sdf.parse(res.getString("data_situacao")));
	    
	    cnpjDomain.setComplemento(res.getString("complemento"));
	    cnpjDomain.setNome(res.getString("nome"));
	    cnpjDomain.setUf(res.getString("uf"));
	    cnpjDomain.setTelefone(res.getString("telefone"));
	    cnpjDomain.setEmail(res.getString("email"));

	    
	    cnpjDomain.setSituacao(res.getString("situacao"));
	    cnpjDomain.setBairro(res.getString("bairro"));
	    cnpjDomain.setLogradouro(res.getString("logradouro"));
	    cnpjDomain.setNumero(res.getString("numero"));
	    
	    cnpjDomain.setCep(res.getString("cep"));
	    cnpjDomain.setMunicipio(res.getString("municipio"));
	    cnpjDomain.setAbertura(res.getString("abertura"));
	    cnpjDomain.setNaturezaJuridica(res.getString("natureza_juridica"));
	    cnpjDomain.setCnpj(res.getString("cnpj"));
	    
		try {
			if (res.getString("ultima_atualizacao") != null && !res.getString("ultima_atualizacao").isEmpty())
				cnpjDomain.setUltimaAtualizacao((sdfEspecial.parse(res.getString("ultima_atualizacao"))));
		} catch (ParseException p) {
			cnpjDomain.setUltimaAtualizacao(null);
		}
	    
	    cnpjDomain.setStatus(res.getString("status"));
	    cnpjDomain.setTipo(res.getString("tipo"));
	    cnpjDomain.setFantasia(res.getString("fantasia"));
	    cnpjDomain.setEfr(res.getString("efr"));
	    cnpjDomain.setMotivoSituacao(res.getString("motivo_situacao"));
	    cnpjDomain.setSituacaoEspecial(res.getString("situacao_especial"));
	    
	    if(res.getString("data_situacao_especial") != null && !res.getString("data_situacao_especial").isEmpty())
	    cnpjDomain.setDataSituacaoEspecial(sdf.parse(res.getString(("data_situacao_especial"))));
	    
	    cnpjDomain.setCapitalSocial(res.getString("capital_social"));
	    
	    
	    popularAtividadesPrincipais(cnpjDomain, res);
	    popularAtividadesSecundarias(cnpjDomain, res);
	    
	    return cnpjDomain;
	}
	
	private static JSONObject inicializarConsulta(String cnpj) throws IOException{
		String linkCodificado = WEB_SERVICE_PRINCIPAL + URLEncoder.encode(cnpj.trim(), "UTF-8");
	    URL url = new URL(linkCodificado);
	 
	    //Abre uma Stream para analisar a URL:
	    Scanner scan = null;
	    		
	    try{
	    	scan = new Scanner(url.openStream());
	    }catch(FileNotFoundException fnf){
	    	return null;
	    }
	    
	    String str = new String();
	    
	    while (scan.hasNext())
	        str += scan.nextLine();
	    scan.close();
	 
	    // constroe o JSON
	    JSONObject res = new JSONObject(str);
	    if (! res.getString("status").equals("OK"))
	        return null;
	    
	    return res;
	}
	
	private static void popularAtividadesPrincipais(CnpjDomain cnpjDomain, JSONObject res){
		JSONArray jsonArray = res.getJSONArray("atividade_principal");
		Atividade atividadePrincipal  = new Atividade();
		
	    if(jsonArray != null){
	    	atividadePrincipal.setCodigo(jsonArray.getJSONObject(0).get("code").toString());
	    	atividadePrincipal.setDescricao(jsonArray.getJSONObject(0).get("text").toString());
	    }
	    cnpjDomain.setAtividadePrincipal(atividadePrincipal);
	}
	
	private static void popularAtividadesSecundarias(CnpjDomain cnpjDomain, JSONObject res){
		JSONArray jsonArray = res.getJSONArray("atividades_secundarias");
		List<Atividade> atividadesSecundarias = new ArrayList<Atividade>();
		
	    if(jsonArray != null){
	    	for(Object json : jsonArray){
	    		Atividade atividadeSecundaria = new Atividade();
	    		atividadeSecundaria.setCodigo(((JSONObject) json).getString("code"));
	    		atividadeSecundaria.setDescricao(((JSONObject) json).getString("text"));
		    	
	    		atividadesSecundarias.add(atividadeSecundaria);
	    	}
	    }
	    cnpjDomain.setAtividadesSecundaria(atividadesSecundarias);
	}
}
