package com.rasz.utils.cnpj;

import java.util.Date;
import java.util.List;

public class CnpjDomain {
	private Date   dataSituacao;
	private String   complemento;
	private String   nome;
	private String   uf;
	private String   telefone;
	private String   email;
	private String   situacao;
	private String   bairro;
	private String   logradouro;
	private String   numero;
	private String   cep;
	private String   municipio;
	private String   abertura;
	private String   naturezaJuridica;
	private String   cnpj;
	private Date   ultimaAtualizacao;
	private String   status;
	private String   tipo;
	private String   fantasia;
	private String   efr;
	private String   motivoSituacao;
	private String   situacaoEspecial;
	private Date   dataSituacaoEspecial;
	private String   capitalSocial;
	private List<Atividade> atividadesSecundaria;
	private Atividade atividadePrincipal;
	
	public Date getDataSituacao() {
		return dataSituacao;
	}
	public void setDataSituacao(Date dataSituacao) {
		this.dataSituacao = dataSituacao;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getAbertura() {
		return abertura;
	}
	public void setAbertura(String abertura) {
		this.abertura = abertura;
	}
	public String getNaturezaJuridica() {
		return naturezaJuridica;
	}
	public void setNaturezaJuridica(String naturezaJuridica) {
		this.naturezaJuridica = naturezaJuridica;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public Date getUltimaAtualizacao() {
		return ultimaAtualizacao;
	}
	public void setUltimaAtualizacao(Date ultimaAtualizacao) {
		this.ultimaAtualizacao = ultimaAtualizacao;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getFantasia() {
		return fantasia;
	}
	public void setFantasia(String fantasia) {
		this.fantasia = fantasia;
	}
	public String getEfr() {
		return efr;
	}
	public void setEfr(String efr) {
		this.efr = efr;
	}
	public String getMotivoSituacao() {
		return motivoSituacao;
	}
	public void setMotivoSituacao(String motivoSituacao) {
		this.motivoSituacao = motivoSituacao;
	}
	public String getSituacaoEspecial() {
		return situacaoEspecial;
	}
	public void setSituacaoEspecial(String situacaoEspecial) {
		this.situacaoEspecial = situacaoEspecial;
	}
	public Date getDataSituacaoEspecial() {
		return dataSituacaoEspecial;
	}
	public void setDataSituacaoEspecial(Date dataSituacaoEspecial) {
		this.dataSituacaoEspecial = dataSituacaoEspecial;
	}
	public String getCapitalSocial() {
		return capitalSocial;
	}
	public void setCapitalSocial(String capitalSocial) {
		this.capitalSocial = capitalSocial;
	}
	public List<Atividade> getAtividadesSecundaria() {
		return atividadesSecundaria;
	}
	public void setAtividadesSecundaria(List<Atividade> atividadesSecundaria) {
		this.atividadesSecundaria = atividadesSecundaria;
	}
  
	public void setAtividadePrincipal(Atividade atividadePrincipal) {
		this.atividadePrincipal = atividadePrincipal;
	}
	public Atividade getAtividadePrincipal() {
		return atividadePrincipal;
	}
	@Override
	public String toString() {
		return "CnpjDomain [dataSituacao=" + dataSituacao + ", complemento=" + complemento + ", nome=" + nome + ", uf="
				+ uf + ", telefone=" + telefone + ", email=" + email + ", situacao=" + situacao + ", bairro=" + bairro
				+ ", logradouro=" + logradouro + ", numero=" + numero + ", cep=" + cep + ", municipio=" + municipio
				+ ", abertura=" + abertura + ", naturezaJuridica=" + naturezaJuridica + ", cnpj=" + cnpj
				+ ", ultimaAtualizacao=" + ultimaAtualizacao + ", status=" + status + ", tipo=" + tipo + ", fantasia="
				+ fantasia + ", efr=" + efr + ", motivoSituacao=" + motivoSituacao + ", situacaoEspecial="
				+ situacaoEspecial + ", dataSituacaoEspecial=" + dataSituacaoEspecial + ", capitalSocial="
				+ capitalSocial + ", \n\n\n\t atividadesSecundaria=" + atividadesSecundaria + ", \n\n\n\t atividadePrincipal="
				+ atividadePrincipal + "]";
	}
	
}
