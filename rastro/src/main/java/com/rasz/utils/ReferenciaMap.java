package com.rasz.utils;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.faces.model.SelectItem;

import org.springframework.stereotype.Component;

@Component
public class ReferenciaMap extends HashMap<String, List<SelectItem>>  {
    private static final long serialVersionUID = -3458241864737088452L;
    
    private String pacote;
    
    public ReferenciaMap() {
        pacote = "com.rasz.domain.";
    }
    
    public String getPacote() {
        return pacote;
    }
    
    @Override
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public List<SelectItem> get(Object key) {
        String chave = (String) key;
        boolean vazia = false;
        boolean vazio = false;
        if (chave.endsWith("Vazio") || chave.endsWith("Vazio")) {
            vazio = true;
            vazia = true;
            chave = chave.substring(0, chave.length() - "vazio".length());
        }
        chave = Character.toUpperCase(chave.charAt(0)) + chave.substring(1);
        if (!this.containsKey(chave)) {
            List<SelectItem> lista = new ArrayList<SelectItem>();
            if (vazio) {
                lista.add(new SelectItem(null, "Vazio"));
            } else if (vazia) {
                lista.add(new SelectItem(null, "Vazia"));
            }
            try {
                Class<Enum> clazz = (Class<Enum>) Class.forName(pacote + chave);
                Method method = clazz.getMethod("getDescricao", (Class<?>[]) null);
                for (Enum valor : clazz.getEnumConstants()) {
                    Object objeto = method.invoke(valor, (Object[]) null);
                    if (objeto instanceof String) {
                        lista.add(new SelectItem(valor, (String) objeto));
                    } else if (objeto instanceof Integer) {
                        lista.add(new SelectItem(objeto, objeto.toString()));
                    }
                }
            } catch (Exception e) {
                lista.add(new SelectItem(null, "Lista n�o encontrada."));
            }
            this.put((String) chave, lista);
        }
        return super.get(chave);
    }

}
