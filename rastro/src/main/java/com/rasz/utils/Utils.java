package com.rasz.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.faces.context.FacesContext;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.rasz.domain.Usuario;
import com.rasz.validator.UserAgentInfo;

public class Utils  {

	private static DateFormat formataDataAAAAMMDD = new SimpleDateFormat("yyyy-MM-dd", new Locale("en", "GB"));

	private static DateFormat formataDataDDMMAAAA_HHMMSS = new SimpleDateFormat("dd-MM-yyyy k:m:s",
			new Locale("en", "GB"));

	public static String preencherZerosAEsquerda(String numero, int posicoes) {
		if (numero == null || numero.equals("null")) {
			numero = "";
		}
		if (numero.length() > posicoes) {
			numero = numero.substring(0, posicoes);
		} else {
			while (numero.length() < posicoes) {
				numero = "0" + numero;
			}
		}
		return numero;
	}

	public String preencherEspacosDireita(String texto, int posicoes) {
		if (texto == null) {
			texto = "";
		}
		if (texto.length() > posicoes) {
			texto = texto.substring(0, posicoes);
		} else {
			while (texto.length() < posicoes) {
				texto += " ";
			}
		}
		return texto;
	}

	public static String preencherEspacosAEsquerda(String numero, int posicoes) {
		if (numero == null || numero.equals("null")) {
			numero = "";
		}
		if (numero.length() > posicoes) {
			numero = numero.substring(0, posicoes);
		} else {
			while (numero.length() < posicoes) {
				numero = " " + numero;
			}
		}
		return numero;
	}

	
	public static String transformaAAAAMMDD(Date data) {
		if (data != null) {
			return formataDataAAAAMMDD.format(data);
		} else {
			return "";
		}
	}

	public static String transformaDDMMAAAA_HHMMSS(Date data) {
		if (data != null) {
			return formataDataDDMMAAAA_HHMMSS.format(data);
		} else {
			return "";
		}
	}

	public static double getArredondarDecimal(double valor, int precisao) {
		try {
			BigDecimal arrendando = new BigDecimal(valor);
			return arrendando.setScale(precisao, BigDecimal.ROUND_HALF_UP).doubleValue();
		} catch (Exception e) {
			return 0.00;
		}

	}

	public static int retornarDiferencaEmDias(Date dataA, Date dataB) {
		if (dataA != null && dataB != null) {
			long data1 = dataA.getTime();
			long data2 = dataB.getTime();
			long diff = data2 - data1;
			long diffEmDias = diff / 1000 / 60 / 60 / 24;
			int intervalo = (int) diffEmDias;
			return intervalo;
		} else {
			return 0;
		}
	}

	public static Boolean validaCPF(String _cpf) {
		String cpf = _cpf;

		cpf = cpf.replace(".", "").replace("-", "").replace("_", "");

		if (cpf.length() != 11 || cpf.equals("11111111111") || cpf.equals("22222222222") || cpf.equals("33333333333")
				|| cpf.equals("44444444444") || cpf.equals("55555555555") || cpf.equals("66666666666")
				|| cpf.equals("77777777777") || cpf.equals("88888888888") || cpf.equals("99999999999")
				|| cpf.indexOf(" ") >= 0) {
			return false;
		}

		int soma = 0;
		Integer resto = 0;

		for (int i = 0; i < 9; i++) {
			soma += Integer.parseInt(String.valueOf(cpf.charAt(i))) * (10 - i);
		}

		resto = 11 - (soma % 11);

		if (resto == 10 || resto == 11) {
			resto = 0;
		}

		if (resto != Integer.parseInt(String.valueOf(cpf.charAt(9)))) {
			return false;
		}

		soma = 0;

		for (int i = 0; i < 10; i++) {
			soma += Integer.parseInt(String.valueOf(cpf.charAt(i))) * (11 - i);
		}

		resto = 11 - (soma % 11);

		if (resto == 10 || resto == 11) {
			resto = 0;
		}

		if (resto != Integer.parseInt(String.valueOf(cpf.charAt(10)))) {
			return false;
		}

		return true;
	}

	public static Boolean validaCNPJ(String _cnpj) {
		String cnpj = _cnpj.replace(".", "").replace("-", "").replace("/", "").replace("_", "");

		boolean digitos_iguais = true;
		int tamanho = 0;
		String numeros = "";
		String digitos = "";
		double soma = 0;
		int i = 0;
		Integer resultado = 0;
		int pos = 0;
		if (cnpj.length() < 14 && cnpj.length() < 15 || cnpj.indexOf(" ") >= 0 || cnpj.equals("")) {
			return false;
		}
		if (cnpj.equals("00000000000000") || cnpj.equals("")) {
			return false;
		}

		for (i = 0; i < cnpj.length() - 1; i++)
			if (Integer.parseInt(String.valueOf(cnpj.charAt(i))) != Integer
					.parseInt(String.valueOf(cnpj.charAt(i + 1)))) {
				digitos_iguais = false;
				break;
			}
		if (!digitos_iguais) {
			tamanho = cnpj.length() - 2;
			numeros = cnpj.substring(0, tamanho);
			digitos = cnpj.substring(tamanho);
			soma = 0;
			pos = tamanho - 7;
			for (i = tamanho; i >= 1; i--) {
				soma += Integer.parseInt(String.valueOf(numeros.charAt(tamanho - i))) * pos--;
				if (pos < 2)
					pos = 9;
			}
			resultado = (int) (soma % 11 < 2 ? 0 : 11 - soma % 11);
			if (resultado != Integer.parseInt(String.valueOf(digitos.charAt(0)))) {
				return false;
			}
			tamanho = tamanho + 1;
			numeros = cnpj.substring(0, tamanho);
			soma = 0;
			pos = tamanho - 7;
			for (i = tamanho; i >= 1; i--) {
				soma += Integer.parseInt(String.valueOf(numeros.charAt(tamanho - i))) * pos--;
				if (pos < 2)
					pos = 9;
			}
			resultado = (int) (soma % 11 < 2 ? 0 : 11 - soma % 11);
			if (resultado != Integer.parseInt(String.valueOf(digitos.charAt(1)))) {
				return false;
			}

			return true;

		} else {
			return false;
		}
	}

	public static Date addDias(Date data, int dias) {
		if (data != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(data);
			cal.add(Calendar.DAY_OF_MONTH, dias);
			data = cal.getTime();
		}
		return data;
	}

	public static int retornarDiferencaEmDiasEntreDatas(Date dataA, Date dataB) {
		if (dataA != null && dataB != null) {
			long data1 = dataA.getTime();
			long data2 = dataB.getTime();
			long diff = data2 - data1;
			long diffEmDias = diff / 1000 / 60 / 60 / 24;
			int intervalo = (int) diffEmDias;
			return intervalo;
		} else {
			return 0;
		}
	}

	public static Date validaVencimentoParcela(GregorianCalendar data) {

		boolean isSabado = data.get(Calendar.DAY_OF_WEEK) - 1 == 6;
		boolean isDomingo = data.get(Calendar.DAY_OF_WEEK) - 1 == 0;

		if (isDomingo) {
			data.add(Calendar.DAY_OF_MONTH, 1);
		}
		if (isSabado) {
			data.add(Calendar.DAY_OF_MONTH, -1);
		}

		return data.getTime();

	}

	public static Date validaDataFinalSemana(GregorianCalendar data) {

		if (data.getFirstDayOfWeek() == 7) {

			data.add(Calendar.DAY_OF_MONTH, 1);
		}

		if (data.getFirstDayOfWeek() == 6) {

			data.add(Calendar.DAY_OF_MONTH, 2);
		}

		return data.getTime();

	}

	public static Date formataData(String data) throws Exception {
		if (data == null || data.equals("")) {
			return null;
		}
		Date date = null;
		try {
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("en", "GB"));
			date = (java.util.Date) formatter.parse(data);
		} catch (ParseException e) {
			throw e;
		}
		return date;
	}

	public static String formataDataString(Date data) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return sdf.format(data);
	}
	
	public static String formataDataHora(Date data) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		return sdf.format(data);
	}

	public static String formataDataStringSemHora(Date data) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(data);
	}
	
	public static Date formataDataSemHora(Date data) {
		if(data != null){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
 			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", new Locale("en", "GB"));
			try {
				data = (java.util.Date) formatter.parse(sdf.format(data));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		return data;
	}

	public static String validaToUpperCase(String descricao) {
		if (descricao != null) {
			return descricao.toUpperCase();
		}

		return descricao;
	}

	public static String getAtualTimeToFiles(){
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_YYYY HH:mm:ss");
		return sdf.format(d);
	}
	// QUALQUER D�VIDA ---> EXEMPLO RETIRADO DO LINK:
	// http://respostas.guj.com.br/6592-api-java-para-buscar-cep-correios
	public static Usuario getInformacoesEndereco(Usuario usuario) throws IOException {

		// try {
		//
		// String endereco = null;
		// String bairro = null;
		// endereco = Utils.getEndereco(usuario.getEmail());
		// bairro = Utils.getBairro(usuario.getEmail());
		// usuario.setSenha(endereco);
		// usuario.setLogin(bairro);
		// // usuario.setEmail(Utils.get(usuario.getLogin()));
		// // usuario.setLogin(Utils.getEndereco(usuario.getLogin()));
		//
		// } catch (Exception e) {
		// e.getMessage();
		// }

		return usuario;

	}

	// Realiza a Conexao uma vez, para a Busca coletiva dos dados ( Bairro,
	// Cidade, Estado, Rua )
	public static Document realizaConexaoBuscaCep(String CEP) {

		try {

			////PARA O HENRIQUE
			////http://www.qualocep.com/ddd/londrina/pr/
			////Para procurar o DDD e DDDi a partir da cidade e estados  trazidos pelo CEP
			
			
			Document doc = Jsoup.connect("http://www.qualocep.com/busca-cep/" + CEP).timeout(120000).get();

			return doc;

		} catch (Exception e) {

			e.getMessage();
		}
		return null;
	}

	public static String getEndereco(String CEP, Document doc) throws IOException {

		// Document doc = Jsoup.connect("http://www.qualocep.com/busca-cep/" +
		// CEP).timeout(120000).get();
		Elements urlPesquisa = doc.select("span[itemprop=streetAddress]");
		for (Element urlEndereco : urlPesquisa) {
			return urlEndereco.text();
		}
		return null;
	}

	public static String getBairro(String CEP, Document doc) throws IOException {

		// Document doc = Jsoup.connect("http://www.qualocep.com/busca-cep/" +
		// CEP).timeout(120000).get();
		Elements urlPesquisa = doc.select("td:gt(1)");
		for (Element urlBairro : urlPesquisa) {
			return urlBairro.text();
		}
		return null;
	}

	public static String getCidade(String CEP, Document doc) throws IOException {

		// Document doc = Jsoup.connect("http://www.qualocep.com/busca-cep/" +
		// CEP).timeout(120000).get();
		Elements urlPesquisa = doc.select("span[itemprop=addressLocality]");
		for (Element urlCidade : urlPesquisa) {
			return urlCidade.text();
		}
		return null;
	}

	public static String getUF(String CEP, Document doc) throws IOException {

		// Document doc = Jsoup.connect("http://www.qualocep.com/busca-cep/" +
		// CEP).timeout(120000).get();
		Elements urlPesquisa = doc.select("span[itemprop=addressRegion]");
		for (Element urlUF : urlPesquisa) {
			return urlUF.text();
		}
		return null;
	}

	public static String formataCNPJ(String cnpj) {
		cnpj = cnpj.replace("-", "");
		cnpj = cnpj.replace("/", "");

		return cnpj;
	}

	public static boolean confirmeCPF(String _cpf) {
		String cpf = _cpf.replace(".", "").replace("-", "").replace("_", "");

		if (!cpf.equals("")) {
			boolean valida = validaCPF(_cpf);

			if (valida) {
				return true;
			}

			return false;
		} else {
			return true;
		}
	}

	public static String removeCPFMask(String cpf) {
		return cpf = cpf.replace(".", "").replace("-", "").replace("_", "").trim();
	}

	public static boolean validateStructureCEPWebServices(String cep) {
		if (!cep.matches("\\d{8}")) {
			return false;
		}

		return true;
	}

	public static String removerAcentuacao(String frase) {
		String fraseNormalizada = Normalizer.normalize(frase, Normalizer.Form.NFD);
		return fraseNormalizada.replaceAll("[^\\p{ASCII}]", "");
	}

	/**
	 * @author rodrigo
	 * @param cep
	 * @return String
	 * 
	 *         Remove a m�scara para um CEP. Este m�todo tamb�m substitui os
	 *         caracteres da m�scara quando vazio, ou seja, o underline "_".
	 */

	public static String removeCEPMask(String cep) {
		return cep.replace("-", "").trim().replace("_", "");
	}

	/**
	 * @author rodrigo
	 * @param tel
	 * @return String
	 * 
	 *         Retornar� apenas o DDD do telefone com m�scara. Ex: (043)
	 *         33333333 -> retornar� apenas o telefone -> 33333333
	 */
	public static String removeTelWithDDDMask(String tel) {
		return tel.substring(tel.indexOf(')') + 1, tel.length()).replace("_", "").trim();
	}

	/**
	 * @author rodrigo
	 * @param tel
	 * @return String
	 * 
	 *         Retornar� apenas o DDD do telefone com m�scara. Ex: (043)
	 *         33333333 -> retornar� 043
	 *         <h3>Aten��o, ao converter o DDD 043 para Inteiro, verificar que o
	 *         d�gito "0" ser� considerado como Octal</h3>
	 */
	public static String removeTelOnlyDDD(String tel) {
		return tel.substring(tel.indexOf('(') + 1, tel.indexOf(')'));
	}

	/**
	 * @author rodrigo
	 * @param string,
	 *            searchCriteria
	 * @return String
	 * 
	 *         Apenas limpa parte de uma string, sendo passada pelo
	 *         searchCriteria. Ex:
	 * 
	 *         String = "Hello World" searchCriteria = "World"
	 * 
	 *         Resultado = "Hello "
	 */
	public static String replaceLastChar(String string, String searchCriteria) {
		if (string.trim().endsWith(searchCriteria)) {
			return string.substring(0, string.trim().length() - searchCriteria.length());
		}

		return string;
	}

	public static Date xmlGregorianCalendarToDate(XMLGregorianCalendar calendar) {
		if (calendar == null) {
			return null;
		}
		return calendar.toGregorianCalendar().getTime();
	}

	public static XMLGregorianCalendar dateToXmlGregorianCalendar(Date date) throws DatatypeConfigurationException {
		if (date == null) {
			return null;
		}
		
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTime(date);
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
	}

	// Retira Mascara dos componentes
	public static String retirarMascaras(String informacoes) {
		return informacoes.replace(".", "").replace("/", "").replace("-", "").replace("(", "").replace(")", "")
				.replace(" ", "").replace("_", "");
	}

	public static String removeNonNumberFromString(String str){
		if(str == null)
			return new String();
		
		return str.replaceAll("\\D", "");
//		return str.replaceAll("[^\\d.]", "");
	}
		
	
	// DDD, DG ESPECIAL E TELEFONE
	public static String[] separaTelefone(String tel) {
		tel = retirarMascaras(tel);

		String[] numeros = new String[12];

		if (tel != null && (tel.length() == 10 || tel.length() == 11)) {
			for (int i = 0; i < tel.length(); i++) {
				if (i < 2) {
					if (numeros[0] == null) {
						numeros[0] = "";
					}
					numeros[0] += tel.charAt(i);
				}
				if (i == 2) {
					if (tel.length() == 11) {
						if (numeros[2] == null) {
							numeros[2] = "";
						}
						numeros[2] += tel.charAt(i);
					}
				}
				if (i > 2) {
					if (tel.length() == 11) {
						if (numeros[1] == null) {
							numeros[1] = "";
						}
						numeros[1] += tel.charAt(i);
					}
				}
				if (i >= 2) {
					if (tel.length() == 10) {
						if (numeros[1] == null) {
							numeros[1] = "";
						}
						numeros[1] += tel.charAt(i);
					}
				}
			}
		}
		return numeros;
	}
	
	
    public static boolean validaEmail(String email) {
        if ((email == null) || (email.trim().length() == 0))
            return false;

        String emailPattern = "\\b(^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@([A-Za-z0-9-])+(\\.[A-Za-z0-9-]+)*((\\.[A-Za-z0-9]{2,})|(\\.[A-Za-z0-9]{2,}\\.[A-Za-z0-9]{2,}))$)\\b";
        Pattern pattern = Pattern.compile(emailPattern, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        if( matcher.matches()){
        	return true;
        }
        return false;
	}
    
    public static Boolean isMobileDevice(){
		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String userAgent = req.getHeader("user-agent");
        String accept = req.getHeader("Accept");

        if (userAgent != null && accept != null) {
            UserAgentInfo agent = new UserAgentInfo(userAgent, accept);
            if (agent.isMobileDevice())
            	return true;
        }
        
        return false;
    }
    
//    public static UnidadeNegocio getUnidadeFromLogin() {
//        return Optional.of(SecurityContextHolder.getContext())
//          .map(SecurityContext::getAuthentication)
//          .map(Authentication::getPrincipal)
//          .filter(Usuario.class::isInstance)
//          .map(Usuario.class::cast)
//          .map(Usuario::getUnidadeNegocio).orElse(null);
//      }
    
    /*
     * Retornar um iis com digito verificador
     * */
    public static String adicionarDigitoVerificador(String iis){
    	if(iis.length() != 23) return null;
    	
    	String[] array = iis.split("");
    	
    	int posicao = 0;
    	
    	int soma_dos_restos = 0;
    	
    	for(String s : array){
    		int valor = Integer.valueOf(s);
    		
    		int resto_divisao = valor*(((22-posicao) % 8)+2);
    		
    		soma_dos_restos += resto_divisao;
    		
    		posicao++;
    	}
    	int resultado = 0;
    	if(((soma_dos_restos*10) % 11) > 9){
    		resultado = ((soma_dos_restos*10) % 10);
    	}else{
    		if(((soma_dos_restos*10) % 11) == 0) resultado = 1;
    		else resultado = ((soma_dos_restos*10) % 11);
    	}
    	
    	return iis+resultado;
    }
    
    /**
     * @author Elder
     * Metodo responsavel por retirar acentos e caracteres especiais
     * @param input
     * @return
     */
    
    public static String retiraAcentos(String input) {
     input = Normalizer.normalize(input, Normalizer.Form.NFD);   
     input = input.replaceAll("[^\\p{ASCII}]", "");   
     return input ;
    }
    
    public static String limitarString(String var, int tamanho){        
        if(var != null && var.length() > tamanho){            
            return var.substring(0, tamanho+1);
        } else {
            return var;
        }        
    }
    
    public static int getDateYear(Date dt){
    	Calendar c = Calendar.getInstance();
    	c.setTime(dt);
    	return c.get(c.YEAR);
    }
    
    public static long getTimeInMillis(Date dt){
    	Calendar c = Calendar.getInstance();
    	c.setTime(dt);
    	return c.getTimeInMillis();
    }
    
    public static Date getDateFromMillis(long millis){
    	Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(millis);
    	return calendar.getTime();
    }
    
    public static int WeekDayToInt(String weekDay) {
    	switch (weekDay) {
		case "SUNDAY":
			return 1;
		case "MONDAY":
			return 2;
		case "TUESDAY":
			return 3;
		case "WEDNESDAY":
			return 4;
		case "THURSDAY":
			return 5;
		case "FRIDAY":
			return 6;
		case "SATURDAY":
			return 7;
		default:
			return 0;
		}
	}
    
    public static String getStackTrace(final Throwable throwable) {
	     final StringWriter sw = new StringWriter();
	     final PrintWriter pw = new PrintWriter(sw, true);
	     throwable.printStackTrace(pw);
	     return sw.getBuffer().toString();
	}
    
    public static void sendEmail(final String sender, final String senderPassword, String receiver, String subject, String text){
    	//Par�metros de conex�o com servidor Gmail
    	Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        
        Session session = Session.getDefaultInstance(props,
	    new javax.mail.Authenticator() {
	    	protected PasswordAuthentication getPasswordAuthentication()
	    	{
	    		return new PasswordAuthentication(sender, senderPassword);
	    	}
	    });
        
        try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(sender)); //Remetente
	      
			
			Address[] toUser = InternetAddress.parse(receiver); //Destinat�rio(s)
	
			message.setRecipients(Message.RecipientType.TO, toUser);
			message.setSubject(subject);//Assunto
			message.setText(text);
			Transport.send(message);
	    } catch (MessagingException e) {
	        throw new RuntimeException(e);
	    }
    }
    
    public static void sendEmailWithAttachment(final String sender, final String senderPassword, String receiver, String subject, String text, DataSource ds){
    	//Par�metros de conex�o com servidor Gmail
    	Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        
        Session session = Session.getDefaultInstance(props,
	    new javax.mail.Authenticator() {
	    	protected PasswordAuthentication getPasswordAuthentication()
	    	{
	    		return new PasswordAuthentication(sender, senderPassword);
	    	}
	    });
        
        try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(sender)); //Remetente
	      
			
			Address[] toUser = InternetAddress.parse(receiver); //Destinat�rio(s)
	
			message.setRecipients(Message.RecipientType.TO, toUser);
			message.setSubject(subject);//Assunto
			message.setText(text);
			
			MimeBodyPart part = new MimeBodyPart();
			part.setDataHandler(new DataHandler(ds));
			part.setFileName("HolidayForm.pdf");
			
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(part);
			
			message.setContent(multipart);
			Transport.send(message);
	    } catch (MessagingException e) {
	        throw new RuntimeException(e);
	    }
    }
    
}
