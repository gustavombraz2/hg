package com.rasz.utils;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class ShowFacesMessage {
	public static void info(String title, String content) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, title, content));
	}

	public static void warn(String title, String content) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_WARN, title, content));
	}
	
	public static void warn(String title, String content, String clientid) {
		FacesContext.getCurrentInstance().addMessage(clientid,
				new FacesMessage(FacesMessage.SEVERITY_WARN, title, content));
	}

	public static void error(String title, String content) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, title, content));
	}

	public static void fatal(String title, String content) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_FATAL, title, content));
	}
}
