package com.rasz.controller;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.webflow.context.ExternalContextHolder;
import org.springframework.webflow.context.servlet.ServletExternalContext;
import org.springframework.webflow.execution.FlowExecution;
import org.springframework.webflow.execution.FlowSession;
import org.springframework.webflow.execution.repository.FlowExecutionRepository;
import org.springframework.webflow.executor.FlowExecutor;
import org.springframework.webflow.executor.FlowExecutorImpl;

import com.rasz.domain.Usuario;
import com.rasz.service.PerfilService;
//import com.rasz.service.UnidadeNegocioService;
import com.rasz.service.UsuarioService;

@Controller
public class MainController {

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private PerfilService perfilService;

	@Autowired
	private FlowExecutor executor;

	@Autowired
	private ServletContext servletContext;

	static final String IDPRODUTO = "id_produto";
	static final String QUANTIDADE = "quantidade";
	static final String LOTE = "lote";
	static final String TIPOEMBALAGEM = "tipo_embalagem";
	static final String IISITEM = "iis_item";
	
	@PreAuthorize("hasRole('ROLE_USER')")
	@RequestMapping("/admin")
	public @ResponseBody Usuario admin() {
		return new Usuario();
	}

	@RequestMapping(value = "/criar/{login}/{senha}")
	public @ResponseBody String criar(@PathVariable String login, @PathVariable String senha) {
		Usuario usuario = new Usuario();
		usuario.setLogin(login);
		usuario.setSenhaNova(senha);
		usuario.setBoAtivo(true);
		usuario.setPerfil(perfilService.findOne(1l));
		usuarioService.save(usuario);
		return "Usu�rio criado.";
	}

	@RequestMapping("/login")
	public String login() {
		return "login";
	}

	@RequestMapping("/usuario/edit")
	public String usuarioEdit() {
		return "usuario/edit";
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Usuario getUsuarioLogado() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario currentUser = null;
		if (auth != null) {
			System.out.println(auth.hashCode());
			currentUser = (Usuario) auth.getPrincipal();
			currentUser = usuarioService.findByLogin(currentUser.getLogin());
		}
		return currentUser;
	}

	@RequestMapping(value = "/rel_reimprimir_etiqueta/{key}")
	public String reimpimirEtiquetas(HttpServletRequest request, HttpServletResponse response, Model model,
			@PathVariable String key) throws Exception {

		Usuario user = getUsuarioLogado();
		if (user != null && user.getBoAtivo()) {

			ExternalContextHolder.setExternalContext(new ServletExternalContext(servletContext, request, response));			
			FlowExecutionRepository repository = ((FlowExecutorImpl) executor).getExecutionRepository();
			FlowExecution flowExecution = repository.getFlowExecution(repository.parseFlowExecutionKey(key));
			FlowSession session = flowExecution.getActiveSession();
			
			
			String imagesPath = servletContext.getRealPath("/resources/images/40rastro.png");
			String nome_relatorio;

			// Tipo de etiqueta que ir� ser impressa atrav�s do id da embalagem
			//Recebe o tipo de embalagem
			boolean isAnfomax = false;
			
						
			
			
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("caminhoImagem", imagesPath);
			map.put("usuario", user.getLogin());
			
			
		}
		
		return null;
	}
	
	@RequestMapping(value = "/rel_emitir_etiqueta/{key}/{tipobotao}")
	public String etiquetas(HttpServletRequest request, HttpServletResponse response, Model model,
			@PathVariable String key, @PathVariable int tipobotao) throws Exception {

		Usuario user = getUsuarioLogado();
		
		if (user != null && user.getBoAtivo()) {
			
//			user.setPlanta(plantaService.findById(null));
			
			ExternalContextHolder.setExternalContext(new ServletExternalContext(servletContext, request, response));
			FlowExecutionRepository repository = ((FlowExecutorImpl) executor).getExecutionRepository();
			FlowExecution flowExecution = repository.getFlowExecution(repository.parseFlowExecutionKey(key));
			FlowSession session = flowExecution.getActiveSession();
			
			String imagesPath = servletContext.getRealPath("/resources/images/40rastro.png");
			
			// Produtos que ser�o gerados as etiquetas
			//ArgOrdemProducao op = argOrdemProducaoCriteria.getOrdem();
			int index = 0; 
			
			Integer quantidade_anterior = 1;

			Double quantidadeTotal1 = 0d;
			Double quantidadeTotal2 = 0d;
			Integer qnt_producao = 0;
			boolean existeCaixa = false;
			
			String tipo_etiqueta = "";
			
							
			//Armazenandos os dados na sess�o novamente

			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("caminhoImagem", imagesPath);
			map.put("usuario", user.getLogin());
			
			String nome_relatorio;
			nome_relatorio = tipo_etiqueta+".jasper";

//			if(tipobotao == 0) getPdf(request, response, map, novo, nome_relatorio);
//			else getPdfOnly(request, response, map, novo, nome_relatorio);
			
			return null;
		} else {
			return null;
		}

	}
	
	/*
	 * Monta as quantidades totais para embalagem de maior nivel
	 * */
	
	protected InputStream obterArquivoJasper(HashMap<String, Object> parametros, String nome_relatorio) {
	    return getClass().getResourceAsStream("/reports/"+nome_relatorio);
	}
	
	//@RequestMapping(value = "/printjob/{user}", produces = MediaType.APPLICATION_JSON_VALUE)
	//@ResponseBody
	@RequestMapping(value="/printjob/{user}")
	//public ResponseEntity<byte[]> getPdfServico(HttpServletRequest request, HttpServletResponse response, @PathVariable String user) throws Exception{
	public Object getPdfServico(HttpServletRequest request, HttpServletResponse response, @PathVariable String user) throws Exception{
	    
		String ipAddress = request.getRemoteAddr();
		
	    if(usuarioService.findByLogin(user) == null){
		    Map<String, Object> map = new HashMap<>();
		    map.put("erro", "Usu�rio n�o encontrado");
		    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		    return null;
	    }
	    
		
	    // Prepare.
//	    String filename = "; filename=" + p.getParametro();
//
//	    // Initialize response.
//	    response.reset(); // Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it may collide.
//	    response.setContentType("application/pdf"); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary ServletContext#getMimeType() for auto-detection based on filename.
//	    //response.setHeader("Content-disposition", "attachment; filename=\"name.pdf\""); // The Save As popup magic is done here. You can give it any filename you want, this only won't work in MSIE, it will use current request URL as filename instead.
//	    response.setHeader("Content-Disposition", ("inline") + filename);
//	    //response.setStatus(arg0);
//	    //Map<String, Object> map = new HashMap<>();
//	    //map.put("id", 1);
//	    //map.put("type", "PDF");
//	    //map.put("application/pdf", pdfData);
//	    
//	    pdfEtiquetaService.delete(p);
//	    
//	    return pdfData;
		
	    // convert JSON to Employee 
	    //Employee emp = convertSomehow(json);

	    // generate the file
	    //PdfUtil.showHelp(emp);

	    // retrieve contents of "C:/tmp/report.pdf" that were written in showHelp
	    //byte[] contents = (...);

	    HttpHeaders headers = new HttpHeaders();
	    //headers.setContentType(MediaType.parseMediaType("application/pdf"));
	    headers.setContentType(MediaType.parseMediaType("text/plain"));
	    String filename = "output.pdf";
	    headers.setContentDispositionFormData(filename, filename);
	    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
	    //ResponseEntity<byte[]> response1 = new ResponseEntity<byte[]>(pdfData, headers, HttpStatus.OK);
	    //ResponseEntity<String> response1 = new ResponseEntity<String>("^XA^MMT^PW400^LL0400^LSO^FT5,384^A0N,41,40^FH\'^FDwww.rastroweb.com.br^FS^BY1,3,99^FT70,322^BCN,,Y,N^FD>:nome do produto^FS^FT10,46^A0N,38,60^FH\'^FDLinguagem ZPL^F5^BY1,3,104^FT96,182^B3N,N,,Y,N^FD1135265909+^FS^PQ1,0,1,Y^XZ", headers, HttpStatus.OK);

	    /*String str = "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR2,2~SD10^JUS^LRN^CI0^XZ"+
	    		"^XA"+
	    		"^MMT"+
	    		"^PW679"+
	    		//"^LL0240"+
	    		"^LL0480"+
	    		"^LS0"+
	    		"^FO160,32^GFA,02304,02304,00012,:Z64:"+
	    		"eJzlVDEOgkAQBCorCyoLK15hZXgM7zAWhtI3UfqIK4wx1pyFFUFjYo7Zy07YBLRxqgmZHWb37jZJDHh+0EacaSy1TD8lQ4AbaNoDL4ZaX4PPQf+XX0PtlvjcoLAiOS+gv4NPDt+PoK/H5+l7yQMa4CXJ0+m1aSbnpulFnhl9NL3PSI8d7wt9MI82N9QLdGTOP+gLYTmvb/Q1mseRPMb3Nft8EumjebYNiEriM9eco/ceeA6e0XvXkEb7J/hE+yf4VFC8MOyKE/AN+Kxs95nNR9O/oc1NnDvuwwforySPIz0Wuj/eW3Fn9sDP4LOD70vgsq1Je5UBczJuqbV4/jFe4T1iBw==:1C4B"+
	    		"^FO416,32^GFA,03072,03072,00016,:Z64:"+
	    		"eJztlkEOgkAMRTEadYEIupAVcBSOwlE4GkeZm+gGm/RlyoAMrPy7n/b/tgPTTJKsxeU9wuCh/KV+IX20fkvwCvkvp+M5eaB+MejAE/pHo/OrTvNz6y/wRdZoXsOvQLx0On5FPaOMxGs3zVMITwG/+zDtl3M+pzn1hj/1W/sLRn9zPuoNf+rpb85n9M/5TL3RP/V7n49Zf+b33ft8CMm/+fsXcD+UnebX1uu/W/8MUL/RfhCs/X8y6sFn7k8Txv4X/eL936Nf5+WCtNf8CP0BnP3/eH9N/YjZ+7mgf6d51k73b9wvnr9Zn3r6b/1+iLT/Q4j9Hlv7/vsjJj7LNPyh:E273"+
	    		"^FO288,32^GFA,02304,02304,00012,:Z64:"+
	    		"eJzdlVEKAjEMRAsKuyjUK/VoPVrFxVUEeyT9kXYWMjahq4LzFcokfQkpdU6hzeOltIyZR5PL/GsxWO/9mmcfa+wDJMZ2zRt47qHGp+iYRP811XgK7dyMft6vqAGYL8AwK5jnZa5YMxGGAeqPQa7ZySwyoGf8PLNmVj3MzdwMnh0wT7o5W/stOvCdZMw997J3VLQN9F58v0Ve837hHPs9A+fxR7vhE81F5iLcjcxzKyfO3MkzVzKb9or18kbWf83qWat+z//7p3oCbzsyew==:6BD9"+
	    		"^FO544,32^GFA,03072,03072,00016,:Z64:"+
	    		"eJztlT0OgkAQhTEWWlCQ2GgkEcHEUqxsCHA0jrYnUwoVs5+Ms2uAWDjdc3g/O4tDEHjX7navq4C15331NP5oeVeNjS/GxofaxpWBEPg0ytA/KvoF9MN+/Q7Htf18Cb0U/TP0I2DqE1emF3e1htBS0eP8qJcgP/21+U+kL/If+r7vD/2pz/zi/ZAv5Bf5PzKfN1nP92fq+Tjn3yr5uR9y+Edu+ZlvsPmzQT79hP0g6o99vzH4jvvzNX/kYWXwd9z/HQ7BnwV2X9vHG+RZgD8HZv4v94/If/4s3Q/Pk0K/RD9uPucX/l+cv+hPPvVz4ALnOxm7vwdOKAj+QN8vrXg+Dfvq+fr9a8BqAXP+JxY=:BAD3"+
	    		"^FO0,32^GFA,03072,03072,00016,:Z64:"+
	    		"eJztljESgjAQRWFggNKSkmNwBI7hETiCJSWlx0FA5AiWHsHSThsGZh8JAc1Qud2f7P/7dwlJHGdzeO8hKjU25W/VM/Ft+52Iucy/veS6XyjXR9yAz3p+Dgx+S/2zzHfLZX2vkDjsUS+T/C6VuI4dVWjnyf45Lw9CrkGveS7r+RH64/zBr9jIPvpafhst16e/QZ/1qU//+v0JPkMzH/rX9rfTfGa2N+6fveez3v/G86eLJa4Pq/zTn7X5V1hg/7P9gnxXY3u378vzt5e4zWR+sO78nPx9d/6P+FJCMFnud/b9EvAfMr86AiNds3+mfuz8v9Tn/Cf9O/im+1H9f3H+2vqm+z0ADk8y/8p5pcCOMmzfj6aw/T779T34D4vxARzdUBc=:93F2"+
	    		"^FT162,170^A0B,21,14^FB100,1,0,C^FH\'^FDCORDEL NP 80^FS"+
	    		"^FT431,185^A0B,21,14^FB130,1,0,C^FH\'^FDINICIADOR APD 240^FS"+
	    		"^FT565,184^A0B,21,14^FB129,1,0,C^FH\'^FDINICIADOR APD 225^FS"+
	    		"^FT296,148^A0B,21,14^FB56,1,0,C^FH\'^FDBRINEL^FS"+
	    		"^FT24,170^A0B,21,14^FB100,1,0,C^FH\'^FDCORDEL NP 40^FS"+
	    		"^PQ1,0,1,Y^XZ";*/
	    
	    String str = "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR2,2~SD15^JUS^LRN^CI0^XZ"+
	    		"^XA"+
	    		"^MMT"+
	    		"^PW680"+
	    		"^LL0480"+
	    		"^LS0"+
	    		"^FO480,64^GFA,07680,07680,00024,:Z64:"+
	    		"eJztzq8KwlAUBvAJFuEGd02a7sXLVgxzwkDwBQY20+LQFzCuGUz+Cd4motViuj6CwsLeQQTD+rLBuQ1kWW3fx0k/Dt85moYgX6RyNIzbZrd03PX27EyGyX7RGp/gcDgcDofD4XA4HA6Hw+FwOBwOh8Ph8LL/LiSISTNe1UNiXYgVHoJ4ljmtCdmcy66QbjoGrc0L1zuyr8kpy1xQvZq7bV2Vd1cjbkcdFZmr7iPvt7mpPG43mHq7kFwUPW0mnz2q5z0sYYPCeXrXoHp+97NP/PKffvHnX/MCnf45mA==:49FD"+
	    		"^FO224,64^GFA,08960,08960,00028,:Z64:"+
	    		"eJztzzEKwkAQhWGxsLJKLVZBLFJsELyAQUbMMUSxsRS0kriQRkS3Cih7BskVIpiYXCC9kFY8gZIUbg5gsHg/233MsFOrIfTXNc7eaT1cGGmnP03SY+++7M73FxgMBoPBYDAYDAaDwWAwGAwGg8FgMBgMBoPBqrPfZGZnNpNs0GJBkwXtVSa/RrqgqUZXMdI/z6OOppjl06M+ujq5HchSLAy0aOZENo8NPzYku+0U4zKa8NB2chPEhbJz54yfCVnFzm24eSnGBU08soq/lOdMt3yDq9xQYW/B0YDz:850E"+
	    		"^FO0,64^GFA,07680,07680,00024,:Z64:"+
	    		"eJztziELwkAYxnGDjDuYMLHcQExDEAzTZDAYvWJf1LFsXhExDGzHHQyGX0YElxSDIOxbKPsC6m4gWtX2/Ln043jft1JB6JvOqjEfH/cbJz1km347nOxrvQscDofD4XA4HA6Hw+FwOBwOh8PhcDgc/uE/yxjmkZmnxI/YNGL+aZhvC6dVyc0dZ5I7j6dodTfSTmLeXPKBKFxSstJus5nXvXodkQSxF6iU3ZbaLZV1RUKF93TJLVnOqQu+sNdEzxGh1SrdeuxVlOi9r/+G+36nW9751+7SSrz5:92F4"+
	    		"^FT569,403^A0N,23,12^FB20,1,0,C^FH\\^FDasdf^FS"+
	    		"^FT329,403^A0N,23,12^FB20,1,0,C^FH\\^FDadsf^FS"+
	    		"^FT586,68^A0I,23,12^FB20,1,0,C^FH\\^FDasdf^FS"+
	    		"^FT346,68^A0I,23,12^FB20,1,0,C^FH\\^FDasdf^FS"+
	    		"^FT108,68^A0I,23,12^FB20,1,0,C^FH\\^FDadsf^FS"+
	    		"^FT77,403^A0N,23,12^FB46,1,0,C^FH\\^FDasdfaasdf^FS"+
	    		"^FT558,462^A0N,31,31^FB38,1,0,C^FH\\^FDdsf^FS"+
	    		"^FT604,8^A0I,31,31^FB53,1,0,C^FH\\^FDasdf^FS"+
	    		"^FT596,37^A0I,31,21^FB37,1,0,C^FH\\^FDasfd^FS"+
	    		"^FT562,434^A0N,31,21^FB30,1,0,C^FH\\^FDasd^FS"+
	    		"^FT364,8^A0I,31,31^FB53,1,0,C^FH\\^FDasdf^FS"+
	    		"^FT351,37^A0I,31,21^FB27,1,0,C^FH\\^FDdsf^FS"+
	    		"^FT310,463^A0N,31,31^FB54,1,0,C^FH\\^FDasdf^FS"+
	    		"^FT127,9^A0I,31,31^FB54,1,0,C^FH\\^FDasdf^FS"+
	    		"^FT118,37^A0I,31,21^FB36,1,0,C^FH\\^FDasdf^FS"+
	    		"^FT324,434^A0N,31,21^FB26,1,0,C^FH\\^FDdsf^FS"+
	    		"^FT79,462^A0N,31,31^FB39,1,0,C^FH\\^FDdsf^FS"+
	    		"^FT84,434^A0N,31,21^FB30,1,0,C^FH\\^FDasd^FS"+
	    		"^PQ1,0,1,Y^XZ";
	    
	    String str2 = "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR2,2~SD15^JUS^LRN^CI0^XZ"+
	    		"^XA"+
	    		"^MMT"+
	    		"^PW680"+
	    		"^LL0480"+
	    		"^LS0"+
	    		"^BY1,3,290^FT17,375^BCN,,Y,N"+
	    		"^FD>:078904520083612345678901^FS"+
	    		"^BY1,3,290^FT255,375^BCN,,Y,N"+
	    		"^FD>:078904520083612345678901^FS"+
	    		"^BY1,3,290^FT495,375^BCN,,Y,N"+
	    		"^FD>:078904520083612345678901^FS"+
	    		"^FT569,403^A0N,23,12^FB20,1,0,C^FH\'^FDasdf^FS"+
	    		"^FT329,403^A0N,23,12^FB20,1,0,C^FH\'^FDadsf^FS"+
	    		"^FT586,68^A0I,23,12^FB20,1,0,C^FH\'^FDasdf^FS"+
	    		"^FT346,68^A0I,23,12^FB20,1,0,C^FH\'^FDasdf^FS"+
	    		"^FT108,68^A0I,23,12^FB20,1,0,C^FH\'^FDadsf^FS"+
	    		"^FT77,403^A0N,23,12^FB46,1,0,C^FH\'^FDasdfaasdf^FS"+
	    		"^FT558,462^A0N,31,31^FB38,1,0,C^FH\'^FDdsf^FS"+
	    		"^FT604,8^A0I,31,31^FB53,1,0,C^FH\'^FDasdf^FS"+
	    		"^FT596,37^A0I,31,21^FB37,1,0,C^FH\'^FDasfd^FS"+
	    		"^FT562,434^A0N,31,21^FB30,1,0,C^FH\'^FDasd^FS"+
	    		"^FT364,8^A0I,31,31^FB53,1,0,C^FH\'^FDasdf^FS"+
	    		"^FT351,37^A0I,31,21^FB27,1,0,C^FH\'^FDdsf^FS"+
	    		"^FT310,463^A0N,31,31^FB54,1,0,C^FH\'^FDasdf^FS"+
	    		"^FT127,9^A0I,31,31^FB54,1,0,C^FH\'^FDasdf^FS"+
	    		"^FT118,37^A0I,31,21^FB36,1,0,C^FH\'^FDasdf^FS"+
	    		"^FT324,434^A0N,31,21^FB26,1,0,C^FH\'^FDdsf^FS"+
	    		"^FT79,462^A0N,31,31^FB39,1,0,C^FH\'^FDdsf^FS"+
	    		"^FT84,434^A0N,31,21^FB30,1,0,C^FH\'^FDasd^FS"+
	    		"^PQ1,0,1,Y^XZ";
	    
		ResponseEntity<String> response1 = new ResponseEntity<String>(str,headers, HttpStatus.OK);
	    return response1;
	}

	public void getPdf(HttpServletRequest request, HttpServletResponse response, 
			HashMap<String, Object> parametros, String nome_relatorio) throws Exception{
	    
//		JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(novo);
	    //Relat�rio principal
	    InputStream arquivoJasper = obterArquivoJasper(parametros, nome_relatorio);
//	    JasperPrint rel = JasperFillManager.fillReport(arquivoJasper, parametros, ds);
	    
//	    JasperPrintManager.printReport(rel,true);
	    	    
//	    byte[] bytes = JasperExportManager.exportReportToPdf(rel);
	    response.setContentType("application/pdf");
	    //response.setContentType("text/plain");
//	    response.setContentLength(bytes.length);
	    String filename = "; filename=" + parametros.get("file");
	    response.setHeader("Content-Disposition", ("inline") + filename);
	    ServletOutputStream outputStream = response.getOutputStream();
//	    outputStream.write(bytes, 0, bytes.length);
	    outputStream.flush();
	    outputStream.close();
	    
	    /* Salvar PDF das etiquetas para ser consumido pelo servi�o */
	    String ipAddress = request.getRemoteAddr();
	    //Armazena as etiquetas para serem impressas em PDF (etiquetas de saco e caixa) 
	    /*if(novo.get(0).getListaIisItem() != null && !novo.get(0).getListaIisItem().isEmpty()) p.setConteudo(bytes);
	    	//Verifica se a impress�o � de unidade
	    	//if(novo.get(0).getListaIisSubItem().get(0).getTipoEmbalagem().getAcronimo().equals("UN")){
	    		//p.setConteudoZPL(conteudoZPL);
	    	//}
	    else{
	    	for(ListIis list: novo){//Este for ser� executado apenas uma vez
	    		for(IisSubItem sub: list.getListaIisSubItem()){
	    			
	    		}
	    	}
	    	p.setConteudoZPL(conteudoZPL);
	    }*/
	    
	}
	
	public void getPdfOnly(HttpServletRequest request, HttpServletResponse response, 
			HashMap<String, Object> parametros, String nome_relatorio) throws Exception{
	    
//		JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(novo);
	    //Relat�rio principal
	    InputStream arquivoJasper = obterArquivoJasper(parametros, nome_relatorio);
//	    JasperPrint rel = JasperFillManager.fillReport(arquivoJasper, parametros, ds);
	    
	    //JasperPrint jasperPrint = JasperFillManager.fillReport(arquivoJasper, parametros, ds);
	    ServletOutputStream stream = response.getOutputStream();
//        JasperExportManager.exportReportToPdfStream(rel, stream);

//        byte[] bytes = JasperExportManager.exportReportToPdf(rel);
        
	    /* Salvar PDF das etiquetas para ser consumido pelo servi�o */
	    String ipAddress = request.getRemoteAddr();
//	    p.setConteudo(bytes);
	    
}

}
