package com.rasz.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.rasz.domain.Perfil;
import com.rasz.service.PerfilService;

@Controller
public class ApplicationController {

	@Autowired private MainController mainController;
	@Autowired private PerfilService perfilService;
	
	public Perfil perfilUsuarioLogado;
	
	public Boolean isUsuarioLogadoAdministrador(){
//		if(perfilUsuarioLogado == null)
		perfilUsuarioLogado = perfilService.localizarPorUsuario(mainController.getUsuarioLogado().getId());
		return perfilUsuarioLogado.getAdministrador();
	}

	public Perfil getPerfilUsuarioLogado() {
		return perfilUsuarioLogado = perfilService.localizarPorUsuario(mainController.getUsuarioLogado().getId());
	}

	public void setPerfilUsuarioLogado(Perfil perfilUsuarioLogado) {
		this.perfilUsuarioLogado = perfilUsuarioLogado;
	}
}
